<a href="javascript:;"><i class="icon-bar-chart"></i> <span class="title">Reports</span><span class="arrow <?php echo ($Page == 'reports') ? 'open' : '' ?>"></span><?php echo ($Page == 'reports') ? '<span class="selected"></span>' : '' ?></a>
<ul class="sub-menu">       
    <li class="<?php echo ($Page == 'reports' && $section == 'list') ? 'active' : '' ?>">         <a href="<?php echo make_admin_url('reports', 'list', 'list'); ?>">                Active Horses</a>        
    </li>        
    <li class="<?php echo ($Page == 'reports' && $section == 'owner') ? 'active' : '' ?>">         <a href="<?php echo make_admin_url('reports', 'owner', 'owner'); ?>">                Owner Report</a>       
    </li>      
    <li class="<?php echo ($Page == 'reports' && $section == 'individual') ? 'active' : '' ?>">    
        <a href="<?php echo make_admin_url('reports', 'individual', 'individual'); ?>">                Other Reports</a>        
    </li>
    <li class="<?php echo ($Page == 'reports' && $section == 'notes') ? 'active' : '' ?>">    
        <a href="<?php echo make_admin_url('reports', 'notes', 'notes'); ?>">                Horses Notes</a>        
    </li>
    <li class="<?php echo ($Page == 'reports' && $section == 'reports') ? 'active' : '' ?>">    
        <a href="<?php echo make_admin_url('reports', 'reports', 'reports'); ?>">                Horses Reports</a>        
    </li>
    <li class="<?php echo ($Page == 'reports' && $section == 'invoice') ? 'active' : '' ?>">    
        <a href="<?php echo make_admin_url('reports', 'invoice', 'invoice'); ?>">                Invoice Reports</a>        
    </li>
    <li class="<?php echo ($Page == 'reports' && $section == 'client') ? 'active' : '' ?>">    
        <a href="<?php echo make_admin_url('reports', 'client', 'client'); ?>">                Client Agreement </a>        
    </li>
</ul>
