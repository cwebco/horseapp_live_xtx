<a href="javascript:;">
    <i class="icon-user"></i> 
    <span class="title">Admin Users</span>
    <span class="arrow "></span>
</a>
<ul class="sub-menu">
        <li class="<?php echo ($Page=='admin' && $section=='list')?'active':''?>">
                <a href="<?php echo make_admin_url('admin', 'list', 'list');?>">
                List Admin Users</a>
        </li>
        <li class="<?php echo ($Page=='admin' && $section=='insert')?'active':''?>">
                <a href="<?php echo make_admin_url('admin', 'list', 'insert');?>">
                New Admin User</a>
        </li>

</ul>