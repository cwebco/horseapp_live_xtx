<a href="javascript:;"><i class="icon-cogs"></i> <span class="title">Settings</span><span class="arrow <?php echo ($Page == 'setting') ? 'open' : '' ?>"></span><?php echo ($Page == 'setting' || $Page == 'change_password' || $Page == 'backup' ) ? '<span class="selected"></span>' : '' ?></a><ul class="sub-menu">        
    <li class="<?php echo ($Page == 'setting' && isset($setting_type) && $setting_type == 'general') ? 'active' : '' ?>">                <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=general' . '&setting_type=general'); ?>">                General Settings</a>        </li>
    <li class="<?php echo ($Page == 'setting' && isset($setting_type) && $setting_type == 'email') ? 'active' : '' ?>">                <a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=email&setting_type=email'); ?>">                Notification Emails</a>        </li>
    <li class="<?php echo ($Page == 'change_password') ? 'active' : '' ?>">                <a href="<?php echo make_admin_url('change_password', 'list', 'list'); ?>">                Change Password</a>        </li>
    <li class="<?php echo ($Page == 'backup') ? 'active' : '' ?>">                <a href="<?php echo make_admin_url('backup', 'list', 'list'); ?>">                DB Backup</a>        </li>
    <!-- <li class="<?php echo ($Page == 'email_template') ? 'active' : '' ?>">                
        <a href="<?php echo make_admin_url('email_template', 'list', 'list'); ?>">
            Email Template
        </a> 
    </li> -->
</ul>
