<a href="javascript:;">
<i class="icon-file-text"></i> 
<span class="title">Email & SMS Templates</span>
<span class="arrow "></span>
</a>
<ul class="sub-menu">
        <li class="<?php echo ($Page=='email' && $section=='list')?'active':''?>">
                <a href="<?php echo make_admin_url('email', 'list', 'list');?>">
                List Templates</a>
        </li>
        <li class="<?php echo ($Page=='email' && $section=='insert')?'active':''?>">
                <a href="<?php echo make_admin_url('email', 'list', 'insert');?>">
                New Template</a>
        </li>
</ul>