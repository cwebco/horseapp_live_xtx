<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->        
    <ul class="page-sidebar-menu">
        <li style="margin-bottom:10px;">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li class="<?php echo ($Page == 'home') ? 'active' : '' ?>"> 
            <a href="<?php echo make_admin_url('home', 'list', 'list') ?>">
                <i class="icon-dashboard"></i>
                <span class="title">Dashboard</span>
                <?php echo ($Page == 'home') ? '<span class="selected"></span>' : '' ?>
            </a>
        </li>
        <li class="<?php echo ($Page == 'horse') ? 'active' : '' ?>"> 
            <a href="<?php echo make_admin_url('horse', 'list', 'list') ?>">
                <i class="icon-list-alt"></i> 
                <span class="title">List all Horses</span>
                <?php echo ($Page == 'horse') ? '<span class="selected"></span>' : '' ?>
            </a>
        </li>
	
	<!--<li class="<?php echo ($Page == 'my_messages') ? 'active' : '' ?>"> 
            <a href="<?php echo make_admin_url('my_messages', 'list', 'list') ?>">
                <i class="icon-list-alt"></i> 
                <span class="title">My Messages</span>
                <?php echo ($Page == 'my_messages') ? '<span class="selected"></span>' : '' ?>
            </a>
        </li>-->
	
        <li class="<?php echo ($Page == 'stallion') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/stallion.php'); ?>
        </li>
        <li class="<?php echo ($Page == 'mare') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/mare.php'); ?>
        </li>
        <li class="<?php echo ($Page == 'reports') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/reports.php'); ?>
        </li>
        <li class="<?php echo ($Page == 'dropdown' || $Page == 'dropdown_values') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/dropdown.php'); ?>
        </li>
        <li class="last <?php echo ($Page == 'setting' || $Page == 'backup' || $Page == 'change_password' || $Page == 'email_template') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/setting.php'); ?>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->
<div class="page-content">

