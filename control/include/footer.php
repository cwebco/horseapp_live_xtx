
</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<div class="footer-inner">
			Designed & Developed by 
                   <a style="color:#fff;" href="http://www.cwebconsultants.com/" title="cWebConsultants India" target="_blank"> cWebConsultants India</a> 
		</div>
		<div class="footer-tools">
			<span class="go-top">
			<i class="icon-angle-up"></i>
			</span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	
        
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	
	<script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<!-- END PAGE LEVEL PLUGINS -->
        
        <!-- BEGIN PAGE LEVEL PLUGINS for dattables-->

	<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
        
         <!-- BEGIN PAGE LEVEL PLUGINS for validation-->
        
        <!-- Validation engine -->
        <script src="assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        
        <!--fancybox-->
        <script src="assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>   
	<script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
        <!--fancybox ends-->
        
         <!-- Image Croping js -->
         <script type="text/javascript" src="assets/plugins/jrac/jquery.jrac.js"></script> 
         <!--jrac ends-->
        
          <!--charts-->
        <script src="assets/plugins/flot/jquery.flot.js"></script>
	<script src="assets/plugins/flot/jquery.flot.resize.js"></script>
	<script src="assets/plugins/flot/jquery.flot.pie.js"></script>
        <!--charts-->
       
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js"></script>
	<script src="assets/scripts/form-components.js"></script>   
        <script src="assets/scripts/table-managed.js"></script>    
        <script src="assets/scripts/ui-jqueryui.js"></script>     
        <script src="assets/scripts/form-validation.js"></script> 
        <script src="assets/scripts/gallery.js"></script>  
        <script src="assets/scripts/form-samples.js"></script>  
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
                   <?php if($Page=='home'): ?>
                   Charts.initPieCharts();
                   <?php endif; ?>
		   FormComponents.init();
                   FormSamples.init();
                   TableManaged.init();
                   UIJQueryUI.init();
                   Gallery.init();
                   FormValidation.init();
                   
		});
	</script>
	<!-- END JAVASCRIPTS -->   
        
        
       

        
        
        
        
        
        
</body>
<!-- END BODY -->
</html>