
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- 
320 and Up boilerplate extension
Andy Clarke http://about.me/malarkey
Keith Clark http://twitter.com/keithclarkcouk
Version: 2
URL: http://stuffandnonsense.co.uk/projects/320andup/
License: http://creativecommons.org/licenses/MIT
-->

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?=SITE_NAME.' | Website Control Panel'?></title>

<meta name="description" content="">
<meta name="author" content="">

<!-- http://t.co/dKP3o1e -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1.0">

<!-- For all browsers -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/color1/color.css">
<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/grid.css">
<link rel="stylesheet" media="print" href="css/print.css">
<!-- For progressively larger displays -->
<link rel="stylesheet" media="only screen and (max-width: 768px)" href="css/320.css">
<link rel="stylesheet" media="only screen and (min-width: 768px) and (max-width: 1024px)" href="css/768.css">
<link rel="stylesheet" media="only screen and (min-width: 1024px)" href="css/1024.css">
<link rel="stylesheet" media="only screen and (min-width: 1440px)" href="css/1440.css">


<!--[if (lt IE 9) & (!IEMobile)]>
<script src="js/libs/selectivizr-min.js"></script>
<![endif]-->

<!-- JavaScript -->
<script src="js/libs/modernizr-2.0.6.min.js"></script>

<!-- For iPhone 4 -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/h/apple-touch-icon.png">
<!-- For iPad 1-->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/m/apple-touch-icon.png">
<!-- For iPhone 3G, iPod Touch and Android -->
<link rel="apple-touch-icon-precomposed" href="images/l/apple-touch-icon-precomposed.png">
<!-- For Nokia -->
<link rel="shortcut icon" href="images/l/apple-touch-icon.png">
<!-- For everything else -->
<link rel="shortcut icon" href="/favicon.ico">

<!--iOS. Delete if not required -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link rel="apple-touch-startup-image" href="images/splash.png">

<!--Microsoft. Delete if not required -->
<meta http-equiv="cleartype" content="on">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 7 ]><script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script><script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script><![endif]-->

<!-- http://t.co/y1jPVnT -->
<link rel="canonical" href="/">
    
    

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.ui.spinner.js"></script><!-- Loading the spinner before jqueryui, because it's officially part of jQuery UI 1.9 and not compatible with 1.8 -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script> -->
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/plugins/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.collapsible.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="js/plugins/jquery.tipsy.js"></script>
<script type="text/javascript" src="js/plugins/autoresize.jquery.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.autotab-1.1b.js"></script>
<script type="text/javascript" src="js/plugins/jquery.uniform.min.js"></script>
<script type="text/javascript" src="js/plugins/ui.multiselect.js"></script>
<script type='text/javascript' src='js/plugins/fullcalendar/fullcalendar.min.js'></script>
<script type="text/javascript" src="js/plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="js/plugins/jquery.jBreadCrumb.1.1.js"></script>
<script type="text/javascript" src="js/plugins/jquery.dataTables.js"></script>
<script type="text/javascript" src="filemanager/js/elfinder.min.js"></script>
<script type="text/javascript" src="js/plugins/jquery.alerts.js"></script>
<script type="text/javascript" src="js/plugins/jquery.miniColors.min.js"></script>

<!-- Validation engine -->
<script src="js/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="js/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>


<script type="text/javascript" src="js/plugins/hoverIntent.js"></script>
<script type="text/javascript" src="js/plugins/superfish.js"></script>

<script src="js/plugins.js"></script>
<script src="js/script.js"></script>
<script src="js/mylibs/helper.js"></script>
<script src="js/jquery.iphone-switch.js"></script>


<!--[if (lt IE 9) & (!IEMobile)]>
<script src="js/libs/imgsizer.js"></script>
<![endif]-->

<script>
// iOS scale bug fix
MBP.scaleFix();

// Respond.js
yepnope({
	test : Modernizr.mq('(only all)'),
	nope : ['js/libs/respond.min.js']
});
</script>

<!-- http://mths.be/aab -->
<script>
var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';s.parentNode.insertBefore(g,s)}(document,'script'));
</script>




<script type="text/javascript" src="js/pure-jquery-style-switcher.js"></script>
<!-- End of picker -->
    
    
    
    
    
</head>
                 
<body class="clearfix">

<header>
	<section class="container_12"><!-- / Using 960's grid container_12 to standartize paddings/margins -->
			<div class="upper clearfix">
				<!-- / Logo -->
				<a style="color:#D5D5D5;height:60px;display:block;" href="#" class="logo mt20"><h3><?php echo SITE_NAME ?></h3></a>
				<!-- / End logo -->
                        
				<!-- / Language and search -->
				<div class="r top">
					
				</div>
				<div class="clear">&nbsp;</div>
				<!-- / End slanguage and search -->
				
				<!-- / Userbar - separated from the main nav to fall back in mobile design. -->
				<div class="r usermenu">
					
				</div>
				<!-- / End userbar -->
                                <?php # include_once(DIR_FS_SITE.'control/include/top-navigation.php');?>
				
			</div>
        </section>
</header>   

