<?php
include_once(DIR_FS_SITE.'include/functionClass/messageClass.php');
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');
$modName='message';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['message_id'])?$message_id=$_GET['message_id']:$message_id='0';
isset($_GET['placeholder_id'])?$placeholder_id=$_GET['placeholder_id']:$placeholder_id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_GET['id_forward'])?$id_forward=$_GET['id_forward']:$id_forward='0';
isset($_GET['id_receiver'])?$id_receiver=$_GET['id_receiver']:$id_receiver='0';


$p=isset($_GET['p'])?$_GET['p']:1;
$max_records='20';/*set maximum records on a page*/

/*get logged in user*/
$user_id=$admin_user->get_user_id();

/*get all senders*/
$all_receivers=array();
$receiver_obj=new user();
$all_receivers=$receiver_obj->getUsers();

 $initial_message="";
 if($id_forward && $id_forward!=0):
     /*get message*/
     $QueryObj = new message();
     $message_forward=$QueryObj->getObject($id_forward);
     if(is_object($message_forward)):
         $initial_message=$message_forward->message;
     endif;
     
 endif;

#handle actions here.
switch ($action):
	case'list':
                $messages=array();
		$QueryObj = new message_user();
                $all_messages=$QueryObj->listMessagesOfUserInPlacehoder($user_id,'1',$p,$max_records);
                $messages=$all_messages['messages'];
                $total_records= $all_messages['TotalRecords'];
                $total_pages= $all_messages['TotalPages'];
               
		break;
            
        case'insert':
               
                /*compose message*/
                $QueryObj = new message();
                $message_new_id=$QueryObj->saveMessage($_POST['id_sender'],$_POST['id_receiver'],$_POST['subject'],$_POST['message']);
                $admin_user->set_pass_msg('Message has been sent successfully.');
                Redirect(make_admin_url('message','list','list')); 
		break;    

        case'view':
                $QueryObj = new message_user();
                $message=$QueryObj->getMessage($id,$user_id);
                
                /*set  request as read*/
                $message_r=new message_user();
                $message_r->setMessageRead($message->message_id);
                
                $user_receiver_obj=new user();
                $user_receiver=$user_receiver_obj->getUserLimitedInfo($message->id_receiver);
                
                $user_sender_obj=new user();
                $user_sender=$user_sender_obj->getUserLimitedInfo($message->id_sender);
                
                
		break;
            
        case'sent':
                $messages=array();
		$QueryObj = new message_user();
                $all_messages=$QueryObj->listMessagesOfUserInPlacehoder($user_id,'2',$p,$max_records);
                $messages=$all_messages['messages'];
                $total_records= $all_messages['TotalRecords'];
                $total_pages= $all_messages['TotalPages'];
               
		break;    
        
        case'change_starred':
                $QueryObj = new message_user();
		$QueryObj->change_starred($id);
		Redirect(make_admin_url('message', $section, $section)); 
           
		break;      
        
        case'change_placeholder':
                $QueryObj = new message_user();
		$QueryObj->change_placeholder($id,$placeholder_id);
                $admin_user->set_pass_msg('Message has been moved successfully');
		Redirect(make_admin_url('message', $section, $section)); 
           
		break;     
            
	
	

           case'delete':
		$QueryObj = new message_user();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Message has been deleted successfully');
		Redirect(make_admin_url('message', 'trash', 'trash'));
        	break;

         

          
            case'trash':
		$messages=array();
		$QueryObj = new message_user();
                $all_messages=$QueryObj->listMessagesOfUserInPlacehoder($user_id,'4',$p,$max_records);
                $messages=$all_messages['messages'];
                $total_records= $all_messages['TotalRecords'];
                $total_pages= $all_messages['TotalPages'];
		break;

	default:break;
endswitch;
?>
