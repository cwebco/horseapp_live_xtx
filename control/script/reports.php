<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['sex']) ? $sex = $_GET['sex'] : $sex = 'horse';
isset($_GET['from_date']) ? $from_date = $_GET['from_date'] : $from_date = '';
isset($_GET['to_date']) ? $to_date = $_GET['to_date'] : $to_date = '';
isset($_GET['type']) ? $type = $_GET['type'] : $type = '';
isset($_GET['sortby']) ? $sortby = $_GET['sortby'] : $sortby = 'name';


#handle actions here.
switch ($action):
    case'list':
	if ($sex == 'stallion'):
	    $QueryObj = new horse();
	    $QueryObj->getAllstallions();
	    $total = $QueryObj->GetNumRows();
	elseif ($sex == 'mare'):
	    $QueryObj = new horse();
	    $QueryObj->getAllmares();
	    $total = $QueryObj->GetNumRows();
	else:
	    $QueryObj = new horse();
	    $QueryObj->getAllHorses();
	    $total = $QueryObj->GetNumRows();
	endif;

	break;
    /* for individual reports */
    case'individual':

	if (isset($_GET['sort']) && $_GET['sort'] == 'sort'):

	    if ($type == 'service'):
		$QueryObj = new query("(select * from mare_info ORDER BY last_service desc) AS m ,horse AS h");
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $from = $date_obj->TocustomDate($from_date);
		    $to = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where m.last_service >='$from' and m.last_service <='$to' AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id order by ";
		else:
		    $QueryObj->Where = " where 1=1 AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id order by ";
		endif;
		if ($sortby == 'name'):
		    $QueryObj->Where.=" h.name ASC";
		elseif ($sortby == 'season'):
		    $QueryObj->Where.=" m.season desc";
		elseif ($sortby == 'type'):
		    $QueryObj->Where.=" m.mare_type ASC";
		elseif ($sortby == 'last_service'):
		    $QueryObj->Where.=" m.last_service ASC";
		endif;
		$QueryObj->DisplayAll();

	    elseif ($type == 'paddock'):
		$QueryObj = new query('paddock_log AS p,horse AS h');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $from_date = $date_obj->TocustomDate($from_date);
		    $to_date = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where p.date >='$from_date' and p.date <='$to_date' AND  p.horse_id = h.id AND h.is_deleted = '0' GROUP BY p.id ORDER BY ";
		else:
		    $QueryObj->Where = " where 1=1  AND  p.horse_id = h.id AND h.is_deleted = '0'  GROUP BY p.id  ORDER BY ";
		endif;
		if ($sortby == 'name'):
		    $QueryObj->Where.=" h.name ASC";
		elseif ($sortby == 'date'):
		    $QueryObj->Where.=" p.date desc";
		elseif ($sortby == 'paddock_title'):
		    $QueryObj->Where.=" p.paddock_title ASC";
		elseif ($sortby == 'move'):
		    $QueryObj->Where.=" p.move ASC";
		endif;
		$QueryObj->DisplayAll();

	    elseif ($type == 'ehv' || $type == 'flu'):

		//SELECT * FROM `horse` LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type="ehv" where horse.is_deleted="0"

		$QueryObj = new query('horse');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $fromdate = $date_obj->TocustomDate($from_date);
		    $todate = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type' and horse_log.date >='$fromdate' and horse_log.date <='$todate'";
		else:
		    $QueryObj->Where = " LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type'";
		endif;
		$QueryObj->Where.=" where horse.is_deleted='0' order by ";
		if ($sortby == 'name'):
		    $QueryObj->Where.=" horse.name ASC";
		elseif ($sortby == 'date'):
		    $QueryObj->Where.=" horse_log.date desc";
		elseif ($sortby == 'type'):
		    $QueryObj->Where.=" horse_log.value ASC";
		endif;
		$QueryObj->DisplayAll();
	    // $rex= $QueryObj ->ListOfAllRecords();
	    //echo'<pre>';  print_r($rex); exit;

	    else:

		$QueryObj = new query('(select * from horse_log where type="' . $type . '" order by date desc) as t,horse AS h');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $fromdate = $date_obj->TocustomDate($from_date);
		    $todate = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where t.horse_id = h.id AND h.is_deleted = '0' AND t.date >='$fromdate' and t.date <='$todate' GROUP BY t.id order by ";
		else:
		    $QueryObj->Where = " where 1=1 AND t.horse_id = h.id AND h.is_deleted = '0' GROUP BY t.id order by ";
		endif;
		if ($sortby == 'name'):
		    $QueryObj->Where.=" h.name ASC";
		elseif ($sortby == 'date'):
		    $QueryObj->Where.=" t.date desc";
		elseif ($sortby == 'value'):
		    $QueryObj->Where.=" t.value ASC";
		endif;
		$QueryObj->DisplayAll();
	    endif;


	elseif (isset($_GET['go']) && $_GET['go'] == 'Go'):
	  

	    if ($type == 'service'):
		$QueryObj = new query("(select * from mare_info ORDER BY last_service desc) AS m ,horse AS h");
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $from = $date_obj->TocustomDate($from_date);
		    $to = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where m.last_service >='$from' and m.last_service <='$to' AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id";
		else:
		    $QueryObj->Where = " where 1=1 AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id";
		endif;
		$QueryObj->DisplayAll();

	    elseif ($type == 'due_date'):
		$QueryObj = new query(" mare_info ORDER BY due_date desc ");
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $from = $date_obj->TocustomDate($from_date);
		    $to = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where ma.due_date >='$from' and ma.due_date <='$to' AND  ma.mare_id = h.id AND h.is_deleted = '0' GROUP BY ma.mare_id";
		else:
		    $QueryObj->Where = " where 1=1 AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id";
		endif;
		$QueryObj->print = 1;
		$QueryObj->DisplayAll();

	    elseif ($type == 'paddock'):
		$QueryObj = new query('paddock_log AS p,horse AS h');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $from_date = $date_obj->TocustomDate($from_date);
		    $to_date = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where p.date >='$from_date' and p.date <='$to_date' AND  p.horse_id = h.id AND h.is_deleted = '0' GROUP BY p.id ORDER BY p.date desc,p.horse_id desc";
		else:
		    $QueryObj->Where = " where 1=1  AND  p.horse_id = h.id AND h.is_deleted = '0'  GROUP BY p.id  ORDER BY p.date desc,p.horse_id desc";
		endif;
		$QueryObj->DisplayAll();

	    elseif ($type == 'ehv' || $type == 'flu'):
		//SELECT * FROM `horse` LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type="ehv" where horse.is_deleted="0"
		$QueryObj = new query('horse');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $fromdate = $date_obj->TocustomDate($from_date);
		    $todate = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type' and horse_log.date >='$fromdate' and horse_log.date <='$todate'";
		else:
		    $QueryObj->Where = " LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type'";
		endif;
		$QueryObj->Where.=" where horse.is_deleted='0' order by horse_log.date desc";
		$QueryObj->DisplayAll();

	    else:
		$QueryObj = new query('(select * from horse_log where type="' . $type . '" order by date desc) as t,horse AS h');
		if ($from_date != '' && $to_date != ''):
		    $date_obj = new date();
		    $fromdate = $date_obj->TocustomDate($from_date);
		    $todate = $date_obj->TocustomDate($to_date);
		    $QueryObj->Where = " where t.horse_id = h.id AND h.is_deleted = '0' AND t.date >='$fromdate' and t.date <='$todate' GROUP BY t.id order by t.date desc";
		else:
		    $QueryObj->Where = " where 1=1 AND t.horse_id = h.id AND h.is_deleted = '0' GROUP BY t.id order by t.date desc";
		endif;
		$QueryObj->DisplayAll();
	    endif;
	endif;

	break;

    case'owner':
	$owners = array();
	foreach (getOwnerDetails() as $ak => $av):
	    $av['horses'] = horse::getOwnerWithHorses($av['id']);
	    $owners[] = $av;
	endforeach;
	break;

    case'notes':
	if ($sex == 'stallion'):
	    $QueryObj = new horse();
	    $QueryObj->getAllstallions();
	    $total = $QueryObj->GetNumRows();
	elseif ($sex == 'mare'):
	    $QueryObj = new horse();
	    $QueryObj->getAllmares();
	    $total = $QueryObj->GetNumRows();
	else:
	    $QueryObj = new horse();
	    $QueryObj->getAllHorses();
	    $total = $QueryObj->GetNumRows();
	endif;
	break;
    case'reports':
	if ($sex == 'stallion'):
	    $QueryObj = new horse();
	    $QueryObj->getAllstallions();
	    $total = $QueryObj->GetNumRows();
	elseif ($sex == 'mare'):
	    $QueryObj = new horse();
	    $QueryObj->getAllmares();
	    $total = $QueryObj->GetNumRows();
	else:
	    $QueryObj = new horse();
	    $QueryObj->getAllHorses();
	    $total = $QueryObj->GetNumRows();
	endif;

	break;
    case'invoice':


	if ($sex == 'stallion'):
	    $QueryObj = new horse();
	    $QueryObj->getAllstallions();
	    $total = $QueryObj->GetNumRows();
	elseif ($sex == 'mare'):
	    $QueryObj = new horse();
	    $QueryObj->getAllmares();
	    $total = $QueryObj->GetNumRows();
	else:
	    $QueryObj = new horse();
	    $QueryObj->getAllHorses();
	    $total = $QueryObj->GetNumRows();
	endif;


	/***********************Filtter**************************************************************/
	$query = new horse_notes;
        $all_notes = $query->all_notes();

        if (isset($_GET['from_date']) && isset($_GET['to_date'])) {
            $query = new horse_notes;
            $result_fillter = $query->filter_horses($_GET['from_date'], $_GET['to_date']);
            //pr($result_fillter);
            if($result_fillter){
            foreach ($result_fillter as $r) {

                $horse_ids_from_filter[] = $r['horse_id'];
                // pr($horse_ids_from_filter);
                $query = new horse();
                $csv_horses[] = $query->get_horse_obj($r['horse_id']);
            }
            if (isset($_GET['download2']) && $_GET['download2'] == 'csv2') {
                $num = 0;
                $query = new horse_notes;
                $result_fillter = $query->filter_horses($_GET['from_date'], $_GET['to_date']);
                $query = new horse();
                $horsenot_filter = $query->horse_not_in_filter($horse_ids_from_filter);



                $result_fillter = array_merge($result_fillter, $horsenot_filter);

//                pr($result_fillter);die;


                foreach ($result_fillter as $notes_ddd) {
                    //if ($notes_ddd['invoice_note']) {
                    $h_id = isset($notes_ddd['horse_id']) ? $notes_ddd['horse_id'] : $notes_ddd['id'];
                    $query = new horse();
                    $horses = $query->get_horse_obj_hr($h_id);
                    if ($horses) {
                        $fl_horse_name = $horses->name;
                        $fl_horse_status = $horses->horse_status;
			$fl_horse_arrived = $horses->arrived;
                        $fl_horse_departed = $horses->departed;
			$rate_to_keep = $horses->rate_to_keep;
                        $rate_foaling_fee = $horses->rate_foaling_fee;
                        $fl_horse_owner_name = $horses->owner_name;

                        $prod[$num]['name'] = $fl_horse_name;
                        $prod[$num]['Type'] = $fl_horse_status;
			$prod[$num]['arrival'] = $fl_horse_arrived;
                        $prod[$num]['departure'] = $fl_horse_departed;
			$prod[$num]['rate_to_keep'] = $rate_to_keep;
                        $prod[$num]['rate_foaling_fee'] = $rate_foaling_fee;
                        $prod[$num]['owner'] = $fl_horse_owner_name;
			$prod[$num]['date'] = isset($notes_ddd['date']) ? $notes_ddd['date'] : '';
			    $prod[$num]['notes'] = html_entity_decode($notes_ddd['invoice_note']);
			    $prod[$num]['notes']=str_replace('&pound;','£',$prod[$num]['notes']);
			    $prod[$num]['notes']=str_replace('&amp;','&',$prod[$num]['notes']);
                            }
                        //}
		    $num++;
		    }
		/**$output = fopen("php://output", 'w') or die("Can't open php://output");
		header("Content-Type:application/csv");
		header("Content-Disposition:attachment;filename=invoice.csv");
		fputcsv($output, array('name', 'sex', 'departure', 'arrival', 'owner', 'date', 'notes'));
		foreach ($prod as $product) {
		    fputcsv($output, $product);
		}
		die;**/

		$head=array('name', 'Type', 'arrival','departure','Rate To Keep', 'Rate Foaling Fee', 'owner', 'date', 'notes');
		header('Content-Type: text/html; charset=UTF-8');
		//header("Content-type: application/vnd.ms-excel");
		header("Cache-Control: cache, must-revalidate");
		header("Pragma: public"); 
		header("Content-type:text/octect-stream");
		header("Content-Disposition:attachment;filename=invoiceList.csv");
		print '"' . utf8_decode(stripslashes(implode('","',$head))) . "\"\n";
		foreach ($prod as $product) {
			
			$pos = strpos($_SERVER['HTTP_USER_AGENT'], 'Windows');
		
			if ($pos === false):
				print '"' . (stripslashes(implode('","',$product))) . "\"\n";
			else:
				print '"' . utf8_decode(stripslashes(implode('","',$product))) . "\"\n";
			endif;

	    }
		die;

	    }
	   }
	}




	/*	 * ***************************Download all Notes in Excel***************************************************** */

	$query = new horse();
	$all_export = $query->all_horse_excel();
	//pr($all_export); die;
	

	if (isset($_GET['download']) && $_GET['download'] == 'csv') {
	    $num = 0;
//	    foreach ($all_export as $horse) {
//		if ($horse) {
//		    $prod[$num]['name'] = $horse['name'];
//		    $prod[$num]['sex'] = $horse['sex'];
//		    $prod[$num]['departure'] = $horse['departed'];
//		    $prod[$num]['arrival'] = $horse['arrived'];
//		    $prod[$num]['owner'] = $horse['owner_name'];

		    $query = new horse_notes;
		    $notes_detail_al = $query->all_notes();

//		    $data = '';
		    foreach ($notes_detail_al as $notes_ddd) {
                        if ($notes_ddd['invoice_note']) {
                            $h_id = $notes_ddd['horse_id'];
                            $query = new horse();
                            $horses = $query->get_horse_obj($h_id); 
                            if ($horses) {
                            $fl_horse_name = $horses->name;
                            $fl_horse_status = $horses->horse_status;
			    $fl_horse_arrived = $horses->arrived;
                            $fl_horse_departed = $horses->departed;
			    $fl_horse_keep = $horses->rate_to_keep;
                            $fl_horse_foaling = $horses->rate_foaling_fee;
                            $fl_horse_owner_name = $horses->owner_name;                
                           
                            $prod[$num]['name'] = $fl_horse_name;
                            $prod[$num]['Type'] = $fl_horse_status;
			    $prod[$num]['arrival'] = $fl_horse_arrived;
                            $prod[$num]['departure'] = $fl_horse_departed;
			    $prod[$num]['rate_to_keep'] = $fl_horse_keep;
                            $prod[$num]['rate_foaling_fee'] = $fl_horse_foaling;
                            $prod[$num]['owner'] = $fl_horse_owner_name;
							$prod[$num]['date'] = $notes_ddd['date']; 
							//$prod[$num]['notes'] =preg_replace('/[^A-Za-z0-9\-\£(\) ]/', '', $notes_ddd['invoice_note']);
							$prod[$num]['notes']=html_entity_decode($notes_ddd['invoice_note']);
							$prod[$num]['notes']=str_replace('&pound;','£',$prod[$num]['notes']);
							$prod[$num]['notes']=str_replace('&amp;','&',$prod[$num]['notes']);
						
                            }
                        }
		    $num++;
		    }

//		}
//	    }
		
	
			 
		$head=array('name', 'Type','arrival', 'departure','Rate To Keep', 'Rate Foaling Fee', 'owner', 'date', 'notes');
		header('Content-Type: text/html; charset=UTF-8');
		//header("Content-type: application/vnd.ms-excel");
		header("Cache-Control: cache, must-revalidate");
		header("Pragma: public"); 
		header("Content-type:text/octect-stream");
		header("Content-Disposition:attachment;filename=invoiceList.csv");
		print '"' . utf8_decode(stripslashes(implode('","',$head))) . "\"\n";
		foreach ($prod as $product) {
			
			$pos = strpos($_SERVER['HTTP_USER_AGENT'], 'Windows');
		
			if ($pos === false):
				print '"' . (stripslashes(implode('","',$product))) . "\"\n";
			else:
				print '"' . utf8_decode(stripslashes(implode('","',$product))) . "\"\n";
			endif;

	    }
		exit;
	
	}
	break;

    /****************************************End******************************************************** */

    case'client':
	if ($sex == 'stallion'):
	    $QueryObj = new horse();
	    $QueryObj->getAllstallions();
	    $total = $QueryObj->GetNumRows();
	elseif ($sex == 'mare'):
	    $QueryObj = new horse();
	    $QueryObj->getAllmares();
	    $total = $QueryObj->GetNumRows();
	else:
	    $QueryObj = new horse();
	    $QueryObj->getAllHorses();
	    $total = $QueryObj->GetNumRows();
	endif;

	break;
    default:break;
endswitch;
?>


