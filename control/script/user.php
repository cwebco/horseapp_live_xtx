<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');
include_once(DIR_FS_SITE.'include/class/validation_u.php');
include_once(DIR_FS_SITE.'include/class/validation_p.php');
include_once(DIR_FS_SITE.'include/functionClass/websitePlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/websitePlanUserClass.php');
include_once(DIR_FS_SITE.'include/functionClass/websiteUserPaymentClass.php');


$modName='user';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';

/*all website plans*/
$website_plans=array();
$QueryObj_plans = new website_plan();
$website_plans=$QueryObj_plans->listWebsitePlans('1','array');

$total_amount=0;
$total_paid=0;
$pending_amount=0;

if(isset($id) && $id!=0):
    
     /*get user info*/
     $QueryObj = new user();
     $user_account= $QueryObj->getObject($id);

     /*check if user have website plan ot not*/
     $QueryObj = new website_plan_user();
     $user_website_plan_id=$QueryObj->checkUserWebsitePlanExists($id);
     
     if($user_website_plan_id && $user_website_plan_id!=0): 
         
         $uplan_obj=new website_plan_user();
         $user_plan= $uplan_obj->getObject($user_website_plan_id);
                   
         /*total biling amount of user*/
         $total_obj=new website_plan_user();
         $total_amount=$total_obj->getTotalAmountToBePaidOfUser($user_website_plan_id);

         
         /*get total paid amount of user*/
         $total_paid_obj=new user_payment_history();
         $total_paid=$total_paid_obj->getTotalAmountPaidByUserUnderWebsitePlan($id,$user_website_plan_id);

         $pending_amount=$total_amount-$total_paid;
         
     endif;    
     
     
     
endif;

switch ($action):
	 case'list':
		 $QueryObj = new user();
	         $QueryObj->listAllUsers();
		 break;
	 case'view':
                
                 break;
         
              
         case 'update2': 
               
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):

                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $query= new query('user');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();

                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('user', 'list', 'list')); 
                            
                         endif;   
                      
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('user', 'list', 'list'));
                break;  
                
	case'update':
		 if(isset($_POST['submit'])):
                     
                        /*server side validation for contact information*/
                        $validation=new user_validation();

                        $validation->add('first_name', 'req');
                        $validation->add('first_name', 'reg_words');

                        $validation->add('last_name', 'req');
                        $validation->add('last_name', 'reg_words');

                        $validation->add('username', 'req');	
                        $validation->add('username', 'reg_words');

                        $validation->add('password', 'req');	
                        $validation->add('password', 'reg_words');

                        $validation->add('cpassword', 'req_conf');
                        $validation->add('cpassword', 'reg_words');
                        $validation->add('password','regcompare', $_POST['cpassword']);

                        $validation->add('phone', 'req');

                        /*server side validation for Business information*/

                        $validation->add('business_name', 'req');
                        $validation->add('business_name', 'reg_words');

                        $validation->add('business_email', 'req');	
                        $validation->add('business_email', 'email');
                        $validation->add('business_email', 'reg_words');

                        $validation->add('business_phone', 'req');
                        $validation->add('business_mobile', 'req');
                        $validation->add('business_address1', 'req');
                        $validation->add('business_state', 'req');
                        $validation->add('business_zip_code', 'req');

                        $valid= new valid();

                        if($valid->validate($_POST, $validation->get())):
                            $error=0;
                        else:
                            $error=1;/*set error*/

                            $admin_user->set_error();    
                            $admin_user->set_pass_msg($valid->error);
                         

                        endif;

                     
                         $UserObj = new user(); /*check if username already exists*/

                        if($UserObj->check_user_name_exists($_POST['username'],$_POST['id'])):
                                $admin_user->set_error();    
                                $admin_user->set_pass_msg("Sorry Username already exists.!!");
                              
                                $error=1;


                        endif;

                        $BussObj = new user();/*check if business email already exists*/

                        if($BussObj->check_business_email_exists($_POST['business_email'],$_POST['id'])):
                                $admin_user->set_error();    
                                $admin_user->set_pass_msg("This Business Email already exists. Please try again.");
        
                                $error=1;


                        endif;   

                        $BussObj = new user();/*check if business name already exists*/

                        if($BussObj->check_business_name_exists($_POST['business_name'],$_POST['id'])):
                                $admin_user->set_error();    
                                $admin_user->set_pass_msg("This Business Name already exists. Please try again.");
                              
                                $error=1;


                        endif;               

                     
                     
                       if($error!='1'): /*if there is no error*/
                    
                               
                                /* save user data and business data in user table */     

                                $QueryObj = new user();

                                $updated_user_id=$QueryObj->updateUserDetail($_POST);



                                $admin_user->set_pass_msg('Profile has been updated successfully.');
                                Redirect(make_admin_url('user', 'list', 'list'));

                       else:
                           
                                Redirect(make_admin_url('user', 'list', 'list'));
                           
                       endif;
                     
                     
                endif;     
                 
                break;
		
	case'delete':
		$QueryObj = new user();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('User has been deleted successfully.');
		Redirect(make_admin_url('user', 'list', 'list'));
		break;
            
        case'permanent_delete':
		$QueryObj = new user();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('User has been deleted successfully');
		Redirect(make_admin_url('user', 'thrash', 'thrash'));
        	break;
          
        case'restore':
            $QueryObj = new user();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg('User has been restored successfully');
            Redirect(make_admin_url('user', 'thrash', 'thrash'));
            break;

        case'thrash':
            $QueryObj = new user();
            $QueryObj->getThrash();
            break;
        
        
        case'billing':
                 /*get user info*/
		 $QueryObj = new user();
	         $user_account= $QueryObj->getObject($id);
                 
                 $QueryObj = new website_plan_user();
                 $user_website_plan_id=$QueryObj->checkUserWebsitePlanExists($id);
	         $website_payments=array();
                 
                 if($user_website_plan_id && $user_website_plan_id!=0): 
                     
                     /*get user website plan details*/
                      $website_plan_ob=new website_plan_user();
                      $user_website_plan= $website_plan_ob->getObject($user_website_plan_id);
            
                     /*get website plan payment details*/
                 
                      $website_pay_obj=new user_payment_history();
                      $website_payments=$website_pay_obj->listWebsiteUserPayments($id,'array');
                   
                                           
                 endif;
                 
               break;
               
         case'invoice':
                 /*get payment object*/
                  $web_obj=new user_payment_history();
                  $payment=$web_obj->getObject($id);
                  
                  /*get user website plan details*/
                  $website_plan_ob=new website_plan_user();
                  $user_website_plan= $website_plan_ob->getObject($payment->website_plan_user_id);
       
                  /*get user object*/
                  $user_obj123=new user();
                  $user_info=$user_obj123->getObject($payment->user_id);
                  
                 break; 
             
        case'invoice_delete':
                   if(isset($_GET['invoice_id']) && $_GET['invoice_id']!=0):
                      /* delete payment*/
                      $web_obj=new user_payment_history();
                      $payment=$web_obj->deleteObject($_GET['invoice_id']);
                      
                      $admin_user->set_pass_msg('User Payment   has been deleted successfully');
                      Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));
                      
                   endif;   
                                    
                   break; 
             
             
        case'website_plan':
                 /*extended of website plan*/    
            
                 $to_date="";
                 if($user_website_plan_id && $user_website_plan_id!=0): 
                      
                     $uplan_obj=new website_plan_user();
                     $user_plan= $uplan_obj->getObject($user_website_plan_id);
                     
                     $QueryObj_date = new website_plan_user();
                     $to_date=$QueryObj_date->getExtendPlanDateOfUser($user_website_plan_id);
                     
                 endif;
                 
                 if(isset($_POST['submit'])):
                     if($user_website_plan_id && $user_website_plan_id!=0): 

                          /*extent plan of user*/
                         $extend_plan=new website_plan_user();
                         $plan_id=$extend_plan->ExtendPlanOfUser($user_website_plan_id);
                          
                         $admin_user->set_pass_msg('User website plan  has been extended successfully');
                         Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));

                     else:
                         /*create website plan of user*/
                         $manual_plan=new website_plan_user();
                         $plan_id=$manual_plan->saveManualWebsitePlanUser($_POST);
                         
                         
                         /*create first payment*/
                         $manual_plan_payment=new user_payment_history();
                         $manual_plan_payment->CreateUserFirstManualPayment($plan_id,$_POST);
                         
                         
                         /*update user information*/
                         $user_obj=new user();
                         $user_obj->setEmailVerifiedAndSignUpComplete($id);
                         
                         $admin_user->set_pass_msg('User website plan  has been created successfully');
                         Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));

                     endif;
                     
                endif;
                 
                break;  
                
        case'change_website_plan':
                 /*extended of website plan*/    
                  if($user_website_plan_id==0):
                     $admin_user->set_error();    
                     $admin_user->set_pass_msg("Sorry, you can not change website plan for this user.");
                     Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));
                  endif;
                  
                 
                 if(isset($_POST['submit'])):
                     
                         /*create website plan of user*/
                         $manual_plan=new website_plan_user();
                         $plan_id=$manual_plan->saveManualWebsitePlanUser($_POST);
                         
                         
                         /*create first payment*/
                         $manual_plan_payment=new user_payment_history();
                         $manual_plan_payment->CreateUserFirstManualPayment($plan_id,$_POST);
                         
                         
                         /*update user information*/
                         $user_obj=new user();
                         $user_obj->setEmailVerifiedAndSignUpComplete($id);
                         
                         /*Delete old website plan associated with user*/
                         $delete_plan=new website_plan_user();
                         $delete_plan->deleteObject($user_website_plan_id);
                         
                         
                         $admin_user->set_pass_msg('User website plan  has been changed successfully');
                         Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));

                     
                endif;
                 
                break;        
        case'insert_payment':
             
               /*create first payment*/
                 $manual_plan_payment=new user_payment_history();
                 $manual_plan_payment->saveWebsiteUserPayment($_POST);
                 
                 $admin_user->set_pass_msg('User payment  has been generated successfully');
                 Redirect(make_admin_url('user', 'billing', 'billing','id='.$id));
                
                 
                break;    
	default:break;
endswitch;
?>
