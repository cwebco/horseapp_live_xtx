<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailTemplateClass.php');

$modName = 'email_template';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;

/* Handle actions here. */
switch ($action):
    case 'list':
        $QueryObj = new email_template();
        $QueryObj->listEmailTemplates();
        break;

    case 'update':
        /* update email */
        if (isset($_POST['submit'])):
            $QueryObj = new email_template();
            if ($QueryObj->saveEmailTemplate($_POST)):
                $admin_user->set_pass_msg('Email has been updated successfully.');
                if ($_POST['submit'] == 'SubmitClose'):
                    Redirect(make_admin_url('email_template', 'list', 'list'));
                endif;
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg('Error occurred while editing email template.');
            endif;
            Redirect(make_admin_url('email_template', 'update', 'update', 'id=' . $id));
        endif;

        /* get email contents */
        $Query_obj = new email_template();
        $values = $Query_obj->getEmailTemplate($id);

        if (!is_object($values)):
            $admin_user->set_error();
            $admin_user->set_pass_msg('Something went wrong.');
            Redirect(make_admin_url('email_template', 'list', 'list'));
        endif;
        break;

    default:
        Redirect(make_admin_url('email_template', 'list', 'list'));
        break;
endswitch;
?>