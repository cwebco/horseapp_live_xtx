<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/dropdownClass.php');
 include_once(DIR_FS_SITE . 'include/functionClass/maredetailsClass.php');
$modName = 'mare';


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['sid']) ? $sid = $_GET['sid'] : $sid = '0';
isset($_GET['b_id']) ? $b_id = $_GET['b_id'] : $b_id = '0';
// isset($_POST['mare_details'])? $mare_details = $_POST['mare_details'] : $mare_details ='0';

   
switch ($action):
    case'list':
	$QueryObj = new horse();
	$QueryObj->getAllmares();

	$query = new horse();
	$all_export = $query->all_mare_excel();

	if (isset($_GET['download']) && $_GET['download'] == 'csv') {
	    $num = 0;
	    foreach ($all_export as $mare) {
		//pr($mare); die;
		if ($mare) {
		    $prod[$num]['name'] = $mare['name'];
		    $prod[$num]['owner'] = $mare['owner_name'];
		    $prod[$num]['Arrival'] = $mare['arrived'];
		    $prod[$num]['Departure Dates'] = $mare['departed'];
		    $query = new mare();
		    $mare_session = $query->getmareinfo($mare['id']);
		   
		    if (isset($mare_session->mare_type)) {
			$prod[$num]['type'] = $mare_session->mare_type;
		    } else {
			$prod[$num]['type'] = '';
		    }
		    
		    if (isset($mare_session->covering_sire)) {
			$prod[$num]['Covering sire'] = $mare_session->covering_sire;
		    } else {
			$prod[$num]['Covering sire'] = '';
		    }
		    if (isset($mare_session->last_service)) {
		    $prod[$num]['Last service'] = $mare_session->last_service;
		    } else {
			 $prod[$num]['Last service'] = '';
		    }
		    if (isset($mare_session->current_status)) {
		    $prod[$num]['Current Status'] = $mare_session->current_status;
		    } else {
			$prod[$num]['Current Status'] = '';
		    }
		     if (isset($mare_session->last_scan)) {
		    $prod[$num]['Last Scan Date'] = $mare_session->last_scan;
		     } else {
			 $prod[$num]['Last Scan Date'] = ''; 
		     }
		      if (isset($mare_session->mare_status)) {
		    $prod[$num]['Status'] = $mare_session->mare_status;
		     } else {
			 $prod[$num]['Status'] = ''; 
		     }
		     if (isset($mare_session->in_foal_to)) {
		    $prod[$num]['In Foal to'] = $mare_session->in_foal_to;
		     } else {
			$prod[$num]['In Foal to'] = ''; 
		     }
		     if (isset($mare_session->due_date)) {
		    $prod[$num]['Due Date'] = $mare_session->due_date;
		     } else {
			 $prod[$num]['Due Date'] = ''; 
		     }
		     if (isset($mare_session->foaling_date)) {
		    $prod[$num]['Foaling Date'] = $mare_session->foaling_date;
		     } else {
			$prod[$num]['Foaling Date'] = ''; 
		     }
		     if (isset($mare_session->foaling_location)) {
		    $prod[$num]['Foaling Location'] = $mare_session->foaling_location;
		     } else {
			$prod[$num]['Foaling Location'] = ''; 
		     }
		     if (isset($mare_session->foal_details)) {
		    $prod[$num]['Foal details'] = $mare_session->foal_details;
		     } else {
			 $prod[$num]['Foal details'] = ''; 
		     }
		    $num++;
		}
	    }
	    $output = fopen("php://output", 'w') or die("Can't open php://output");
	    header("Content-Type:application/csv");
	    header("Content-Disposition:attachment;filename=seasonal info.csv");
	    fputcsv($output, array('name', 'owner', 'Arrival', 'Departure Dates','type' , 'Covering sire', 'Last service', 'Current Status', 'Last Scan Date', 'Status', 'In Foal to', 'Due Date', 'Foaling Date', 'Foaling Location', 'Foal details'));
	    foreach ($prod as $product) {
		fputcsv($output, $product);
	    }
	    die;
	}



	break;
    case'insert':

	if (isset($_POST['submit'])):

	    $QueryObj = new horse();
	    $getID = $QueryObj->saveHorse($_POST);

	    if ($_FILES['file']['error'] == 0) {
		$file = rand() . $_FILES['file']['name'];
		$destination = DIR_FS_SITE_UPLOAD . 'horse_documents/' . $file;
		$is_file_upload = move_uploaded_file($_FILES['file']['tmp_name'], $destination);
		if ($is_file_upload) {
		    $data = array(
			'file' => $file,
			'horse_id' => $getID,
			'time' => time()
		    );
		    $query = new horse_documents;
		    $query->save($data);
		}
	    }
	    $data_note = array(
		'horse_id' => $getID,
//                'name' => $_POST['name_note'],
		'note' => $_POST['note'],
		'time' => time(),
		'invoice_note' => $_POST['invoice_note']
	    );
	    $query = new horse_notes;
	    $query->save($data_note);

	    if (isset($getID) && is_numeric($getID)):
		/* add paddock details in paddock_log table */
		if ($_POST['to_paddock_date'] != '' && $_POST['to_paddock'] != ''):
		    $QueryObj1 = new paddock_log();
		    $QueryObj1->addPaddock($getID, $_POST);
		endif;

		/* add details in horse log table */
		$date_obj = new date();
		if ($_POST['last_wormer'] != ''):
		    $date_wormer = $date_obj->TocustomDate($_POST['last_wormer']);
		    $QueryObj2 = new horse_log();
		    $QueryObj2->addHorseLog($getID, 'wormer', $_POST['last_wormer_value'], $date_wormer);
		endif;
		if ($_POST['last_farrier'] != ''):
		    $date_farrier = $date_obj->TocustomDate($_POST['last_farrier']);
		    $QueryObj3 = new horse_log();
		    $QueryObj3->addHorseLog($getID, 'farrier', '', $date_farrier);
		endif;
		if ($_POST['flu_date'] != ''):
		    $date_flu = $date_obj->TocustomDate($_POST['flu_date']);
		    $QueryObj4 = new horse_log();
		    $QueryObj4->addHorseLog($getID, 'flu', $_POST['flu_value'], $date_flu);
		endif;
		if ($_POST['ehv_date'] != ''):
		    $date_ehv = $date_obj->TocustomDate($_POST['ehv_date']);
		    $QueryObj5 = new horse_log();
		    $QueryObj5->addHorseLog($getID, 'ehv', $_POST['ehv_value'], $date_ehv);
		endif;
	    else:
		$admin_user->set_error(1);
		$admin_user->set_pass_msg('Some error occured while added the details.');
		Redirect(make_admin_url('mare', 'list', 'list'));
	    endif;

	    $owners_all = '';
	    $owners_all_count = count($_POST['owner']);
	    $owner_loop = 1;
	    foreach ($_POST['owner'] as $o) {
		$owners = explode('**', $o);

		$surname = explode(' ', $owners[1]);

		$conn = mysqli_connect($DBHostNameCMA, $DBUserNameCMA, $DBPasswordCMA);
		
		mysqli_select_db($conn,$DBDataBaseCMA);

//                echo $query = "SELECT * FROM `contacts` WHERE `surname` LIKE '%$surname[0]%' AND `first_name` LIKE '%$surname[1]%'";die;
		$query = "SELECT * FROM `contacts` WHERE `id` = '$owners[0]'";

		$result = mysqli_query($conn,$query);

		$contacts = mysqli_fetch_assoc($result);

		if ($owner_loop++ == $owners_all_count) {
		    $owners_all .= '<strong>(</strong> ' . $owners[1] . ' - ' . $contacts['address1'] . ' <strong>)</strong>';
		} else {
		    $owners_all .= '<strong>(</strong> ' . $owners[1] . ' - ' . $contacts['address1'] . ' <strong>)</strong> , ';
		}
	    }

	    $email_array = array(
		'HORSE_NAME' => $_POST['name'],
		'ARRIVED_DATE' => $_POST['arrived'],
		'OWNERS' => $owners_all,
	    );


	    // $query = new email;
		// $query->send_db_email(1, NOTIFICATION_EMAIL, $email_array);
		
			$text = 'New Mare Added.';
            $text .= '<br /><br />';
            $text .= '<strong>Horse Name : </strong>' .$_POST['name'];
            $text .= '<br /><br />';
            $text .= '<strong>Arrived Date : </strong>' .$_POST['arrived'];
            $text .= '<br /><br />';
            $text .= '<strong>Owner and Owner\'s Address : </strong>' .$owners_all;

            $subject = 'New Mare Added';
            $result = sendMailgunEmail(NOTIFICATION_EMAIL, $text, $subject);

	    $admin_user->set_pass_msg('Entry has been inserted successfully.');
	    Redirect(make_admin_url('mare', 'insert2', 'insert2', 'id=' . $getID));

	endif;
	break;
    case'insert2':
	$QueryObj = new horse();
	$horse = $QueryObj->getObject($id);

	if (isset($_POST['submit'])):

	    /* check if season info not added */
	    if (!isset($_POST['season']) || $_POST['season'] == ''):
		$admin_user->set_error('1');
		$admin_user->set_pass_msg('Please enter a valid season.');
		Redirect(make_admin_url('mare', 'update2', 'update2', 'id=' . $_POST['mare_id']));
	    endif;
	    /* check if season information is already added or not */
	    $QueryObj0 = new query('mare_info');
	    $QueryObj0->Where = "where mare_id='" . $_POST['mare_id'] . "' AND season ='" . $_POST['season'] . "'";
	    $objct = $QueryObj0->DisplayOne();
	    if (is_object($objct)):
		$admin_user->set_error('1');
		$admin_user->set_pass_msg('Season "' . $_POST['season'] . '" already added.');
		Redirect(make_admin_url('mare', 'update2', 'update2', 'id=' . $_POST['mare_id']));
	    else:
		$QueryObj = new mare();
		$getID = $QueryObj->saveMareSeasonInfo($_POST);
	    endif;

	    $admin_user->set_pass_msg('Mare seasonal info has been added successfully.');
	    Redirect(make_admin_url('mare', 'update2', 'update2', 'id=' . $_POST['mare_id']));

	endif;
	break;
    case 'update_table':
	if (isset($_POST['multiopt_go']) && $_POST['multiopt_go'] == 'Go'):
	    if ($_POST['multiopt_action'] == 'delete'):
		if (count($_POST['multiopt'])):
		    foreach ($_POST['multiopt'] as $k => $v):
			$query = new query('horse');
			$query->Data['id'] = "$k";
			$query->Data['is_deleted'] = '1';
			$query->Update();

		    endforeach;
		else:
		    $admin_user->set_error();
		    $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
		    Redirect(make_admin_url('mare', 'list', 'list'));
		endif;

	    endif;
	endif;

	$admin_user->set_pass_msg('Operation has been performed successfully');
	Redirect(make_admin_url('mare', 'list', 'list'));
	break;
	case'update':


	 $QueryObj2 = new mare();
	 $abc = $QueryObj2->getMareSeasonscurrentLatest($id);
	//  pr('dsfsdf');
	//  pr($abc);
	// pr($_POST);die;
	
	if(isset($_POST['submitAttachment'])) {
        $message = '';
        foreach($_POST['attachments'] as $file){
            $message .= DIR_WS_SITE_UPLOAD . 'horse_documents/'.$file.'<br /><br />';
        }
        if($message) {
            $subject = 'Horse Document from Horse Management Application';
            $result = sendMailgunEmail('bharat@cwebconsultants.com', $message , $subject);
            $admin_user->set_pass_msg('Please check email.');
            Redirect(make_admin_url('mare', 'update', 'update','id=' . $id.'#tab_1_5'));
        }
    }
	$QueryObj = new horse();
	$horse = $QueryObj->getObject($id);

	/* get single season details */
	$QueryObj2 = new mare();
	$res = $QueryObj2->getMareSeasonSingle($id);
	
	$foaling_date = $res->foaling_date;
	$foaling_detail = $res->foal_details;

	if(!$horse) {
		Redirect(make_admin_url('mare'));
	}

	/* get horse logs from horse_log table */
	$QueryObj1 = new horse_log();
	$horse_log = $QueryObj1->getLogs($id);

	/* get paddock details of horse from paddock details table */
	$QueryObj2 = new paddock_log();
	$paddock_log = $QueryObj2->getPaddocks($id);

	/* get current paddock of horse */
	$query = new paddock_log();
	$current = $query->currentPaddock($id);

	if (isset($_POST['submit'])):

	    $QueryObj11 = new horse();
	    $getID = $QueryObj11->saveHorse($_POST);

	    $admin_user->set_pass_msg('Entry has been updated successfully.');
	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $getID));

	endif;

	if (isset($_POST['file_upload'])) {
	    if ($_FILES['file']['error'] == 0) {
		$file = rand() . $_FILES['file']['name'];
		$destination = DIR_FS_SITE_UPLOAD . 'horse_documents/' . $file;
		$is_file_upload = move_uploaded_file($_FILES['file']['tmp_name'], $destination);
		if ($is_file_upload) {
		    $data = array(
			'file' => $file,
			'horse_id' => $id,
			'time' => time()
		    );
		    $query = new horse_documents;
		    $query->save($data);
		    $admin_user->set_pass_msg('Document uploaded');
		}
	    } else {
		$admin_user->set_pass_msg('Something went wrong');
	    }
	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $id . '#tab_1_5'));
	}

	if (isset($_POST['note_submit'])) {
	    extract($_POST);
	    if ($note) {
		$query = new horse_notes;
		$query->save($_POST);
		$admin_user->set_pass_msg('Note uploaded');
	    } else {
		$admin_user->set_pass_msg('Fields are required');
	    }

	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $_GET['id'] . '#tab_1_6'));
	}

	if (isset($_POST['invoice_note_submit'])) {
	    extract($_POST);
	    if ($invoice_note) {
		$query = new horse_notes;
		$query->save($_POST);
		$admin_user->set_pass_msg('Note uploaded');
	    } else {
		$admin_user->set_pass_msg('Fields are required');
	    }

	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $_GET['id'] . '#tab_1_7'));
	}

	$query = new horse_documents;
	$horse_documents = $query->all($id);

	$query = new horse_notes;
	$horse_notes = $query->all($id);

	$query = new horse_notes;
	$notes_all = $query->notes_all($id);

	$query = new horse_notes;
	$invoice_all = $query->invoice_all($id);


	#mare Detail form insert/Update:

	$QueryObj1 = new maredetails();
	$mareINfoData = $QueryObj1->getmaredata($id);

	$QueryObj1 = new infectionFree();
	$infectionfreeData = $QueryObj1->getinfectionFreeddata($id);
	
	if($_POST['mare_details']=='mare_details_form'){
		$QueryObj = new maredetails();
		$mare_details = $QueryObj->savemaredetails($_POST);
		$admin_user->set_pass_msg('Mare details info has been added successfully.');
		Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $_GET['id'] . '&act=maireData#tab_1_8'));
	}
	if($_POST['infectionFree_details']=='infectionFree_details_form'){

		$QueryObj = new infectionFree();
		$mare_details = $QueryObj->saveinfectionFreedetails($_POST);
		$admin_user->set_pass_msg('Freedom From infection details info has been added successfully.');
		Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $_GET['id'] . '&act=infectionFreeData#tab_1_9'));
}


	break;
    case'update2':
	/* get details from main table */
	$QueryObj = new horse();
	$horse = $QueryObj->getObject($id);

	/* get seasons details */
	$QueryObj2 = new mare();
	$QueryObj2->getMareSeasons($id);



	/* get details from mare_info table */

	$QueryObj1 = new mare();
	$mare = $QueryObj1->getmareinfo($id);



	if (isset($_POST['submit'])):
		if(!$_POST['last_service']) {
			unset($_POST['last_service']);
		}
		if(!$_POST['last_scan']) {
			unset($_POST['last_scan']);

		}
		if(!$_POST['due_date']) {
			unset($_POST['due_date']);
			
		}
		if(!$_POST['foaling_date']) {
			unset($_POST['foaling_date']);
			
		}
	    $QueryObj11 = new mare();
	    $getID = $QueryObj11->saveMareSeasonInfo($_POST);

	    $admin_user->set_pass_msg('Entry has been updated successfully.');
	    Redirect(make_admin_url('mare', 'update2', 'update2', 'id=' . $getID));

	endif;
	break;
    case'delete':
	$QueryObj = new horse();
	$QueryObj->deleteObject($id);
	$admin_user->set_pass_msg('Deleted successfully.');
	Redirect(make_admin_url('mare', 'list', 'list'));
	break;
    case 'delete_season':
	$QueryObj = new mare();
	$QueryObj->id = $sid;
	$QueryObj->Delete();
	$admin_user->set_pass_msg('Deleted successfully.');
	Redirect(make_admin_url('mare', 'update2', 'update2', 'id=' . $id));

	break;
    case'restore':
	$QueryObj = new horse();
	$QueryObj->restoreObject($id);
	$admin_user->set_pass_msg('Entry has been restored successfully');
	Redirect(make_admin_url('mare', 'thrash', 'thrash'));
	break;
    case 'delete_image':
	if ($id) {
	    $object = get_object('horse', $id);
	    $QueryObj = new query('horse');
	    $QueryObj->Data['image'] = '';
	    $QueryObj->Data['id'] = $id;
	    $QueryObj->Update();

	    #delete images from all folders
	    $image_obj = new imageManipulation();
	    $image_obj->DeleteImagesFromAllFolders('horse', $object->image);

	    $admin_user->set_pass_msg('Image has been successfully deleted.');
	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $id));
	}
	break;
    case'thrash':
	$QueryObj = new horse();
	$QueryObj->getarchived('mare');
	break;

    case'view':

	$QueryObj = new horse();
	$horse = $QueryObj->get_horse_obj($id);


	/* get owner details if owner id exists */
	if ($horse->owner_id != ''):
	    $owner = getOwnerDetails($horse->owner_id);
	else:
	    $owner = array();
	endif;
	/* get mare season info */
	if ($horse->sex == 'mare'):
	    $QueryObj1 = new mare();
	    $QueryObj1->getMareSeasons($id);
	endif;


	break;

    case'permanent_delete':
	$QueryObj = new horse();
	$QueryObj->purgeObject($id);

	$admin_user->set_pass_msg('Deleted successfully');
	Redirect(make_admin_url('mare', 'thrash', 'thrash'));
	break;

    case'delete_document':

	$query = new horse_documents();
	$single = $query->single($id);
	$destination = DIR_FS_SITE_UPLOAD . 'horse_documents/' . $single->file;
	$QueryObj = new horse_documents();
	$is_delete = $QueryObj->delete_horse($id);
	$is_unlink = unlink($destination);
	$admin_user->set_pass_msg('Deleted successfully.');
	Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $b_id . '#tab_1_5'));
	break;
    case'delete_notes':
	$QueryObj = new horse_notes();
	$is_delete = $QueryObj->delete_horse($id);
	$admin_user->set_pass_msg('Deleted successfully.');
	if (isset($_GET['notes_type']) && $_GET['notes_type'] == 'notes') {

	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $b_id . '#tab_1_6'));
	} else {

	    Redirect(make_admin_url('mare', 'update', 'update', 'id=' . $b_id . '#tab_1_7'));
	}
	break;
	case'mare_report':
	
	if($_GET['type']=='mare_report'){

		$QueryObj = new maredetails();
		$mare_data = $QueryObj->getmaredata($id);
		
	}

	

	break;
	case'infectionfree_report':
	if($_GET['type']=='infectionfree_report'){

		$QueryObj1 = new infectionFree();
		$infectionfreeData = $QueryObj1->getinfectionFreeddata($id);
	}


	break;

    default:break;
endswitch;
?>

