<?php
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['oby'])?$oby=$_GET['oby']:$oby='order_date';
isset($_GET['so'])?$so=$_GET['so']:$so='ASC';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['update_id'])?$update_id=$_GET['update_id']:$update_id='0';
isset($_GET['g_sort'])?$g_sort=$_GET['g_sort']:$g_sort='';

#handle actions here.
switch ($action):
	case'list':
		
		#get total visits for today.
		$query=new query('web_stat');
		$query->Field="count(*) as total";
		$query->Where="where DATE(on_date)=CURDATE()";
		$webstat=$query->DisplayOne();
		$total_visit_today=$webstat->total;

		#get total visits for ever
		$query=new query('web_stat');
		$query->Field="count(*) as total";
		$webstat=$query->DisplayOne();
		$total_visits=$webstat->total;
		
		#get total visits for this month.
	    $month=date("n");
	    $year= date("Y");
	    $query=new query('web_stat');
		$query->Field="count(*) as total";
		$query->Where="where MONTH(on_date)='$month' and YEAR(on_date)='$year'";
		$webstat=$query->DisplayOne();
		$total_visit_month=$webstat->total;
        
		#get total visits for this week.
		$week=date("W");
		$query=new query('web_stat');
		$query->Field="count(*) as total";
		$query->Where="where WEEK(on_date,1)='$week' and YEAR(on_date)='$year' and MONTH(on_date)='$month'";
		$webstat=$query->DisplayOne();
		$total_visit_week=$webstat->total;
		
		#get total visits for this year.
		$query=new query('web_stat');
		$query->Field="count(*) as total";
		$query->Where="where YEAR(on_date)='$year'";
		$webstat=$query->DisplayOne();
		$total_visit_year=$webstat->total;
		
                /*get total values of horses and mares*/
                $QueryObjj = new horse();
                $QueryObjj->getAllstallions(); 
                $total_s = $QueryObjj->GetNumRows();

                $QueryObjj1 = new horse();
                $QueryObjj1->getAllmares();
                $total_m = $QueryObjj1->GetNumRows();
                
                /*get recent stallions*/
                $QueryObj = new horse();
                $QueryObj->getTopstallions(); 
                
                /*get recent mares*/
                $QueryObj1 = new horse();
                $QueryObj1->getTopmares();

                
		break;
	
		case'insert':
		break;
	case'update':
		break;
	case'delete':
		break;
	default:break;
endswitch;
?>
