<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/themeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/themeCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/themeSubRelClass.php');
include_once(DIR_FS_SITE.'include/functionClass/themeImgRelClass.php');


$modName='theme';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['category_id'])?$category_id=$_GET['category_id']:$category_id='0';

/*Get all theme Categories*/
$QueryCat = new category();
$all_cat=$QueryCat->get_all_categories_name();

/*get all parent themes*/
$parent_themes=array();
$query_parent=new theme();
$parent_themes=$query_parent->GetParentThemes('1','array');

                
switch ($action):
	case'list':
		 $QueryObj = new theme();
	         $QueryObj->ListAll();
		 break;
	case'insert':
		 if(isset($_POST['submit'])):
                    $QueryObj = new theme();
                    $getID=$QueryObj->saveTheme($_POST);
                    
                     /*Save Category document relation*/
                    $QueryObj1 = new theme_cat_rel();
                    $QueryObj1->saveThemeCatRel($_POST['categories'],$document_id);
                    
                     /*save theme images according to category*/
                    $QueryObjimg = new theme_img_rel();
                    $QueryObjimg->saveThemeCatImageRel($_POST['categories'],$_FILES['images'],$getID);
                    
                    /*save sub theme relation*/
                    if(!isset($_POST['is_main'])):
                        
                        if(isset($_POST['theme_parent_id'])):
                                
                                /*save parent relation*/
                                 $theme_sub_rel=new theme_sub_rel();
                                 $theme_sub_rel->saveThemeRel($getID,$_POST['theme_parent_id']);
                            
                        endif;
                        
                    endif;
                    
                    
                     /** Check Url Name it should be unique *****/
                    if($getID):
                        $get_url=new theme();
                        $urlname=$get_url->getUrlname($getID);
                        if($urlname):
                            $QueryObj123=new theme();
                            if($QueryObj123->checkUrlName($urlname,$getID)):
                                $update_url=new theme();
                                $update_url->updateUrlname($urlname.'-'.$getID,$getID);
                            endif;
                        endif;
                     endif;   
                     /** End Check Url Name code *****/
                    
                    $admin_user->set_pass_msg('Theme has been inserted successfully.');
		    Redirect(make_admin_url('theme', 'list', 'list'));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();    
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('theme', 'list', 'list'));     
		 endif;
                break;
         case 'update2': 
                if(is_var_set_in_post('submit_position')):
                       
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('theme');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):

                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $query= new query('theme');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();

                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('theme', 'list', 'list')); 
                            
                         endif;   
                      
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('theme', 'list', 'list'));
                break;         
	case'update':
		$QueryObj = new theme();
		$theme=$QueryObj->getObject($id);  
                
                $selectedcats=array("0"=>"0");
                $QueryCatRel = new theme_cat_rel();
                $selectedcat= $QueryCatRel->get_all_cat_of_theme($theme->id);
                if($selectedcat):
                  $selectedcats= $selectedcat;
                endif;
                
                /*get parent id of theme*/
                $parent_id=0;
                $parent_sub=new theme_sub_rel();
                $parent_id=$parent_sub->getParentThemeIdOfSubTheme($theme->id);
                
                
		if(isset($_POST['submit'])):
                   
                    $QueryObj = new theme();
                    $getID=$QueryObj->saveTheme($_POST);
                    
                    /*Save Theme category relation*/
                    $QueryObj1 = new theme_cat_rel();
                    $QueryObj1->saveThemeCatRel($_POST['categories'],$getID);
                    
                    
                    /*save theme images according to category*/
                    $QueryObjimg = new theme_img_rel();
                    $QueryObjimg->saveThemeCatImageRel($_POST['categories'],$_FILES['images'],$getID);
                                  
                    
                    /*save sub theme relation*/
                    /*delete all parent relation of this theme*/
                     $theme_sub=new theme_sub_rel();
                     $theme_sub->deleteRecordsOfTheme($getID);
                                 
                    if(!isset($_POST['is_main'])):
                        
                        if(isset($_POST['theme_parent_id'])):
                                
                                /*save parent relation*/
                                 $theme_sub_rel=new theme_sub_rel();
                                 $theme_sub_rel->saveThemeRel($getID,$_POST['theme_parent_id']);
                            
                        endif;
                        
                    endif;
                    
                     /** Check Url Name it should be unique *****/
                    if($getID):
                        $get_url=new theme();
                        $urlname=$get_url->getUrlname($getID);
                        if($urlname):
                            $QueryObj123=new theme();
                            if($QueryObj123->checkUrlName($urlname,$getID)):
                                $update_url=new theme();
                                $update_url->updateUrlname($urlname.'-'.$getID,$getID);
                            endif;
                        endif;
                     endif;   
                     /** End Check Url Name code *****/
                    
                    
                    $admin_user->set_pass_msg('Theme has been updated successfully.');
		    Redirect(make_admin_url('theme', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('theme', 'list', 'list'));     
	        endif;   
		break;
	case'delete':
		$QueryObj = new theme();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Theme has been deleted successfully.');
		Redirect(make_admin_url('theme', 'list', 'list'));
		break;
        case 'delete_image':
                if($id){
                    /*Get image of theme of particular category*/
                    $theme_image_obj=new theme_img_rel();
                    $theme_image=$theme_image_obj->getThemeCategoryImage($id,$category_id);
                   
                    #delete images from all folders
                    $image_obj=new imageManipulation();
                    $image_obj->DeleteImagesFromAllFolders('theme',$theme_image->image);

                    /*delete theme category image entry*/
                                        
                     $query_image=new theme_img_rel();
                     $query_image->deleteRecordsOfThemeOfCategory($id,$category_id);
               
                    /*End Clear Cache code*/
                    $admin_user->set_pass_msg('Theme Image has been successfully deleted.');
                    Redirect(make_admin_url('theme', 'update', 'update','id='.$id));
                }
                
        case'permanent_delete':
		$QueryObj = new theme();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Theme has been deleted successfully');
		Redirect(make_admin_url('theme', 'thrash', 'thrash'));
        	break;
          
        case'restore':
            $QueryObj = new theme();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg('Theme has been restored successfully');
            Redirect(make_admin_url('theme', 'thrash', 'thrash'));
            break;

        case'thrash':
            $QueryObj = new theme();
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
