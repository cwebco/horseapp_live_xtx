<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/websitePlanClass.php');

$modName='website_plan';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';

                
switch ($action):
	case'list':
		 $QueryObj = new website_plan();
                 $QueryObj->listWebsitePlans('1');
		 break;
	case'insert':
		 if(isset($_POST['submit'])):
                    $QueryObj = new website_plan();
                    $getID=$QueryObj->saveWebsitePlan($_POST);
              
                        
                    $admin_user->set_pass_msg('Website Plan has been inserted successfully.');
		    Redirect(make_admin_url('website_plan', 'list', 'list'));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();    
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('website_plan', 'list', 'list'));     
		 endif;
                break;
         case 'update2': 
                if(is_var_set_in_post('submit_position')):
                       
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('website_plan');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):

                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $query= new query('website_plan');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();

                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('website_plan', 'list', 'list')); 
                            
                         endif;   
                      
                    endif;
                endif;
       
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('website_plan', 'list', 'list'));
                break;         
	case'update':
		$QueryObj = new website_plan();
                $website_plan=$QueryObj->getObject($id);  
                               
		if(isset($_POST['submit'])):
                    $QueryObj = new website_plan();
                    $getID=$QueryObj->saveWebsitePlan($_POST);
                    
                  
                    $admin_user->set_pass_msg('Website Plan has been updated successfully.');
		    Redirect(make_admin_url('website_plan', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('website_plan', 'list', 'list'));     
	        endif;   
		break;
	case'delete':
		$QueryObj = new website_plan();
		$QueryObj->deleteObject($id);
              
                    
		$admin_user->set_pass_msg('Website Plan has been deleted successfully.');
		Redirect(make_admin_url('website_plan', 'list', 'list'));
		break;
      
                
        case'permanent_delete':
		$QueryObj = new website_plan();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Website Plan has been deleted successfully');
		Redirect(make_admin_url('website_plan', 'thrash', 'thrash'));
        	break;
          
        case'restore':
            $QueryObj = new website_plan();
            $QueryObj->restoreObject($id);
  
                    
            $admin_user->set_pass_msg('Website Plan has been restored successfully');
            Redirect(make_admin_url('website_plan', 'thrash', 'thrash'));
            break;

        case'thrash':
            $QueryObj = new website_plan();
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
