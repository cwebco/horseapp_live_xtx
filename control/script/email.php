<?php
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');

$modName='email';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new emails();
		$QueryObj->listEmails();
		break;
	case'insert':
		if(isset($_POST['submit'])):
                    $QueryObj = new emails();
                    $QueryObj->saveEmail($_POST);
                    $admin_user->set_pass_msg('Email has been inserted successfully.');
                    Redirect(make_admin_url('email', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('email', 'list', 'list'));    
		endif;
		break;
	case'update':
		$QueryObj = new emails();
		$email=$QueryObj->getObject($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new emails();
                    $QueryObj->saveEmail($_POST);
                    $admin_user->set_pass_msg('Email has been updated successfully.');
                    Redirect(make_admin_url('email', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('email', 'list', 'list'));       
		endif;
		break;
	case 'update2':
                if(is_var_set_in_post('submit_position')):
                    
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('email');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        foreach($_POST['multiopt'] as $k=>$v):
                                $query= new query('email');
                                $query->id="$k";
                                $query->Delete();
                        endforeach;
                    endif;
                endif;     
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('email', 'list', 'list', 'id='.$id.'&page='.$page));
                break;
	 case'delete':
		$QueryObj = new emails();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Email has been deleted successfully');
		Redirect(make_admin_url('email', 'list', 'list'));
		break;
	
	default:break;
endswitch;
?>
