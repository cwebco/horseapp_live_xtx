<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');

$modName = 'horse';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['redirect']) ? $redirect = $_GET['redirect'] : $redirect = 'horse';

switch ($action):
    case'list':
        $QueryObj = new horse();
        $QueryObj->getAllHorses();
        break;
    case'view':
        $QueryObj = new horse();
        $horse = $QueryObj->get_horse_obj_hr($id);

        if(!$horse) {
            Redirect(make_admin_url('horse'));
        }

	/* get horse logs from horse_log table */
        $QueryObj1 = new horse_log();
        $horse_log = $QueryObj1->getLogs($id);

        /* get paddock details of horse from paddock details table */
        $QueryObj2 = new paddock_log();
        $paddock_log = $QueryObj2->getPaddocks($id);

        /* get current paddock of horse */
        $query = new paddock_log();
        $current = $query->currentPaddock($id);

        /* get owner details if owner id exists */
        $owners = array();
        if ($horse->owner_id != ''):
            $owner_idd = trim(str_replace(array('(', ')'), ' ', $horse->owner_id));
            $owner_idd_array = explode(',', $owner_idd);

            if (!empty($owner_idd_array)):
                foreach ($owner_idd_array as $kk => $vv):
                    $owners[] = getOwnerDetails(trim($vv));
                endforeach;
            endif;
        endif;
        /* get mare season info */
        if ($horse->sex == 'mare'):
            $QueryObj1 = new mare();
            $QueryObj1->getMareSeasons($id);
        endif;

        $query = new horse_documents;
        $horse_documents = $query->all($id);

        $query = new horse_notes;
        $notes_all = $query->notes_all($id);
	
        $query = new horse_notes;
        $invoice_all = $query->invoice_all($id);
	
        break;
    case'delete':
        $QueryObj = new horse();
        $QueryObj->deleteObject($id);
        $admin_user->set_pass_msg('Deleted successfully.');
        Redirect(make_admin_url('horse', 'list', 'list'));
        break;
    case'restore':
        $QueryObj = new horse();
        $QueryObj->restoreObject($id);
        $admin_user->set_pass_msg('Entry has been restored successfully');
        Redirect(make_admin_url('horse', 'thrash', 'thrash'));
        break;
    case'thrash':
        $QueryObj = new horse();
        $QueryObj->getarchived();
        break;

    case'permanent_delete':
        $QueryObj = new horse();
        $QueryObj->purgeObject($id);

        $admin_user->set_pass_msg('Deleted successfully');
        Redirect(make_admin_url('horse', 'thrash', 'thrash'));
        break;
    default:break;
endswitch;
?>

