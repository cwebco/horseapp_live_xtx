<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/categoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/moduleClass.php');
$modName='category';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';


/*Get all modules*/
$QueryMod = new module();
$all_mod=$QueryMod->getAllModules();


switch ($action):
	case'list':
		 $QueryObj = new category();
	         $QueryObj->ListAll();
		 break;
	case'insert':
		 if(isset($_POST['submit'])):
                    
                    $_POST['module']=serialize($_POST['module']); 
                     
                    $QueryObj = new category();
                    $getID=$QueryObj->saveObjectWithSeo($_POST);
                    
                     /** Check Url Name it should be unique *****/
                    if($getID):
                        $get_url=new category();
                        $urlname=$get_url->getUrlname($getID);
                        if($urlname):
                            $QueryObj123=new category();
                            if($QueryObj123->checkUrlName($urlname,$getID)):
                                $update_url=new category();
                                $update_url->updateUrlname($urlname.'-'.$getID,$getID);
                            endif;
                        endif;
                     endif;   
                     /** End Check Url Name code *****/
                    
                    $admin_user->set_pass_msg('Category has been inserted successfully.');
		    Redirect(make_admin_url('category', 'list', 'list'));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();    
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('category', 'list', 'list'));     
		 endif;
                break;
         case 'update2': 
                if(is_var_set_in_post('submit_position')):
                       
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('category');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):

                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $query= new query('category');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();

                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('category', 'list', 'list')); 
                            
                         endif;   
                      
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('category', 'list', 'list'));
                break;         
	case'update':
		$QueryObj = new category();
		$news=$QueryObj->getObject($id);   
                
                $selected_modules=array();
                  // Find out the modules selected
                  if(isset($news->module) && $news->module):
                     $det=html_entity_decode($news->module);
                     $selected_modules= unserialize($det);

                  endif;
               
      
		if(isset($_POST['submit'])):
                    $_POST['module']=serialize($_POST['module']); 
                    $QueryObj = new category();
                    $getID=$QueryObj->saveObjectWithSeo($_POST);
                    
                     /** Check Url Name it should be unique *****/
                    if($getID):
                        $get_url=new category();
                        $urlname=$get_url->getUrlname($getID);
                        if($urlname):
                            $QueryObj123=new category();
                            if($QueryObj123->checkUrlName($urlname,$getID)):
                                $update_url=new category();
                                $update_url->updateUrlname($urlname.'-'.$getID,$getID);
                            endif;
                        endif;
                     endif;   
                     /** End Check Url Name code *****/
                    
                    
                    $admin_user->set_pass_msg('Category has been updated successfully.');
		    Redirect(make_admin_url('category', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('category', 'list', 'list'));     
	        endif;   
		break;
	case'delete':
		$QueryObj = new category();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Category has been deleted successfully.');
		Redirect(make_admin_url('category', 'list', 'list'));
		break;
            
        case'permanent_delete':
		$QueryObj = new category();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Category has been deleted successfully');
		Redirect(make_admin_url('category', 'thrash', 'thrash'));
        	break;
          
        case'restore':
            $QueryObj = new category();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg('Category has been restored successfully');
            Redirect(make_admin_url('category', 'thrash', 'thrash'));
            break;

        case'thrash':
            $QueryObj = new category();
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
