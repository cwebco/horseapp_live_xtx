<?php
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');
$modName='dropdown';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;

switch ($action):
    case'list':
             $QueryObj = new dropdown();
	     $QueryObj->ListDropdowns();
         break;
    case'insert':
            if(isset($_POST['submit'])):
                $QueryObj = new dropdown();
                $QueryObj->saveObject($_POST);
                $admin_user->set_pass_msg('Option has been inserted successfully .');
		Redirect(make_admin_url('dropdown', 'list', 'list'));
             elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                     Redirect(make_admin_url('dropdown', 'list', 'list'));     
             endif;
           break;

    case'update':

                $QueryObj = new dropdown();
		$pack=$QueryObj->getObject($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new dropdown();
                    $QueryObj->saveObject($_POST);
                    $admin_user->set_pass_msg('The Option has been updated');
                    Redirect(make_admin_url('dropdown', 'list', 'list', 'id=' . $pack->id));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                     Redirect(make_admin_url('dropdown', 'list', 'list'));     
                 endif;
        break;
    case'delete':
                $QueryObj = new dropdown();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Option has been deleted successfully');
	        Redirect(make_admin_url('dropdown', 'list', 'list', 'id=' . $id));
        break;

    default:break;

endswitch;
?>