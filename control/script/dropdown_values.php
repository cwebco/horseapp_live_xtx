<?php
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');
$modName='dropdown_values';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['dd_id'])?$dd_id=$_GET['dd_id']:$dd_id='';
isset($_GET['del'])?$del=$_GET['del']:$del='';
isset($_GET['id'])?$id=$_GET['id']:$id='';

$drop_name=get_object('dropdown', $dd_id);

switch ($action):
    case'list':
                $QueryObj= new dropdownValues();
		$QueryObj->setDropdownId($dd_id);
                $QueryObj->getAllItems();

        break;
    case'insert':
                 if(isset($_POST['submit'])):
                    $QueryObj = new dropdownValues();
                    $QueryObj->saveDropdownValue($_POST);
                    $admin_user->set_pass_msg('Option Value has been inserted successfully.');
                    Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$_POST['dd_id']));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id=' . $_POST['dd_id']));       
		 endif;
        break;
    case'update':
                $QueryObj = new dropdownValues();
		$category=$QueryObj->getObject($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new dropdownValues();
                    $QueryObj->saveDropdownValue($_POST);
                    $admin_user->set_pass_msg('Option Value has been updated successfully.');
                    Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$_POST['dd_id']));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$_POST['dd_id'])); 
		endif;
        break;

    case 'update2':
            if(is_var_set_in_post('submit_position')):
                    
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('dropdown_values');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                  $admin_user->set_pass_msg('Operation has been performed successfully');                   
                endif;
                
           /**************for listing page multiple delete option***************************/
                
                if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        foreach($_POST['multiopt'] as $k=>$v):
                                $QueryObj= new query('dropdown_values');
                                $QueryObj->Data['is_deleted']='1';
                                $QueryObj->Data['id']=$k;
                                $QueryObj->Update();  
                        endforeach;
                    endif;
                  $admin_user->set_pass_msg('Operation has been performed successfully');  
                endif;  
                
          /*********************for listing page multiple delete option**********************************/  
                
            Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id));
           break;

    case'delete':
            $QueryObj = new dropdownValues();
            $QueryObj->deleteObject($id);
            $admin_user->set_pass_msg('Option Value has been deleted successfully');
            Redirect(make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id));

        break;
    case 'delete_image':
            if($del){
                    $object= get_object('dropdown_values', $del);
                    $QueryObj= new query('dropdown_values');
                    $QueryObj->Data['image']='';
                    $QueryObj->Data['id']=$del;
                    $QueryObj->Update();
                    
                    #delete images from folder
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/values/large/'.$object->image);
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/values/medium/'.$object->image);
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/values/thumb/'.$object->image);
                    
                    $admin_user->set_pass_msg('Option Values image has been successfully deleted.');
                    Redirect(make_admin_url('dropdown_values', 'update', 'update','dd_id='.$id.'&id='.$del));
                } 
     break;
                          
     /****************************for thrash changes********************************************/

             case 'update3':	              
                            if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                                  if($_POST['multiopt_action']=='delete'):
                                     foreach($_POST['multiopt'] as $k=>$v):
                                                $QueryObj = new query('dropdown_values');
                                                $QueryObj->id=$k;
                                                $QueryObj->Delete();                                
                                      endforeach;
                                   elseif($_POST['multiopt_action']=='restore'):
                                      foreach($_POST['multiopt'] as $k=>$v):
                                            $QueryObj = new query('dropdown_values');
                                            $QueryObj->Data['is_deleted']='0';
                                            $QueryObj->Data['id']=$k;
                                            $QueryObj->Update(); 
                                      endforeach;       
                                endif;                                   
                                    $admin_user->set_pass_msg('Operation has been performed successfully');
                             endif;       
                        Redirect(make_admin_url('dropdown_values', 'thrash', 'thrash', 'dd_id='.$dd_id));              
                    break; 

     /**************************thrash page changes ends here****************************************/   
                    
         case'thrash':
		$QueryObj = new dropdownValues();
		$QueryObj->getThrash();
		break; 
            
          case'permanent_delete':
		$QueryObj = new dropdownValues();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Option value has been deleted successfully');
		Redirect(make_admin_url('dropdown_values', 'thrash', 'thrash','dd_id='.$dd_id));
        	break;
          
          case'restore':
		$QueryObj = new dropdownValues();
		$QueryObj->restoreObject($id);
		$admin_user->set_pass_msg('Option value has been restored successfully');
		Redirect(make_admin_url('dropdown_values', 'thrash', 'thrash','dd_id='.$dd_id));
        	break;
                       
                    
    default:break;

endswitch;
?>