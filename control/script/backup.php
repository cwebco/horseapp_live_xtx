<?php
include_once(DIR_FS_SITE.'include/functionClass/backupClass.php');
$modName='backup';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new mysqlBackup();
		$QueryObj->ListAll();
		break;
	case'delete':
		$QueryObj = new mysqlBackup();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Backup has been deleted successfully');
		Redirect(make_admin_url('backup', 'list', 'list'));
		break;
        case 'create':
                $QueryObj= new mysqlBackup();
                $filename1=date("ymdhis").'.sql.gz';
                $filename=DIR_FS_SITE_UPLOAD.'backup/'.$filename1;
                $QueryObj->_execute('2', $filename, true);
                $_POST['document']=$filename1;
                $_POST['ip_address']=$_SERVER['REMOTE_ADDR'];
                $QueryObj->saveObject($_POST);            
                Redirect(make_admin_url('backup', 'list', 'list'));
		break;
        case 'download':
                break;
             
	case'permanent_delete':
		$QueryObj = new mysqlBackup();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Backup has been deleted successfully');
		Redirect(make_admin_url('backup', 'thrash', 'thrash'));
        	break;
          
        case'restore':
		$QueryObj = new mysqlBackup();
		$QueryObj->restoreObject($id);
		$admin_user->set_pass_msg('Backup has been restored successfully');
		Redirect(make_admin_url('backup', 'thrash', 'thrash'));
        	break;
            
        case'thrash':
		$QueryObj = new mysqlBackup();
		$QueryObj->getThrash();
		break;
	default:break;
endswitch;
?>
