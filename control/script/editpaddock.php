<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['delete_id'])?$delete_id=$_GET['delete_id']:$delete_id='0';

isset($_GET['redirect_id'])?$redirect_id=$_GET['redirect_id']:$redirect_id='0';
isset($_GET['redirect_page'])?$redirect_page=$_GET['redirect_page']:$redirect_page='stallion';
isset($_GET['redirect_tab'])?$redirect_tab=$_GET['redirect_tab']:$redirect_tab='';

switch ($action):
	case'update':
            if(isset($_POST['submit'])):
                if($_POST['date']!=''):
                    $query_obj = new paddock_log();
                    $query_obj->editPaddockLog($_POST['pad_id'],$_POST);
                    $admin_user->set_pass_msg('Updated successfully.');
                else:
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Please add date first.');  
                endif;
                
                Redirect(make_admin_url($_POST['page'], 'update', 'update','id='.$_POST['id']).'#tab_1_4');
            endif;
            break;
        case 'new':
                if(isset($_POST['submit'])):
                    if($_POST['to_paddock_date']!=''):
                         /*get current paddock of horse*/
                         $query = new paddock_log();
                         $current = $query -> currentPaddock($id);

                         if(!empty($current)):
                             /* add out from the current paddock */   
                             $query_obj = new paddock_log();
                             $query_obj ->outFromCurrentPaddock($_POST['to_paddock_date'],$current);
                         endif;

                         /* entry in new paddock */
                         $QueryObj1 = new paddock_log();
                         $QueryObj1 -> addPaddock($_POST['id'],$_POST);

                         $admin_user->set_pass_msg('Information added successfully.');
                     else:
                         $admin_user->set_error();
                         $admin_user->set_pass_msg('Please add date first.');  
                     endif;
                     Redirect(make_admin_url($_POST['page'], 'update', 'update','id='.$_POST['id']).'#tab_1_4');
                endif;
            break;
        case 'delete':
                if($delete_id!=''):
                    $query_obj = new paddock_log();
                    $query_obj -> deletePaddockLog($delete_id);
                    
                    $admin_user->set_pass_msg('Deleted successfully.');
                    Redirect(make_admin_url($redirect_page, 'update', 'update','id='.$redirect_id).'#'.$redirect_tab);
                else:
                    Redirect(make_admin_url('horse', 'list', 'list'));
                endif;
            break;
	default:break;
endswitch;
?>

