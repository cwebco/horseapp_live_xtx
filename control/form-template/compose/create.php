
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                New Message
            </h3>
            <ul class="breadcrumb">
                <li class="pull-right"> 
		    <i class="icon-angle-left"></i>
                    <a href="<?php echo make_admin_url('horse', 'view', 'view&id=' . $id); ?>">Back </a> 
                </li>

                <li class="last">
                    New Message
                </li>
		<li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php //include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('compose', 'insert', 'insert') ?>" method="GET" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-heart"></i>New Message</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
			    <?php if (isset($r->primary_email_id)) { ?>
    			    <div class="span12">
    				<div class="control-group">
    				    <label class="control-label" for="name">To<span class="required">*</span></label>
    				    <div class="controls" style="width:50%;">
    					<input type="email" name="" value="<?php echo $email_to = $r->primary_email_id ?>" id="name" class="span10 m-wrap validate[required]"/>
    				    </div>
    				</div> 
				<div class="pull-right" style="margin-right: 161px; margin-top: -48px;">
				    <?php echo $r->first_name ?>
				</div>
				<div class="control-group">
    				    <label class="control-label" for="name">subject<span class="required">*</span></label>
    				    <div class="controls">
    					<input type="text" name="subject" value="" id="name" class="span10 m-wrap validate[required]"/>
    				    </div>
    				</div>
    				<div class="control-group">
    				    <label class="control-label" for="name">Message<span class="required">*</span></label>
    				    <div class="controls">
    					<textarea name="message" id="message" class="input-text msg_box" placeholder="Please Type Your Message ..." rows="10" style="width:81%;" ></textarea>
    				    </div>
    				</div>
				<?php 
				$hourse_id = $_GET['id'];
					//pr ($id);
				?>
    				<div class="form-actions">
    				    <input class="btn blue" type="submit" name="send_message" value="Send" tabindex="7" /> 
    				    <input type="hidden" name="id" value="<?php echo $id ?>"  /> 
				    <input type="hidden" name="hourse_id" value="<?php echo $hourse_id ?>"  />
    				    <input type="hidden" name="Page" value="compose"  /> 
    				    <input type="hidden" name="action" value="insert"  /> 
    				    <input type="hidden" name="section" value="insert"  /> 
    				    <a href="<?php echo make_admin_url('horse', 'view', 'view&id=' . $id); ?>" class="btn" name="cancel" > Cancel</a>
    				</div>
    			    </div> 
				<?php
			    } else {
				echo 'Email Not Found';
			    }
			    ?>
			</div>
		    </div>
		    </form>
		    <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	    </div>
	    <!-- END PAGE CONTAINER-->    

