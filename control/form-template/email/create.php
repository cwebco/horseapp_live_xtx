
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Email & SMS Templates
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-file-text"></i>
                                               <a href="<?php echo make_admin_url('email', 'list', 'list');?>">List Templates</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        New Template
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('email', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span<?php echo (defined('RIGHT_ACTION') && RIGHT_ACTION==1)?"8":"12"?>">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-file-text"></i>New Template</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">


                                             <div class="control-group">
                                                     <label class="control-label" for="email_name">Name<span class="required">*</span></label>
                                                     <div class="controls">
                                                        <input type="text" name="email_name" id="email_name" value="" class="span6 m-wrap validate[required]"  />
                                                    </div>
                                             </div>        
                                             <div class="control-group">
                                                     <label class="control-label" for="email_subject">Subject<span class="required">*</span></label>
                                                     <div class="controls">
                                                        <input type="text" name="email_subject" id="email_subject" value="" class="span6 m-wrap validate[required]"  />
                                                    </div>
                                             </div> 
                                        
                                            <div class="control-group">
                                                     <label class="control-label" for="email_type">Format<span class="required">*</span></label>
                                                     <div class="controls">
                                                         <select  name="email_type" id="email_type"  class="span6 m-wrap ">
                                                            <option value="html">Html</option>
                                                            <option value="text">Text</option>
                                                        </select>
                                                    </div>
                                             </div>  
                                        
                                              <div class="control-group">
                                                     <label class="control-label" for="type">Type<span class="required">*</span></label>
                                                     <div class="controls">
                                                         <select  name="type" id="type"  class="span6 m-wrap ">
                                                            <option value="email">Email</option>
                                                            <option value="sms">SMS</option>
                                                        </select>
                                                    </div>
                                             </div>  
                                            
                                             <div class="control-group">
                                                    <label class="control-label" for="email_text">Text<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="email_text" class="span12 ckeditor m-wrap" name="email_text" rows="6"></textarea>
                                                       
                                                    </div>
                                             </div>
                                        
                                           
                                           
                                        
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('email', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                     <?php endif;?>        
                                  
                              </div> 
                            </div>
                        </div>
                          
                             <?php if(defined('RIGHT_ACTION') && RIGHT_ACTION==1):?>   
                                        <div class="span4">
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                            <div class="portlet box blue">
                                                    <div class="portlet-title">
                                                            <div class="caption"><i class="icon-file-text"></i>Action</div>
                                                            <div class="tools">
                                                                    <a href="javascript:;" class="collapse"></a>


                                                            </div>
                                                    </div>
                                                    <div class="portlet-body">

                                                          <section>  
                                                               
                                                                      <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                                      <a href="<?php echo make_admin_url('email', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>

                                                                

                                                         </section>    
                                                  </div>  
                                            </div>

                                        </div>
                            <?php endif;?>  

                     </form>
                     <div class="clearfix"></div>
                                          
                                   
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



