
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                View Message
            </h3>
            <ul class="breadcrumb">
                <li class="pull-right"> 
		    <i class="icon-angle-left"></i>
                    <a href="<?php echo make_admin_url('my_messages', 'list', 'list'); ?>">Back </a> 
                </li>

                <li class="last">
                    View Message
                </li>
		<li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php //include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('my_messages', 'insert', 'insert') ?>" method="GET" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-heart"></i>View Message</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
			    
    			    <div class="span6 ">
    				<div class="control-group">
    				    <label class="control-label" for="name">To<span class="required">*</span></label>
    				    <div class="controls">
    					<?php echo $horse_name->name ?>
    				    </div>
    				</div> 
    				<div class="control-group">
    				    <label class="control-label" for="name">Message<span class="required">*</span></label>
    				    <div class="controls">
    				<?php echo $one_msg->message; ?>
    				    </div>
    				</div>
				
    			    </div> 
				
			</div>
		    </div>
		    </form>
		    <div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	    </div>
	    <!-- END PAGE CONTAINER-->    

