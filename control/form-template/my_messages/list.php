<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">My Messages</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Messages</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
   
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list-alt"></i>List Messages</div>
                    
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th class="hidden-480">Sr. No</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
			     <?php $k = 1; ?>
			    <?php foreach ($list_msg as $all_msgs) { 
				$query = new horse;
				$horse_detail = $query->get_horse_obj($all_msgs['hourse_id']);
				
				?>
                                <tr class="odd gradeX">
                                    <td class="hidden-480"><?php
					echo $k++;
					?></td>
                                    <td><?php echo $horse_detail->name ?></td>
                                        <td><a class="btn blue mini tooltips" href="<?php echo make_admin_url('my_messages', 'view', 'view', 'id='.$all_msgs['id'])?>" title="click to view/edit this record"><i class="icon-edit icon-white"></i> View</a>&nbsp;&nbsp;
                                                                     <a class="btn red mini tooltips" href="<?php echo make_admin_url('my_messages', 'delete', 'delete&id=' .$all_msgs['id'])?>" onclick="return confirm('Are you sure?');" title="click to archive this record"><i class="icon-archive icon-white"></i> Archive</a></td>
                                </tr>
			    <?php } ?>
                        </tbody>

                    </table>  
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    



