<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Dropdown</h3>
                            <ul class="breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                    <i class="icon-angle-right"></i>
                                </li>
                                
                                <li>List Dropdowns</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php  //include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
		       <div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-sort-by-attributes"></i>Manage Dropdowns</div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body">
                                  <form action="<?php echo make_admin_url('dropdown', 'update2', 'list', 'id='.$id);?>" method="post" id="form_data" name="form_data" >
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th class="hidden-480">Sr. No.</th>
                                                <th>Name</th>
                                                <th>Manage Option Values</th>                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <? if($QueryObj->GetNumRows()!=0):?>
                                                <? $sr=1;while($pack=$QueryObj->GetObjectFromRecord()):?>
                                                    <tr>
                                                            <td class="hidden-480"><?php echo $sr++;?></td>  
                                                            <td><?=$pack->name;?></td>
                                                            <td>
                                                               <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$pack->id);?>" style="margin-left: 20px;"><btn class="btn red mini">Manage Values <i class="icon-circle-arrow-right icon-white"></i></btn></a>
                                                            </td>
                                                            <td>
                                                                  <a class="btn blue mini tooltips" href="<?php echo make_admin_url('dropdown', 'update', 'update', 'id='.$pack->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i> Edit</a>                       
                                                            </td>
                                                   </tr>
                                                <?php endwhile; ?>
                                              <?php endif;?>
                                       </tbody>
                                    </table>
                                  </form>      
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                       </div>
                </div>
             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    