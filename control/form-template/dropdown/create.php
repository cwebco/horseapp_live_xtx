<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->
   <div class="row-fluid">
       <div class="span12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
          <h3 class="page-title">Categories</h3>
            <ul class="breadcrumb">
                    <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                            <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <i class="icon-sitemap"></i>
                        <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">List Dropdowns</a>
                        <i class="icon-angle-right"></i>
                    </li>
                    <li class="last">
                        New Dropdown
                    </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
         </div>
     </div>
  <!-- END PAGE HEADER-->
  <div class="clearfix"></div>
<?php  //include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>   
  <div class="clearfix"></div>
  <?php 
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
      <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
     <form id="validation" action="<?php echo make_admin_url('dropdown', 'insert', 'list', 'id='.$id)?>" method="post" name="payment-method" enctype="multipart/form-data" class="form-horizontal" >
          <!-- / Box -->
          <div class="span12">
		  <!-- BEGIN EXAMPLE TABLE PORTLET-->
             <div class="portlet box blue">
                  <div class="portlet-title">
                        <div class="caption"><i class="icon-sort-by-attributes"></i>New Dropdown</div>
                        <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                        </div>
                  </div>
                  <div class="portlet-body form">
                        <div class="control-group">
                                <label class="control-label" for="name">Option Name:</label>
                                <div class="controls">
                                <input type="text" name="name" id="name" class="span6 m-wrap validate[required]" />

                                </div>
                        </div>     
                       <div class="form-actions">
                             <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                             <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>

                    </div>
              </div> 
             </div> 
          </div>   
     </form>   
    <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
    <!-- END PAGE CONTAINER-->