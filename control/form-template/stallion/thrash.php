  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Archived Horses</h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>Archived Horses</li>
                            </ul>
                           <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption"><i class="icon-heart"></i>Manage Horses</div>
                                        <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                        </div>
                                </div>
                                <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                <thead>
                                                     <tr>
                                                        <th class="hidden-480">Sr. No</th>
                                                        <th>Name</th>
                                                        <th class="hidden-480">Status</th>
                                                        <th class="hidden-480">Sex</th>
                                                        <!---<th>Arrived Date</th>--->
                                                        <th>Departed Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      <? if($QueryObj->GetNumRows()!=0):?>
                                                     <?php $sr=1;while($horse=$QueryObj->GetObjectFromRecord()):?>
                                                            <tr class="odd gradeX">
                                                                <td class="hidden-480"><?php echo $sr++;?></td>
                                                                <td><?php echo $horse->name?></td>
                                                                <td class="hidden-480"><?php echo $horse->horse_status?></td>
                                                                <td class="hidden-480"><?php echo $horse->horse_sex?></td>
                                                                <!---<td><?php //echo ($horse->arrived!='' && $horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';?></td>--->
                                                                <td><?php echo ($horse->departed!='' && $horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';?></td>
                                                                <td>
                                                                     <a class="btn green mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'redirect=stallion&id='.$horse->id)?>" class="tipTop smallbtn" title="click here to view this record"><i class="icon-share icon-white"></i> </a>
                                                                     <a class="btn green mini tooltips" href="<?php echo make_admin_url('stallion', 'restore', 'restore', 'id='.$horse->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>
                                                                     <a class="btn green mini tooltips" href="<?php echo make_admin_url('stallion', 'permanent_delete', 'thrash', 'id='.$horse->id)?>" class="tipTop smallbtn" title="click here to delete this record permanently" onclick="return confirm('Are you sure? All records of this horse will be permanently deleted.');"><i class="icon-remove icon-white"></i> </a>
                                                                </td>
                                                            </tr>
                                                     <?php endwhile;?>
                                                </tbody>
                                               <?php endif;?>  
                                        </table>  
                                </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                </div>
           </div>
           <div class="clearfix"></div>
    </div>
<!-- END PAGE CONTAINER-->    
