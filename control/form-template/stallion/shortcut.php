<?php if($section=='update'): ?>
<div class="tiles pull-left">
    <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id='.$id.'&type=arrived')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                               Arrived Report
                        </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-yellow">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id='.$id.'&type=departed')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                               Departed Report
                        </div>
                </div>
            </a>   
        </div>
</div>
<?php endif;?>
<div class="tiles pull-right">
        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('stallion', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Horses
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('stallion', 'list', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Horse
                        </div>
                </div>
            </a> 
        </div>
        <div class="tile bg-red <?php echo ($Page=="stallion" && $section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('stallion', 'thrash', 'thrash');?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-archive"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Archive
                        </div>
                </div>
            </a> 
        </div>
</div>