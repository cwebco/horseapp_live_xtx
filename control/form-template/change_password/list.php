<!--<script type="text/javascript">
  function ChangeToPassField() {
      if(document.getElementById("show_password").checked==true){
          document.getElementById('password').type="text";
        }
      if(document.getElementById("show_password").checked==false){
          document.getElementById('password').type="password";
        }
  }
  </script>-->

<script>
    $(document).on('change', '#show_password', function () {
        var el = $(this);
        var checked = el.is(':checked');
        if (checked) {
            $('#password').attr('type', 'text');
            $('#confirm_passsword').attr('type', 'text');
        } else {
            $('#password').attr('type', 'password');
            $('#confirm_passsword').attr('type', 'password');
        }
    });
</script>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Change Password</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Change Password</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/settings/shortcut.php'); ?>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-lock"></i>Change Password</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="<?php echo make_admin_url('change_password', 'update', 'list'); ?>" method="post" id="validation" name="form_data" class="form-horizontal">	
                        <div class="control-group">
                            <label class="control-label">New Password</label>
                            <div class="controls">
                                <label class="input line">
                                    <div>
                                        <input type="password" name="passsword" class="span8 m-wrap validate[required]" placeholder="" id="password"/>
                                    </div>
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" name="show_password" id="show_password" value="1" />
                                    Show Password
                                </label> 
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Confirm Password</label>
                            <div class="controls">
                                <input type="password" name="confirm_passsword" class="span8 m-wrap validate[required,equals[password]]" placeholder="" id="confirm_passsword"/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input class="btn blue" type="submit" name="Submit" value="Submit" tabindex="7"> 
                            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                        </div>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    