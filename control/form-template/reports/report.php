<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Print Reports</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <?php if ($sex == 'mare' || $sex == 'stallion'): ?>
                    <li>
                        <i class="icon-bar-chart"></i>
                        <a href="<?php echo make_admin_url('reports', 'list', 'list'); ?>">Download Reports</a>
                        <i class="icon-angle-right"></i></li>
                    <li>Print All <?php echo $sex . 's'; ?></li>
                <?php else: ?>
                    <li>Download Reports</li>
                <?php endif; ?>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left">
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printallhorses', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Print <?php echo ucfirst($sex) . 's'; ?> Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url_window('printshortreport', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Print Microchip Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-red">
            <a href="<?php echo make_admin_url_window('document_horse', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Documents Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
    </div>
    <div class="tiles pull-right">
        <div class="tile bg-purple <?php echo ($sex == 'horse') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        All Horses
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green <?php echo ($sex == 'stallion') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list', 'sex=stallion'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Horse
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-yellow <?php echo ($sex == 'mare') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list', 'sex=mare'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Mares
                    </div>
                </div>
            </a>   
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <?php
            $div_color = 'green';
            if ($sex == 'stallion'):
                $div_color = 'green';
            elseif ($sex == 'mare'):
                $div_color = 'yellow';
            else:
                $div_color = 'purple';
            endif;
            ?>
            <div class="portlet box <?php echo $div_color; ?>">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-bar-chart"></i>All <?php echo ucfirst($sex) . 's'; ?> Report</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <?php if ($sex == 'stallion'): ?>
                                    <th>Sex</th>
                                <?php elseif ($sex == 'mare'): ?>
                                    <th>Sire</th>    
                                <?php else: ?>
                                    <th>Sex</th>
                                <?php endif; ?>
                                <th>D.O.B.</th>
                                <th width="20%">Owner</th>
                                <th>Farrier</th>
                                <th>Wormer</th>
                                <th>Notes</th>
                                <!--<th>Actions</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <? if($QueryObj->GetNumRows()!=0):?>
                            <?php
                            $sr = 1;
                            while ($horse = $QueryObj->GetObjectFromRecord()):
                                $total_notes = horse_notes::total_notes($horse->id);
                                $query = new horse_log();
                                $count_getLogs = $query->count_getLogs($horse->id, 'farrier');
                                $query = new horse_log();
                                $count_getLogs_wormer = $query->count_getLogs($horse->id, 'wormer');
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $sr++; ?></td>
                                    <td><?php echo $horse->name ?></td>
                                    <?php if ($sex == 'stallion'): ?>
                                        <td><?php echo $horse->horse_sex; ?></td>
                                    <?php elseif ($sex == 'mare'): ?>
                                        <td><?php echo $horse->sire; ?></td>
                                    <?php else: ?>
                                        <?php if ($horse->sex == 'stallion'): ?>
                                            <td>Horse</td>
                                        <?php else: ?>
                                            <td><?php echo ucfirst($horse->sex); ?></td>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <td><?php echo date('d/m/y', strtotime($horse->born)) ?></td>
                                    <td><?php echo $horse->owner_name ?></td>
                                    <td>

                                        <a class="btn blue mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'id=' . $horse->id . '&#tab_1_2') ?>" title="click to view full details"><i class="icon-zoom-in icon-white"></i> View (<?php echo $count_getLogs; ?>)</a>                                    

                                    </td>

                                    <td>
                                        <a class="btn blue mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'id=' . $horse->id . '&#tab_1_2') ?>" title="click to view full details"><i class="icon-zoom-in icon-white"></i> View (<?php echo $count_getLogs_wormer; ?>)</a>
                                    </td>
                                    <td>
                                        <a class="btn blue mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'id=' . $horse->id . '&#tab_1_6') ?>" title="click to view full details"><i class="icon-zoom-in icon-white"></i> View (<?php echo $total_notes; ?>)</a>&nbsp;&nbsp;                                    
                                    </td>
    <!--                                    <td>
                                        <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id=' . $horse->id . '&type=arrived') ?>" class="btn green mini"  title="click to print arrived report"><i class="icon-print icon-white"></i> Report</a>
                                    </td>-->
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                        <?php endif; ?>  
                    </table>  
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    
