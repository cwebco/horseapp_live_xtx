<!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                        <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart"></i>
                                    <?php if($type=='service'): ?>
                                         <?php echo 'Last Service Report';?>
                                    <?php elseif($type=='flu'): ?>
                                         <?php echo 'Flu Vaccinations Report';?>
                                    <?php else: ?>
                                        <?php echo ucfirst($type).' Report'; ?>
                                    <?php endif; ?>
                                    
                                    <?php if($from_date!='' && $to_date!=''): ?>
                                        <?php echo '('.$from_date.' - '.$to_date.')';?> 
                                    <?php else: ?>
                                        <?php echo ' - Complete' ;?>
                                    <?php endif; ?>
                                </div>
                            <?php if($QueryObj->GetNumRows()!=0):?>
                                <div class="tools" style="margin-top: 0px;">
                                     <a target="_blank" class="btn grey mini" href="<?php echo make_admin_url('printreport','list','list','type='.$type.'&from_date='.$from_date.'&to_date='.$to_date);?>" >
                                       <i class="icon-print"></i> Print
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                </div>
                            <?php endif; ?>
                                
                        </div>
                        <div class="portlet-body">
                             <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                                 <tr>
                                                        <th class="hidden-480">#</th>
                                                        <th>Horse Name</th>
                                                        <!--<th class="hidden-480">Sex</th>-->

                                                        <?php if($type=='paddock'): ?>
                                                            <th>Paddock</th>
                                                            <th class="hidden-480">Move</th>
                                                        <?php elseif($type=='service'): ?>
                                                            <th class="hidden-480">Type</th>
                                                            <th>Season</th>
                                                        <?php elseif($type=='flu' || $type=='ehv'): ?>
                                                            <!--<th class="hidden-480">Owner</th>-->
                                                            <th>Type</th>
                                                        <?php else: ?>
                                                            <!--<th class="hidden-480">Owner</th>-->
                                                            <!--<th>Arrived Date</th>-->
                                                        <?php endif; ?>

                                                        <?php if($type=='service'): ?>
                                                            <th>Last Service</th>
                                                        <?php else: ?>
                                                            <th>Date</th>
                                                        <?php endif; ?>
                                                        <?php if($type=='wormer'): ?>
                                                            <th>Value</th>
                                                        <?php endif; ?>
                                                </tr>
                                        </thead>
                                         <tbody>
                                              <?php if($QueryObj->GetNumRows()!=0):?>
                                             <?php $sr=1;while($log=$QueryObj->GetObjectFromRecord()):?>
                                             <?php if($type=='service'): ?>
                                                <?php $horse = getHorseSelectedDetails($log->mare_id); ?>
                                             <?php else: ?>
                                                <?php $horse = getHorseSelectedDetails($log->horse_id); ?>
                                             <?php endif; ?>
                                                    <tr class="odd gradeX">
                                                        <td class="hidden-480"><?php echo $sr++;?></td>
                                                        <td><?php echo ucfirst($log->name);?></td>
                                                        <!--
                                                         <?php //if($horse->sex=='stallion'): ?>
                                                            <td>Horse</td>
                                                        <?php //else: ?>
                                                            <td><?php //echo ucfirst($horse->sex); ?></td>
                                                        <?php //endif; ?>
                                                            -->
                                                        <?php if($type=='paddock'): ?>
                                                            <td><?php echo ucfirst($log->paddock_title);?></td>
                                                            <td class="hidden-480"><?php echo ucwords($log->move);?></td>
                                                        <?php elseif($type=='service'): ?>
                                                            <td class="hidden-480"><?php echo ucfirst($log->mare_type);?></td>
                                                            <td><?php echo $log->season;?></td>
                                                        <?php elseif($type=='flu' || $type=='ehv'): ?>
                                                            <!--<td class="hidden-480"><?php echo ucfirst($horse->owner_name);?></td>-->
                                                            <td><?php echo $log->value; ?></td>
                                                        <?php else: ?>
                                                            <!--<td class="hidden-480"><?php echo ucfirst($horse->owner_name);?></td>-->
                                                            <!--<td><?php echo date('d/m/Y',strtotime($horse->arrived));?></td>-->
                                                        <?php endif; ?>

                                                        <?php if($type=='service'): ?>
                                                            <td><?php echo ($log->last_service!='' && $log->last_service!='0000-00-00')?date('d/m/Y',strtotime($log->last_service)):'';?></td>
                                                        <?php else: ?>
                                                            <td><?php echo ($log->date!='' && $log->date!='0000-00-00')?date('d/m/Y',strtotime($log->date)):'';?></td>
                                                        <?php endif; ?>
                                                        <?php if($type=='wormer'): ?>
                                                            <td><?php echo $log->value; ?></td>
                                                        <?php endif; ?>    
                                                    </tr>
                                             <?php endwhile;?>
                                        </tbody>
                                       <?php endif;?>  
                                </table>  
                        </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    <div class="clearfix"></div>