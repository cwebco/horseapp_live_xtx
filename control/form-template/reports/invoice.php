<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Print Reports</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
		<?php if ($sex == 'mare' || $sex == 'stallion'): ?>
    		<li>
    		    <i class="icon-bar-chart"></i>
    		    <a href="<?php echo make_admin_url('reports', 'list', 'list'); ?>">Download Reports</a>
    		    <i class="icon-angle-right"></i></li>
    		<li>Print All <?php echo $sex . 's'; ?></li>
		<?php else: ?>
    		<li>Download Reports</li>
		<?php endif; ?>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left">
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printallhorses', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Print <?php echo ucfirst($sex) . 's'; ?> Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url_window('printshortreport', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Print Microchip Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-red">
            <a href="<?php echo make_admin_url_window('document_horse', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Documents Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url_window('invoice', 'list', 'list', 'sex=' . $sex); ?>">
                <div class="corner"></div>
                <!--<div class="tile-body">
                        <i class="icon-print"></i>
                </div>-->
                <div class="tile-object">
                    <div class="name">
                        Invoice Report (<?php echo $total; ?>)
                    </div>
                </div>
            </a>   
        </div>
    </div>
    <div class="tiles pull-right">
        <div class="tile bg-purple <?php echo ($sex == 'horse') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        All Horses
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green <?php echo ($sex == 'stallion') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list', 'sex=stallion'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Horse
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-yellow <?php echo ($sex == 'mare') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('reports', 'list', 'list', 'sex=mare'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Mares
                    </div>
                </div>
            </a>   
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
	    <?php
	    $div_color = 'green';
	    if ($sex == 'stallion'):
		$div_color = 'green';
	    elseif ($sex == 'mare'):
		$div_color = 'yellow';
	    else:
		$div_color = 'purple';
	    endif;
	    ?>
            <div class="portlet box <?php echo $div_color; ?>">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-bar-chart"></i>All <?php echo ucfirst($sex) . 's'; ?> Report</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
		    <div class="tiles pull-left">
			<form name="frmSearch" method="GET">
			    <input type="hidden" name="Page" value="<?php echo $Page ?>" />
			    <input type="hidden" name="action" value="<?php echo $action ?>" />
			    <input type="hidden" name="section" value="<?php echo $section ?>" />
			    <p class="search_input">
				<input type="text" placeholder="From Date" id="post_at" name="from_date"  value="<?php echo isset($_GET['from_date']) ? $_GET['from_date'] : '' ?>" class="input-control validate[required]" required />
				<input type="text" placeholder="To Date" id="post_at_to_date" name="to_date" style="margin-left:10px"  value="<?php echo isset($_GET['to_date']) ? $_GET['to_date'] : '' ?>" class="input-control" required />			 
				<input type="submit" class="bg-blue filtter_search" >
			    </p>
			</form>
		    </div>

		    <!--		    <div class="tiles pull-right">
					    
					</div>-->




		    <?php if (isset($_GET['from_date']) && isset($_GET['to_date'])) { ?>

    		    <table class="table table-striped table-bordered table-hover" id="sample_2">
    			<div class="tiles pull-right filter_export_btn">
    			    <a href="<?php echo make_admin_url('reports', 'invoice', 'invoice') ?>" class="bg-blue export_excl">All Reports</a>
    			    <a href="<?php echo make_admin_url('reports', 'invoice', 'invoice', '&from_date=' . $_GET['from_date'] . '&to_date=' . $_GET['to_date'] . '&download2=csv2') ?>" class="bg-blue export_excl">Download Excel</a>
    			</div>
    			<thead>
    			    <tr>
    				<th width="5%">Sr. No</th>
    				<th width="10%">Name</th>
				    <?php if ($sex == 'stallion'): ?>
					<th width="10%">Type</th>
				    <?php elseif ($sex == 'mare'): ?>
					<th width="10%">Sire</th>    
				    <?php else: ?>
					<th width="8%">Type</th>
				    <?php endif; ?>
				<th width="8%">Arrival Date</th>
    				<th width="8%">Departure Date</th>
				<th width="8%">Rate To Keep</th>
                                <th width="8%">Rate Foaling Fee</th>
    				<th width="8%">Owner</th>
				<th width="8%">Date</th>
                                                                                            <!--                                <th width="15%">Farrier</th>
    				<th width="15%">Wormer</th>-->
    				<th width="15%">Invoice Notes</th>
    			    </tr>
    			</thead>
    			<tbody>
			<?php
				$query = new horse_notes;
                    $result_fillter = $query->filter_horses($_GET['from_date'], $_GET['to_date']);
                    

                    if($result_fillter){ 
                    $query = new horse();
                    $horsenot_filter = $query->horse_not_in_filter($horse_ids_from_filter);
                    
                    
                    //pr($horsenot_filter); die;

                    
                    $result_fillterq = array_merge($result_fillter, $horsenot_filter);
                    //pr($result_fillter);
                    foreach ($result_fillterq as $keysr=>$notes_ddd) {
                    $h_id = isset($notes_ddd['horse_id']) ? $notes_ddd['horse_id'] : $notes_ddd['id'];
                    $query = new horse();
                    $horses = $query->get_horse_obj_hr($h_id);
                    if ($horses) {
                        $fl_horse_name = $horses->name;
                        $fl_horse_status = $horses->horse_status;
                        $fl_horse_departed = $horses->departed;
		        $frate_to_keep = $horses->rate_to_keep;
                        $rate_foaling_fee = $horses->rate_foaling_fee;
                        $fl_horse_arrived = $horses->arrived;
                        $fl_horse_owner_name = $horses->owner_name;
                        ?>
                                <tr>
                                    <td><?php echo $keysr+=1; ?></td>
                                    <td> <?php echo $fl_horse_name; ?></td>
                                    <td> <?php echo $fl_horse_status; ?></td>
				    <td> <?php echo $fl_horse_arrived; ?></td>
                                    <td> <?php echo $fl_horse_departed; ?></td>
				    <td> <?php echo $frate_to_keep; ?></td>
                                    <td> <?php echo $rate_foaling_fee; ?></td>
                                    <td> <?php echo $fl_horse_owner_name; ?></td>
                                    <td> <?php echo isset($notes_ddd['date']) ? $notes_ddd['date'] : ''; ?></td>
                                    <td> <?php echo isset($notes_ddd['invoice_note']) ? $notes_ddd['invoice_note'] : ''; ?></td>
                                    
                                    
                                    <?php 
                    }
                }
                    }   ?>
    			</tbody>
    		    </table>
<?php } else { ?>

    		    <table class="table table-striped table-bordered table-hover" id="sample_2">
    			<a href="<?php echo make_admin_url('reports', 'invoice', 'invoice', 'download=csv') ?>" class="bg-blue export_excl">Download In Excel</a>
    			<br><br>
    			<thead>
    			    <tr>
    				<th width="5%">Sr. No</th>
    				<th width="10%">Name</th>
				    <?php if ($sex == 'stallion'): ?>
					<th width="10%">Type</th>
				    <?php elseif ($sex == 'mare'): ?>
					<th width="10%">Sire</th>    
				    <?php else: ?>
					<th width="8%">Type</th>
    <?php endif; ?>
				<th width="8%">Arrival Date</th>
    				<th width="8%">Departure Date</th>
				<th width="8%">Rate To Keep</th>
                                <th width="8%">Rate Foaling Fee</th>
    				<th width="8%">Owner</th>
				<th width="8%">Date</th>
                                                                                            <!--                                <th width="15%">Farrier</th>
    				<th width="15%">Wormer</th>-->
    				<th width="15%">Invoice Notes</th>
    			    </tr>
    			</thead>
    			<tbody>
				<?php foreach ($all_notes as $keysr => $fillter_note_detail) {
				    if($fillter_note_detail['invoice_note']){
				   // pr($fillter_note_detail);
				     ?>
				    <?php
				    $horse_id = $fillter_note_detail['horse_id'];
				    $query = new horse();
				    $horses = $query->get_horse_ob($horse_id);
					//pr($horses);
				    if ($horses) {
					$fl_horse_name = $horses->name;
					$fl_horse_status = $horses->horse_status;
					$fl_horse_departed = $horses->departed;
					$fl_horse_arrived = $horses->arrived;
					$fl_horse_owner_name = $horses->owner_name;
					$rate_to_keep = $horses->rate_to_keep;
                                        $rate_foaling_fee = $horses->rate_foaling_fee;
					?>

	    			    <tr>
	    				<td><?php echo $keysr+=1; ?></td>
	    				<td><?php echo $fl_horse_name; ?></td>
	    				<td><?php echo $fl_horse_status; ?></td>
					<td><?php echo $fl_horse_arrived; ?></td>
	    				<td><?php echo $fl_horse_departed; ?></td>
					<td><?php echo $rate_to_keep; ?></td>
                                        <td><?php echo $rate_foaling_fee; ?></td>
	    				<td><?php echo $fl_horse_owner_name; ?></td>
<!--	    				<td>
						<?php
//						if ($fillter_note_detail['invoice_note']) {
//						    if ($fillter_note_detail['date'] != 0) {
//							echo $fillter_note_detail['date'] . ',<br />';
//						    }
//						    echo $fillter_note_detail['invoice_note'] . ',<br />';
//						}
						?>
	    				</td>-->
					<td><?php echo $fillter_note_detail['date'] ?></td>
					<td><?php echo html_entity_decode($fillter_note_detail['invoice_note']) ?></td>
					
					
	    			    </tr> 
				    <?php }
				} } ?>
    			</tbody>
			<?php } ?> 
    		    </table>  

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->  
<script>
    $(function () {
	$("#post_at").datepicker({
	    format: 'yyyy-mm-dd'
	});
    });
    $(function () {
	$("#post_at_to_date").datepicker({
	    format: 'yyyy-mm-dd'
	});
    });

</script>



<?php
//if (isset($_GET['download2']) && $_GET['download2'] == 'csv2') {
//
//	$result = '';
//	foreach ($result_fillter as $fillter_note_detail) {
//		$result = $fillter_note_detail['invoice_note'];
//		//pr($result);
//		}
//		
//    $data = array('name,sex,departure,arrival,owner');
//    $fp = fopen('data.csv', 'w');
//    foreach ($data as $line) {
//	$val = explode(",", $line);
//	fputcsv($fp, $val);
//    }
//    fclose($fp);
//}
?>
