<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Print Reports</h3>
                        <ul class="breadcrumb">
                                <li>
                                        <i class="icon-home"></i>
                                        <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                        <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                  <i class="icon-bar-chart"></i>
                                  <a href="<?php echo make_admin_url('reports', 'list', 'list');?>">Reports</a>
                                  <i class="icon-angle-right"></i>
                                </li>
                                <li>Owner Reports</li>
                        </ul>
                       <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-right">
                    <div class="tile bg-blue">
                        <a href="<?php echo make_admin_url_window('printownerreport','list','list'); ?>">
                            <div class="corner"></div>
                            <div class="tile-body">
                                    <i class="icon-print"></i>
                            </div>
                            <div class="tile-object">
                                    <div class="name">
                                           Print Report
                                    </div>
                            </div>
                        </a>   
                    </div>
        </div>
        <div class="clearfix"></div>
         <div class="row-fluid">
            <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-bar-chart"></i>Owner Report</div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body">
                                 <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                     <tr>
                                                            <th class="hidden-480">Sr. No</th>
                                                            <th>Name</th>
                                                            <th class="hidden-480">Contact</th>
                                                            <th class="hidden-480">Phone</th>
                                                            <th>Horses</th>
<!--                                                            <th>Actions</th>-->
                                                    </tr>
                                            </thead>
                                             <tbody>
                                                  <? if(!empty($owners)):?>
                                                 <?php $sr=1;foreach($owners as $owner):?>
                                                    <?php if(!empty($owner['horses'])):?>
                                                        <tr class="odd gradeX">
                                                            <td class="hidden-480"><?php echo $sr++;?></td>
                                                            <td><?php echo $owner['surname'].' '.$owner['first_name'];?></td>
                                                            <td class="hidden-480"><?php echo ucwords($owner['contact_type']);?></td>
                                                            <td class="hidden-480">
                                                                        <?php if($owner['home_phone']!=''):
                                                                            echo $owner['home_phone']." / ";
                                                                        endif;?>
                                                                        <?php if($owner['work_phone']!=''):
                                                                            echo $owner['work_phone']." / ";
                                                                        endif;?>
                                                                        <?php if($owner['mobile_phone']!=''):
                                                                            echo $owner['mobile_phone'];
                                                                        endif;?>
                                                            </td>
                                                            <td>
                                                                <?php $horse_name = '';
                                                                foreach($owner['horses'] as $horse):
                                                                        $horse_name .= $horse->name;
                                                                      if($horse->horse_sex!=''):
                                                                        $horse_name .= ' ('.$horse->horse_sex.')';    
                                                                      endif;
                                                                        $horse_name .= ',<br/>';
                                                                endforeach;
                                                                echo ucwords($horse_name);
                                                                ?>
                                                            </td>
<!--                                                            <td>
                                                                 <a class="btn blue mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'id='.$owner->id)?>" title="click to view full details"><i class="icon-print icon-white"></i> Print Single Report</a>&nbsp;&nbsp;
                                                            </td>-->
                                                        </tr>
                                                      <?php endif;?>     
                                                 <?php endforeach;?>
                                            </tbody>
                                           <?php endif;?>  
                                    </table>  
                            </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
 </div>
<!-- END PAGE CONTAINER-->    