<!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span6">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h5 class="page-title" style="font-size:23px;">Owner Report</h5>
                        <small>Date: <?php echo date('d M Y'); ?></small>
                </div>
            <div class="span6 pull-right">
                <img src="assets/img/kaithlogo.png" alt="Logo"/>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row-fluid invoice">
            <table class="table table-bordered table-hover">
                <thead>
                         <tr>
                                <th class="hidden-480">Sr. No</th>
                                <th>Name</th>
                                <th class="hidden-480">Contact</th>
                                <th class="hidden-480">Phone</th>
                                <th>Horses</th>
                        </tr>
                </thead>
                 <tbody>
                      <? if(!empty($owners)):?>
                     <?php $sr=1;foreach($owners as $owner):?>
                        <?php if(!empty($owner['horses'])):?>
                            <tr class="odd gradeX">
                                <td class="hidden-480"><?php echo $sr++;?></td>
                                <td><?php echo $owner['surname'].' '.$owner['first_name'];?></td>
                                <td class="hidden-480"><?php echo ucwords($owner['contact_type']);?></td>
                                <td class="hidden-480">
                                            <?php if($owner['home_phone']!=''):
                                                echo $owner['home_phone']." / ";
                                            endif;?>
                                            <?php if($owner['work_phone']!=''):
                                                echo $owner['work_phone']." / ";
                                            endif;?>
                                            <?php if($owner['mobile_phone']!=''):
                                                echo $owner['mobile_phone'];
                                            endif;?>
                                </td>
                                <td>
                                    <?php $horse_name = '';
                                    foreach($owner['horses'] as $horse):
                                            $horse_name .= $horse->name;
                                          if($horse->horse_sex!=''):
                                            $horse_name .= ' ('.$horse->horse_sex.')';    
                                          endif;
                                            $horse_name .= ',<br/>';
                                    endforeach;
                                    echo ucwords($horse_name);
                                    ?>
                                </td>
                            </tr>
                          <?php endif;?>     
                     <?php endforeach;?>
                </tbody>
               <?php endif;?>  
        </table>  
        <?php if($print!='1'): ?>
        <div class="row-fluid hidden-print">
            <div class="clearfix" style="height:20px;"></div>
            <div class="span11 invoice-block">
                <a target="_blank" class="btn blue big pull-right" href="<?php echo make_admin_url('printownerreport', 'list', 'list', 'id='.$id.'&print=1')?>">Print <i class="icon-print icon-big"></i></a>&nbsp;&nbsp;
                <a class="btn green big pull-right" style="margin-right:20px;" href="javascript:history.back()">Back <i class="icon-print icon-backward"></i></a>
            </div>
            <div class="clearfix" style="margin-bottom:30px;"></div>
        </div>
        <?php endif; ?>
        <div class="clearfix" style="heigth:30px;"></div>
    </div>