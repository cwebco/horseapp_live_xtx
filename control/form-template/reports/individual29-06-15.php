<script type="text/javascript">
jQuery("#go").live("click",function(){
    var report = jQuery('#select2_sample1').val();
    if(report==''){
        alert('Please select the report type first.');
        return false;
    }
});
</script>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">Print Reports</h3>
                        <ul class="breadcrumb">
                                <li>
                                        <i class="icon-home"></i>
                                        <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                        <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                  <i class="icon-bar-chart"></i>
                                  <a href="<?php echo make_admin_url('reports', 'list', 'list');?>">Download Reports</a>
                                </li>
                        </ul>
                       <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left">
            <form class="form-horizontal" action="<?php echo make_admin_url('reports', 'individual', 'individual')?>" method="GET" enctype="multipart/form-data" id='filter_report'>
                <input type='hidden' name='Page' value='reports'/>
                <input type='hidden' name='action' value='individual'/>
                <input type='hidden' name='section' value='individual'/>

                <div class="control-group alert alert-success span8" style='margin-left:0px;padding-right:0px;'>
                    <div class='span2' style='margin-left: 5px;'>
                        <label class="control-label" style='float: none; text-align: left;'>From Date <span style="font-size: 12px;color: grey;">(dd/mm/yyyy)</span></label>
                        <input type='text' class="span2 upto_current_date" placeholder="From date" id="date1" name='from_date' value='<?php echo $from_date;?>'/>
                    </div>

                    <div class='span2' style='margin-left: 15px;'>
                        <label class="control-label" style='float: none; text-align: left;'>To Date <span style="font-size: 12px;color: grey;">(dd/mm/yyyy)</span></label>
                        <input type='text' class="span2 upto_current_date" placeholder="To date" id="date2" name='to_date' value='<?php echo $to_date;?>'/>
                    </div>

                    <div class='span4' style='margin-left: 15px;'>
                        <label class="control-label" style='float: none; text-align: left;margin-bottom: 3px;'>Select Report</label>
                        <div style="margin-left:-30px;">
                        <select id="select2_sample1" class="span3 select2" data-placeholder="Select" name='type' >
                            <option value=''>Select</option>
                            <option value='farrier' <?php echo ($type=='farrier')?'selected':'';?>>Farrier Report</option>
                            <option value='wormer' <?php echo ($type=='wormer')?'selected':'';?>>Wormer Report</option>
                            <option value='flu' <?php echo ($type=='flu')?'selected':'';?>>Flu Vaccinations Report</option>
                            <option value='ehv' <?php echo ($type=='ehv')?'selected':'';?>>EHV 1.4 Report</option>
                            <option value='service' <?php echo ($type=='service')?'selected':'';?>>Last Service Report</option>
                            <option value='paddock' <?php echo ($type=='paddock')?'selected':'';?>>Paddock Report</option>
                        </select>
                        </div>
                        <input type='submit' name='go' value='Go' id="go" class='btn blue' style="margin-left:10px;"/>
                    </div>
                    <div class="clearfix"></div>
                    &nbsp;&nbsp;<span style="color:red;">Don't Select dates if you want to generate the complete report.</span>
                </div>
            </form>
        </div>
        <div class="clearfix"></div>
        <?php 
        if(isset($_GET['go']) && $_GET['go']=='Go'):
           include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/reports/report.php');
        endif;
      ?>
 </div>
<!-- END PAGE CONTAINER-->    