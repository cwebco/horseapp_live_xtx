
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid" style="margin-bottom: 00px;">
            <div class="span6">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title" style="margin-bottom: 5px;"><?php echo ucfirst($horse->name); ?></h3>
                    <small><strong>Departed Date : <?php echo ($horse->departed!='' && $horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';?></strong></small>
            </div>
            <div class="span6 pull-right" style="margin-bottom: 10px;text-align: center;">
                <img src="assets/img/keithlogo_big.jpg" alt="Logo"/>
                <address style="margin-top: 10px;margin-bottom: 0px;font-size: 12px;">
                        MILL FARM<br/>
                        MILL LANE<br/>
                        FINCHINGFIELD<br/>
                        ESSEX CM7 4PQ<br/>
                        Eileen Mob: 0774 875 5548<br/>
                        Keith Mob: 0774 875 5553<br/>
                        Email: info@keithharte.com<br/>
                        www.keithharte.com
                </address>
            </div>
        </div>
        <hr style="border-top: 1px solid #000;-webkit-margin-before: 0;"/>
        <!-- END PAGE HEADER-->
        <div class="row-fluid invoice">
                <div class="row-fluid invoice-logo">
                    <div class="profile-classic row-fluid">
                        <div class="span6">
                                <ul class="unstyled span12">
                                        <?php if($horse->sex=='stallion'):  ?>
                                        <li><strong>Status : </strong><?php echo ucfirst($horse->horse_status);?></li>
                                        <?php endif; ?>
                                        <li><strong>D.O.B. : </strong><?php echo ($horse->born!='' && $horse->born!='0000-00-00')?date('d/m/Y',  strtotime($horse->born)):'';?></li>
                                        <li><strong>Color : </strong><?php echo ucfirst($horse->color);?></li>
                                        <li><strong>Sex : </strong><?php echo ($horse->sex=='stallion')?'Horse'.' - '.ucfirst($horse->horse_sex):ucfirst($horse->sex);?></li>
                                        <li><strong>Passport Number : </strong><?php echo $horse->passport_number;?></li>
                                        <li><strong>Microchip Number : </strong><?php echo $horse->microchip;?></li>
                                </ul>
                        </div>
                        <div class="span6">
                                <ul class="unstyled span12">
                                    <li><strong>Sire : </strong><?php echo ucfirst($horse->sire);?></li>
                                    <li><strong>Dam : </strong><?php echo ucfirst($horse->dam);?></li>
                                    <li><strong>Grandsire : </strong><?php echo ucfirst($horse->grandsire);?></li>
                                    <li><strong>Arrived Date : </strong><?php echo ($horse->arrived!='' && $horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';?></li>
                                    <li><strong>Owner Name : </strong><?php echo str_replace(array('(',')')," ",$horse->owner_name);?></li>
                                    <!--<li><strong>Rate agreed to keep (&euro;): </strong><?php //echo $horse->rate_to_keep;?></li>
                                    <?php //if($horse->sex=='mare'):  ?>
                                    <li><strong>Rate agreed foaling fee (&euro;): </strong><?php //echo $horse->rate_foaling_fee;?></li>
                                    <?php //endif; ?>-->
                                </ul>
                        </div>
                    </div>
                </div>
                <hr />
                  <div class="row-fluid invoice">
                      <div class="profile-classic row-fluid">
                          <div class="span6">
                                <ul class="unstyled span12">
                                    <?php //print_r($last_log); ?>
                                    <?php foreach($last_log as $kk=>$vv): ?>
                                        <?php if($vv['type']=='farrier'): ?>
                                            <li><strong>Last Farrier : </strong><?php echo date('d-m-Y',strtotime($vv['date']));?></li>
                                        <?php elseif($vv['type']=='wormer'): ?>
                                            <li><strong>Last Wormer  : </strong><?php echo date('d-m-Y',strtotime($vv['date']))." ".$vv['value'];?></li>
                                        <?php elseif($vv['type']=='flu'): ?>
                                            <li><strong>Last Flu Vaccination  : </strong><?php echo ucfirst($vv['value']).' / '.date('d-m-Y',strtotime($vv['date']));?></li>
                                        <?php elseif($vv['type']=='ehv'): ?>
                                            <li><strong>Last EHV 1.4 : </strong><?php echo ucfirst($vv['value']).' / '.date('d-m-Y',strtotime($vv['date']));?></li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                          </div>
                          <div class="span6">
                                <ul class="unstyled span12">
                                    <li><strong>Departure Notes : </strong><?php echo $horse->departure_notes;?></li>
                                </ul>
                          </div>
                      </div>
                 </div>
                 <div class="clearfix"></div>
                <?php if($horse->sex=='mare'): ?>
                    <?php if($QueryObj1->GetNumRows()!=0):?>
                     <hr />
                             <?php $sr=1; while($season=$QueryObj1->GetObjectFromRecord()):?>
                                <div class="row-fluid table_outer"  style="margin-bottom: 0px; border:1px solid black;width: 94%;">
                                    <div class="span6">
                                        <h4>Season <?php echo $season->season; ?> </h4>
                                        <ul class="pricing-content unstyled">
                                                <li><i class="icon-circle-arrow-right"></i><span>Status: </span><?php echo $season->mare_status; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Type: </span><?php echo $season->mare_type; ?></li>
                                                <?php if($season->mare_status=='In Foal'): ?>
                                                    <li><i class="icon-circle-arrow-right"></i><span>In foal to: </span><?php echo $season->in_foal_to; ?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Due date: </span><?php echo ($season->due_date!='' && $season->due_date!='0000-00-00')?date('d/m/Y',  strtotime($season->due_date)):'';?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Foaling date: </span><?php echo ($season->foaling_date!='' && $season->foaling_date!='0000-00-00')?date('d/m/Y',  strtotime($season->foaling_date)):'';?></li>
                                                    <!--<li><i class="icon-circle-arrow-right"></i><span>Foaling location:</span><?php echo $season->foaling_location; ?></li>-->
                                                    <?php if($season->foal_details!=''): ?>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Foal details:</span><?php echo $season->foal_details; ?></li>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                        </ul>
                                    </div>
                                   
                                    <div class="span6">
                                        <ul class="pricing-content unstyled">
                                             <li><i class="icon-circle-arrow-right"></i><span>Covering sire: </span><?php echo $season->covering_sire; ?></li>
                                             <li><i class="icon-circle-arrow-right"></i><span>Last service: </span><?php echo ($season->last_service!='' && $season->last_service!='0000-00-00')?date('d/m/Y',  strtotime($season->last_service)):'';?></li>
                                             <li><i class="icon-circle-arrow-right"></i><span>Current status: </span><?php echo $season->current_status; ?></li>       
                                             <li><i class="icon-circle-arrow-right"></i><span>Last scan date: </span><?php echo ($season->last_scan!='' && $season->last_scan!='0000-00-00')?date('d/m/Y',  strtotime($season->last_scan)):'';?></li>
                                             <li><i class="icon-circle-arrow-right"></i><span>Last Scan:</span><?php echo $season->last_scan_text;?></li>
                                        </ul>
                                    </div>
                                    
                                    <?php if($sr%3==0): ?>
                                        <div class="clearfix"></div>
                                    <?php endif; ?>
                                </div>
                    <?php $sr++; endwhile; ?>
                    <?php endif; ?>
                <?php endif; ?>
        </div>
