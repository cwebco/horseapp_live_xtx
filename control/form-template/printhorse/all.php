
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span6">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title"><?php echo ucfirst($horse->name); ?></h3>
                </div>
            <div class="span6 pull-right">
                <img src="assets/img/kaithlogo.png" alt="Logo"/>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row-fluid invoice">
                <div class="row-fluid invoice-logo">
                    <div class="profile-classic row-fluid">
                        <div class="span6">
                                <ul class="unstyled span12">
                                        <li><strong>Name : </strong><?php echo ucfirst($horse->name);?></li>
                                        <?php if($horse->sex=='stallion'):  ?>
                                        <li><strong>Status : </strong><?php echo ucfirst($horse->horse_status);?></li>
                                        <?php endif; ?>
                                        <li><strong>D.O.B. : </strong><?php echo ($horse->born!='0000-00-00')?date('d/m/Y',  strtotime($horse->born)):'';?></li>
                                        <li><strong>Color : </strong><?php echo ucfirst($horse->color);?></li>
                                        <li><strong>Sex : </strong><?php echo ($horse->sex=='stallion')?'Horse':ucfirst($horse->sex);?></li>
                                        <li><strong>Passport Number : </strong><?php echo $horse->passport_number;?></li>
                                </ul>
                        </div>
                        <div class="span6">
                                <ul class="unstyled span12">
                                    <li><strong>Sire : </strong><?php echo ucfirst($horse->sire);?></li>
                                    <li><strong>Dam : </strong><?php echo ucfirst($horse->dam);?></li>
                                    <li><strong>Grandsire : </strong><?php echo ucfirst($horse->grandsire);?></li>
                                    <li><strong>Arrived Date : </strong><?php echo ($horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';?></li>
                                    <li><strong>Departed Date : </strong><?php echo ($horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';?></li>
                                </ul>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
                        <div class="span6">
                            <div class="profile-classic row-fluid">
                                <h4>Owner Details:</h4>
                                <ul class="unstyled">
                                        <li><strong>Owner Name: </strong><?php echo str_replace(array('(',')')," ",$horse->owner_name);?></li>
                                        <?php// if(!empty ($owner)): ?>
<!--                                        <li><strong>Contact Type: </strong><?php echo $owner->contact_type;?></li>
                                        <li><strong>Email Id: </strong><?php echo $owner->primary_email_id;?></li>
                                        <li><strong>Contact Home: </strong><?php echo $owner->home_phone;?></li>
                                        <li><strong>Mobile No.: </strong><?php echo $owner->mobile_phone;?></li>-->
                                        <?php //endif; ?>
                                        <li><strong>Rate agreed to keep (&euro;): </strong><?php echo $horse->rate_to_keep;?></li>
                                        <?php if($horse->sex=='mare'):  ?>
                                        <li><strong>Rate agreed foaling fee (&euro;): </strong><?php echo $horse->rate_foaling_fee;?></li>
                                        <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                </div>
                <?php if($horse->sex=='mare'): ?>
                <hr />
                <?php if($QueryObj1->GetNumRows()!=0):?>
                         <?php $sr=1; while($season=$QueryObj1->GetObjectFromRecord()):?>
                            <?php $margin = ($sr%4==0)?'style="margin-left:0px;"':''; ?>
                            <div class="row-fluid">
                                <div class="span4">
                                    <h4>Season <?php echo $season->season; ?> </h4>
                                    <ul class="pricing-content unstyled">
                                            <li><i class="icon-circle-arrow-right"></i><span>Status :</span><?php echo $season->mare_status; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Type: </span><?php echo $season->mare_type; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Covering sire:</span><?php echo $season->covering_sire; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Last service:</span><?php echo date('d/m/Y',  strtotime($season->last_service));?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Current status:</span><?php echo $season->current_status; ?></li>
                                             <li><i class="icon-circle-arrow-right"></i><span>Last scan date:</span><?php echo date('d/m/Y',  strtotime($season->last_scan));?></li>
                                            <?php if($season->mare_status=='In Foal'): ?>
                                            <li><i class="icon-circle-arrow-right"></i><span>In foal to:</span><?php echo $season->in_foal_to; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Due date:</span><?php echo date('d/m/Y',  strtotime($season->due_date));?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Foaling date:</span><?php echo date('d/m/Y',  strtotime($season->foaling_date));?></li>
                                            <!--<li><i class="icon-circle-arrow-right"></i><span>Foaling location:</span><?php echo $season->foaling_location; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Foal details:</span><?php echo $season->foal_details; ?></li>-->
                                            <?php endif; ?>
                                    </ul>
                                </div>
                                <?php if($sr%3==0): ?>
                                    <div class="clearfix"></div>
                                <?php endif; ?>
                            </div>
                <?php $sr++; endwhile; ?>
                <?php endif; ?>
                <?php endif; ?>
                <div class="row-fluid" style="height: 30px;"></div>
        </div>