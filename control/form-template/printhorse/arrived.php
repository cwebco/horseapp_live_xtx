<?php
$query = new horse_notes;
$horse_notes = $query->all($id);
?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span6">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title" style="margin-bottom: 5px;"><?php echo ucfirst($horse->name); ?></h3>
        <small><span style="font-weight:bold;">Arrived Date : <?php echo ($horse->arrived != '' && $horse->arrived != '0000-00-00') ? date('d/m/Y', strtotime($horse->arrived)) : ''; ?></span></small>
    </div>
    <div class="span6 pull-right" style="margin-bottom: 10px;">
        <img src="assets/img/kaithlogo.png" alt="Logo"/>
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row-fluid invoice">
    <div class="row-fluid invoice-logo">
        <div class="profile-classic row-fluid">
            <div class="span4">
                <ul class="unstyled span12">
<!--                                        <li><span style="font-weight:bold;">Name : </span><?php echo ucfirst($horse->name); ?></li>-->
                    <?php if ($horse->sex == 'stallion'): ?>
                        <li><span style="font-weight:bold;">Status : </span><?php echo ucfirst($horse->horse_status); ?></li>
                    <?php endif; ?>
                    <li><span style="font-weight:bold;">D.O.B. : </span><?php echo ($horse->born != '' && $horse->born != '0000-00-00') ? date('d/m/Y', strtotime($horse->born)) : ''; ?></li>
                    <li><span style="font-weight:bold;">Color : </span><?php echo ucfirst($horse->color); ?></li>
                    <li><span style="font-weight:bold;">Sex : </span><?php echo ($horse->sex == 'stallion') ? 'Horse' . ' - ' . ucfirst($horse->horse_sex) : ucfirst($horse->sex); ?></li>
                    <li><span style="font-weight:bold;">Passport Number : </span><?php echo $horse->passport_number; ?></li>
                    <li><span style="font-weight:bold;">Microchip Number : </span><?php echo $horse->microchip; ?></li>
                </ul>
            </div>
            <div class="span4">
                <ul class="unstyled span12">
                    <li><span style="font-weight:bold;">Sire : </span><?php echo ucfirst($horse->sire); ?></li>
                    <li><span style="font-weight:bold;">Dam : </span><?php echo ucfirst($horse->dam); ?></li>
                    <li><span style="font-weight:bold;">Grandsire : </span><?php echo ucfirst($horse->grandsire); ?></li>
                    <li><span style="font-weight:bold;">Arrived Date : </span><?php echo ($horse->arrived != '' && $horse->arrived != '0000-00-00') ? date('d/m/Y', strtotime($horse->arrived)) : ''; ?></li>
                <!--<li><span style="font-weight:bold;">Departed Date : </span><?php echo ($horse->departed != '' && $horse->departed != '0000-00-00') ? date('d/m/Y', strtotime($horse->departed)) : ''; ?></li>-->
                </ul>
            </div>
            <div class="span4">
                <ul class="unstyled">
                    <li><span style="font-weight:bold;">Owner Name: </span><?php echo str_replace(array('(', ')'), " ", $horse->owner_name); ?></li>
                    <li><span style="font-weight:bold;">Rate agreed to keep (&euro;): </span><?php echo $horse->rate_to_keep; ?></li>
                    <?php if ($horse->sex == 'mare'): ?>
                        <li><span style="font-weight:bold;">Rate agreed foaling fee (&euro;): </span><?php echo $horse->rate_foaling_fee; ?></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <hr />
    <div class="row-fluid"> 
        <div class="span4">
            <!-- BEGIN CONDENSED TABLE PORTLET-->
            <div class="table_outer" style="border: 1px solid green;">
                <div class="title">Farrier Information</div>
                <div class="body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($horse_log) > 0): ?>
                                <?php
                                $sr = 1;
                                foreach ($horse_log as $fk => $fv):
                                    ?>
        <?php if ($fv['type'] == 'farrier'): ?>
                                        <tr>
                                            <td><?php echo $sr; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endif;
                                    ?>
                                <?php endforeach; ?>

                                <?php if ($sr == 1): ?>
                                    <tr><td colspan="2">No Record Found.</td></tr>
                                <?php endif; ?>  
                            <?php else: ?>
                                <tr><td colspan="2">No Record Found.</td></tr> 
<?php endif; ?>      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>	

        <div class="span4">
            <!-- BEGIN CONDENSED TABLE PORTLET-->
            <div class="table_outer"  style="border: 1px solid blue;">
                <div class="title">Wormer Information</div>
                <div class="body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($horse_log) > 0): ?>
                                <?php
                                $sr = 1;
                                foreach ($horse_log as $fk => $fv):
                                    ?>
        <?php if ($fv['type'] == 'wormer'): ?>
                                        <tr>
                                            <td><?php echo $sr; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                            <td><?php echo $fv['value']; ?></td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endif;
                                    ?>
                                <?php endforeach; ?>

                                <?php if ($sr == 1): ?>
                                    <tr><td colspan="3">No Record Found.</td></tr>
    <?php endif; ?>  
<?php else: ?>
                                <tr><td colspan="3">No Record Found.</td></tr> 
<?php endif; ?>      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>	

        <div class="span4">
            <!-- BEGIN CONDENSED TABLE PORTLET-->
            <div class="table_outer"  style="border: 1px solid purple;">
                <div class="title">Flu Vaccination</div>
                <div class="body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($horse_log) > 0): ?>
    <?php
    $sr = 1;
    foreach ($horse_log as $fk => $fv):
        ?>
                                    <?php if ($fv['type'] == 'flu'): ?>
                                        <tr>
                                            <td><?php echo $sr; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                            <td><?php echo $fv['value']; ?></td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endif;
                                    ?>
                                <?php endforeach; ?>
    <?php if ($sr == 1): ?>
                                    <tr><td colspan="3">No Record Found.</td></tr>
    <?php endif; ?>  
<?php else: ?>
                                <tr><td colspan="3">No Record Found.</td></tr> 
<?php endif; ?>     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span4">
            <!-- BEGIN CONDENSED TABLE PORTLET-->
            <div class="table_outer"  style="border: 1px solid red;">
                <div class="title">EHV 1.4</div>
                <div class="body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
<?php if (count($horse_log) > 0): ?>
    <?php
    $sr = 1;
    foreach ($horse_log as $fk => $fv):
        ?>
                                    <?php if ($fv['type'] == 'ehv'): ?>
                                        <tr>
                                            <td><?php echo $sr; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                            <td><?php echo $fv['value']; ?></td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endif;
                                    ?>
    <?php endforeach; ?>
    <?php if ($sr == 1): ?>
                                    <tr><td colspan="3">No Record Found.</td></tr>
    <?php endif; ?>  
<?php else: ?>
                                <tr><td colspan="3">No Record Found.</td></tr> 
<?php endif; ?>     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
<!--        <div class="span5">
             BEGIN CONDENSED TABLE PORTLET
            <div class="table_outer"  style="border: 1px solid red;">
                <div class="title">Notes Information</div>
                <div class="body">
<?php if ($horse_notes) { ?>
                        <table class="table table-condensed table-hover dataTable" id="sample_2">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($horse_notes as $key => $horse_note) { ?>
                                    <tr>
                                        <td><?php echo $key += 1 ?></td>
                                        <td><?php echo $horse_note['name'] ?></td>
                                        <td><?php echo $horse_note['note'] ?></td>
                                                            <td>
                                            <a href="<?php echo make_admin_url('stallion', 'delete_notes', 'delete_notes&id=' . $horse_note['id'] . '&b_id=' . $id); ?>" class="btn red mini">Delete</a>
                                        </td>
                                    </tr>
    <?php } ?>
                            </tbody>
                        </table>
<?php } else { ?>
                        No record found..!
<?php } ?>
                </div>
            </div>
        </div>-->
        <!--<div class="span7">
<?php //if(count($paddock_log)>0):   ?>
           <div class="table_outer"  style="border: 1px solid violet;">
                <div class="title">Paddock Data</div>
                <div class="body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Paddock</th>
                                <th>Move</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
        <?php
        $sr = 1;
        foreach ($paddock_log as $pk => $pv):
            ?>
                                                <tr>
                                                    <td><?php echo $sr; ?> <?php echo ($current->id == $pv['id']) ? '<i class="icon-star"></i>' : ''; ?></td>
                                                    <td><?php echo $pv['paddock_title']; ?></td>
                                                    <td><?php echo ucfirst($pv['move']); ?></td>
                                                    <td><?php echo date('d/m/Y', strtotime($pv['date'])); ?></td>
                                                </tr>
        <?php $sr++; ?>
    <?php endforeach; ?>
                        </tbody>
                </table>
                </div>
           </div>
<?php //endif;    ?>
           </div>-->
    </div>
<?php if ($horse->sex == 'mare'): ?>
    <?php if ($QueryObj1->GetNumRows() != 0): ?>
            <hr />
                        <?php
                        $sr = 1;
                        while ($season = $QueryObj1->GetObjectFromRecord()):
                            ?>
                <div class="row-fluid table_outer"  style="border:1px solid black;width: 94%;">
                    <div class="span6">
                        <h4>Season <?php echo $season->season; ?> </h4>
                        <ul class="pricing-content unstyled">
                            <li><i class="icon-circle-arrow-right"></i><span>Status :</span><?php echo $season->mare_status; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Type: </span><?php echo $season->mare_type; ?></li>
            <?php if ($season->mare_status == 'In Foal'): ?> 
                                <li><i class="icon-circle-arrow-right"></i><span>In foal to: </span><?php echo $season->in_foal_to; ?></li>
                                <li><i class="icon-circle-arrow-right"></i><span>Due date: </span><?php echo ($season->due_date != '' && $season->due_date != '0000-00-00') ? date('d/m/Y', strtotime($season->due_date)) : ''; ?></li>
                                <li><i class="icon-circle-arrow-right"></i><span>Foaling date: </span><?php echo ($season->foaling_date != '' && $season->foaling_date != '0000-00-00') ? date('d/m/Y', strtotime($season->foaling_date)) : ''; ?></li>
                                <!--<li><i class="icon-circle-arrow-right"></i><span>Foaling location:</span><?php echo $season->foaling_location; ?></li>-->
            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="span6">
                        <ul class="pricing-content unstyled">
                            <li><i class="icon-circle-arrow-right"></i><span>Covering sire: </span><?php echo $season->covering_sire; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Last service: </span><?php echo ($season->last_service != '' && $season->last_service != '0000-00-00') ? date('d/m/Y', strtotime($season->last_service)) : ''; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Current status: </span><?php echo $season->current_status; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Last scan date: </span><?php echo ($season->last_scan != '' && $season->last_scan != '0000-00-00') ? date('d/m/Y', strtotime($season->last_scan)) : ''; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Last Scan:</span><?php echo $season->last_scan_text; ?></li>
                            <li><i class="icon-circle-arrow-right"></i><span>Foal details:</span><?php echo $season->foal_details; ?></li>
                        </ul>
                    </div>

            <?php if ($sr % 3 == 0): ?>
                        <div class="clearfix"></div>
            <?php endif; ?>
                </div>
            <?php
            $sr++;
        endwhile;
        ?>
    <?php endif; ?>
<?php endif; ?>
</div>
