
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?php echo ucfirst($setting_type); ?> Settings
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li><?php echo ucfirst($setting_type); ?> Settings</li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
                                                  <!-- / Box -->
                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                     <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                
                                                                    <div class="caption">
                                                                        <?php if(isset($setting_type) && $setting_type=='email'):?>
                                                                            <i class="icon-envelope-alt"></i>
                                                                        <?php else: ?>
                                                                            <i class="icon-cogs"></i>
                                                                        <?php endif; ?>
                                                                    <?php echo ucfirst($sname);?> Settings </div>
                                                                    <div class="tools">
                                                                            <a href="javascript:;" class="collapse"></a>


                                                                    </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <form class="form-horizontal" action="<?php echo make_admin_url('setting', 'update', 'list');?>" method="POST" enctype="multipart/form-data" id="validation">
                                                                 <?php
                                                            
                                                                    foreach($ob as $kk=>$vv):
                                                                 ?>
                                                                         <div class="control-group">
                                                                                <label class="control-label"><?php echo ucfirst($vv['title']);?>:</label>
                                                                                <div class="controls">
                                                                                    <?php echo get_setting_control($vv['id'], $vv['type'], $vv['value']);?>
                                                                                </div>    
                                                                        </div>
                                                                     <?php if($vv['key']=='FDP' && $vv['value']):?>
                                                                         <div class="control-group">
                                                                                <label class="control-label">Setup Facebook Account(only if not set):</label>
                                                                                <div class="controls">
                                                                                   <a style="color:#0066CC" target="_blank" href="<?php echo DIR_WS_SITE?>admin/facebook/get-code.php">Click here to setup FB Account</a>
                                                                       
                                                                                </div>
                                                                         </div>       
                                                                    <?php endif;?>
                                                                    <?php if($vv['key']=='TDP' && $vv['value']):?>
                                                                        <div class="control-group">
                                                                                <label class="control-label">Setup Twitter Account(only if not set):</label>
                                                                                <div class="controls">
                                                                                 <a  style="color:#0066CC"target="_blank" href="<?php echo DIR_WS_SITE?>admin/twitter/connect.php">Click here to setup Twitter Account</a>
                                                                                 </div>
                                                                        </div>        
                                                                    <?php endif;?>

                                                              <?php endforeach;?>   
                                                                
                                                                
                                                           
                                                                <div class="form-actions">
                                                                    
                                                                         <input  type="hidden" name="sname1" value="<?php echo $sname?>"/>
                                                                         <input  type="hidden" name="setting_type" value="<?php echo $setting_type?>"/>
                                                                         
                                                                         <input class="btn blue" type="submit" name="Submit" value="Submit" tabindex="7" /> 
                                                                         <a href="<?php echo make_admin_url('setting', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>

                                                                </div>

                                                                </form>
                                                    </div>
                                                </div>                             
					</div>
				</div>

            <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    





