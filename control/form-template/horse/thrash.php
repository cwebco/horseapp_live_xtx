<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Archive</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Archive</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url($Page, 'list', 'list'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-backward"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Back to Listing
                    </div>
                </div>
            </a> 
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-heart-empty"></i>Manage Horse</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th class="hidden-480">Sr. No</th>
                                <th>Name</th>
                                <th class="hidden-480">Sex</th>
                                <th class="hidden-480">Sire</th>
                                <th class="hidden-480">Status</th>
                                <!---<th>Arrived Date</th>--->
                                <th>Departed Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($QueryObj->GetNumRows() != 0): ?>
                                <?php
                                $sr = 1;
                                while ($horse = $QueryObj->GetObjectFromRecord()):
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="hidden-480"><?php echo $sr++; ?></td>
                                        <td><?php echo $horse->name ?></td>
                                        <?php if ($horse->sex == 'stallion'): ?>
                                            <td>Horse</td>
                                        <?php else: ?>
                                            <td><?php echo ucfirst($horse->sex); ?></td>
        <?php endif; ?>
                                        <td class="hidden-480"><?php echo $horse->sire ?></td>
                                        <td class="hidden-480"><?php echo str_replace(array('(', ')'), " ", $horse->owner_name); ?></td>
                                        <td><?php echo ($horse->departed != '' && $horse->departed != '0000-00-00') ? date('d/m/Y', strtotime($horse->departed)) : ''; ?></td>
                                        <td>
                                            <a class="btn green mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'redirect=horse&id=' . $horse->id) ?>" class="tipTop smallbtn" title="click here to view this record"><i class="icon-share icon-white"></i> </a>
                                            <a class="btn green mini tooltips" href="<?php echo make_admin_url('horse', 'restore', 'restore', 'id=' . $horse->id) ?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>
                                            <a class="btn green mini tooltips" href="<?php echo make_admin_url('horse', 'permanent_delete', 'thrash', 'id=' . $horse->id) ?>" class="tipTop smallbtn" title="click here to delete this record permanently" onclick="return confirm('Are you sure? All records of this horse will be permanently deleted.');"><i class="icon-remove icon-white"></i> </a>
                                        </td>
                                    </tr>
                            <?php endwhile; ?>
                            </tbody>
<?php endif; ?>  
                    </table>  
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    

