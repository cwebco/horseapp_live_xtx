<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                <?php echo ucfirst($horse->name); ?>
            </h3>
        </div>
    </div>
    <?php 
/* display message */
display_message(1);
$error_obj->errorShow();
?>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/horse/shortcut.php'); ?>
    <div class="clearfix"></div>
    <div class="row-fluid profile">
        <!--BEGIN TABS-->
        <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1_1" data-toggle="tab">Profile</a></li>
                <!--<li><a href="#tab_1_2" data-toggle="tab">Vaccinations and Paddock</a></li>-->
                <li><a href="#tab_1_2" data-toggle="tab">Vaccinations</a></li>
                <li><a href="#tab_1_3" data-toggle="tab">Owner/Rates</a></li>
                <?php if ($horse->sex == 'mare'): ?>
                    <li><a href="#tab_1_4" data-toggle="tab">Season Info</a></li>
                <?php endif; ?>
                <li><a href="#tab_1_5" data-toggle="tab">Documents</a></li>
                <li><a href="#tab_1_6" data-toggle="tab">Notes</a></li>
                <li><a href="#tab_1_7" data-toggle="tab">Invoice Notes</a></li>

            </ul>
            <div class="tab-content">
                <!--tab1-->
                <div class="tab-pane profile-classic row-fluid active" id="tab_1_1">
                    <div class="span4">
                        <ul class="unstyled profile-nav" style="margin-top:10px;">
                            <?php if ($horse->image != ''): ?>
                                <?php if (file_exists(DIR_FS_SITE_UPLOAD . '/photo/horse/big/' . $horse->image)): ?>
                                    <img src="<?php echo DIR_WS_SITE_UPLOAD . '/photo/horse/big/' . $horse->image ?>" alt="<?php echo $horse->image; ?>" />
                                <?php else: ?>
                                    <img src="assets/img/noimage_horse.png" alt="owner"/>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="assets/img/noimage_horse.png" alt="owner"/>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="span8">
			<div class="pull-right">
                            <!--<a class="btn mini blue" href="<?php echo make_admin_url('compose', 'insert','insert&id='.$horse->id); ?>">Send Message</a>&nbsp;&nbsp;-->
                        
                        
                            <a class="btn mini blue" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id); ?>"><i class="icon-edit"></i> Edit</a>
                        </div>
                        <div class="clearfix"></div>
                        <ul class="unstyled">
                            <li><span>Name:</span><?php echo ucfirst($horse->name); ?></li>
                            <?php if ($horse->sex == 'stallion'): ?>
                                <li><span>Status:</span><?php echo ucfirst($horse->horse_status); ?></li>
                            <?php endif; ?>
                            <li><span>D.O.B.:</span><?php echo ($horse->born != '' && $horse->born != '0000-00-00') ? date('d/m/Y', strtotime($horse->born)) : ''; ?></li>
                            <li><span>Color:</span><?php echo ucfirst($horse->color); ?></li>
                            <?php if ($horse->sex == 'stallion'): ?>
                                <li><span>Sex:</span> Horse - <?php echo $horse->horse_sex; ?></li>
                            <?php else: ?>
                                <li><span>Sex:</span><?php echo ucfirst($horse->sex); ?></li>
                            <?php endif; ?>
                            <li><span>Sire:</span><?php echo ucfirst($horse->sire); ?></li>
                            <li><span>Dam:</span><?php echo ucfirst($horse->dam); ?></li>
                            <li><span>Grandsire:</span><?php echo ucfirst($horse->grandsire); ?></li>
                            <li><span>Passport Number:</span><?php echo $horse->passport_number; ?></li>
                            <li><span>Microchip Number:</span><?php echo $horse->microchip; ?></li>
                            <li><span>Arrived:</span><?php echo ($horse->arrived != '' && $horse->arrived != '0000-00-00') ? date('d/m/Y', strtotime($horse->arrived)) : ''; ?></li>
                            <li><span>Departed:</span><?php echo ($horse->departed != '' && $horse->departed != '0000-00-00') ? date('d/m/Y', strtotime($horse->departed)) : ''; ?></li>
                        </ul>
                    </div>
                </div>
                <!--tab2--> 
                <div class="tab-pane profile-classic row-fluid" id="tab_1_2">
                    <div class="row-fluid"> 
                        <div class="span4">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Farrier Information</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id) . '#tab_1_2'; ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (count($horse_log) > 0): ?>
                                                <?php
                                                $sr = 1;
                                                foreach ($horse_log as $fk => $fv):
                                                    ?>
                                                    <?php if ($fv['type'] == 'farrier'): ?>
                                                        <tr>
                                                            <td><?php echo $sr; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endif;
                                                    ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>	
                        <div class="span4">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">Wormer Information</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id) . '#tab_1_2'; ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (count($horse_log) > 0): ?>
                                                <?php
                                                $sr = 1;
                                                foreach ($horse_log as $fk => $fv):
                                                    ?>
                                                    <?php if ($fv['type'] == 'wormer'): ?>
                                                        <tr>
                                                            <td><?php echo $sr; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                                            <td><?php echo $fv['value']; ?></td>
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endif;
                                                    ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>	
                        <div class="span4">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">Flu Vaccination</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id) . '#tab_1_3'; ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (count($horse_log) > 0): ?>
                                                <?php
                                                $sr = 1;
                                                foreach ($horse_log as $fk => $fv):
                                                    ?>
                                                    <?php if ($fv['type'] == 'flu'): ?>
                                                        <tr>
                                                            <td><?php echo $sr; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                                            <td><?php echo $fv['value']; ?></td>
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endif;
                                                    ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row-fluid">
                        <div class="span5">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">EHV 1.4</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id) . '#tab_1_3'; ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (count($horse_log) > 0): ?>
                                                <?php
                                                $sr = 1;
                                                foreach ($horse_log as $fk => $fv):
                                                    ?>
                                                    <?php if ($fv['type'] == 'ehv'): ?>
                                                        <tr>
                                                            <td><?php echo $sr; ?></td>
                                                            <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
                                                            <td><?php echo $fv['value']; ?></td>
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endif;
                                                    ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="span7">
                                                    <div class="portlet box yellow">
                                                        <div class="portlet-title">
                                                            <div class="caption">Paddock Data</div>
                                                            <div class="tools" style="margin-top: 0px;">
                                                                <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id) . '#tab_1_4'; ?>">
                                                                    <i class="icon-edit"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <table class="table table-condensed table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Paddock</th>
                                                                        <th>Move</th>
                                                                        <th>Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                        <?php if (count($paddock_log) > 0): ?>
                            <?php
                            $sr = 1;
                            foreach ($paddock_log as $pk => $pv):
                                ?>
                                                                                                                                                                            <tr>
                                                                                                                                                                                <td><?php echo $sr; ?> <?php echo ($current->id == $pv['id']) ? '<i class="icon-star"></i>' : ''; ?></td>
                                                                                                                                                                                <td><?php echo $pv['paddock_title']; ?></td>
                                                                                                                                                                                <td><?php echo ucfirst($pv['move']); ?></td>
                                                                                                                                                                                <td><?php echo date('d/m/Y', strtotime($pv['date'])); ?></td>
                                                                                                                                                                            </tr>
                                <?php $sr++; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
                <!--tab3--> 
                <div class="tab-pane profile-classic row-fluid" id="tab_1_3">
                    <div class="span4" style="margin-top:10px;">
                        <img src="assets/img/owner.jpg" alt="owner"/>
                    </div>
                    <div class="span8">
                        <div class="pull-right" style="margin-top:-25px;">
                            <a class="btn mini blue" href="<?php echo make_admin_url($horse->sex, 'update', 'update', 'id=' . $horse->id); ?>"><i class="icon-edit"></i> Edit</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row-fluid">
                            <?php if (!empty($owners)): ?>
                                <?php
                                $sr = 1;
                                foreach ($owners as $owner):
                                    ?>
                                    <div class="span5" style="float: left;">
                                        <ul class="unstyled">
                                            <li><span>Owner Name:</span><?php echo $owner->surname . ' ' . $owner->first_name; ?></li>    
                                            <li><span>Contact Type:</span><?php echo $owner->contact_type; ?></li>
                                            <li><span>Email Id:</span><?php echo $owner->primary_email_id; ?></li>
                                            <li><span>Contact Home:</span><?php echo $owner->home_phone; ?></li>
                                            <li><span>Mobile No.:</span><?php echo $owner->mobile_phone; ?></li>
                                            <li><span>Address:</span><?php echo $owner->address1 . ', ' . $owner->address2 . ', ' . $owner->address3; ?></li>
                                            <li><span>City:</span><?php echo $owner->city; ?></li>
                                        </ul>
                                    </div>
                                    <?php echo ($sr % 2 == 0) ? '</div><hr/><div class="clearfix"></div><div class="row-fluid">' : ''; ?>
                                    <?php
                                    $sr++;
                                endforeach;
                                ?>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                            <ul class="unstyled">
                                <li><span>Rate agreed to keep (&euro;):</span><?php echo $horse->rate_to_keep; ?></li>
                                <?php if ($horse->sex == 'mare'): ?>
                                    <li><span>Rate agreed foaling fee (&euro;):</span><?php echo $horse->rate_foaling_fee; ?></li>
                                <?php endif; ?>
                            </ul>

                        </div>
                    </div>
                </div>   
                <!--tab4--> 
                <?php if ($horse->sex == 'mare'): ?>
                    <div class="tab-pane profile-classic row-fluid" id="tab_1_4">
                        <div class="span12">
                            <div class="pull-right" style="margin-top:-25px;">
                                <a class="btn mini blue" href="<?php echo make_admin_url('mare', 'update2', 'update2', 'id=' . $horse->id); ?>"><i class="icon-edit"></i> Edit</a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row-fluid">
                                <?php if ($QueryObj1->GetNumRows() != 0): ?>
                                    <?php
                                    $sr = 1;
                                    while ($season = $QueryObj1->GetObjectFromRecord()):
                                        ?>
                                        <?php $margin = ($sr % 5 == 0) ? 'style="margin-left:0px;"' : ''; ?>
                                        <div class="span3 pricing pricing-active hover-effect" <?php echo $margin; ?>>
                                            <div class="pricing-head pricing-head-active">
                                                <h3>Season <?php echo $season->season; ?> </h3>
                                            </div>
                                            <ul class="pricing-content unstyled">
                                                <li><i class="icon-circle-arrow-right"></i><span>Status :</span><?php echo $season->mare_status; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Type: </span><?php echo $season->mare_type; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Covering sire:</span><?php echo $season->covering_sire; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Last service:</span><?php echo ($season->last_service != '' && $season->last_service != '0000-00-00') ? date('d/m/Y', strtotime($season->last_service)) : ''; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Current status:</span><?php echo $season->current_status; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Last Scan Date:</span><?php echo ($season->last_scan != '' && $season->last_scan != '0000-00-00') ? date('d/m/Y', strtotime($season->last_scan)) : ''; ?></li>
                                                <li><i class="icon-circle-arrow-right"></i><span>Last Scan:</span><?php echo $season->last_scan_text; ?></li>
                                                <?php if ($season->mare_status == 'In Foal'): ?>
                                                    <li><i class="icon-circle-arrow-right"></i><span>In foal to:</span><?php echo $season->in_foal_to; ?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Due date:</span><?php echo ($season->due_date != '' && $season->due_date != '0000-00-00') ? date('d/m/Y', strtotime($season->due_date)) : ''; ?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Foaling date:</span><?php echo ($season->foaling_date != '' && $season->foaling_date != '0000-00-00') ? date('d/m/Y', strtotime($season->foaling_date)) : ''; ?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Foaling location:</span><?php echo $season->foaling_location; ?></li>
                                                    <li><i class="icon-circle-arrow-right"></i><span>Foal details:</span><?php echo $season->foal_details; ?></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                        <?php if ($sr % 4 == 0): ?>
                                            <div class="clearfix"></div>
                                        <?php endif; ?>
                                        <?php
                                        $sr++;
                                    endwhile;
                                    ?>
                                <?php else: ?> 
                                    <div class="span12" style="text-align: center;">
                                        <img src="assets/img/no-record-found.png"/>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>  
                <?php endif; ?>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_5">
                    <div class="row-fluid"> 
                        <div class="span12"><div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Documents
                                    </div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url('stallion', 'update', 'update', 'id=' . $id . '#tab_1_5') ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <?php if ($horse_documents) { ?>
                                        <table class="table table-condensed table-hover" id="sample_2_">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>File (Name)</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($horse_documents as $key => $horse_document) { ?>
                                                    <tr>
                                                        <td><?php echo $key += 1 ?></td>
                                                        <td>
                                                            <i class="icon-file" aria-hidden="true"></i>  <?php echo $horse_document['file'] ?>
                                                        </td>
                                                        <td>
                                                            <a href="<?php echo DIR_WS_SITE_UPLOAD . 'horse_documents/' . $horse_document['file'] ?>" class="btn blue mini" download><i class="icon-download icon-white"></i> Download</a>
                                                            <a href="<?php echo DIR_WS_SITE_UPLOAD . 'horse_documents/' . $horse_document['file'] ?>" class="btn blue mini" target="_blank"><i class="icon-zoom-in icon-white"></i> View</a>
                                                            <!--<a href="<?php echo make_admin_url('stallion', 'delete_document', 'delete_document&id=' . $horse_document['id'] . '&b_id=' . $id); ?>" class="btn red mini">Delete</a>-->
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } else { ?>
                                        No record found..!
                                    <?php } ?>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_6">
                    <div class="row-fluid"> 
                        <div class="span12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Notes</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url('stallion', 'update', 'update', 'id=' . $id . '#tab_1_6') ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <?php if ($notes_all) { ?>
                                        <table class="table table-condensed table-hover dataTable" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <!--<th>Name</th>-->
                                                    <th>Note</th>
                                                    <!--<th>Action</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $notes_k = 1; ?>
                                                <?php foreach ($notes_all as $key => $notes_al) { ?>
                                                    <?php if ($notes_al['note']) { ?>
                                                        <tr>
                                                            <td><?php echo $notes_k++ ?></td>
                                                            <!--<td><?php echo $notes_al['name'] ?></td>-->
                                                            <td><?php echo $notes_al['note'] ?></td>
            <!--                                                    <td>
                                                                <a href="<?php echo make_admin_url('stallion', 'delete_notes', 'delete_notes&id=' . $notes_al['id'] . '&b_id=' . $id); ?>" class="btn red mini">Delete</a>
                                                            </td>-->
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } else { ?>
                                        No record found..!
                                    <?php } ?>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_7">
                    <div class="row-fluid"> 
                        <div class="span12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">Invoice Notes</div>
                                    <div class="tools" style="margin-top: 0px;">
                                        <a title="Edit" class="btn grey mini" href="<?php echo make_admin_url('stallion', 'update', 'update', 'id=' . $id . '#tab_1_7') ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <?php if ($invoice_all) { ?>
                                        <table class="table table-condensed table-hover dataTable" id="sample_2">

                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <!--<th>Name</th>-->
                                                    <th>Invoices Note</th>
                                                    <!--<th>Action</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $invoice_k = 1; ?>
                                                <?php foreach ($invoice_all as $key => $invoice_al) { ?>
                                                    <?php if ($invoice_al['invoice_note']) { ?>
                                                        <tr>
                                                            <td><?php echo $invoice_k++ ?></td>
                                                            <!--<td><?php echo $invoice_al['name'] ?></td>-->
                                                            <td><?php echo $invoice_al['invoice_note'] ?></td>
            <!--                                                    <td>
                                                                <a href="<?php echo make_admin_url('stallion', 'delete_notes', 'delete_notes&id=' . $invoice_al['id'] . '&b_id=' . $id); ?>" class="btn red mini">Delete</a>
                                                            </td>-->
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } else { ?>
                                        No record found..!
                                    <?php } ?>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    $("#print").live("click", function () {
        var id = '<?php echo $horse->id; ?>';

        var dataString = 'id=' + id;
        $.ajax({
            type: "GET",
            url: "<?php echo make_admin_url_window('printhorse', 'id=' . $horse->id); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>
