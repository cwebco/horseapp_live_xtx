
<div class="tiles pull-right">
    <?php if ($redirect == 'stallion' || $redirect == 'mare'): ?>
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url($redirect, 'restore', 'restore', 'id=' . $id) ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-undo"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Restore
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-red">
            <a href="<?php echo make_admin_url($redirect, 'permanent_delete', 'thrash', 'id=' . $id) ?>" onclick="return confirm('Are you sure? All records of this horse will be permanently deleted.');">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-remove"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Delete
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url($redirect, 'thrash', 'thrash'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-backward"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Back to Listing
                    </div>
                </div>
            </a> 
        </div>
    <?php else : ?>
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id=' . $id . '&type=arrived') ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Arrived Report
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-yellow">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id=' . $id . '&type=departed') ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Departed Report
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-green">
            <a href="<?php echo make_admin_url($Page, 'list', 'list'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-backward"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Back to Listing
                    </div>
                </div>
            </a> 
        </div>
    <?php endif; ?>
</div>