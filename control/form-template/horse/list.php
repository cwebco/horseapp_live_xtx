<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Horses</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Horses</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-red <?php echo ($Page == "horse" && $section == 'thrash') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('horse', 'thrash', 'thrash'); ?>">
                <div class="corner"></div>
                <div class="tile-body">
                    <i class="icon-archive"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Archive
                    </div>
                </div>
            </a> 
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list-alt"></i>List Horses</div>
                    <div class="tools" style="margin-top: 0px;">
                        <a class="btn grey mini" href="<?php echo make_admin_url_window('printallhorses', 'list', 'list', 'sex=horse'); ?>" >
                            <i class="icon-print"></i> Print
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th class="hidden-480">Sr. No</th>
                                <th>Name</th>
                                <th>Sex</th>
                                <th class="hidden-480">D.O.B.</th>
                                <th class="hidden-480">Owner</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if($QueryObj->GetNumRows()!=0):?>
                            <?php
                            $sr = 1;
                            while ($horse = $QueryObj->GetObjectFromRecord()):
                                ?>
                                <tr class="odd gradeX">
                                    <td class="hidden-480"><?php echo $sr++; ?></td>
                                    <td><?php echo $horse->name ?></td>
                                    <?php if ($horse->sex == 'stallion'): ?>
                                        <td>Horse</td>
                                    <?php else: ?>
                                        <td><?php echo ucfirst($horse->sex); ?></td>
    <?php endif; ?>
                                    <td class="hidden-480"><?php echo ($horse->born != '' && $horse->born != '0000-00-00') ? date('d/m/Y', strtotime($horse->born)) : ''; ?></td>
                                    <td class="hidden-480"><?php echo str_replace(array('(', ')'), " ", $horse->owner_name); ?></td>
                                    <td>
                                        <a class="btn blue mini tooltips" href="<?php echo make_admin_url('horse', 'view', 'view', 'id=' . $horse->id) ?>" title="click to view full details"><i class="icon-zoom-in icon-white"></i> View</a>
                                        <a class="btn red mini tooltips" href="<?php echo make_admin_url('horse', 'delete', 'list', 'id=' . $horse->id . '&delete=1') ?>" onclick="return confirm('Are you sure?');" title="click to archive this record"><i class="icon-archive icon-white"></i> Archive</a>
                                    </td>
                                </tr>
                        <?php endwhile; ?>
                        </tbody>
<?php endif; ?>  
                    </table>  
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    




