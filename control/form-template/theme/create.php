<script>
    function ToggleState(checkbox, field) {
        if ($(checkbox).attr('checked'))
                $(field).attr('disabled', 'disabled');
        else
                $(field).removeAttr('disabled');
     }
</script>    

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Themes
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-bookmark-empty"></i>
                                               <a href="<?php echo make_admin_url('theme', 'list', 'list');?>">List Themes</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        New Theme
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('theme', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-bookmark-empty"></i>New Theme</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="" id="name" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="urlname">Folder Name</label>
                                                    <div class="controls">
                                                       <input type="text" name="urlname" id="urlname"  value="" class="span12 m-wrap" />
                                                    
                                                    </div>  
                                            </div>
                                           
                                            <div class="control-group">
                                                    <label class="control-label" for="description">Description<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="description" class="span12 ckeditor m-wrap" name="description" rows="6"></textarea>
                                                       
                                                    </div>
                                             </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Show on Website</label>
                                                    <div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1"  />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>


                                        
                                             <br/>
                                            <h3>Set Theme Relation</h3> 
                                            
                                            <p>Any theme can be a parent theme or can be sub theme of another theme.</p>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_main">Is Parent Theme?</label>
                                                    <div class="controls">
                                                           <input type="checkbox" name="is_main" id="is_main" value="1" class="" onclick="javascript:ToggleState('#is_main', '#theme_parent_id');" />
                                                     

                                                    </div>  
                                             </div>
                                             <h4 style="text-align:center !important;margin-top:-10px !important"> OR </h4>
                                             <div class="control-group">
                                                     <label for="email_type" class="control-label">Select Parent</label>
                                                     <div class="controls">
                                                         <select class="span12 m-wrap validate[required] " id="theme_parent_id" name="theme_parent_id" >
                                                                <option value>Select Parent</option>
                                                                <?php foreach($parent_themes as $kk=>$parent):?>
                                                                    <option value="<?php echo $parent->id?>"><?php echo ucfirst($parent->name);?></option>
                                                                <?php endforeach?>
                                                        </select>
                                                         
                                                    </div>
                                             </div>


                                            <div class="clear"></div>
                                            
                                            
                                            <br/>
                                            <h3>Select Category and category image</h3> 
                                            
                                            
                                            <div class="control-group">
                                                   
                                                    <div class="controls">
                                            
                                                      <?php
                                                                  if(isset($all_cat) && is_array($all_cat) && count($all_cat)):
                                                                          foreach ($all_cat as $key=>$value):?>
                                                                                      <div class="span4" style="margin-left:0px;">
                                                                                        <input  type="checkbox" name="categories[]" value="<?php echo $key?>" id="tags[<?php echo $key?>]" class="validate[minCheckbox[1]]"/>
                                                                                        <?php echo $value ?>
                                                                                      </div>
                                                                                     <div class="span4">
                                                                                       <input type="file" name="images[<?php echo $key?>]"/> 
                                                                                     </div>
                                                        
                                                                                     
                                                                                    <div class="clear" style="clear:both;height:20px"></div>
                                                                            
                                                                              <?php

                                                                      endforeach;
                                                                  endif
                                                                      ?>
                                          
                                                    </div>
                                           </div> 
                                        
                                        
                                        
                                            <div class="clear"></div>
                                            <br/>
                                            <h3>SEO Information</h3>
                                            <div class="control-group">
                                                   <label class="control-label" for="meta_name">Meta Title</label>
                                                   <div class="controls">
                                                       <input type="text" name="meta_name" id="meta_name"  value="" class="span12 m-wrap" /> 
                                           
                                                   </div>  
                                            </div>  
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Meta Keywords</label>
                                                    <div class="controls">
                                                    <input type="text" name="meta_keyword" id="meta_keyword"  value="" class="span12 m-wrap" />
                                           
                                                    </div>
                                            </div>          
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_description">Meta Description</label>
                                                    <div class="controls">
                                            
                                                       <textarea rows="3" class="span12 m-wrap"  style=" height: 82px;" id="meta_description"  name="meta_description"></textarea>
                                          
                                                    </div>
                                           </div>           
                                        
                                        
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('theme', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                     <?php endif;?>        
                                  
                              </div> 
                            </div>
                        </div>
                          
                             
                         
                          
                             
                          
                            

                     </form>
                 
                   
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



