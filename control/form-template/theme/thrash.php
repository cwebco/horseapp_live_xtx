
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Themes
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                     <li>
                                        <i class="icon-bookmark-empty"></i>
                                               <a href="<?php echo make_admin_url('theme', 'list', 'list');?>">List Themes</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                     <li class="last">
                                        Trash
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-bookmark-empty"></i>Trash</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								
									
								</div>
							</div>
							<div class="portlet-body">
							        <table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										 <tr>
                                                                                        <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                                                        <th class="hidden-480">Sr. No</th>
                                                                                        <th>Name</th>
                                                                                        <th class="hidden-480">Folder Name </th>
                                                                                        <th class="hidden-480">Position </th>
                                                                                        <th>Status</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($theme=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                        <td>
                                                                                            <input class="checkboxes" id="multiopt[<?php echo $theme->id ?>]" name="multiopt[<?php echo $theme->id ?>]" type="checkbox" />
                                                                                        </td>
                                                                                        
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
											<td><?php echo $theme->name?></td>
                                                                                        <th class="hidden-480"><?php echo $theme->urlname?> </th>
											<td class="hidden-480">
                                                                                           <?php echo $theme->position?> 
                                                                                        </td>
											<td>
                                                                                            <span class="label label-<?php echo ($theme->is_active=='1')?'success':'important';?>"><?php echo ($theme->is_active=='1')?'Active':'Inactive';?></span>
                                                                                        </td>
											<td>
                                                                                            
                                                                                               <a class="btn green icn-only tooltips" href="<?php echo make_admin_url('theme', 'restore', 'restore', 'id='.$theme->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
                                                                                               <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('theme', 'permanent_delete', 'permanent_delete', 'id='.$theme->id.'&delete=1')?>" onclick="return confirm('Are you deleting this record permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a>  
                                                                                         </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                      
                                                                       <?php endif;?>  
								</table>
                                                            
                                                            
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

