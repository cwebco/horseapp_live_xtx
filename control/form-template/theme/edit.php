<script>
    function ToggleState(checkbox, field) {
        if ($(checkbox).attr('checked'))
                $(field).attr('disabled', 'disabled');
        else
                $(field).removeAttr('disabled');
     }
</script>   
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Themes
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-bookmark-empty"></i>
                                               <a href="<?php echo make_admin_url('theme', 'list', 'list');?>">List Themes</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Edit Theme
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('theme', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-bookmark-empty"></i>Edit Theme</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="<?=$theme->name?>" id="name" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="urlname">Folder Name</label>
                                                    <div class="controls">
                                                       <input type="text" name="urlname" id="urlname"  value="<?=$theme->urlname;?>" class="span12 m-wrap" />
                                                    
                                                    </div>  
                                            </div>
                                           
                                            <div class="control-group">
                                                    <label class="control-label" for="description">Description<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="description" class="span12 editor_full m-wrap" name="description" rows="6"><?php echo html_entity_decode($theme->description);?></textarea>
                                                       
                                                    </div>
                                             </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Show on Website</label>
                                                    <div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1" <?=($theme->is_active=='1')?'checked':'';?> />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="<?=$theme->position?>" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>

                                            <input type="hidden" name="id" value="<?php echo $theme->id?>" />

                                            <br/>
                                            <h3>Set Theme Relation</h3> 
                                            
                                            <p>Any theme can be a parent theme or can be sub theme of another theme.</p>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_main">Is Parent Theme?</label>
                                                    <div class="controls">

                                                        <input type="checkbox" name="is_main" id="is_main" value="1" class="" <?=($theme->is_main=='1')?'checked':'';?> onclick="javascript:ToggleState('#is_main', '#theme_parent_id');" />

                                                    </div>  
                                             </div>
                                             <h4 style="text-align:center !important;margin-top:-10px !important"> OR </h4>
                                             <div class="control-group">
                                                     <label for="email_type" class="control-label">Select Parent</label>
                                                     <div class="controls">
                                                         <select <?=($theme->is_main=='1')?'disabled':'';?> class="span12 m-wrap validate[required] " id="theme_parent_id" name="theme_parent_id" >
                                                            <option value>Select Parent</option>
                                                            <?php foreach($parent_themes as $kk=>$parent):?>
                                                                    <option value="<?php echo $parent->id?>" <?php echo $theme->is_main!='1' && $parent_id==$parent->id?"selected":""?>><?php echo ucfirst($parent->name);?></option>
                                                            <?php endforeach?>
                                                        </select>
                                                    </div>
                                             </div>


                                            <div class="clear"></div>
                                            
                                            
                                            <br/>
                                            <h3>Select Category and category image</h3> 
                                            
                                            
                                            <div class="control-group">
                                                   
                                                    <div class="controls">
                                            
                                                      <?php
                                                                  if(isset($all_cat) && is_array($all_cat) && count($all_cat)):
                                                                          foreach ($all_cat as $key=>$value):?>
                                                                                      <div class="span4" style="margin-left:0px;">
                                                                                        <input  type="checkbox" name="categories[]" value="<?php echo $key?>" id="tags[<?php echo $key?>]" <?php  if (in_array("$key",$selectedcats)):echo'checked';endif;?> class="validate[minCheckbox[1]]"/>
                                                                                        <?php echo $value ?>
                                                                                      </div>
                                                                                     <div class="span4">
                                                                                       <input type="file" name="images[<?php echo $key?>]"/> 
                                                                                     </div>
                                                        
                                                                                     <div class="span4">
                                                                                         <?php
                                                                                            /*Get image of theme of particular category*/
                                                                                            $theme_image=array();
                                                                                            $theme_image_obj=new theme_img_rel();
                                                                                            $theme_image=$theme_image_obj->getThemeCategoryImage($theme->id,$key);
                                                                                         ?>
                                                                                         
                                                                                         <?php 
                                                                                          $image_obj=new imageManipulation();
                                                                                          if($theme_image && is_object($theme_image) && $theme_image->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/theme/large/'.$theme_image->image))):?>
                                                                                                 <div class="fileupload-new thumbnail" style="max-width: 200px;position:relative ">
                                                                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?php echo $theme->name?>" href="<?=$image_obj->get_image('theme','big', $theme_image->image);?>">

                                                                                                          <img style="max-width:200px" src="<?=$image_obj->get_image('theme','thumb', $theme_image->image);?>">
                                                                                                    </a>
                                                                                                  <div style="position:absolute;top:4px;right:4px;"  class="btn-group" data-toggle="buttons-radio">
                                                                                                       <a onclick="return confirm('Image shall be permanently deleted. Are you sure?');" title="delete image" href="<?php echo make_admin_url('theme', 'delete_image', 'delete_image', 'id='.$theme->id.'&category_id='.$key);?>" class="grey btn icn-only mini"><i class="icon-white icon-remove"></i></a>
                                                                                                          &nbsp;
                                                                                                       <a href="<?=$image_obj->get_image('theme','big', $theme_image->image);?>"  title="View Image" class="grey btn icn-only mini fancybox-button"><i class="icon-white icon-zoom-in"></i></a>
                                                                                
                                                                                                  </div>
                                                                                                 </div>
                                                                                         <?php endif;?>
                                                                                   
                                                                                     </div>    
                                                                                    <div class="clear" style="clear:both;height:20px"></div>
                                                                            
                                                                              <?php

                                                                      endforeach;
                                                                  endif
                                                                      ?>
                                          
                                                    </div>
                                           </div> 
                                        
                                             <br/>
                                             <div class="clear"></div> 
                                            <h3>SEO Information</h3>
                                            <div class="control-group">
                                                   <label class="control-label" for="meta_name">Meta Title</label>
                                                   <div class="controls">
                                                       <input type="text" name="meta_name" id="meta_name"  value="<?=$theme->meta_name;?>" class="span12 m-wrap" /> 
                                           
                                                   </div>  
                                            </div>  
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Meta Keywords</label>
                                                    <div class="controls">
                                                    <input type="text" name="meta_keyword" id="meta_keyword"  value="<?=$theme->meta_keyword;?>" class="span12 m-wrap" />
                                           
                                                    </div>
                                            </div>          
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_description">Meta Description</label>
                                                    <div class="controls">
                                            
                                                       <textarea rows="3" class="span12 m-wrap"  style=" height: 82px;" id="meta_description"  name="meta_description"><?=$theme->meta_description;?></textarea>
                                          
                                                    </div>
                                           </div>    
                                            
                                            
                                            
                                            
                                        
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('theme', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                     <?php endif;?>        
                                  
                              </div> 
                            </div>
                        </div>
                         

                     </form>
                 
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



