<!-- / Breadcrumbs -->
    <script type="text/javascript">
        jQuery(document).ready(function() {
            /* on click */
            $(".status_on").live("click",function(){

                var id=$(this).attr('on');
                var parent_id="on_off_button"+id;
              
                $("#" + parent_id + " .status_on").addClass("active");
                $("#" + parent_id + " .status_off").removeClass("active");
                        
                var dataString = 'table=theme&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_on')?>",
                    data: dataString,
                    success: function(data, textStatus) {}
               });
             });

            $(".status_off").live("click",function(){
                var id=$(this).attr('off');
                var parent_id="on_off_button"+id;
                
                $("#" + parent_id + " .status_on").removeClass("active");
                $("#" + parent_id + " .status_off").addClass("active");
                        
                var dataString = 'table=theme&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_off')?>",
                    data: dataString,
                    success: function(data, textStatus) {}
                });
            });
        });
</script>

  

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Themes
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Themes</li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-bookmark-empty"></i>Manage Themes</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								
									
								</div>
							</div>
							<div class="portlet-body">
							     <form action="<?php echo make_admin_url('theme', 'update2', 'list', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
                                                                                        <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                                                     
                                                                                        <th>Name</th>
                                                                                        <th class="hidden-480">Parent Theme</th>
                                                                                        <th class="hidden-480">Category</th>
                                                                                        <th class="hidden-480">Position </th>
                                                                                        <th>Status</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($theme=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                        <td>
                                                                                            <input class="checkboxes" id="multiopt[<?php echo $theme->id ?>]" name="multiopt[<?php echo $theme->id ?>]" type="checkbox" />
                                                                                        </td>
                                                                                         
                                                                                        
											<td>
                                                                                               <?php echo $theme->name?>
                                                                                        </td>
                                                                                         <td class="hidden-480">
                                                                                            <?php
                                                                                            
                                                                                            if($theme->is_main!=1):/*if it is a sub theme*/
                                                                                                
                                                                                                /*get parent id of theme*/
                                                                                                $parent_id=0;
                                                                                                $parent_sub=new theme_sub_rel();
                                                                                                $parent_id=$parent_sub->getParentThemeIdOfSubTheme($theme->id);
                                                                                                 if($parent_id!=0):
                                                                                                     $theme_par=new theme();
                                                                                                     $parent_theme_name=$theme_par->getThemeName($parent_id);
                                                                                                     echo $parent_theme_name;
                                                                                                 endif;
                                                                                            endif;
                                                                                            
                                                                                            ?>
                                                                                            
                                                                                        </td>   
                                                                                        <td class="hidden-480">
                                                                                                   <?php
                                                                                                    /*get all categories of theme*/
                                                                                                    $all_cat=array();
                                                                                                    $cat_obj=new theme_cat_rel();
                                                                                                    $all_cat=$cat_obj->get_all_cat_of_theme($theme->id);

                                                                                                    ?>
                                                                                                   <ul  class="unstyled inline blog-tags">
                                                                                                   <?php foreach($all_cat as $keyyy=>$cattt):
                                                                                                         /*category name*/
                                                                                                         $category_name="";
                                                                                                         $cat_name=new category();
                                                                                                         $category_name=$cat_name->get_category_name($cattt);
                                                                                                
                                                                                                        /*Get image of theme of particular category*/
                                                                                                        $theme_image=array();
                                                                                                        $theme_image_obj=new theme_img_rel();
                                                                                                        $theme_image=$theme_image_obj->getThemeCategoryImage($theme->id,$cattt);
                                                                                                    ?>
                                                                                                     
                                                                                                         <li>
                                                                                                                <i class="icon-tags"></i> 
                                                                                                                <span class="label">
                                                                                                                         <?php 
                                                                                                                          $image_obj=new imageManipulation();
                                                                                                                          if($theme_image && is_object($theme_image) && $theme_image->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/theme/large/'.$theme_image->image))):?>
                                                                                                                                  <a style="color:white;text-decoration:underline" class="fancybox-button" data-rel="fancybox-button" title="<?php echo $theme->name?>" href="<?=$image_obj->get_image('theme','big', $theme_image->image);?>">
                                                                                                                                     <?php echo ucfirst($category_name);?>
                                                                                                                                  </a>    

                                                                                                                    <?php else:    
                                                                                                                               echo ucfirst($category_name);
                                                                                                                          endif;?>
                                                                                                                </span>
                                                                                                         </li>
                                                                                                   <?php endforeach;?>
                                                                                                   </ul>
                                                                                                
                                                                                        </td>
											<td class="hidden-480">
                                                                                            <input type="text" name="position[<?php echo $theme->id?>]" value="<?=$theme->position;?>" size="3" style="width:20%;" />
                                                                                        </td>
                                                                                        
                                                                                        <td>
                                                                                            <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php echo $theme->id?>'>
                                                                                                <div class="btn status_on mini_buttons <?php echo ($theme->is_active=='1')?'active':'';?>" on="<?php echo $theme->id;?>" rel="<?php echo ($theme->is_active=='1')?'on':'off';?>" >SHOW</div>
                                                                                                <div class="btn status_off mini_buttons button_right <?php echo ($theme->is_active=='0')?'active':'';?>" off="<?php echo $theme->id;?>" rel="<?php echo ($theme->is_active=='1')?'off':'on';?>">HIDE</div>
                                                                                            </div>
                                                                                        </td>
                                                                                       
											<td>
                                                                                            
                                                                                                <a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('theme', 'update', 'update', 'id='.$theme->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                                                            
                                                                                                <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('theme', 'delete', 'list', 'id='.$theme->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                                                           
                                                                                        </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                        <tfoot>
                                                                            <tr class="odd gradeX">
                                                                       
                                                                                <td colspan="4">
                                                                                    <div style=" width:220px;float:left">
                                                                                        <select  name="multiopt_action" style="width:150px" class="left_align regular">
                                                                                            <option value="delete">Delete</option>
                                                                                        </select>
                                                                                        <input style="float:right" type="submit" class="btn green large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                                                                    </div>
                                                                                </td>
                                                                                <td colspan="3" class="hidden-480"><input type="submit" class="btn green" name="submit_position" value="Update" /></td>

                                                                            </tr> 
                                                                        </tfoot>
                                                                       <?php endif;?>  
								</table>
                                                              
                                                             </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



