<?php if($section=='update'): ?>
<div class="tiles pull-left">
    <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id='.$id.'&type=arrived')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                               Arrived Report
                        </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-yellow">
            <a href="<?php echo make_admin_url_window('printhorse', 'view', 'view', 'id='.$id.'&type=departed')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                               Departed Report
                        </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url_window('printMareInfo', 'mare_report', 'mare_report', 'id='.$id.'&type=mare_report')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                               Infection Freedom 
                        </div>
                </div>
            </a>   
        </div>
         <div class="tile bg-yellow">
            <a href="<?php echo make_admin_url_window('printMareInfo', 'infectionfree_report', 'infectionfree_report', 'id='.$id.'&type=infectionfree_report')?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-print"></i>
                </div>
                <br>
                <div class="tile-object">
                        <div class="name">
                                 Mare Details
                        </div>
                </div>
            </a>   
        </div>
</div>
<?php endif;?>
<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('mare', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Mare
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('mare', 'list', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Mare
                        </div>
                </div>
            </a> 
        </div>
        <div class="tile bg-red <?php echo ($Page=="mare" && $section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('mare', 'thrash', 'thrash');?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-archive"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Archive
                        </div>
                </div>
            </a> 
        </div>
</div>