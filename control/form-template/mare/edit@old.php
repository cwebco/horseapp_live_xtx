
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Edit
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-heart-empty"></i>
                                               <a href="<?php echo make_admin_url('mare', 'list', 'list');?>">List Mare</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        <?php echo ucfirst($horse->name); ?>
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('mare', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-heart-empty"></i>Edit: <strong>"<?php echo ucfirst($horse->name); ?>"</strong></div>
                                            <div class="tools" style="margin-top:0px;">
                                                <a class="btn green mini" href="<?php echo make_admin_url('mare','update2','update2','id='.$id); ?>">
                                                   <i class="icon-edit"></i> Edit Season Info
                                                </a>
<!--                                                <a href="javascript:;" class="collapse"></a>-->
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="row-fluid">
                                            <div class="span6 ">
                                                <div class="control-group">
  	                                        <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="<?php echo $horse->name; ?>" id="name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                                </div> 
                                                
						<div class="control-group">
  	                                        <label class="control-label" for="born">Born Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="born" value="<?php echo date('d/m/Y',  strtotime($horse->born));?>" id="born" class="span10 m-wrap upto_current_date"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="color">Color</label>
                                                    <div class="controls">
                                                      <input type="text" name="color" value="<?php echo $horse->color; ?>" id="color" class="span10 m-wrap"/>
                                                    </div>
                                                </div>
						
                                                <div class="control-group">
  	                                        <label class="control-label" for="sire">Sire</label>
                                                    <div class="controls">
                                                      <input type="text" name="sire" value="<?php echo $horse->sire; ?>" id="sire" class="span10 m-wrap"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="dam">Dam</label>
                                                    <div class="controls">
                                                      <input type="text" name="dam" value="<?php echo $horse->dam; ?>" id="dam" class="span10 m-wrap"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="grandsire">Grandsire</label>
                                                    <div class="controls">
                                                      <input type="text" name="grandsire" value="<?php echo $horse->grandsire; ?>" id="grandsire" class="span10 m-wrap"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
                                                   <label class="control-label" for="to_paddock">To Paddock</label>
                                                        <div class="controls">
                                                            <select name="to_paddock" id="to_paddock" class="span6 m-wrap">
                                                              <?php $query1  = new dropdownValues();
                                                                $paddoks = $query1->getDropdown('2');
                                                                foreach($paddoks as $kkk=>$vvv): ?>
                                                                <option value="<?php echo $vvv->value?>" <?php echo ($horse->to_paddock==$vvv->value)?'selected':''; ?>><?php echo $vvv->title?></option>
                                                              <?php endforeach;?>  
                                                            </select>&nbsp;
                                                            <input type="text" name="to_paddock_date" placeholder="Date" value="<?php echo date('d/m/Y',  strtotime($horse->to_paddock_date));?>" id="to_paddock_date" class="span4 m-wrap new_format"/>
                                                        </div>  
                                                </div> 

                                                <div class="control-group">
                                                    <label class="control-label">Image Upload</label>
                                                    <div class="controls item">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                               <?php if($horse->image!=''): ?>
                                                                    <?php if(file_exists(DIR_FS_SITE_UPLOAD.'/photo/horse/thumb/'.$horse->image)): ?>
                                                                        <a class="fancybox-button" data-rel="fancybox-button" href="<?php echo DIR_WS_SITE_UPLOAD.'/photo/horse/big/'.$horse->image ?>">
                                                                            <div class="zoom">
                                                                            <img src="<?php echo DIR_WS_SITE_UPLOAD.'/photo/horse/thumb/'.$horse->image ?>" alt="<?php echo $horse->image;?>" />
                                                                            <div class="zoom-icon"></div>
                                                                            </div>
                                                                        </a>
                                                                        <div class="details">
                                                                                <a href="<?php echo make_admin_url('mare', 'delete_image', 'delete_image', 'id='.$horse->id);?>" onclick="return confirm('Image shall be permanently deleted. Are you sure?');" class="icon" ><i class="icon-remove"></i></a>    
                                                                        </div>
                                                                   <?php else: ?>
                                                                        <img src="<?php echo DIR_WS_SITE.'assets/img/noimage.gif' ?>" alt="" />
                                                                    <?php endif; ?>
                                                               <?php else: ?>
                                                                    <img src="<?php echo DIR_WS_SITE.'assets/img/noimage.gif' ?>" alt="" />
                                                               <?php endif; ?>
                                                            </div>
                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                            <div>
                                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                    <span class="fileupload-exists">Change</span>
                                                                    <input type="file" class="default" name="image"/></span>
                                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>	
                                        <div class="span6 ">
                                                <div class="control-group">
  	                                        <label class="control-label" for="passport_number">Passport Number</label>
                                                    <div class="controls">
                                                      <input type="text" name="passport_number" value="<?php echo $horse->passport_number; ?>" id="passport_number" class="span10 m-wrap"/>
                                                    </div>
                                                </div> 	
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="arrived">Arrived Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="arrived" value="<?php echo date('d/m/Y',  strtotime($horse->arrived));?>" id="arrived" class="span10 m-wrap new_format"/>
                                                    </div>
                                                </div> 	
                                                
                                            <div class="control-group">
  	                                        <label class="control-label" for="departed">Departed Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="departed" value="<?php echo date('d/m/Y',  strtotime($horse->departed));?>" id="departed" class="span10 m-wrap new_format"/>
                                                    </div>
                                            </div> 
                                            
                                            <div class="control-group">
  	                                        <label class="control-label" for="owner">Owner</label>
                                                    <div class="controls">
                                                        <select name="owner" class="span10 m-wrap select2_category" data-placeholder="Owner">
                                                            <option value=''>Select Owner</option>
                                                           <?php foreach(getOwnerDetails() as $ak=>$av):  ?> 
                                                                <option value="<?php echo $av['id'].'**'.$av['surname'].' '.$av['first_name'];?>" <?php echo ($horse->owner_id==$av['id'])?'selected':''; ?>>
                                                                <?php echo $av['surname'].' '.$av['first_name'];?>
                                                                </option>
                                                           <?php endforeach; ?> 
                                                        </select>
                                                    </div>
                                            </div> 

                                            <div class="control-group">
  	                                        <label class="control-label" for="last_farrier">Last Farrier</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_farrier" value="<?php echo $horse->last_farrier; ?>" id="last_farrier" class="span10 m-wrap"/>
                                                    </div>
                                            </div>   
											
                                            <div class="control-group">
  	                                        <label class="control-label" for="last_wormer">Last Wormer</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_wormer" value="<?php echo $horse->last_wormer; ?>" id="last_wormer" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 	  
											
                                            <div class="control-group">
  	                                        <label class="control-label" for="last_flu_vaccination">Last Flu Vaccination</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_flu_vaccination" value="<?php echo date('d/m/Y',  strtotime($horse->last_flu_vaccination));?>" id="last_flu_vaccination" class="span10 m-wrap new_format"/>
                                                    </div>
                                            </div>	
                                            
                                            <div class="control-group">
  	                                        <label class="control-label" for="rate_to_keep">Rate Agreed to Keep (&euro;)</label>
                                                    <div class="controls">
                                                      <input type="text" name="rate_to_keep" value="<?php echo $horse->rate_to_keep; ?>" id="rate_to_keep" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
                                            
                                            <div class="control-group">
  	                                        <label class="control-label" for="rate_foaling_fee">Rate Foaling Fee (&euro;)</label>
                                                    <div class="controls">
                                                      <input type="text" name="rate_foaling_fee" value="<?php echo $horse->rate_foaling_fee; ?>" id="rate_foaling_fee" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
                                            
                                            <div class="control-group">
  	                                        <label class="control-label" for="invoice_details">Invoice Details</label>
                                                    <div class="controls">
                                                      <textarea name="invoice_details" id="invoice_details" class="span10 m-wrap"><?php echo $horse->invoice_details; ?></textarea>
                                                    </div>
                                            </div>
										 											
                                        </div>	
                                    </div>
      
                                   <input type="hidden" name="sex" id="sex" value="mare"/>     
                                   <input type="hidden" name="id" value="<?php echo $horse->id; ?>"/> 
                                   
                                   <div class="form-actions">
                                       <input type="submit"  class="btn blue" name="submit" value="Submit"/>
                                         <a href="<?php echo make_admin_url('stallion', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                   </div>
                              </div> 
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    