<script type="text/javascript">
    jQuery("#horse_edit_btn").live("click", function () {
	var log_id = $(this).attr("log_id");
	var type = '';
	if (log_id == 'new') {
	    type = $(this).attr("type");
	}
	var pge = 'mare';
	var id = '<?php echo $id; ?>';
	$("#horse_edit").html('<div style="width:100%;text-align:center;padding-top:5px;"><br/><br/><img src="assets/img/ajax-loading.gif"/><br/><br/>Please Wait...<br/><br/><br/></div>');
	var dataString = 'log_id=' + log_id + '&pge=' + pge + '&id=' + id + '&type=' + type;

	$.ajax({
	    type: "POST",
	    url: "<?php echo make_admin_url_window('edithorse'); ?>",
	    data: dataString,
	    success: function (data, textStatus) {
		$("#horse_edit").show();
		$("#horse_edit").html(data);
		$("#horse_edit").fadeIn(1500);
		//App.init();
		FormComponents.init();
		FormSamples.init();
	    }
	});
    });

    /*paddock details*/
    jQuery("#paddock_edit_btn").live("click", function () {
	var pad_id = $(this).attr("pad_id");

	var pge = 'mare';
	var id = '<?php echo $id; ?>';
	$("#paddock_edit").html('<div style="width:100%;text-align:center;padding-top:5px;"><br/><br/><img src="assets/img/ajax-loading.gif"/><br/><br/>Please Wait...<br/><br/><br/></div>');
	var dataString = 'pad_id=' + pad_id + '&pge=' + pge + '&id=' + id;

	$.ajax({
	    type: "POST",
	    url: "<?php echo make_admin_url_window('editpaddock'); ?>",
	    data: dataString,
	    success: function (data, textStatus) {
		$("#paddock_edit").show();
		$("#paddock_edit").html(data);
		$("#paddock_edit").fadeIn(1500);
		//App.init();
		FormComponents.init();
		FormSamples.init();
	    }
	});
    });
</script>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Edit Horses
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>

                <li>
                    <i class="icon-heart-empty"></i>
                    <a href="<?php echo make_admin_url('mare', 'list', 'list'); ?>">List Mare</a>
                    <i class="icon-angle-right"></i>

                </li>
                <li class="last">
		    <?php echo ucfirst($horse->name); ?>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid profile">
        <!--BEGIN TABS-->
        <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1_1" data-toggle="tab">Main Info</a></li>
                <li><a href="#tab_1_2" data-toggle="tab">Farrier/Wormer</a></li>
                <li><a href="#tab_1_3" data-toggle="tab">Flu/Ehv</a></li>
                <li><a href="#tab_1_4" data-toggle="tab">Paddock Details</a></li>
                <li><a href="#tab_1_5" data-toggle="tab">Documents</a></li>
                <li><a href="#tab_1_6" data-toggle="tab">Notes</a></li>
                <li><a href="#tab_1_7" data-toggle="tab">Invoice Notes</a></li>
                <li <?= $_GET['act']=='maireData'?'active':'' ?>><a href="#tab_1_8" data-toggle="tab"> Freedom from infection</a></li>
                <li <?= $_GET['act']=='infectionFreeData'?'active':'' ?>><a href="#tab_1_9" data-toggle="tab">Mare Details</a></li>
            </ul>
            <div class="tab-content">
                <a class="btn green mini pull-right" href="<?php echo make_admin_url('mare', 'update2', 'update2', 'id=' . $id); ?>"  style="margin-bottom: 10px;margin-top: -15px;">
                    <i class="icon-edit"></i> Edit Season Info
                </a>
                <div class="clearfix"></div>
                <!--tab1-->
                <div class="tab-pane profile-classic row-fluid active" id="tab_1_1">

                    <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                        <!-- / Box -->
                        <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-heart"></i>Edit: <?php echo ucfirst($horse->name); ?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
				    <div class="pull-right">
					<a class="btn mini blue" href="<?php echo make_admin_url('compose', 'insert', 'insert&id=' . $horse->id); ?>">Send Message</a>
				    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="name" value="<?php echo $horse->name; ?>" id="name" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="born">D.O.B.</label>
                                                <div class="controls">
                                                    <input type="text" name="born" value="<?php echo ($horse->born != '' && $horse->born != '0000-00-00') ? date('d/m/Y', strtotime($horse->born)) : ''; ?>" id="mask_date"  class="span10 m-wrap" placeholder="dd/mm/year"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="color">Color</label>
                                                <div class="controls">
                                                    <input type="text" name="color" value="<?php echo $horse->color; ?>" id="color" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="sire">Sire</label>
                                                <div class="controls">
                                                    <input type="text" name="sire" value="<?php echo $horse->sire; ?>" id="sire" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="dam">Dam</label>
                                                <div class="controls">
                                                    <input type="text" name="dam" value="<?php echo $horse->dam; ?>" id="dam" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="grandsire">Grandsire</label>
                                                <div class="controls">
                                                    <input type="text" name="grandsire" value="<?php echo $horse->grandsire; ?>" id="grandsire" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Image Upload</label>
                                                <div class="controls item">
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
							    <?php if ($horse->image != ''): ?>
								<?php if (file_exists(DIR_FS_SITE_UPLOAD . '/photo/horse/thumb/' . $horse->image)): ?>
								    <a class="fancybox-button" data-rel="fancybox-button" href="<?php echo DIR_WS_SITE_UPLOAD . '/photo/horse/big/' . $horse->image ?>">
									<div class="zoom">
									    <img src="<?php echo DIR_WS_SITE_UPLOAD . '/photo/horse/thumb/' . $horse->image ?>" alt="<?php echo $horse->image; ?>" />
									    <div class="zoom-icon"></div>
									</div>
								    </a>
								    <div class="details">
									<a href="<?php echo make_admin_url('mare', 'delete_image', 'delete_image', 'id=' . $horse->id); ?>" onclick="return confirm('Image shall be permanently deleted. Are you sure?');" class="icon" ><i class="icon-remove"></i></a>    
								    </div>
								<?php else: ?>
								    <img src="<?php echo DIR_WS_SITE . 'assets/img/noimage.gif' ?>" alt="" />
								<?php endif; ?>
							    <?php else: ?>
    							    <img src="<?php echo DIR_WS_SITE . 'assets/img/noimage.gif' ?>" alt="" />
							    <?php endif; ?>
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                        <div>
                                                            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                <span class="fileupload-exists">Change</span>
                                                                <input type="file" class="default" name="image"/></span>
                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>	
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="passport_number">Passport Number</label>
                                                <div class="controls">
                                                    <input type="text" name="passport_number" value="<?php echo $horse->passport_number; ?>" id="passport_number" class="span10 m-wrap"/>
                                                </div>
                                            </div> 	

                                            <div class="control-group">
                                                <label class="control-label" for="microchip">Microchip Number</label>
                                                <div class="controls">
                                                    <input type="text" name="microchip" value="<?php echo $horse->microchip; ?>" id="microchip" class="span10 m-wrap"/>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="arrived">Arrived Date</label>
                                                <div class="controls">
                                                    <input type="text" name="arrived" value="<?php echo ($horse->arrived != '' && $horse->arrived != '0000-00-00') ? date('d/m/Y', strtotime($horse->arrived)) : ''; ?>" id="arrived" class="span10 m-wrap new_format"/>
                                                </div>
                                            </div> 	

                                            <div class="control-group">
                                                <label class="control-label" for="departed">Departed Date</label>
                                                <div class="controls">
                                                    <input type="text" name="departed" value="<?php echo ($horse->departed != '' && $horse->departed != '0000-00-00') ? date('d/m/Y', strtotime($horse->departed)) : ''; ?>" id="departed" class="span10 m-wrap new_format"/>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="owner">Owner</label>
                                                <div class="controls">
						    <?php
						    $horse_ids = $horse->owner_id;
						    $horse_ids = str_replace(array(')', '('), '', $horse_ids);
						    $horse_id_array = explode(',', $horse_ids);
						    ?>
                                                    <select name="owner[]" class="span10 m-wrap select2_category" multiple data-placeholder="Owner">
                                                        <option value=''>Select Owner</option>

							<?php foreach (getOwnerDetails() as $ak => $av): ?> 
    							<option value="<?php echo $av['id'] . '**' . $av['surname'] . ' ' . $av['first_name']; ?>" <?php echo (in_array($av['id'], $horse_id_array)) ? 'selected' : ''; ?>>
								<?php echo $av['surname'] . ' ' . $av['first_name']; ?>
    							</option>
							<?php endforeach; ?> 
                                                    </select>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="rate_to_keep">Rate Agreed to Keep(&euro;)</label>
                                                <div class="controls">
                                                    <input type="text" name="rate_to_keep" value="<?php echo $horse->rate_to_keep; ?>" id="rate_to_keep" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="rate_foaling_fee">Rate Foaling Fee (&euro;)</label>
                                                <div class="controls">
                                                    <input type="text" name="rate_foaling_fee" value="<?php echo $horse->rate_foaling_fee; ?>" id="rate_foaling_fee" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="invoice_details">Invoice Details</label>
                                                <div class="controls">
                                                    <textarea name="invoice_details" id="invoice_details" class="span10 m-wrap"><?php echo $horse->invoice_details; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="posted_date">Client Agreement Posted Date</label>
                                                <div class="controls">
                                                    <input type="text" name="posted_date" value="<?php echo $horse->posted_date ?>" id="posted_date" class="span10 m-wrap new_format">
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="invoice_details">Client Agreement Signed and Returned </label>
                                                <div class="controls">
                                                    <input type="checkbox" id="check_box_show" <?php echo ($horse->date) ? 'checked' : '' ?> />
                                                </div>
                                            </div>
                                            <div class="date_checkbox" <?php echo ($horse->date) ? '' : 'style="display: none"' ?>>
                                                <div class="control-group">
                                                    <label class="control-label" for="date">Returned Date</label>
                                                    <div class="controls">
                                                        <input type="text" name="date" value="<?phP echo $horse->date ?>" id="date_checkbox" class="span10 m-wrap new_format">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="departure_notes">Departure Notes</label>
                                                <div class="controls">
                                                    <textarea rows="5" name="departure_notes" id="departure_notes" class="span10 m-wrap"><?php echo $horse->departure_notes; ?></textarea>
                                                </div>
                                            </div>
                                        </div>	
                                    </div>

                                    <input type="hidden" name="sex" id="sex" value="mare"/>     
                                    <input type="hidden" name="id" value="<?php echo $horse->id; ?>"/> 

                                    <div class="form-actions">
                                        <input type="submit"  class="btn blue" name="submit" value="Submit"/>
                                        <a href="<?php echo make_admin_url('mare', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </form>
                </div>
                <!--tab2--> 
                <div class="tab-pane profile-classic row-fluid" id="tab_1_2">
                    <div class="span6">
                        <!-- BEGIN CONDENSED TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">Farrier Information</div>
                                <div class="tools" style="margin-top: 0px;">
                                    <a class="btn grey mini" type="farrier" log_id="new" data-toggle="modal" href="#horse_edit" id="horse_edit_btn" >
                                        <i class="icon-plus"></i> Add New
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php if (count($horse_log) > 0): ?>
					    <?php
					    $sr = 1;
					    foreach ($horse_log as $fk => $fv):
						?>
						<?php if ($fv['type'] == 'farrier'): ?>
	    					<tr>
	    					    <td><?php echo $sr; ?></td>
	    					    <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
	    					    <td align="middle">
	    						<a class="btn blue mini tooltips" id="horse_edit_btn" log_id="<?php echo $fv['id']; ?>" data-toggle="modal" href="#horse_edit"><i class="icon-edit"></i> Edit</a></td>
	    					    <td align="middle">
	    						<a class="btn red mini tooltips" onclick="return confirm('You are deleted the entry. Are you sure?');"  href="<?php echo make_admin_url('edithorse', 'delete', 'delete', 'delete_id=' . $fv['id'] . '&redirect_page=mare' . '&redirect_id=' . $id . '&redirect_tab=tab_1_2'); ?>">
	    						    <i class="icon-remove"></i> Delete
	    						</a>
	    					    </td>
	    					</tr>
						    <?php
						    $sr++;
						endif;
						?>
					    <?php endforeach; ?>
					<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                    <div class="span6">
                        <!-- BEGIN CONDENSED TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Wormer Information</div>
                                <div class="tools" style="margin-top: 0px;">
                                    <a class="btn grey mini" type="wormer" log_id="new" data-toggle="modal" href="#horse_edit" id="horse_edit_btn" >
                                        <i class="icon-plus"></i> Add New
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php if (count($horse_log) > 0): ?>
					    <?php
					    $sr = 1;
					    foreach ($horse_log as $fk => $fv):
						?>
						<?php if ($fv['type'] == 'wormer'): ?>
	    					<tr>
	    					    <td><?php echo $sr; ?></td>
	    					    <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
	    					    <td><?php echo $fv['value']; ?></td>
	    					    <td align="middle">
	    						<a class="btn blue mini tooltips" id="horse_edit_btn" log_id="<?php echo $fv['id']; ?>" data-toggle="modal" href="#horse_edit"><i class="icon-edit"></i> Edit</a></td>
	    					    </td>
	    					    <td align="middle">
	    						<a class="btn red mini tooltips" onclick="return confirm('You are deleted the entry. Are you sure?');"  href="<?php echo make_admin_url('edithorse', 'delete', 'delete', 'delete_id=' . $fv['id'] . '&redirect_page=mare' . '&redirect_id=' . $id . '&redirect_tab=tab_1_2'); ?>">
	    						    <i class="icon-remove"></i> Delete
	    						</a>
	    					    </td>
	    					</tr>
						    <?php
						    $sr++;
						endif;
						?>
					    <?php endforeach; ?>
					<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
                <!--tab3--> 
                <div class="tab-pane profile-classic row-fluid" id="tab_1_3">
                    <div class="span6">
                        <!-- BEGIN CONDENSED TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Flu Vaccination</div>
                                <div class="tools" style="margin-top: 0px;">
                                    <a class="btn grey mini" type="flu" log_id="new" data-toggle="modal" href="#horse_edit" id="horse_edit_btn" >
                                        <i class="icon-plus"></i> Add New
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php if (count($horse_log) > 0): ?>
					    <?php
					    $sr = 1;
					    foreach ($horse_log as $fk => $fv):
						?>
						<?php if ($fv['type'] == 'flu'): ?>
	    					<tr>
	    					    <td><?php echo $sr; ?></td>
	    					    <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
	    					    <td><?php echo $fv['value']; ?></td>
	    					    <td align="middle">
	    						<a class="btn blue mini tooltips" id="horse_edit_btn" log_id="<?php echo $fv['id']; ?>" data-toggle="modal" href="#horse_edit"><i class="icon-edit"></i> Edit</a>
	    						&nbsp;&nbsp;
	    						<a class="btn red mini tooltips" onclick="return confirm('You are deleted the entry. Are you sure?');"  href="<?php echo make_admin_url('edithorse', 'delete', 'delete', 'delete_id=' . $fv['id'] . '&redirect_page=mare' . '&redirect_id=' . $id . '&redirect_tab=tab_1_3'); ?>">
	    						    <i class="icon-remove"></i> Delete
	    						</a>
	    					    </td>
	    					</tr>
						    <?php
						    $sr++;
						endif;
						?>
					    <?php endforeach; ?>
					<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                    <div class="span6">
                        <!-- BEGIN CONDENSED TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">EHV 1.4</div>
                                <div class="tools" style="margin-top: 0px;">
                                    <a class="btn grey mini" type="ehv" log_id="new" data-toggle="modal" href="#horse_edit" id="horse_edit_btn" >
                                        <i class="icon-plus"></i> Add New
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php if (count($horse_log) > 0): ?>
					    <?php
					    $sr = 1;
					    foreach ($horse_log as $fk => $fv):
						?>
						<?php if ($fv['type'] == 'ehv'): ?>
	    					<tr>
	    					    <td><?php echo $sr; ?></td>
	    					    <td><?php echo date('d/m/Y', strtotime($fv['date'])); ?></td>
	    					    <td><?php echo $fv['value']; ?></td>
	    					    <td align="middle">
	    						<a class="btn blue mini tooltips" id="horse_edit_btn" log_id="<?php echo $fv['id']; ?>" data-toggle="modal" href="#horse_edit"><i class="icon-edit"></i> Edit</a>
	    						&nbsp;&nbsp;
	    						<a class="btn red mini tooltips" onclick="return confirm('You are deleted the entry. Are you sure?');"  href="<?php echo make_admin_url('edithorse', 'delete', 'delete', 'delete_id=' . $fv['id'] . '&redirect_page=mare' . '&redirect_id=' . $id . '&redirect_tab=tab_1_3'); ?>">
	    						    <i class="icon-remove"></i> Delete
	    						</a>
	    					    </td>
	    					</tr>
						    <?php
						    $sr++;
						endif;
						?>
					    <?php endforeach; ?>
					<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
                <!--tab4--> 
                <div class="tab-pane profile-classic row-fluid" id="tab_1_4">
                    <div class="span12">
                        <!-- BEGIN CONDENSED TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Paddock Data</div>
                                <div class="tools" style="margin-top: 0px;">
                                    <a class="btn grey mini" pad_id="new" data-toggle="modal" href="#paddock_edit" id="paddock_edit_btn" >
                                        <i class="icon-move"></i> Move To
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Paddock</th>
                                            <th>Move</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php if (count($paddock_log) > 0): ?>
					    <?php
					    $sr = 1;
					    foreach ($paddock_log as $pk => $pv):
						?>
						<tr>
						    <td><?php echo $sr; ?> <?php echo ($current->id == $pv['id']) ? '<i class="icon-star"></i>' : ''; ?></td>
						    <td><?php echo $pv['paddock_title']; ?></td>
						    <td><?php echo ucfirst($pv['move']); ?></td>
						    <td><?php echo date('d/m/Y', strtotime($pv['date'])); ?></td>
						    <td align="middle">
							<a class="btn blue mini tooltips" id="paddock_edit_btn" pad_id="<?php echo $pv['id']; ?>" data-toggle="modal" href="#paddock_edit"><i class="icon-edit"></i> Edit</a>
							&nbsp;&nbsp;
							<a class="btn red mini tooltips" onclick="return confirm('You are deleted the entry. Are you sure?');"  href="<?php echo make_admin_url('editpaddock', 'delete', 'delete', 'delete_id=' . $pv['id'] . '&redirect_page=mare' . '&redirect_id=' . $id . '&redirect_tab=tab_1_4'); ?>">
							    <i class="icon-remove"></i> Delete
							</a>
						    </td>
						</tr>
						<?php $sr++; ?>
					    <?php endforeach; ?>
					<?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>	
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_5">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Upload Document</div>
                            </div>
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <form method="POST" enctype="multipart/form-data" id="validation">
                                            <input type="file" name="file" id="file" class="validate[required]" />
                                            <br />
                                            <br />
                                            <input type="submit" name="file_upload" class="btn blue" />
                                        </form>
                                    </div>
                                    <div class="span8">
					<?php if ($horse_documents) { ?>
                    <form method="POST">
    					<table class="table table-condensed table-hover" id="sample_2_">
    					    <thead>
    						<tr>
    						    <th>#</th>
    						    <th>Files (Name)</th>
    						    <th>Action</th>
    						</tr>
    					    </thead>
    					    <tbody>
						    <?php foreach ($horse_documents as $key => $horse_document) { ?>
							<tr>
							    <td>
                                <input type="checkbox" name="attachments[]" value="<?php echo $horse_document['file'] ?>" />
                                </td>
							    <td>
								<i class="icon-file" aria-hidden="true"></i> <?php echo $horse_document['file'] ?>
							    </td>
							    <td>
								<a href="<?php echo DIR_WS_SITE_UPLOAD . 'horse_documents/' . $horse_document['file'] ?>" class="btn blue mini" download><i class="icon-download"></i> Download</a>
								<a href="<?php echo DIR_WS_SITE_UPLOAD . 'horse_documents/' . $horse_document['file'] ?>" class="btn blue mini" target="_blank"><i class="icon-zoom-in icon-white"></i> View</a>
								<a href="<?php echo make_admin_url('mare', 'delete_document', 'delete_document&id=' . $horse_document['id'] . '&b_id=' . $id); ?>" class="btn red mini tooltips" onclick="return confirm('Are you sure')"><i class="icon-remove icon-white"></i> Delete</a>
							    </td>
							</tr>
						    <?php } ?>
    					    </tbody>
    					</table>
                        <button type="submit" name="submitAttachment">Send Attachement</button>

                        </form>
					<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_6">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Notes</div>
                            </div>
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <form method="POST" class="" id="validation">
                                            <div class="form-group">
                                                <!--                                                <label for="name">Name</label>
                                                                                                <input type="text" name="name" value="" id="name" class="validate[required]">-->

                                                <label for="note">Note</label>
                                                <textarea name="note" value="" id="note" class="validate[required]"></textarea>
                                                <br />
                                                <input class="btn blue" type="hidden" name="time" value="<?php echo time() ?>"> 
                                                <input class="btn blue" type="hidden" name="horse_id" value="<?php echo $id ?>"> 
                                                <input class="btn blue" type="submit" name="note_submit" value="Submit"> 
                                            </div>
                                        </form>
                                    </div>
                                    <div class="span8">
					<?php if ($notes_all) { ?>
    					<table class="table table-condensed table-hover" id="sample_2_">
    					    <thead>
    						<tr>
    						    <th>#</th>
    						    <!--<th>Name</th>-->
    						    <th>Notes</th>
    						    <th>Action</th>
    						</tr>
    					    </thead>
    					    <tbody>
						    <?php $k_notes = 1 ?>
						    <?php foreach ($notes_all as $key => $note_all) { ?>
							<?php if ($note_all['note']) { ?>
	    						<tr>
	    						    <td><?php echo $k_notes++ ?></td>
	    						    <!--<td><?php echo $note_all['name'] ?></td>-->
	    						    <td><?php echo $note_all['note'] ?></td>
	    						    <td>
	    							<a class="btn blue mini exit_note" data-name="<?php echo $note_all['name'] ?>" data-note="<?php echo $note_all['note'] ?>" data-id="<?php echo $note_all['id'] ?>"><i class="icon-pencil"></i></a>
	    							<a href="<?php echo make_admin_url('mare', 'delete_notes', 'delete_notes&id=' . $note_all['id'] . '&b_id=' . $id . '&notes_type=notes'); ?>" class="btn red mini" onclick="return confirm('Are you sure')"><i class="icon-remove icon-white"></i></a>
	    						    </td>
	    						</tr>
							<?php } ?>
						    <?php } ?>
    					    </tbody>
    					</table>
					<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_7">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">Invoice Notes</div>
                            </div>
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <form method="POST" class="" id="validation">
                                            <div class="form-group">
                                                <!--                                                <label for="name">Name</label>
                                                                                                <input type="text" name="name" value="" id="name" class="validate[required]">-->

                                                <label for="note">Invoice Note</label>
						<p>Date: <input type="text" id="datepicker" name="date" class="my_date_picker"></p>
                                                <textarea name="invoice_note" value="" id="invoice_note" class="validate[required]"></textarea>
                                                <br />
                                                <input class="btn blue" type="hidden" name="time" value="<?php echo time() ?>"> 
                                                <input class="btn blue" type="hidden" name="horse_id" value="<?php echo $id ?>"> 
                                                <input class="btn blue" type="submit" name="invoice_note_submit" value="Submit"> 
                                            </div>
                                        </form>
                                    </div>
                                    <div class="span8">
					<?php if ($invoice_all) { ?>
    					<table class="table table-condensed table-hover" id="sample_2_">
    					    <thead>
    						<tr>
    						    <th>#</th>
    						    <!--<th>Name</th>-->
    						    <th>Notes</th>
    						    <th>Action</th>
    						</tr>
    					    </thead>
    					    <tbody>
						    <?php $k_invoice = 1; ?>
						    <?php foreach ($invoice_all as $key => $invoic_all) { ?>
							<?php if ($invoic_all['invoice_note']) { ?>
	    						<tr>
	    						    <td><?php echo $k_invoice++ ?></td>
	    						    <!--<td><?php echo $invoic_all['name'] ?></td>-->
	    						    <td>
								    <?php if ($invoic_all['date'] != 0) { ?>
									<span><?php echo $invoic_all['date'] ?>-</span><?php } ?>
								    <?php echo $invoic_all['invoice_note'] ?></td>
	    						    <td>
	    							<a class="btn blue mini exit_invoice_note" data-name="<?php echo $invoic_all['name'] ?>" data-note="<?php echo $invoic_all['invoice_note'] ?>" data-id="<?php echo $invoic_all['id'] ?>" data-date="<?php echo $invoic_all['date'] ?>"><i class="icon-pencil"></i></a>
	    							<a href="<?php echo make_admin_url('mare', 'delete_notes', 'delete_notes&id=' . $invoic_all['id'] . '&b_id=' . $id); ?>" class="btn red mini" onclick="return confirm('Are you sure')"><i class="icon-remove icon-white"></i></a>
	    						    </td>
	    						</tr>
							<?php } ?>
						    <?php } ?>
    					    </tbody>
    					</table>
					<?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_8"> 
                    <?php //require 'form2.html' ?>
                     <?php require 'mare_details.php' ?>
                </div>
                <div class="tab-pane profile-classic row-fluid" id="tab_1_9"> 
                <?php //require 'form.html' ?>
                   <?php require 'infection_free.php' ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">

    </div>
    <div class="clearfix"></div>
    <!--update form div---->
    <div class="modal hide fade" id="horse_edit" data-width="400"></div><!--ajax response in this div-->
    <div class="modal hide fade" id="paddock_edit" data-width="400"></div><!--ajax response in this div-->
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->


<div id="edit_note" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                        </div>-->
            <div class="modal-body">
                <form method="POST" class="" id="validation">
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea name="note" value="" id="note" class="note_value validate[required]" style="width:97%"></textarea>
                        <br />
                        <input class="btn blue" type="hidden" name="time" value="<?php echo time() ?>"> 
                        <input class="btn blue" type="hidden" name="horse_id" value="<?php echo $id ?>"> 
                        <input class="btn blue note_id" type="hidden" name="id" value=""> 
                        <input class="btn blue" type="submit" name="note_submit" value="Submit"> 
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="edit_invoice_notes" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                        </div>-->
            <div class="modal-body">
                <form method="POST" class="" id="validation">
                    <div class="form-group">
                        <label for="note">Note</label>
			<p>Date: <input type="text" id="datepicker2" name="date" class="note_date_value" ></p>
                        <textarea name="invoice_note" value="" id="invoice_note" class="note_value validate[required]" style="width:97%"></textarea>
                        <br />
                        <input class="btn blue" type="hidden" name="time" value="<?php echo time() ?>"> 
                        <input class="btn blue" type="hidden" name="horse_id" value="<?php echo $id ?>"> 
                        <input class="btn blue note_id" type="hidden" name="id" value=""> 
                        <input class="btn blue" type="submit" name="invoice_note_submit" value="Submit"> 
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.exit_note', function () {
	var el = $(this);
	var name = el.attr('data-name');
	var note = el.attr('data-note');
	var note_id = el.attr('data-id');
	$('.note_value').val(note);
	$('.note_id').val(note_id);
	$('#edit_note').modal();
    });
</script>

<script>
    $(document).on('click', '.exit_invoice_note', function () {
	var el = $(this);
	var name = el.attr('data-name');
	var note = el.attr('data-note');
	var note_id = el.attr('data-id');
	var note_date = el.attr('data-date');
	$('.note_value').val(note);
	$('.note_date_value').val(note_date);
	$('.note_id').val(note_id);
	$('#edit_invoice_notes').modal();
    });
</script>

<script>
    $(document).on('click', '#check_box_show', function () {
	var el = $(this);
	var is_checked = $('#check_box_show').is(':checked');
	if (is_checked) {
	    $('.date_checkbox').show();
	} else {
	    $('.date_checkbox').hide();
	    $('#date_checkbox').val('');
//            $('#posted_date').val('');
	}
    });

    $(function () {
	$("#datepicker").datepicker({
	    format: 'yyyy-mm-dd'
	});
    });
    $(function () {
	$("#datepicker2").datepicker({
	    format: 'yyyy-mm-dd'
	});
    });


</script>