<!DOCTYPE html>
<html>
  <style>

    .radio input[type="radio"]{
      margin-left: 0px !important;
    }
    input[type="text"],
    select {
      width: 100% !important;
      padding: 12px 6px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    input[type="submit"] {
      width: 100%;
      background-color: #4caf50;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      font-size: 18px;
      margin-top: 6px;
    }

    .form-field-heading h1 {
      padding: 10px;
      margin: 0px;
      margin-bottom: 10px;
      margin-top: 20px;
      background: #4b8df8 ;
      font-weight: normal;
      font-size: 20px;
      color: #fff;
      text-align: center;
    }

    textarea {
      width: 100%;
      padding: 12px 6px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      border-radius: 4px;
      box-sizing: border-box;
    }

    p.red {
      color: red;
      font-weight: 500;
    }
  </style>

  <div class="container-fluid">
    <form
      class="form-vertical login-form"
      method="post"
      name="form_login"
      id="validation"
       action="<?php echo make_admin_url('mare', 'update', 'update','id='.$id.'&act=infectionFreeData'); ?>" autocomplete="off" 
    >

    
      <div class="row-fluid">
        <div class="span12 form-field-heading">
          <h1>Mare Name</h1>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field">
            <input type="text" placeholder="" value="<?php if(isset($horse->name)) {echo $horse->name;} ?>" name="mare_name"  readonly/>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12 form-field-heading">
          <h1>Breeding</h1>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Date of birth: <input type="text" placeholder="" value="<?php echo $infectionfreeData->date_of_birth; ?>" name="date_of_birth" id="dob" />
          </div>
          <div class="span6 form-field">
            Colour: <input type="text" placeholder="" value="<?php echo $infectionfreeData->color; ?>"   name="color" />
          </div>
        </div>

        <div class="row-fluid">
          <div class="span6 form-field"> 
            Sire: <input type="text" placeholder="" value="<?php if(isset($horse->sire)){ echo $horse->sire;} ?>"   name="sire" readonly/>
          </div>
          <div class="span6 form-field">
            Dam: <input type="text" placeholder="" value="<?php if(isset($horse->dam)){ echo $horse->dam; }?>"  name="dam" readonly />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Dam Sire: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->dam_sire; ?>" name="dam_sire" />
          </div>
          <div class="span6 form-field">
            Grand Dam: <input type="text" placeholder="" value="<?php echo $infectionfreeData->grand_dam; ?>"  name="grand_dam" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Passport No: <input type="text" placeholder="" value="<?php if(isset($horse->passport_number)){ echo $horse->passport_number; }?>"  name="passport_no" readonly/>
          </div>
          <div class="span6 form-field">
            Microchip: <input type="text" placeholder="" value="<?php if(isset($horse->microchip)){ echo $horse->microchip;} ?>"   name="microchip" readonly/>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12 form-field-heading">
          <h1>2018 Cover Details</h1>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Status: <input type="text" placeholder="" value="<?php echo $infectionfreeData->status; ?>"  name="status" />
          </div>
          <div class="span6 form-field">
            Last covering Sire:
            <input type="text" placeholder="" value="<?php echo $infectionfreeData->last_covering_sire; ?>"  name="last_covering_sire" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Last Service Date: <input type="text" placeholder="" value="<?php echo $infectionfreeData->last_service_date; ?>"  id="last_service_date" name="last_service_date" />
          </div>
          <div class="span6 form-field">
            2018 Boarding Stud.: <input type="text" value="<?= $infectionfreeData->boarding_stud_2018; ?>"  placeholder="" name="boarding_stud_2018" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            2019 Foal Details:
            <input type="text" value="<?= $infectionfreeData->foal_details_2019; ?>" name="foal_details_2019" />
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12 form-field-heading">
          <h1>Mare Produce Record</h1>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field">
            In 2015 the mare boarded at:
            <input type="text" placeholder=""  value="<?php echo $infectionfreeData->boarded_at_2015; ?>"  name="boarded_at_2015" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Mated With: <input type="text" placeholder="" value="<?php echo $infectionfreeData->mated_with_2015; ?>"  name="mated_with_2015" />
          </div>

          <div class="span6 form-field">
            Result: <input type="text" placeholder="" value="<?php echo $infectionfreeData->result_2015; ?>"   name="result_2015" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field">
            In 2016 the mare boarded at:
            <input type="text" placeholder="" value="<?php echo $infectionfreeData->boarded_at_2016; ?>"  name="boarded_at_2016" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Mated With: <input type="text" placeholder="" value="<?php echo $infectionfreeData->mated_with_2016; ?>"   name="mated_with_2016" />
          </div>
          <div class="span6 form-field">
            Result: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->result_2016; ?>"  name="result_2016" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field">
            In 2017 the mare boarded at:
            <input type="text" placeholder="" value="<?php echo $infectionfreeData->boarded_at_2017; ?>"   name="boarded_at_2017" class="form-control" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span6 form-field">
            Mated With: <input type="text" placeholder="" value="<?php echo $infectionfreeData->mated_with_2017; ?>"  name="mated_with_2017" />
          </div>
          <div class="span6 form-field">
            Result: <input type="text" placeholder="" value="<?php echo $infectionfreeData->result_2017; ?>"   name="result_2017" />
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Owner Details</h1>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Registered Owner's Name & Address:
              <input type="text" placeholder="" value="<?php echo $infectionfreeData->owner_name_address; ?>"  name="owner_name_address" />
            </div>
            <div class="span6 form-field">
              Address of Stud Where Mare Is Normally Kept:
              <input type="text" placeholder="" value="<?php echo $infectionfreeData->stud_name_address; ?>"  name="stud_name_address" />
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Phone No: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->owner_phone_no ; ?>" name="owner_phone_no" />
            </div>
            <div class="span6 form-field">
              Phone No: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->stud_phone_no; ?>" name="stud_phone_no" />
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Mobile No: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->owner_mobile; ?>" name="owner_mobile" />
            </div>
            <div class="span6 form-field">
              Mobile No: <input type="text" placeholder="" value="<?php echo $infectionfreeData->stud_mobile; ?>"  name="stud_mobile" />
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Fax No: <input type="text" placeholder="" value="<?php echo $infectionfreeData->owner_fax; ?>" name="owner_fax" />
            </div>
            <div class="span6 form-field">
              Fax No: <input type="text" placeholder="" value="<?php echo $infectionfreeData->stud_fax; ?>"  name="stud_fax" />
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Email: <input type="text" placeholder="" value="<?php echo $infectionfreeData->owner_mail; ?>" name="owner_mail" />
            </div>
            <div class="span6 form-field">
              Email: <input type="text" placeholder="" value="<?php echo $infectionfreeData->stud_mail; ?>" name="stud_mail" />
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Veterinary Information</h1>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <p>
                Has your mare tested positive for CEMO, Klebsiella pneumoniae
                and Pseudomonas aeruginosa any time?
              </p>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <input type="radio" name="is_tested_positive_for_CEMO" <?= ($infectionfreeData->is_tested_positive_for_CEMO==1)?'checked':'' ?>   value="1" />Yes
                <input type="radio" name="is_tested_positive_for_CEMO" <?= ($infectionfreeData->is_tested_positive_for_CEMO==0)?'checked':'' ?>   value="0" />No<br />
                <textarea
                  row-fluids="4" 
                  cols="200" name="tested_positive_for_CEMO"
                  placeholder="if yes please gives full details."
                ><?php echo $infectionfreeData->tested_positive_for_CEMO; ?></textarea>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <p>
                  Has your mare ever had or been on a Stud Farm where an
                  outbreak of Equine Rhinopneumonltis *Virus Abortion* has
                  occured?
                </p>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <input type="radio"  name="is_stud_farm" <?= ($infectionfreeData->is_stud_farm==1)?'checked':'' ?>  value="1" />Yes
                <input type="radio" name="is_stud_farm" <?= ($infectionfreeData->is_stud_farm==0)?'checked':'' ?>   value="0" />No<br />
                <textarea
                  row-fluids="4" value=""
                  cols="200" name="stud_farm"
                  placeholder="if yes please gives full details."
                ><?php echo $infectionfreeData->stud_farm; ?></textarea>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <p>
                  Has your mare been in contact with, any other infectious
                  diseases?
                </p>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <input type="radio" name="is_any_other_infectious" 
                <?= ($infectionfreeData->is_any_other_infectious==1)?'checked':'' ?>  value="1" />Yes

                <input type="radio" name="is_any_other_infectious" 
                <?= ($infectionfreeData->is_any_other_infectious==0)?'checked':'' ?> value="0" />No<br />
                <textarea
                  row-fluids="4" value=""
                  cols="200" name="any_other_infectious"
                  placeholder="if yes please gives full details."
                ><?php echo $infectionfreeData->any_other_infectious; ?></textarea>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <p>Has your mare been in contact with any non-throughbreads?</p>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12 form-field">
                <input type="radio" name="is_non_throughbreads" <?= ($infectionfreeData->is_non_throughbreads==1)?'checked':'' ?>  value="1" />Yes
                <input type="radio" name="is_non_throughbreads" <?= ($infectionfreeData->is_non_throughbreads==0)?'checked':'' ?>   value="0" />No<br />
                <textarea
                  row-fluids="4"
                  cols="200" name="non_throughbreads" 
                  placeholder="if yes please gives full details."
                ><?php echo $infectionfreeData->non_throughbreads; ?></textarea>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span6 form-field">
                <p>Has the mare been stiched?</p>
                  <input type="radio" name="is_stiched" value="1" <?= ($infectionfreeData->is_stiched==1)?'checked':'' ?> />Yes
                <input type="radio" name="is_stiched" value="0" <?= ($infectionfreeData->is_stiched==0)?'checked':'' ?>  />No<br />
              </div>
            </div>
          <!--   <div class="row-fluid">
              <div class="span6 form-field">
                <p>Yes / No</p>
              </div>
            </div> -->
            <div class="row-fluid">
              <div class="span6 form-field">
                <p>Has the mare shown readily to a teaser?</p>
                  <input type="radio" name="is_readily_to_a_teaser" <?= ($infectionfreeData->is_readily_to_a_teaser==1)?'checked':'' ?>  value="1" />Yes
                <input type="radio"  name="is_readily_to_a_teaser"  <?= ($infectionfreeData->is_readily_to_a_teaser==0)?'checked':'' ?>  value="0" />No<br />
              </div>
            </div>
            <!-- <div class="row-fluid">
              <div class="span6 form-field">
                <p>Yes / No</p>
              </div>
            </div> -->
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Mare Temprament</h1>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <p>Does the mare possess any temperamental abnormalities?</p>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <input type="radio" name="is_temperamental_abnormalities" <?= ($infectionfreeData->is_temperamental_abnormalities==1)?'checked':'' ?>   value="1" />Yes
              <input type="radio" name="is_temperamental_abnormalities" <?= ($infectionfreeData->is_ouside_the_UK==0)?'checked':'' ?>   value="0" />No<br />
              <textarea
                row-fluids="4"  value=""
                cols="200" name="temperamental_abnormalities"
                placeholder="if yes please gives full details."
              ><?php echo $infectionfreeData->temperamental_abnormalities; ?></textarea>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="row-fluid">
            <div class="span12 form-field-heading">
              <h1>Mare Movement History</h1>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <p>
                Has the mare been ouside the UK in the last year (12 Months)?
              </p>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <input type="radio" name="is_ouside_the_UK" <?= ($infectionfreeData->is_ouside_the_UK==1)?'checked':'' ?>  value="1" />Yes
              <input type="radio" name="is_ouside_the_UK" <?= ($infectionfreeData->is_ouside_the_UK==0)?'checked':'' ?> value="0" />No<br />
              <textarea
                row-fluids="4" value=""
                placeholder="if yes please state which country and confirm expected arrival date in the UK."
              ><?php echo $infectionfreeData->ouside_the_UK; ?>
               </textarea>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Boarding Stud Details</h1>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <p>
                Is the mare likely to be 'Walked' in from an outside Stud Farm?
              </p>
            </div>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <input type="radio" name="is_walked" <?= ($infectionfreeData->is_walked==1)?'checked':'' ?>  value="1" />Yes
              <input type="radio" name="is_walked" <?= ($infectionfreeData->is_walked==0)?'checked':'' ?>  value="0" />No<br />
              <textarea
                row-fluids="4"
                cols="200" name="walked"   value=""
                placeholder="if yes please gives full name, address, phone,fax,e-mail and contact at the boarding stud together with expected arrival date."
              ><?php echo $infectionfreeData->walked; ?></textarea>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Points To Note</h1>
          </div>
          <div class="row-fluid">
            <div class="span12 form-field">
              <p>
                Whllst we strive to maintain the highest standards of stud
                management, the stud can not accept any responsibilties of
                accident or disease to third party animals, however caused.
              </p>
              <p class="red">
                We will unable to cover your mare if you do not fully complete
                this form and return it to the Stud Office.
              </p>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12 form-field-heading">
            <h1>Signature Required</h1>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Signed: <input type="text" placeholder=""  value="<?php echo $infectionfreeData->signed; ?>" name="signed" />
            </div>
            <div class="span6 form-field">
              Name: <input type="text" placeholder="" value="<?php echo $infectionfreeData->name; ?>"  name="name" />
            </div>
          </div>
          <div class="row-fluid">
            <div class="span6 form-field">
              Owner/Agent* (Delete as applicable):
              <input type="text" placeholder="" name="agent" value="<?php echo $infectionfreeData->agent; ?>"  required>
            </div>
            <div class="span6 form-field">
              Date: <input type="text" placeholder="" value="<?php echo $infectionfreeData->date; ?>" id="date1" name="date" />
            </div>
          </div>
        </div>
        <div class="span2 pull-left" style="margin-bottom: 10px;text-align: center;">
        <img src="assets/img/sign.png" alt="Logo"/>
      </div>
        <input type="hidden" name="infectionFree_details" value="infectionFree_details_form">
       <input type="hidden" name="mare_id" value="<?= $id ?>">
         <?php if(!empty($infectionfreeData)): ?>
      <input type="hidden" name="id" value="<?= $infectionfreeData->id ?>">
       <?php endif; ?>
        <input type="submit" value="Submit" />
      </div>
    </form>
  </div>
</html>
<script type="text/javascript">

  $("#dob").datepicker({
    format: 'yyyy-mm-dd'
  });
   $("#last_service_date").datepicker({
    format: 'yyyy-mm-dd'
  });
    $("#date1").datepicker({
    format: 'yyyy-mm-dd'
  });
  </script>