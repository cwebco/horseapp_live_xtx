
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                New Mare
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>

                <li>
                    <i class="icon-heart-empty"></i>
                    <a href="<?php echo make_admin_url('mare', 'list', 'list'); ?>">List Mare</a>
                    <i class="icon-angle-right"></i>

                </li>
                <li class="last">
                    New Mare
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('mare', 'insert', 'list') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-heart-empty"></i>New Mare</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="row-fluid">
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="name">Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="name" value="" id="name" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 

                                <div class="control-group">
                                    <label class="control-label" for="born">D.O.B.</label>
                                    <div class="controls">
                                        <input type="text" name="born" value="" id="mask_date"  class="span10 m-wrap" placeholder="dd/mm/year"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="color">Color</label>
                                    <div class="controls">
                                        <input type="text" name="color" value="" id="color" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="sire">Sire</label>
                                    <div class="controls">
                                        <input type="text" name="sire" value="" id="sire" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="dam">Dam</label>
                                    <div class="controls">
                                        <input type="text" name="dam" value="" id="dam" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="grandsire">Grandsire</label>
                                    <div class="controls">
                                        <input type="text" name="grandsire" value="" id="grandsire" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="to_paddock">To Paddock</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Date" name="to_paddock_date" value="" id="to_paddock_date" class="span4 m-wrap new_format"/>
                                        &nbsp;
                                        <select name="to_paddock" id="to_paddock" class="span6 m-wrap">
                                            <?php
                                            $query1 = new dropdownValues();
                                            $paddoks = $query1->getDropdown('2');
                                            foreach ($paddoks as $kkk => $vvv):
                                                ?>
                                                <option value="<?php echo $vvv->title . '**' . $vvv->id ?>"><?php echo $vvv->title ?></option>
                                            <?php endforeach; ?>  
                                        </select>

                                    </div>  
                                </div> 

                                <div class="control-group">
                                    <label class="control-label">Image Upload</label>
                                    <div class="controls">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo DIR_WS_SITE . 'assets/img/noimage.gif' ?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" class="default" name="image"/></span>
                                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="passport_number">Passport Number</label>
                                    <div class="controls">
                                        <input type="text" name="passport_number" value="" id="passport_number" class="span10 m-wrap"/>
                                    </div>
                                </div> 	

                                <div class="control-group">
                                    <label class="control-label" for="microchip">Microchip Number</label>
                                    <div class="controls">
                                        <input type="text" name="microchip" value="" id="microchip" class="span10 m-wrap"/>
                                    </div>
                                </div> 	

                                <div class="control-group">
                                    <label class="control-label" for="arrived">Arrived Date</label>
                                    <div class="controls">
                                        <input type="text" name="arrived" value="" id="arrived" class="span10 m-wrap new_format"/>
                                    </div>
                                </div> 	

                                <div class="control-group">
                                    <label class="control-label" for="departed">Departed Date</label>
                                    <div class="controls">
                                        <input type="text" name="departed" value="" id="departed" class="span10 m-wrap new_format"/>
                                    </div>
                                </div> 

                                <div class="control-group">
                                    <label class="control-label" for="owner">Owner</label>
                                    <div class="controls">
                                        <select name="owner[]" multiple class="span10 m-wrap select2_category" data-placeholder="Owner">
                                            <option value=''>Select Owner</option>
                                            <?php foreach (getOwnerDetails() as $ak => $av): ?> 
                                                <option value="<?php echo $av['id'] . '**' . $av['surname'] . ' ' . $av['first_name']; ?>"><?php echo $av['surname'] . ' ' . $av['first_name']; ?></option>
                                            <?php endforeach; ?> 
                                        </select>
                                    </div>
                                </div> 

                                <div class="control-group">
                                    <label class="control-label" for="last_farrier">Last Farrier</label>
                                    <div class="controls">
                                        <input type="text" name="last_farrier" value="" id="last_farrier" class="span10 m-wrap new_format"/>
                                    </div>
                                </div>   

                                <div class="control-group">
                                    <label class="control-label" for="last_wormer">Last Wormer</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Date" name="last_wormer" value="" id="last_wormer" class="span4 m-wrap new_format"/>
                                        &nbsp;
                                        <input type="text" name="last_wormer_value" value="" id="last_wormer_value" class="span6 m-wrap"/>
                                    </div>
                                </div> 	  

                                <div class="control-group">
                                    <label class="control-label" for="flu_value">Last Flu Vaccination</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Date" name="flu_date" value="" id="flu_date" class="span4 m-wrap new_format"/>
                                        &nbsp;
                                        <select name="flu_value" id="flu_value" class="span6 m-wrap">
                                            <option value="1st">1st</option>
                                            <option value="2nd">2nd</option>
                                            <option value="Booster">Booster</option>                                                              
                                        </select>
                                    </div>
                                </div>	

                                <div class="control-group">
                                    <label class="control-label" for="ehv_value">Last EHV 1.4</label>
                                    <div class="controls">
                                        <input type="text" placeholder="Date" name="ehv_date" value="" id="ehv_date" class="span4 m-wrap new_format"/>
                                        &nbsp;
                                        <select name="ehv_value" id="ehv_value" class="span6 m-wrap">
                                            <option value="1st">1st</option>
                                            <option value="2nd">2nd</option>
                                            <option value="3rd">3rd</option>                                                              
                                        </select>
                                    </div>
                                </div>	

                                <div class="control-group">
                                    <label class="control-label" for="rate_to_keep">Rate Agreed to Keep (&euro;)</label>
                                    <div class="controls">
                                        <input type="text" name="rate_to_keep" value="" id="rate_to_keep" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="rate_foaling_fee">Rate Foaling Fee (&euro;)</label>
                                    <div class="controls">
                                        <input type="text" name="rate_foaling_fee" value="" id="rate_foaling_fee" class="span10 m-wrap"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="invoice_details">Invoice Details</label>
                                    <div class="controls">
                                        <textarea name="invoice_details" value="" id="invoice_details" class="span10 m-wrap"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="posted_date">Posted Date</label>
                                    <div class="controls">
                                        <input type="text" name="posted_date" value="" id="posted_date" class="span10 m-wrap new_format">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="invoice_details">Client Agreement Signed and Returned </label>
                                    <div class="controls">
                                        <input type="checkbox" id="check_box_show" />
                                    </div>
                                </div>
                                <div class="date_checkbox" style="display: none">
                                    <div class="control-group">
                                        <label class="control-label" for="date">Returned Date</label>
                                        <div class="controls">
                                            <input type="text" name="date" value="" id="date_checkbox" class="span10 m-wrap new_format">
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="document">Document</label>
                                    <div class="controls">
                                        <input type="file" name="file" id="file" />

                                    </div>
                                </div>
                                <!--                                <div class="control-group">
                                                                    <label class="control-label" for="name_note">Name</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="name_note" value="" id="name_note">
                                                                    </div>
                                                                </div>-->

                                <div class="control-group">
                                    <label class="control-label" for="note">Note</label>
                                    <div class="controls">
                                        <textarea name="note" id="note"></textarea>

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="invoice_note">Invoice Note</label>
                                    <div class="controls">
                                        <textarea name="invoice_note" id="invoice_note"></textarea>

                                    </div>
                                </div>
                            </div>	
                        </div>

                        <input type="hidden" name="sex" id="sex" value="mare"/>     
                        <input type="hidden" name="added_date" id="added_date" value="<?php echo date('Y-m-d'); ?>"/> 

                        <div class="form-actions">
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('mare', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    



<script>
    $(document).on('click', '#check_box_show', function () {
        var el = $(this);
        var is_checked = $('#check_box_show').is(':checked');
        if (is_checked) {
            $('.date_checkbox').show();
        } else {
            $('.date_checkbox').hide();
            $('#date_checkbox').val('');
//            $('#posted_date').val('');
        }
    });
</script>
