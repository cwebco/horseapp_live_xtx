<script type="text/javascript">
     jQuery(document).ready(function() {
         jQuery("#mare_status").live("click",function(){
             var status=$(this).val();
             if(status!='In Foal'){
                 jQuery("#due_date").attr('disabled',true);
                 jQuery("#in_foal_to").attr('disabled',true);
                 jQuery("#foaling_date").attr('disabled',true);
                 jQuery("#foaling_location").attr('disabled',true);
                 jQuery("#foal_details").attr('disabled',true);
             }
             else{
                 jQuery("#due_date").attr('disabled',false);
                 jQuery("#in_foal_to").attr('disabled',false);
                 jQuery("#foaling_date").attr('disabled',false);
                 jQuery("#foaling_location").attr('disabled',false);
                 jQuery("#foal_details").attr('disabled',false);
             }
         });
     });
</script>
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Mare Season Info
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-heart-empty"></i>
                                               <a href="<?php echo make_admin_url('mare', 'list', 'list');?>">List Mare</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Mare Season Info
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('mare', 'insert2', 'list','id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-heart-empty"></i><strong>"<?php echo ucfirst($horse->name);?>"</strong> Season Info</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="control-group">
  	                                        <label class="control-label" for="season">Season<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="season" value="" id="season" class="span6 m-wrap validate[required] year-picker"/>
                                                    </div>
                                                </div> 
                                                
						<div class="control-group">
  	                                        <label class="control-label" for="mare_type">Type</label>
                                                    <div class="controls">
                                                            <select name="mare_type" id="mare_type" class="span6 m-wrap">
                                                              <?php $query1  = new dropdownValues();
                                                                $type = $query1->getDropdown('3');
                                                                foreach($type as $kkk=>$vvv): ?>
                                                                <option value="<?php echo $vvv->value?>"><?php echo $vvv->title?></option>
                                                              <?php endforeach;?>  
                                                            </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="current_status">Current status</label>
                                                    <div class="controls">
                                                      <!--<input type="text" name="current_status" value="" id="current_status" class="span6 m-wrap"/>-->
                                                        <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="In Foal" />
                                                        In Foal
                                                        </label>
                                                        <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="NIF" />
                                                        NIF
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="covering_sire">Covering sire</label>
                                                    <div class="controls">
                                                      <input type="text" name="covering_sire" value="" id="covering_sire" class="span6 m-wrap"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_service">Last service Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_service" value="" id="last_service" class="span6 m-wrap new_format"/>
                                                    </div>
                                                </div>
						
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_scan">Last Scan Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_scan" value="" id="last_scan" class="span6 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="mare_status">Status</label>
                                                    <div class="controls">
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="In Foal" />
                                                            In Foal
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="Maiden" />
                                                            Maiden
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="Barren" />
                                                            Barren
                                                            </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="in_foal_to">In Foal to</label>
                                                    <div class="controls">
                                                      <input type="text" name="in_foal_to" value="" id="in_foal_to" class="span6 m-wrap"/>
                                                    </div>
                                                </div>
						
                                                <div class="control-group">
  	                                        <label class="control-label" for="due_date">Due Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="due_date" value="" id="due_date" class="span6 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_date">Foaling Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_date" value="" id="foaling_date" class="span6 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_location">Foaling Location</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_location" value="" id="foaling_location" class="span6 m-wrap"/>
                                                    </div>
                                                </div>

                                                
                                                <div class="control-group">
                                                    <label class="control-label" for="foal_details">Foal details</label>
                                                        <div class="controls">
                                                          <textarea name="foal_details" id="foal_details" class="span6 m-wrap"></textarea>
                                                        </div>
                                                </div>				

                                                <input type="hidden" name="mare_id" value="<?php echo $id;?>"/>
                                        </div>	
                                    </div>
        
                                   <div class="form-actions">
                                         <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                         <a href="<?php echo make_admin_url('mare', 'update', 'update','id='.$id);?>" class="btn" name="cancel" > Cancel</a>
                                   </div>
                              </div> 
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    
        


