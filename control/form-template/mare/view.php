<!DOCTYPE html>
<html>
<style>
  .form2 input[type="text"],
  select {
    width: 20% !important;
    padding: 5px 6px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type="submit"] {
    width: 100%;
    background-color: #4caf50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    font-size: 18px;
    margin-top: 50px;
  }

  .form-field-heading h1 {
    padding: 10px;
    margin: 0px;
    margin-bottom: 10px;
    margin-top: 20px;
    background: #e01212;
    font-weight: normal;
    font-size: 20px;
    color: #fff;
  }

  .container {
    margin: 0 auto;
  }

  textarea {
    width: 98%;
    padding: 12px 6px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  p.form_p {
    line-height: 25px;
  }
  span.bold {
    font-weight: bold;
  }
  h2.heading_form {
    margin: 0 auto;
    text-align: center;
    margin-bottom: 50px;
  }
</style>

<head>
  <title>Page Title</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
  <link
  rel="stylesheet"
  href="http://horseapp.keithharteapp.com/assets/plugins/bootstrap/css/bootstrap-responsive.min.css"
  />
</head>
<body>
 <div class="row-fluid" style="margin-bottom: 00px;">
  <div class="span6">
    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
    <h3 class="page-title" style="margin-bottom: 5px;"><?php echo ucfirst($mare_data->mare_name); ?></h3>
    <!-- <small><strong>Departed Date : <?php //echo ($horse->departed!='' && $horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';?></strong></small> -->
  </div>
  <div class="span6 pull-right" style="margin-bottom: 10px;text-align: center;">
    <img src="assets/img/keithlogo_big.jpg" alt="Logo"/>
    <address style="margin-top: 10px;margin-bottom: 0px;font-size: 12px;">

      MILL FARM<br/>
      MILL LANE </br>
       FINCHINGFIELD<br/>
      ESSEX CM7 4PQ<br/>
      Eileen Mob: 0774 875 5548<br/>
      Keith Mob: 0774 875 5553<br/>
      Email: info@keithharte.com<br/>
      www.keithharte.com
    </address>
  </div>
</div>
<div class="container form2">
  <h2 class="heading_form">
    NEWMARKET STUD FARMERS’ ASSOCIATION<br />FREEDOM FROM INFECTION
    CERTIFICATE 2020
  </h2>
 
  <form action="<?php echo make_admin_url('mare', 'update', 'update','id='.$id.'&act=maireData'); ?>" autocomplete="off" class="form-vertical login-form" method="post" name="form_login" id="validation" >
    <p class="form_p">
      MARE &nbsp;<input type="text" style="width:70% !important;" placeholder=""  disabled value="<?php if(isset($horse->name)){echo $horse->name; } ?>"name="mare_name" />&nbsp;

      Foaling / Barren / Maiden <br />BOARDING AT &nbsp;
      <input type="text" style="width:40% !important;" placeholder="" disabled name="boarding_at" value="MILL FARM, FINCHINGFIELD, CM7 4PQ" autocomplete="off" id="boarding_at" />&nbsp;
      COVERING SIRE &nbsp;<input type="text" style="width:30% !important;" placeholder="" disabled value="<?php echo $abc[0]['covering_sire']; ?>"name="visting" id="visting" />&nbsp;<br/>
      Date of arrival at the Boarding Stud &nbsp;<input  disabled type="text" style="width:70% !important;" placeholder="" name="date_of_arrival_borading_stud" value="<?php if(isset($horse->arrived)){echo $horse->arrived; } ?>" id="date_of_arrival_borading_stud" />&nbsp;<br />

      FOAL DETAILS (if applicable) D.O.B &nbsp;<input disabled type="text" autocomplete="off" value="<?php echo $foaling_date; ?>"  id="date_of_birth" placeholder="" name="date_of_birth"/>&nbsp;
      Foal Detail &nbsp;<input type="text"placeholder="" disabled value="<?php echo $foaling_detail; ?>" name="color"/>&nbsp;<!-- Sex
      <input type="text"placeholder="" disabled value="<?php //echo $mare_data->sex; ?>"  name="sex"/> -->
      &nbsp; Sire &nbsp;<input type="text" disabled  value="<?php echo $abc[0]['in_foal_to'];?>" placeholder=""  name="sire"  />&nbsp;<br /><br/>


      I hereby declare that to the best of my knowledge the above mentioned
      resident mare is not suffering from any contagious disease likely to
      be a risk to the stallion at the stud.<br /><br />
      No EHV-1 virus abortions, cases of Strangles, stillbirths or relevant
      contagious diseases (e.g. EVA, EIA, CEM, Influenza, Salmonellosis,
      Dourine, etc.) have occurred within the last six weeks, nor are any
      such cases currently under investigation other than those who have
      received total clearance from the appropriate authorities.<br /><br />
      Signed &nbsp; <img style="width:20%" src="assets/img/sign.png" alt="Logo"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Name &nbsp;<input type="text" style="width:40% !important;" placeholder="" disabled  value="EILEEN HARTE" name="name" />&nbsp;<br/>

      Position &nbsp;<input type="text" style="width:40% !important" placeholder="" disabled value="DIRECTOR"  name="position" />&nbsp; 

      On behalf of &nbsp;<input type="text" style="width:40% !important" placeholder="" disabled  name="on_behalf" value="KEITH HARTE BLOODSTOCK LTD"  />&nbsp; <br />Date
      &nbsp;<input type="text" style="width:50% !important" placeholder="" disabled value="<?php echo $mare_data->date; ?>" id="date" name="date" />&nbsp; <br /><br />
      <span class="bold">ALL MARES</span> &nbsp;<br/>
      <input  type="checkbox" value="1" disabled placeholder="" value="1" <?= ($mare_data->is_passport_or_legible_photocopy==1)?'checked':'' ?>  name="is_passport_or_legible_photocopy" />
      &nbsp; Passport or legible photocopy. &nbsp; <br/>

      <input type="checkbox"  value="1" placeholder="" disabled  value="1"  <?= ($mare_data->is_attached_one_clitoral_swab==1)?'checked':'' ?>  name="is_attached_one_clitoral_swab" />&nbsp; Attached one Clitoral Swab taken

      on &nbsp;<input type="text" style="width:42% !important" placeholder="" disabled value="<?php echo $mare_data->attached_one_clitoral_swab; ?>"id="attached_one_clitoral_swab" name="attached_one_clitoral_swab" />&nbsp;<br/>
      <input type="checkbox" disabled value="1" <?= ($mare_data->is_endometrial==1)?'checked':'' ?> placeholder="" name="is_endometrial" />&nbsp;
      Attached one 
      Endometrial
      Swab taken on &nbsp;<input
      type="text"
      style="width:39% !important"
      placeholder="" disabled value="<?php echo $mare_data->endometrial; ?>"
      name="endometrial" id="endometrial"
      />&nbsp; &nbsp;<br/><input
      type="checkbox" 
      placeholder="" disabled value="1" <?= ($mare_data->is_negative_EVA_blood==1)?'checked':'' ?>
      name="is_negative_EVA_blood" 
      />&nbsp; Attached one Negative EVA blood test result taken after 1st
      January taken on &nbsp;<input
      type="text"
      placeholder="" disabled id="negative_EVA_blood"  value="<?php echo $mare_data->negative_EVA_blood; ?>"
      name="negative_EVA_blood"
      />&nbsp; &nbsp;<br/><input
      type="checkbox"
      placeholder="" disabled value="1" <?= ($mare_data->is_EIA_blood_test==1)?'checked':'' ?>
      name="is_EIA_blood_test"
      />&nbsp; Attached one
      EIA blood test taken after 1st January taken on &nbsp;<input
      type="text"
      placeholder="" disabled  id="EIA_blood_test"
      name="EIA_blood_test" value="<?php echo $mare_data->EIA_blood_test; ?>"
      />&nbsp;<br />
      <br />
      <span class="bold"
      >MARES ORIGINATING FROM OUTSIDE THE UNITED KINGDOM AND IRELAND</span
      ><br/>
      These mares will require negative EVA and EIA blood tests to be taken
      as detailed below. All mares should be placed in quarantine on arrival
      until satisfactory post import results have been obtained.<br /><br/>
      <span class="bold">Date of arrival in this Country</span> &nbsp;<input
      type="text"
      style="width:30% !important;"
      placeholder=""
      name="date_of_outside_arrival_in_Country" disabled value="<?php echo $mare_data->date_of_outside_arrival_in_Country; ?>" id="date_of_outside_arrival_in_Country"
      />&nbsp; <span class="bold">Country of Origin</span> &nbsp;<input
      type="text"
      style="width:30% !important;"
      placeholder="" disabled  value="<?php echo $mare_data->country_of_origin; ?>"
      name="country_of_origin"
      />&nbsp;<br />
      <input disabled type="checkbox" value="1" <?= ($mare_data->is_EVA_blood_test_no_more_than_28_days==1)?'checked':'' ?> placeholder="" name="is_EVA_blood_test_no_more_than_28_days" />&nbsp;
      Attached first EVA blood test taken no more than 28 days prior to
      arrival in this country. Taken on &nbsp;<input
      type="text" disabled
      placeholder=""
      name="EVA_blood_test_no_more_than_28_days"   value="<?php echo $mare_data->EVA_blood_test_no_more_than_28_days; ?>" id="EVA_blood_test_no_more_than_28_days"
      />&nbsp; &nbsp;<br/><input
      type="checkbox" 
      placeholder="" disabled value="1" <?= ($mare_data->is_EVA_blood_test_minimum_of_14_days==1)?'checked':'' ?>
      name="is_EVA_blood_test_minimum_of_14_days"
      />&nbsp; Attached second
      EVA blood test taken a minimum of 14 days after arrival in this
      country. Taken on &nbsp;<input
      type="text" disabled  value="<?php echo $mare_data->EVA_blood_test_minimum_of_14_days; ?>"
      placeholder="" disabled
      name="EVA_blood_test_minimum_of_14_days" id="EVA_blood_test_minimum_of_14_days"
      />&nbsp; &nbsp;<br/>
      <input
      type="checkbox" 
      placeholder="" disabled  value="1" <?= ($mare_data->is_EIA_blood_test_no_more_14_days==1)?'checked':'' ?>
      name="is_EIA_blood_test_no_more_14_days"
      />&nbsp; Attached first
      Negative EIA blood test taken no more than 14 days prior to arrival in
      this country. Taken on &nbsp;<input
      type="text"
      placeholder="" disabled value="<?php echo $mare_data->EIA_blood_test_no_more_14_days; ?>" 
      name="EIA_blood_test_no_more_14_days" id="EIA_blood_test_no_more_14_days"
      />&nbsp; &nbsp;<br/><input
      type="checkbox" value="1"
      placeholder=""value="1" <?= ($mare_data->is_EIA_blood_test_taken_min_14_days==1)?'checked':'' ?>
      name="is_EIA_blood_test_taken_min_14_days"
      />&nbsp; Attached second Negative EIA blood test taken a minimum of 14 days after arrival in
      this country. Taken on &nbsp;<input
      type="text"
      placeholder=""  disabled value="<?php echo $mare_data->EIA_blood_test_taken_min_14_days; ?>"
      name="EIA_blood_test_taken_min_14_days" id="EIA_blood_test_taken_min_14_days"
      />&nbsp; &nbsp;<br/><input
      type="checkbox" disabled value="1" <?= ($mare_data->is_EIA_blood_test_taken_within_21_days==1)?'checked':'' ?>
      placeholder=""
      name="is_EIA_blood_test_taken_within_21_days" 
      />&nbsp; Attached further Negative EIA Blood test taken within 21 days prior to covering. Taken
      on &nbsp;<input type="text" placeholder="" disabled  value="<?php echo $mare_data->EIA_blood_test_taken_within_21_days; ?>" id="EIA_blood_test_taken_within_21_days" name="EIA_blood_test_taken_within_21_days" />&nbsp; (if
      required) &nbsp;<br/><input
      type="checkbox" 
      placeholder="" disabled value="1" <?= ($mare_data->is_arrived_from_germany==1)?'checked':'' ?>
      name="is_arrived_from_germany"
      />&nbsp; Mares which arrived from Germany or Italy with a foal at foot require a negative EIA blood
      test taken from the foal a minimum of 14<br />
      days after arrival in this country. Taken on&nbsp;<input
      type="text" disabled 
      placeholder=""
      name="arrived_from_germany" value="<?php echo $mare_data->arrived_from_germany; ?>" id="arrived_from_germany"
      />&nbsp;<br /><br />
      Copies of all international travel documents should accompany the mare
      when visiting the stallion stud. Please refer to the Newmarket Stud
      Farmers’ Association Breeding Regulations 2020 for further details.<br /><br /><br />
      <span class="bold"
      >The declaration must EITHER accompany the mare at the time of
      covering or PREFERABLY to be lodged by fax at the Stud Office before
      the mare will be accepted for EACH INDIVIDUAL SERVICE.</span
      >
    </p>
    
    <input type="hidden" name="mare_details" value="mare_details_form">
    <input type="hidden" name="mare_id" value="<?= $id ?>">
    <!--  <input type="submit" value="Submit" /> -->
  </form>
</div>
</body>
</html>
<script type="text/javascript">

  $("#date_of_birth").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#date").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#attached_one_clitoral_swab").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#endometrial").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#negative_EVA_blood").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EIA_blood_test").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#date_of_outside_arrival_in_Country").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EVA_blood_test_no_more_than_28_days").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EVA_blood_test_minimum_of_14_days").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EIA_blood_test_no_more_14_days").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EIA_blood_test_taken_min_14_days").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#EIA_blood_test_taken_within_21_days").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#arrived_from_germany").datepicker({
    format: 'yyyy-mm-dd'
  });
  // $("#boarding_at").datepicker({
  //   format: 'yyyy-mm-dd'
  // });
  $("#visting").datepicker({
    format: 'yyyy-mm-dd'
  });
  $("#date_of_arrival_borading_stud").datepicker({
    format: 'yyyy-mm-dd'
  });
</script>
