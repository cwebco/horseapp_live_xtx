  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Mare</h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Mare</li>
                            </ul>
                           <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                                <div class="portlet-title">
                                        <div class="caption"><i class="icon-heart-empty"></i>Manage Mare</div>
                                        <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                        </div>
                                </div>
                                <div class="portlet-body">
                                     <form action="<?php echo make_admin_url('mare', 'update_table', 'list', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
					    <a href="<?php echo make_admin_url('mare', 'list', 'list', 'download=csv') ?>" class="bg-blue export_excl">Download In Excel</a>
					    <br><br>
                                                <thead>
                                                     <tr>
                                                        <th style="width:8px;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                                        <th class="hidden-480">Sr. No</th>
                                                        <th>Name</th>
                                                        <th class="hidden-480">Sire</th>
                                                        <th class="hidden-480">Owner Name</th>
                                                        <th>Arrived Date</th>
							<th>Departure Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      <? if($QueryObj->GetNumRows()!=0):?>
                                                     <?php $sr=1;while($horse=$QueryObj->GetObjectFromRecord()):?>
                                                            <tr class="odd gradeX">
                                                                <td class="hidden-480">
                                                                    <input class="checkboxes" id="multiopt[<?php echo $horse->id ?>]" name="multiopt[<?php echo $horse->id ?>]" type="checkbox" />
                                                                </td>
                                                                <td class="hidden-480"><?php echo $sr++;?></td>
                                                                <td><?php echo $horse->name?></td>
                                                                <td class="hidden-480"><?php echo $horse->sire?></td>
                                                                <td class="hidden-480"><?php echo str_replace(array('(',')')," ",$horse->owner_name);?></td>
                                                                <td><?php echo ($horse->arrived!='' && $horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';?></td>
								<td><?php echo $horse->departed ?></td>
                                                                <td>
                                                                     <!--<a class="btn black mini tooltips" href="<?php echo make_admin_url('mare', 'view', 'view', 'id='.$horse->id)?>" title="click to view full details"><i class="icon-zoom-in icon-white"></i> View</a>&nbsp;&nbsp;-->
                                                                     <a class="btn blue mini tooltips" href="<?php echo make_admin_url('mare', 'update', 'update', 'id='.$horse->id)?>" title="click to edit main info of mare"><i class="icon-edit icon-white"></i> Main Info</a>&nbsp;&nbsp;
                                                                     <a class="btn yellow mini tooltips" href="<?php echo make_admin_url('mare', 'update2', 'update2', 'id='.$horse->id)?>" title="click to edit season info"><i class="icon-edit icon-white"></i> Season Info</a>&nbsp;&nbsp;
                                                                     <a class="btn red mini tooltips" href="<?php echo make_admin_url('mare', 'delete', 'list', 'id='.$horse->id.'&delete=1')?>" onclick="return confirm('Are you sure?');" title="click to archive this record"><i class="icon-archive icon-white"></i> Archive</a>
                                                                </td>
                                                            </tr>
                                                     <?php endwhile;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr class="odd gradeX">
                                                        <td colspan="3" class="hidden-480">
                                                            <div style=" width:220px;float:left">
                                                                <select  name="multiopt_action" style="width:150px" class="left_align regular">
                                                                    <option value="delete">Archive</option>
                                                                </select>
                                                                <input style="float:right" type="submit" class="btn green large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                                            </div>
                                                        </td>
                                                        <td class="hidden-480"></td>
                                                        <td colspan="3"></td>
                                                    </tr> 
                                                </tfoot>
                                               <?php endif;?>  
                                        </table>
                                     </form>    
                                </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                </div>
           </div>
           <div class="clearfix"></div>
    </div>
<!-- END PAGE CONTAINER-->    
