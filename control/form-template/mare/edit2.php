<script type="text/javascript">
jQuery("#btn_msf").live("click", function(){
    var mare_id=$(this).attr("mare_id");
    var season=$(this).attr("season");
    $("#msf").html('<div style="width:100%;text-align:center;padding-top:5px;"><br/><br/><img src="assets/img/ajax-loading.gif"/><br/><br/>Please Wait...<br/><br/><br/></div>');
    var dataString = 'mare_id='+mare_id+'&season='+season;

    $.ajax({
        type: "POST",
        url: "<?php echo make_admin_url_window('editseason');?>",
        data: dataString,
        success: function(data, textStatus) {
           $("#msf").show();
           $("#msf").html(data);
           $("#msf").fadeIn(1500);
           //App.init();
           FormComponents.init();
           FormSamples.init();
        }
    });
});
</script>
<script type="text/javascript">
     jQuery(document).ready(function() {
         jQuery("#mare_status1").live("click",function(){
             var status=$(this).val();
             if(status!='In Foal'){
                 jQuery("#due_date1").attr('disabled',true);
                 jQuery("#in_foal_to1").attr('disabled',true);
                 jQuery("#foaling_date1").attr('disabled',true);
                 jQuery("#foaling_location1").attr('disabled',true);
                 jQuery("#foal_details1").attr('disabled',true);
                 jQuery("#due_date1").val('');
                 jQuery("#in_foal_to1").val('');
                 jQuery("#foaling_date1").val('');
                 jQuery("#foaling_location1").val('');
                 jQuery("#foal_details1").val('');
             }
             else{
                 jQuery("#due_date1").attr('disabled',false);
                 jQuery("#in_foal_to1").attr('disabled',false);
                 jQuery("#foaling_date1").attr('disabled',false);
                 jQuery("#foaling_location1").attr('disabled',false);
                 jQuery("#foal_details1").attr('disabled',false);
             }
         });
     });
</script>
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Mare Season Info
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-heart-empty"></i>
                                               <a href="<?php echo make_admin_url('mare', 'list', 'list');?>">List Mare</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li>
                                         <a href="<?php echo make_admin_url('mare', 'list', 'list');?>"><?php echo $horse->name;?></a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Mare Season Info
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
              <div class="row-fluid">
                <div class="span12">
                        <!-- BEGIN PORTLET-->   
                        <div class="portlet box blue">
                                <div class="portlet-title">
                                        <div class="caption"><i class="icon-heart-empty"></i>Edit Season Info: <strong>"<?php echo ucfirst($horse->name); ?>"</strong></div>
                                        <div class="tools" style="margin-top: 0px;">
                                                <a class="btn grey mini" data-toggle="modal" href="#msf_add">
                                                   <i class="icon-plus"></i> Add New Season
                                                </a>
                                                <a class="btn green mini" href="<?php echo make_admin_url('mare','update','update','id='.$id); ?>">
                                                   <i class="icon-edit"></i> Edit Main Info
                                                </a>
                                        </div>
                                </div>
                                <div class="portlet-body form">
                                        <table class="table table-hover table-striped table-bordered">
                                            <tbody>
                                              <?php if($QueryObj2->GetNumRows()!=0):?>
                                                 <?php $sr=1;while($season=$QueryObj2->GetObjectFromRecord()):?>
                                                    <tr>
                                                            <td>Season <?php echo $season->season?></td>
                                                            <td>
                                                                <a class="btn green" mare_id="<?php echo $season->mare_id?>" season="<?php echo $season->season?>" data-toggle="modal" href="#msf" id="btn_msf">Edit/View Info <i class="m-icon-swapright m-icon-white"></i></a>
                                                                <a class="btn red"  onclick="return confirm('You are deleted the season info. Are you sure?');"   href="<?php echo make_admin_url('mare','delete_season','update2','id='.$id.'&sid='.$season->id);?>" class="pull-right">Delete <i class="icon-remove m-icon-white"></i></a>
                                                            </td>
                                                    </tr>
                                                    <?php endwhile; ?>
                                                <?php else: ?>     
                                                    <div class="span12" style="text-align: center;">
                                                            <img src="assets/img/no-record-found.png"/>
                                                        </div>
                                                <?php endif; ?>
                                        </tbody>
                                        </table>
                                </div>
                        </div>
                        <!-- END PORTLET-->
                </div>
        </div>
             <!--new form div---->
            <div class="modal hide fade" id="msf_add" data-width="760">
                          <!-- / Box -->
                          <div class="portlet box blue"  style="margin-bottom: 0px;">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-heart-empty"></i>Add Season Info</div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="horizontal-form" action="<?php echo make_admin_url('mare', 'insert2', 'insert2')?>" method="POST" enctype="multipart/form-data" id="validation">
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <div class="control-group">
  	                                        <label class="control-label" for="season">Season<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="season" id="season" class="span12 m-wrap year-picker hasDatepicker"/>
                                                    </div>
                                                </div> 
                                                
						<div class="control-group">
  	                                        <label class="control-label" for="mare_type">Type</label>
                                                    <div class="controls">
                                                            <select name="mare_type" id="mare_type" class="span12 m-wrap">
                                                              <?php $query1  = new dropdownValues();
                                                                $type = $query1->getDropdown('3');
                                                                foreach($type as $kkk=>$vvv): ?>
                                                                <option value="<?php echo $vvv->value?>"><?php echo $vvv->title?></option>
                                                              <?php endforeach;?>  
                                                            </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="covering_sire">Covering sire</label>
                                                    <div class="controls">
                                                      <input type="text" name="covering_sire" value="" id="covering_sire" class="span12 m-wrap"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_service">Last service</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_service" value="" id="last_service" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
  	                                        <label class="control-label" for="current_status">Current Status</label>
                                                    <div class="controls">
                                                      <!--<input type="text" name="current_status" value="" id="current_status" class="span12 m-wrap"/>-->
                                                      <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="In Foal" />
                                                        In Foal
                                                        </label>
                                                        <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="NIF" />
                                                        NIF
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_scan">Last Scan Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_scan" value="" id="last_scan" class="span4 m-wrap new_format"/>
                                                      &nbsp;
                                                      <input type="text" name="last_scan_text" value="" id="last_scan_text" class="span6 m-wrap"/>
                                                    </div>
                                                </div>
                                                
                                        </div>	
                                        <div class="span6">
                                                <div class="control-group">
  	                                        <label class="control-label" for="mare_status">Status</label>
                                                    <div class="controls">
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status1" name="mare_status" value="In Foal" />
                                                            In Foal
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status1" name="mare_status" value="Maiden" />
                                                            Maiden
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status1" name="mare_status" value="Barren"/>
                                                            Barren
                                                            </label>
                                                    </div>
                                                </div>
                                            
                                                <div class="control-group">
  	                                        <label class="control-label" for="in_foal_to">In Foal to</label>
                                                    <div class="controls">
                                                      <input type="text" name="in_foal_to" id="in_foal_to1" class="span12 m-wrap"/>
                                                    </div>
                                                </div>
						
                                                <div class="control-group">
  	                                        <label class="control-label" for="due_date">Due Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="due_date" id="due_date1" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_date">Foaling Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_date" id="foaling_date1" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_location">Foaling Location</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_location"  id="foaling_location1" class="span12 m-wrap"/>
                                                    </div>
                                                </div>

                                                
                                                <div class="control-group">
                                                    <label class="control-label" for="foal_details">Foal details</label>
                                                        <div class="controls">
                                                          <textarea name="foal_details" id="foal_details1" class="span12 m-wrap"></textarea>
                                                        </div>
                                                </div>
                                                
                                            </div>
                                    </div>
                                    <input type="hidden" name="mare_id" value="<?php echo $id; ?>"/>
                                   <div class="form-actions">
                                         <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" />
                                         <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                   </div>
                                </form>
                            </div>
                        </div>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
             <!--update form div---->
             <div class="modal hide fade" id="msf" data-width="760"></div><!--ajax response in this div-->
             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    
        


