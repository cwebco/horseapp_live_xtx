<div class="tiles pull-right">
        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list','dd_id='.$dd_id);?>#newdropdown">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Add New Value
                        </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-red <?php echo ($Page=="dropdown_values" && $section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('dropdown_values', 'thrash', 'thrash','dd_id='.$dd_id);?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-trash"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Trash
                        </div>
                </div>
            </a> 
        </div>
        <div class="tile bg-yellow">
            <?php if($section=='thrash' || $section =='update'): ?>
                <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list','dd_id='.$dd_id);?>">
            <?php else: ?>
                <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">
            <?php endif;   ?>  
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-backward"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Back to options
                        </div>
                </div>
            </a> 
        </div>
</div>