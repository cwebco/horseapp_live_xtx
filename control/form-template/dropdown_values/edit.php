<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Dropdown</h3>
                            <ul class="breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <i class="icon-sort-by-attributes"></i>
                                    <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">List Options</a>
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list','dd_id='.$dd_id);?>">
                                      <?php $main_drop=get_object('dropdown', $dd_id);  
                                       echo $main_drop->name;?>
                                    </a>
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>Edit Option Value</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
		       <div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-sort-by-attributes"></i>
                                        Edit Option Value for <?php echo $drop_name->name;?>
                                    </div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="validation" action="<?=make_admin_url('dropdown_values', 'update', 'list', 'dd_id='.$dd_id)?>" method="post" name="payment-method" enctype="multipart/form-data"  class="form-horizontal">
                                    <!-- / Box -->
                                     <div class="control-group">
                                            <label class="control-label" for="title">Title:</label>
                                            <div class="controls">
                                            <input type="text" name="title" value="<?php echo $category->title ?>" id="title" class="span6 m-wrap validate[required]" />
                                            </div>
                                     </div>   
                                     <div class="control-group">
                                            <label class="control-label" for="value">Value:</label>
                                            <div class="controls">
                                            <input type="text" name="value" value="<?php echo $category->value ?>" id="value" class="span6 m-wrap validate[required]" />
                                            </div>
                                     </div>   
                                    <div class="control-group">
                                            <label class="control-label" for="position">Position:</label>
                                            <div class="controls">
                                            <input type="text" style="width:10%" value="<?php echo $category->position ?>"  name="position" id="position" class="span6 m-wrap" />
                                            </div>
                                     </div> 
<!--                                    <div class="control-group">
                                            <label class="control-label" for="is_active">Show on Website:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="is_active" id="is_active" value="1"  <?=($category->is_active=='1')?'checked':'';?>  />
                                            </div>  
                                     </div>-->
                                        <input type="hidden" name="id" value="<?php echo $id?>" />
                                        <input type="hidden" name="dd_id" value="<?php echo $dd_id?>" />
                                        <input type="hidden" name="is_active" value="1" />
                                        <div class="form-actions">
                                                 <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                 <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id);?>" class="btn" name="cancel" > Cancel</a>
                                                 
                                        </div>
                                </form>
                                  </div> 
                              </div>   
                       </div>
                </div>
            </div>
                        
