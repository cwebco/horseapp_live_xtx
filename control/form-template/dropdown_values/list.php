<script type="text/javascript">
        jQuery(document).ready(function() {
            
            /* on click */
            $(".status_on").live("click",function(){

                var id=$(this).attr('on');
                var parent_id="on_off_button"+id;
              
                $("#" + parent_id + " .status_on").addClass("active");
                $("#" + parent_id + " .status_off").removeClass("active");
                        
                var dataString = 'table=dropdown_values&id='+id;

                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_on')?>",
                    data: dataString,
                    success: function(data, textStatus) {}
               });
             });

            $(".status_off").live("click",function(){
                var id=$(this).attr('off');
                var parent_id="on_off_button"+id;
                
                $("#" + parent_id + " .status_on").removeClass("active");
                $("#" + parent_id + " .status_off").addClass("active");
                        
                var dataString = 'table=dropdown_values&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_off')?>",
                    data: dataString,
                    success: function(data, textStatus) {}
                });
            });
        });
</script>
<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Dropdown</h3>
                            <ul class="breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <i class="icon-sort-by-attributes"></i>
                                    <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">List Options</a>
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list','dd_id='.$dd_id);?>">
                                      <?php $main_drop=get_object('dropdown', $dd_id);  
                                       echo $main_drop->name;?>
                                    </a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
		       <div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box green">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-sort-by-attributes"></i>
                                    Manage Options Values for <?php echo $drop_name->name;?>
                                    </div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body ">
                                <form action="<?php echo make_admin_url('dropdown_values', 'update2', 'list', 'dd_id='.$dd_id);?>" method="post" id="form_data" name="form_data" >
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th style="width:8px;" class="hidden-480"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                            <th class="hidden-480">Sr. No.</th>
                                            <th>Title</th>
                                            <th class="hidden-480">Value</th>
                                            <th class="hidden-480">Position</th>
                                            <!--<th>Show on Website</th>-->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           <? if($QueryObj->GetNumRows()!=0):?>
                                            <? $sr=1;while($image=$QueryObj->GetObjectFromRecord()):?>
                                                <tr  class="odd gradeX">
                                                    <td style="width:8px;" class="hidden-480">
                                                        <input class="checkboxes" id="multiopt[<?php echo $image->id ?>]" name="multiopt[<?php echo $image->id ?>]" type="checkbox" />
                                                    </td>                                 
                                                     <td class="hidden-480"><?php echo $sr++;?></td>  
                                                     <td>   <?php echo $image->title;?></td>
                                                     <td class="hidden-480">   <?php echo $image->value;?></td>
                                                     <td class="hidden-480">
                                                         <input type="text" name="position[<?php echo $image->id?>]" value="<?=$image->position;?>" size="3" style="width:20%;" />
                                                     </td>
                                                    <!--<td>
                                                        <div class="btn-group mini_buttons on_off_button" id='on_off_button<?php //echo $image->id?>'>
                                                            <div class="btn status_on mini_buttons <?php //echo ($image->is_active=='1')?'active':'';?>" on="<?php echo $image->id;?>" rel="<?php echo ($image->is_active=='1')?'on':'off';?>" >SHOW</div>
                                                            <div class="btn status_off mini_buttons button_right <?php //echo ($image->is_active=='0')?'active':'';?>" off="<?php echo $image->id;?>" rel="<?php echo ($image->is_active=='1')?'off':'on';?>">HIDE</div>
                                                        </div>
                                                    </td>-->
                                                     <td>
                                                        <a class="btn blue mini tooltips" href="<?php echo make_admin_url('dropdown_values', 'update', 'update', 'dd_id='.$dd_id.'&id='.$image->id);?>" title="click here to edit this record"><i class="icon-edit icon-white"></i> Edit</a>&nbsp;&nbsp;
                                                        <a class="btn red mini tooltips" href="<?php echo make_admin_url('dropdown_values', 'delete', 'update', 'dd_id='.$dd_id.'&id='.$image->id);?>" onclick="return confirm('Are you sure? You are deleting this page.');" title="click here to delete this record"><i class="icon-remove icon-white"></i> Delete</a>                                       
                                                     </td>
                                                </tr>
                                            <? endwhile; ?>
                                          <?php endif;?>
                                   </tbody>
                                   <tfoot>
                                        <tr>
                                            <td colspan="3">
                                                <div style=" width:220px;float:left">
                                                    <select  name="multiopt_action" style="width:150px" class="left_align regular">
                                                        <option value="delete">Delete</option>
                                                    </select>
                                                    <input style="float:right" type="submit" class="btn grey large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                                </div>
                                            </td>
                                            <td class="hidden-480"></td>
                                            <td class="hidden-480"><input type="submit" class="btn grey" name="submit_position" value="Update" /></td>
                                            <!--<td class="hidden-480"></td>--><td class="hidden-480"></td>
                                        </tr> 
                                    </tfoot>
                                </table>
                              </form>      
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                       </div>
                </div>
             <div class="clearfix"></div>
        <!-- END PAGE CONTAINER-->    
        <!--Enter new value in dropdown-->
        <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
		       <div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-plus"></i>
                                        Add New Option Value for <?php echo $drop_name->name;?>
                                    </div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="validation" action="<?=make_admin_url('dropdown_values', 'insert', 'list', 'dd_id='.$dd_id)?>" method="post" name="payment-method" enctype="multipart/form-data"  class="form-horizontal">
                                    <!-- / Box -->
                                     <div class="control-group">
                                            <label class="control-label" for="title">Title:</label>
                                            <div class="controls">
                                            <input type="text" name="title" id="title" class="span6 m-wrap validate[required]" />
                                            </div>
                                     </div>   
                                     <div class="control-group">
                                            <label class="control-label" for="value">Value:</label>
                                            <div class="controls">
                                            <input type="text" name="value" id="value" class="span6 m-wrap validate[required]" />
                                            </div>
                                     </div>   
                                    <div class="control-group">
                                            <label class="control-label" for="position">Position:</label>
                                            <div class="controls">
                                            <input type="text" style="width:10%"  name="position" id="position" class="span6 m-wrap" />
                                            </div>
                                     </div> 
<!--                                    <div class="control-group">
                                            <label class="control-label" for="is_active">Show on Website:</label>
                                            <div class="controls">
                                                <input type="checkbox" name="is_active" id="is_active" value="1"  />
                                            </div>  
                                     </div>-->
                                        <input type="hidden" name="dd_id" value="<?php echo $dd_id?>" />
                                        <div class="form-actions">
                                                 <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                 <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id);?>" class="btn" name="cancel" > Cancel</a>
                                                 
                                        </div>
                                </form>
                                       <a name="newdropdown"></a> 
                                  </div> 
                              </div>   
                       </div>
                </div>
            </div>
                        
