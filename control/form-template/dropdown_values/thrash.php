<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">Dropdown</h3>
                            <ul class="breadcrumb">
                                <li>
                                    <i class="icon-home"></i>
                                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    <i class="icon-sort-by-attributes"></i>
                                    <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">List Options</a>
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li>
                                    Trash
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
		       <div class="span12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box red">
                            <div class="portlet-title">
                                    <div class="caption"><i class="icon-sort-by-attributes"></i>
                                    Manage Options
                                    </div>
                                    <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                    </div>
                            </div>
                            <div class="portlet-body ">
                                <form action="<?php echo make_admin_url('dropdown_values', 'update3', 'list', 'dd_id='.$dd_id);?>" method="post" id="form_data" name="form_data" >
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
                                            <th class="hidden-480">Sr. No.</th>
                                            <th>Title</th>
                                            <th>Value</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           <? if($QueryObj->GetNumRows()!=0):?>
                                            <? $sr=1;while($image=$QueryObj->GetObjectFromRecord()):?>
                                                <tr  class="odd gradeX">
                                                    <td style="width:8px;">
                                                        <input class="checkboxes" id="multiopt[<?php echo $image->id ?>]" name="multiopt[<?php echo $image->id ?>]" type="checkbox" />
                                                    </td>                                 
                                                     <td class="hidden-480"><?php echo $sr++;?></td>  
                                                     <td>   <?php echo $image->title;?></td>
                                                     <td>   <?php echo $image->value;?></td>
                                                     <td>
                                                         <a class="btn green mini tooltips" href="<?php echo make_admin_url('dropdown_values', 'restore', 'restore', 'id='.$image->id.'&dd_id='.$dd_id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i> Restore</a>&nbsp;&nbsp;
                                                         <a class="btn red mini tooltips" href="<?php echo make_admin_url('dropdown_values', 'permanent_delete', 'permanent_delete', 'id='.$image->id.'&dd_id='.$dd_id.'&delete=1')?>" onclick="return confirm('Are you deleting this item permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i> Delete</a>  
                                                     </td>
                                                </tr>
                                            <? endwhile; ?>
                                          <?php endif;?>
                                   </tbody>
                                   <tfoot>
                                        <tr>
                                            <td colspan="5">
                                                <div style=" width:220px;float:left">
                                                    <select  name="multiopt_action" style="width:150px" class="left_align regular">
                                                        <option value="delete">Delete</option>
                                                    </select>
                                                    <input style="float:right" type="submit" class="btn green large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                                </div>
                                            </td>
                                        </tr> 
                                    </tfoot>
                                </table>
                              </form>      
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                       </div>
                </div>
             <div class="clearfix"></div>
        <!-- END PAGE CONTAINER-->    
      </div>
                        
