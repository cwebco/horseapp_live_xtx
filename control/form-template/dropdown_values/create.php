<?php
 /*
  * Creating a new Business Category
  */
?>
<div class="breadCrumb two module mt15">
            <ul>
                <li class="first">
                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>
                </li>
                <li>
                     <a href="<?php echo make_admin_url('dropdown', 'list', 'list');?>">List Drop Downs</a>
                </li>
                <li class="last">
                       New Drop Down
                </li>
            </ul>
</div>
  <!--Shortcut-->
<div style="float:right;">
        <a title="Back to options" style="margin-top:10px;" href="<?php echo make_admin_url('dropdown', 'list', 'list');?>" class="tipTop btn_icon textright mrb15"><img src="images/icons/fatcow-hosting-icons/16x16/arrow_left.png" alt="" width="16"/>
           <span class="l title">Back to options</span>
        </a>
</div>
<div class="clear"></div> 
<div class="clear"></div>
   <form  id="validation"  action="<?php echo make_admin_url('dropdown', 'insert', 'list', 'id='.$id)?>" method="post" name="payment-method" enctype="multipart/form-data" class="data">
          <!-- / Box -->
	    <div class="box twothirds">
		  <div class="boxheading clearfix"><h3>Add New Drop Down for <?php echo $drop_name->name;?></h3><a class="move"></a></div>
                  <section>
                        <div class="row">
                                <label for="name">Drop Down Name:</label>
                                <input type="text" name="name" id="name" class="validate[required]" />
                        </div>
                        
                      
                       
                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                        <div  class="row">
                         <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id);?>" class="right_align btn red submit mt15" style="text-decoration: none;">Cancel</a>    
                         <input class="right_align btn green submit mt15" type="submit" name="submit" value="Submit" tabindex="7" />
                         <div class="clear"></div>
                      </div> 
                   <?php endif;?>  
                       
              </section>  
          </div> 
         <?php if(defined('RIGHT_ACTION') && RIGHT_ACTION==1):?>   
               <div class="box onethird">
                 <div class="boxheading clearfix"><h3>Action</h3><a class="move"></a></div>
                 <section>  
                          <div  class="row">
                             <input class="btn green submit mt15" type="submit" name="submit" value="Submit" tabindex="7" />
                             <a href="<?php echo make_admin_url('dropdown_values', 'list', 'list', 'dd_id='.$dd_id);?>" class="btn red submit mt15" style="text-decoration: none;">Cancel</a>
                             <div class="clear"></div>
                          </div>   

                 </section>    
              </div>    
        <?php endif;?>    
        <div class="box onethird">
             <div class="boxheading clearfix"><h3>Guidelines</h3><a class="move"></a></div>
             <section>  
                      <div  class="row">
                         <h4>How to add content</h4>
                         <ul>
                                <li>Never copy & paste content directly from third party web page or word editor. </li>
                                <li>In case you need to copy & paste large chunk of text, use simple text editor as intermediary. Firstly paste the content into text editor and then paste into the CMS window here from text editor. </li>
                         </ul>
                         <div class="clear"></div>
                      </div>   

             </section>    
        </div>    
     </form>   


  







