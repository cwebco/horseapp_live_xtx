<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('backup', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List DB Backups
                        </div>
                </div>
            </a>   
        </div>




        <div class="tile bg-blue">
            <a href="<?php echo make_admin_url('backup', 'create', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New DB Backup
                        </div>
                </div>
            </a> 
        </div>

       

</div>