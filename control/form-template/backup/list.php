<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    DB Backup
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List DB Backups</li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-upload-alt"></i>Manage DB Backups</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								
									
								</div>
							</div>
							<div class="portlet-body">
							     <form action="<?php echo make_admin_url('category', 'update2', 'list', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
                                                                                   
                                                                                        <th class="hidden-480">Sr. No</th>
                                                                                        <th>Backup Date</th>
                                                                                        <th>Download</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($news=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                   
                                                                                        
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
											<td><?php echo $news->last_updated_dt;?></td>
											<td>
                                                                   
                                                                                            <a class="btn blue mini" href="<?php echo DIR_WS_SITE_UPLOAD.'backup/'.$news->document;?>">Download Backup <i class="icon-circle-arrow-right icon-white"></i></a>
                                                                                        </td>
											<td>
                                                                                           
                                                                                               <a class="btn red mini tooltips" href="<?php echo make_admin_url('backup', 'delete', 'list', 'id='.$news->id); ?>" onclick="return confirm('Are you sure? You are deleting this page.');" title="click here to delete this record"><i class="icon-remove icon-white"></i> Delete</a>
                                                                                           
                                                                                        </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                      
                                                                       <?php endif;?>  
								</table>
                                                              
                                                             </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    


