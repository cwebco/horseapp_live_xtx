<div class="breadCrumb two module mt15">
            <ul>
                <li class="first">
                    <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>
                </li>
                <li class="last">
                       List Backups
                </li>
            </ul>
</div>
<!--Shortcut-->
<div style="float:right;">
      
     <a title="Deleted Backups" style="margin-top:10px;" href="<?php echo make_admin_url('backup', 'list', 'list');?>" class="tipTop btn_icon textright mrb15"><img src="images/icons/fatcow-hosting-icons/16x16/newspaper.png" alt="" width="16"/>
           <span class="l title"> < Back to backup</span>
        </a>
</div>
<div class="clear"></div>
<div style="display:none;" id="ajax"><!--this div if for display message after changing status--></div>
<div class="box threethirds">
            <div class="boxheading clearfix"><h3>Thrashed Backups</h3><a class="move"></a></div>
            <section>
                    <table class="display" id="tabled">
                    <thead>
                        <tr>
                            <th width="5%" class="readonly check"><input type="checkbox" /></th>
                            <th width="20%"  align="left">Backup Date</th>
                            <th width="20%" class="center" >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                           <? if($QueryObj->GetNumRows()!=0):?>
                                <?$sr=1;while($news=$QueryObj->GetObjectFromRecord()):?>
                                     <tr>
                                            <td width="5%" class="readonly check"><input type="checkbox" /></td>    
                                            <td width="20%" class="left" ><?php echo $news->last_updated_dt;?></td>
                                            <td class="left" width="20%">
                                                    <a href="<?php echo make_admin_url('backup', 'delete', 'list', 'id='.$news->id); ?>" onclick="return confirm('Are you sure? You are deleting this backup.');" class="tipTop smallbtn" title="Delete backup" > <?php echo get_control_icon('delete');?></a>
                                            </td>
                                     </tr>

                                <? endwhile; ?>
                          <?php endif;?>
                   </tbody>
                </table>
             <div class="clear">&nbsp;</div>
             </section>
</div>