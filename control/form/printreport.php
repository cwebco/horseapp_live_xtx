<html>
    <head>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        <style>
            .table thead tr th {
                font-weight: 900;
            }
            .table tr td {
                font-size:13px;
                font-weight: normal;
            }
        </style>
    </head>
<!-- section coding -->
<?php
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
isset($_GET['from_date'])?$from_date=$_GET['from_date']:$from_date='';
isset($_GET['to_date'])?$to_date=$_GET['to_date']:$to_date='';
isset($_GET['type'])?$type=$_GET['type']:$type='';
isset($_GET['sortby'])?$sortby=$_GET['sortby']:$sortby='';

/*
if($type=='service'):
    //$QueryObj = new mare();
    //$QueryObj -> printServiceReport($from_date,$to_date); 
    $QueryObj = new query("(select * from mare_info ORDER BY last_service desc) as m ");
    if($from_date!='' && $to_date!=''):
        $date_obj = new date();
        $from = $date_obj->TocustomDate($from_date);
        $to = $date_obj->TocustomDate($to_date);
        $QueryObj -> Where=" where m.last_service >='$from' and m.last_service <='$to' GROUP BY m.mare_id";
    else:
        $QueryObj -> Where=" where 1=1 GROUP BY m.mare_id";
    endif;
    $QueryObj -> DisplayAll();
    
elseif($type=='paddock'): 
    $QueryObj = new paddock_log();
    $QueryObj -> printPaddockReport($from_date,$to_date); 
    
else:
    //$QueryObj = new horse_log();
    //$QueryObj->printReport($type,$from_date,$to_date);  
    $QueryObj = new query('(select * from horse_log,horse where type="'.$type.'" and horse_log.horse_id = horse.id and horse.is_deleted=0 order by date desc) as t');
    if($from_date!='' && $to_date!=''):
       $date_obj = new date();
       $fromdate = $date_obj->TocustomDate($from_date);
       $todate = $date_obj->TocustomDate($to_date);
       $QueryObj -> Where=" where t.date >='$fromdate' and t.date <='$todate' GROUP BY t.horse_id order by t.date desc";
    else:
        $QueryObj -> Where=" where 1=1 GROUP BY t.horse_id order by t.date desc";
    endif;
    $QueryObj ->DisplayAll();
endif;
*/
 if($sortby!=''):
         if($type=='service'):
                        $QueryObj = new query("(select * from mare_info ORDER BY last_service desc) AS m ,horse AS h");
                        if($from_date!='' && $to_date!=''):
                            $date_obj = new date();
                            $from = $date_obj->TocustomDate($from_date);
                            $to = $date_obj->TocustomDate($to_date);
                            $QueryObj -> Where=" where m.last_service >='$from' and m.last_service <='$to' AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id order by ";
                        else:
                            $QueryObj -> Where=" where 1=1 AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id order by ";
                        endif;                       
                        if($sortby=='name'):
                            $QueryObj -> Where.=" h.name ASC"; 
                        elseif($sortby=='season'):
                            $QueryObj -> Where.=" m.season desc";
                        elseif($sortby=='type'):
                            $QueryObj -> Where.=" m.mare_type ASC";
                         elseif($sortby=='last_service'):
                            $QueryObj -> Where.=" m.last_service ASC";
                        endif;
                        $QueryObj ->DisplayAll();
                        
                    elseif($type=='paddock'): 
                        $QueryObj = new query('paddock_log AS p,horse AS h');
                        if($from_date!='' && $to_date!=''):
                            $date_obj = new date();
                            $from_date = $date_obj->TocustomDate($from_date);
                            $to_date = $date_obj->TocustomDate($to_date);
                            $QueryObj->Where=" where p.date >='$from_date' and p.date <='$to_date' AND  p.horse_id = h.id AND h.is_deleted = '0' GROUP BY p.id ORDER BY ";
                        else:
                            $QueryObj->Where=" where 1=1  AND  p.horse_id = h.id AND h.is_deleted = '0'  GROUP BY p.id  ORDER BY ";
                        endif;
                         if($sortby=='name'):
                            $QueryObj -> Where.=" h.name ASC"; 
                        elseif($sortby=='date'):
                            $QueryObj -> Where.=" p.date desc";
                        elseif($sortby=='paddock_title'):
                            $QueryObj -> Where.=" p.paddock_title ASC";
                         elseif($sortby=='move'):
                            $QueryObj -> Where.=" p.move ASC";
                        endif;
                        $QueryObj->DisplayAll();
                        
                    elseif($type=='ehv' || $type=='flu'): 
                       
                         //SELECT * FROM `horse` LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type="ehv" where horse.is_deleted="0"
                       
                        $QueryObj = new query('horse');
                        if($from_date!='' && $to_date!=''):
                           $date_obj = new date();
                           $fromdate = $date_obj->TocustomDate($from_date);
                           $todate = $date_obj->TocustomDate($to_date);
                           $QueryObj -> Where=" LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type' and horse_log.date >='$fromdate' and horse_log.date <='$todate'";
                        else:
                           $QueryObj -> Where=" LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type'";
                        endif;
                        $QueryObj -> Where.=" where horse.is_deleted='0' order by ";
                        if($sortby=='name'):
                            $QueryObj -> Where.=" horse.name ASC"; 
                        elseif($sortby=='date'):
                            $QueryObj -> Where.=" horse_log.date desc";
                        elseif($sortby=='type'):
                            $QueryObj -> Where.=" horse_log.value ASC";
                        endif;
                        $QueryObj ->DisplayAll();
                       // $rex= $QueryObj ->ListOfAllRecords();
                      //echo'<pre>';  print_r($rex); exit;
                        
                    else:
                       
                        $QueryObj = new query('(select * from horse_log where type="'.$type.'" order by date desc) as t,horse AS h');
                        if($from_date!='' && $to_date!=''):
                           $date_obj = new date();
                           $fromdate = $date_obj->TocustomDate($from_date);
                           $todate = $date_obj->TocustomDate($to_date);
                           $QueryObj -> Where=" where t.horse_id = h.id AND h.is_deleted = '0' AND t.date >='$fromdate' and t.date <='$todate' GROUP BY t.id order by ";
                        else:
                           $QueryObj -> Where=" where 1=1 AND t.horse_id = h.id AND h.is_deleted = '0' GROUP BY t.id order by ";
                        endif;
                        if($sortby=='name'):
                            $QueryObj -> Where.=" h.name ASC"; 
                        elseif($sortby=='date'):
                            $QueryObj -> Where.=" t.date desc"; 
                        elseif($sortby=='value'):
                            $QueryObj -> Where.=" t.value ASC";
                        endif;
                        $QueryObj ->DisplayAll();
                    endif; 
                              
    else:
if($type=='service'):
   
    $QueryObj = new query("(select * from mare_info ORDER BY last_service desc) AS m ,horse AS h");
    if($from_date!='' && $to_date!=''):
        $date_obj = new date();
        $from = $date_obj->TocustomDate($from_date);
        $to = $date_obj->TocustomDate($to_date);
        $QueryObj -> Where=" where m.last_service >='$from' and m.last_service <='$to' AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id";
    else:
        $QueryObj -> Where=" where 1=1 AND  m.mare_id = h.id AND h.is_deleted = '0' GROUP BY m.mare_id";
    endif;
    $QueryObj -> DisplayAll();

elseif($type=='paddock'): 
    $QueryObj = new query('paddock_log AS p,horse AS h');
    if($from_date!='' && $to_date!=''):
        $date_obj = new date();
        $from_date = $date_obj->TocustomDate($from_date);
        $to_date = $date_obj->TocustomDate($to_date);
        $QueryObj->Where=" where p.date >='$from_date' and p.date <='$to_date' AND  p.horse_id = h.id AND h.is_deleted = '0' GROUP BY p.id ORDER BY p.date desc,p.horse_id desc";
    else:
        $QueryObj->Where=" where 1=1  AND  p.horse_id = h.id AND h.is_deleted = '0'  GROUP BY p.id  ORDER BY p.date desc,p.horse_id desc";
    endif;
    $QueryObj->DisplayAll();
elseif($type=='ehv' || $type=='flu'):
     //SELECT * FROM `horse` LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type="ehv" where horse.is_deleted="0"
    $QueryObj = new query('horse');
    if($from_date!='' && $to_date!=''):
       $date_obj = new date();
       $fromdate = $date_obj->TocustomDate($from_date);
       $todate = $date_obj->TocustomDate($to_date);
       $QueryObj -> Where=" LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type' and horse_log.date >='$fromdate' and horse_log.date <='$todate'";
    else:
       $QueryObj -> Where=" LEFT JOIN horse_log on horse.id=horse_log.horse_id and horse_log.type='$type'";
    endif;
    $QueryObj -> Where.=" where horse.is_deleted='0' order by horse_log.date desc";
    $QueryObj -> DisplayAll();

else:
    $QueryObj = new query('(select * from horse_log where type="'.$type.'" order by date desc) as t,horse AS h');
    if($from_date!='' && $to_date!=''):
       $date_obj = new date();
       $fromdate = $date_obj->TocustomDate($from_date);
       $todate = $date_obj->TocustomDate($to_date);
       $QueryObj -> Where=" where t.horse_id = h.id AND h.is_deleted = '0' AND t.date >='$fromdate' and t.date <='$todate' GROUP BY t.id order by t.date desc";
    else:
       $QueryObj -> Where=" where 1=1 AND t.horse_id = h.id AND h.is_deleted = '0' GROUP BY t.id order by t.date desc";
    endif;
    $QueryObj ->DisplayAll();
endif;
endif;
?>
<body>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span6 pull-left">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title"><?php echo ucfirst(($type=='service')?'Last Service':$type); ?> Report</h3>
                        <small><?php if($from_date!='' && $to_date!=''): ?>
                                        <?php echo 'Date :'.$from_date.' - '.$to_date;?> 
                                    <?php else: ?>
                                        <?php echo ' - Complete' ;?>
                                    <?php endif; ?>
                        </small>
                </div>
            <div class="span6 pull-right">
                <img src="assets/img/kaithlogo.png" alt="Logo"/>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row-fluid invoice">
                             <table class="table table-bordered table-hover">
                                        <thead>
                                                 <tr>
                                                        <th class="hidden-480">#</th>
                                                        <th>Horse Name</th>
<!--                                                        <th class="hidden-480">Sex</th>-->

                                                        <?php if($type=='paddock'): ?>
                                                            <th>Paddock</th>
                                                            <th class="hidden-480">Move</th>
                                                        <?php elseif($type=='service'): ?>
                                                            <th class="hidden-480">Type</th>
                                                            <th>Season</th>
                                                        <?php elseif($type=='flu' || $type=='ehv'): ?>
<!--                                                            <th class="hidden-480">Owner</th>-->
                                                            <th>Type</th>
                                                        <?php else: ?>
<!--                                                            <th class="hidden-480">Owner</th>-->
                                                            <!--<th>Arrived Date</th>-->
                                                        <?php endif; ?>

                                                        <?php if($type=='service'): ?>
                                                            <th>Last Service</th>
                                                        <?php else: ?>
                                                            <th>Date</th>
                                                        <?php endif; ?>
                                                        <?php if($type=='wormer'): ?>
                                                            <th>Value</th>
                                                        <?php endif; ?>
                                                </tr>
                                        </thead>
                                         <tbody>
                                              <?php if($QueryObj->GetNumRows()!=0):?>
                                             <?php $sr=1;while($log=$QueryObj->GetObjectFromRecord()):?>
                                             <?php if($type=='service'): ?>
                                                <?php $horse = getHorseSelectedDetails($log->mare_id); ?>
                                             <?php else: ?>
                                                <?php $horse = getHorseSelectedDetails($log->horse_id); ?>
                                             <?php endif; ?>
                                                    <tr class="odd gradeX">
                                                        <td class="hidden-480"><?php echo $sr++;?></td>
                                                        <td><?php echo ucfirst($log->name);?></td>
                                                        
                                                         <?php //if($horse->sex=='stallion'): ?>
                                                            <!--<td>Horse</td>-->
                                                        <?php //else: ?>
                                                            <!--<td><?php echo ucfirst($horse->sex); ?></td>-->
                                                        <?php //endif; ?>

                                                        <?php if($type=='paddock'): ?>
                                                            <td><?php echo ucfirst($log->paddock_title);?></td>
                                                            <td class="hidden-480"><?php echo ucwords($log->move);?></td>
                                                        <?php elseif($type=='service'): ?>
                                                            <td class="hidden-480"><?php echo ucfirst($log->mare_type);?></td>
                                                            <td><?php echo $log->season;?></td>
                                                        <?php elseif($type=='flu' || $type=='ehv'): ?>
<!--                                                            <td class="hidden-480"><?php echo ucfirst($horse->owner_name);?></td>-->
                                                            <td><?php echo $log->value; ?></td>
                                                        <?php else: ?>
<!--                                                            <td class="hidden-480"><?php echo ucfirst($horse->owner_name);?></td>-->
                                                            <!--<td><?php echo ($horse->arrived!='0000-00-00' && $horse->arrived!='')?date('d/m/Y',  strtotime($horse->arrived)):'';?></td>-->
                                                        <?php endif; ?>

                                                        <?php if($type=='service'): ?>
                                                            <td><?php echo ($log->last_service!='0000-00-00' && $log->last_service!='')?date('d/m/Y',  strtotime($log->last_service)):'';?></td>
                                                        <?php else: ?>
                                                            <td><?php echo ($log->date!='0000-00-00' && $log->date!='')?date('d/m/Y',strtotime($log->date)):'';?></td>
                                                        <?php endif; ?>
                                                        <?php if($type=='wormer'): ?>
                                                            <td><?php echo $log->value; ?></td>
                                                        <?php endif; ?>    
                                                    </tr>
                                             <?php endwhile;?>
                                        </tbody>
                                       <?php endif;?>  
                                </table>  
                            <hr />
                            <div class="row-fluid" style="height: 30px;"></div>
                        </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
    <div class="clearfix"></div> 
</body>
<script type="text/javascript">
    $(window).load(function(){
        window.print(); 
        setTimeout('window.close()', 1000);  
    });
</script>
</html>
