<?php
#handle sections here.
switch ($section):
	case 'list':	
		?>
                       <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							Dashboard
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>Dashboard</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="dashboard">
					<!-- BEGIN DASHBOARD STATS -->
					<div class="row-fluid">
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                                                    <a class="more" href="<?php echo make_admin_url('horse', 'list', 'list');?>">
							<div class="dashboard-stat blue">
								<div class="visual">
									<i class="icon-list"></i>
								</div>
								<div class="details">
									<div class="number">Horses</div>
									<div class="desc">List of all Active horses</div>
								</div>
						             
							</div>
                                                        </a>    
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                                                    <a class="more" href="<?php echo make_admin_url('stallion', 'list', 'list');?>">
							<div class="dashboard-stat green">
								<div class="visual">
									<i><img width="64" src="<?php echo DIR_WS_SITE; ?>assets/img/horse-filled.png"/></i>
								</div>
								<div class="details">
                                                                        <div class="number">Horse</div>
									<div class="desc">View/Edit/Add Horses</div>
								</div>
							              
							</div>
                                                        </a>   
						</div>
						<div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
                                                    <a class="more" href="<?php echo make_admin_url('mare', 'list', 'list');?>">
							<div class="dashboard-stat purple">
								<div class="visual">
									<img width="64" src="<?php echo DIR_WS_SITE; ?>assets/img/horse.png"/>
								</div>
								<div class="details">
                                                                    <div class="number">Mare</div>
									<div class="desc">View/Edit/Add Mares </div>
								</div>
							</div>
                                                        </a> 
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
                                                    <a class="more" href="<?php echo make_admin_url('reports', 'list', 'list');?>">
							<div class="dashboard-stat yellow">
								<div class="visual">
									<i class="icon-print"></i>
								</div>
								<div class="details">
                                                                        <div class="number">Reports</div>
									<div class="desc">Print reports of all active horses.</div>
								</div>								                
							</div>
                                                    </a> 
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
<!--					<div class="clearfix"></div> 
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <div class="portlet box purple ">
                                                    <div class="portlet-title">
                                                        <div class="caption"><i class="icon-reorder"></i>Records</div>
                                                        <div class="tools">
                                                            <a href="javascript:;" class="collapse"></a>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <h4></h4>
                                                        <div id="pie_chart_9" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="span6 responsive" data-tablet="span12 fix-offset" data-desktop="span6">
						 BEGIN EXAMPLE TABLE PORTLET
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-sun"></i>Recent Foaling Dates</div>
								<div class="tools">
                                                                    <a href="javascript:;" class="collapse"></a>
                                                                </div>
							</div>
							<div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover" id="sample_2" style="min-height: 225px;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th class="hidden-480">Foaling Date</th>
                                                                        <th>Due Date</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php $mare_info = allInFoalMares(); ?> 
                                                                    <?php if(!empty($mare_info)): ?>
                                                                        <?php foreach($mare_info as $kk=>$vv): ?>
                                                                            <tr class="odd gradeX">
                                                                                <td><?php echo $vv['name'];?></td>
                                                                                <td class="hidden-480"><?php echo date('d M Y',strtotime($vv['foaling_date']));?></td>
                                                                                <td><?php echo date('d M Y',strtotime($vv['due_date']));?></td>
                                                                                <td>
                                                                                    <a href="<?php echo make_admin_url('mare','update2','update2','id='.$vv['id']); ?>">
                                                                                    <span class="label label-success"><i class="icon-zoom-in"></i> 
                                                                                        View
                                                                                    </span>
                                                                                        </a>
                                                                                </td>
                                                                            </tr>
                                                                   <?php endforeach; ?>
                                                                 <?php endif; ?>
                                                                </tbody>
                                                            </table>
							</div>
						</div>
						 END EXAMPLE TABLE PORTLET
					</div>
                                       
                                        </div>
                                        <div class="clearfix"></div> 
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <div class="portlet box blue ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i><img style="margin-top: -15px;" width="16" src="<?php echo DIR_WS_SITE; ?>assets/img/horse-filled.png"/></i>
                                                            Stallions
                                                        </div>
                                                        <div class="actions">
                                                                <a href="<?php echo make_admin_url('stallion','list','list');?>" class="btn green mini"><i class="icon-list-alt"></i> View All</a>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover" id="sample3" style="min-height: 225px;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th class="hidden-480">Paddock</th>
                                                                        <th>Arrived</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if($QueryObj->GetNumRows()>0):?>
                                                                   <?php while($horses=$QueryObj->GetObjectFromRecord()):?>
                                                                    <tr class="odd gradeX">
                                                                        <td><?php echo $horses->name;?></td>
                                                                        <td class="hidden-480"><?php echo $horses->name;?></td>
                                                                        <td><?php echo $horses->name;?></td>
                                                                        <td>
                                                                            <a href="<?php echo make_admin_url('horse','view','view','id='.$horses->id); ?>">
                                                                            <span class="label label-success"><i class="icon-zoom-in"></i> 
                                                                                View
                                                                            </span>
                                                                            </a>
                                                                            <a href="<?php echo make_admin_url('mare','update','update','id='.$horses->id); ?>">
                                                                            <span class="label label-info"><i class="icon-edit"></i> 
                                                                                edit
                                                                            </span>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                   <?php endwhile; ?>
                                                                    <?php else: ?>
                                                                    <tr><td colspan="3">No Record Found</td><td class="hidden-480"></td></tr>
                                                                    <?php endif; ?>
                                                                </tbody>
                                                            </table>
							</div>
                                                </div>
                                            </div>
                                            <div class="span6 responsive" data-tablet="span12 fix-offset" data-desktop="span6">
						 BEGIN EXAMPLE TABLE PORTLET
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption">
                                                                    <i><img style="margin-top: -15px;" width="16" src="<?php echo DIR_WS_SITE; ?>assets/img/horse.png"/></i>
                                                                    Mares</div>
                                                                <div class="actions">
									<a href="<?php echo make_admin_url('mare','list','list');?>" class="btn red mini"><i class="icon-list-alt"></i> View All</a>
								</div>
							</div>
							<div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-hover" id="sample2" style="min-height: 225px;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th class="hidden-480">Paddock</th>
                                                                        <th>Arrived</th>
                                                                        <th>Actions</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if($QueryObj1->GetNumRows()>0):?>
                                                                   <?php while($mares=$QueryObj1->GetObjectFromRecord()):?>
                                                                    <tr class="odd gradeX">
                                                                        <td><?php echo $mares->name;?></td>
                                                                        <td class="hidden-480"><?php echo $mares->name;?></td>
                                                                        <td><?php echo $mares->name;?></td>
                                                                        <td>
                                                                            <a href="<?php echo make_admin_url('horse','view','view','id='.$mares->id); ?>">
                                                                            <span class="label label-success"><i class="icon-zoom-in"></i> 
                                                                                View
                                                                            </span>
                                                                            </a>
                                                                            <a href="<?php echo make_admin_url('mare','update','update','id='.$mares->id); ?>">
                                                                            <span class="label label-info"><i class="icon-edit"></i> 
                                                                                edit
                                                                            </span>
                                                                            </a>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                   <?php endwhile; ?>
                                                                    <?php else: ?>
                                                                    <tr><td colspan="3">No Record Found</td><td class="hidden-480"></td></tr>
                                                                    <?php endif; ?>
                                                                </tbody>
                                                            </table>
							</div>
						</div>
						 END EXAMPLE TABLE PORTLET
					</div>
                                        </div>-->
                                        <div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->   
		<?
			
		break;
	case 'insert':
		#html code here.
		break;
	case 'update':
		#html code here.
		break;
	case 'delete':
		#html code here.
		break;
	default:break;
endswitch;
?>
<script>
var Charts = function () {
    return {
        //main function to initiate the module
        initPieCharts: function () {
            var data = [];
            data[0]={label:"Stallion (<?php echo $total_s; ?>)",data:<?php echo $total_s; ?>}
            data[1]={label:"Mare (<?php echo $total_m; ?>)",data:<?php echo $total_m; ?>}
            // GRAPH 9
            $.plot($("#pie_chart_9"), data, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            tilt: 0.5,
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function (label, series) {
                                    return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + Math.round(series.percent) + '%</div>';
                                },
                                background: {
                                    opacity: 0.8
                                }
                            },
                            combine: {
                                color: '#000',
                                threshold: 0.1
                            }
                        }
                    },
                    legend: {
                        show: true
                    }
                });
        }
    };
}();
</script>