<html>
    <head>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        <style>
            .title{
                padding: 8px 10px 2px 10px;
                border-bottom: 1px solid #eee;
                color: #000 !important;
                font-size: 18px;
                font-weight: 400;
            }
            .invoice table{
                margin-top: 10px;
            }
            .table_outer{
                padding:2%;
                margin-bottom:20px;
            }
        </style>
    </head>
<!-- section coding -->
<?php
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['print'])?$print=$_GET['print']:$print='0';

/*get horse main info*/
$owners = array();
foreach(getOwnerDetails() as $ak=>$av):
      $av['horses'] = horse::getOwnerWithHorses($av['id']);
      $owners[] = $av;
endforeach;
?>
<body>
    <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid print_layout"  <?php echo ($print==1)?'style="margin:0px;width:94%;"':'';?>>
            <!--include template file for print out-->
            <?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/reports/ownerreport.php');?>
        </div>
    <!-- END PAGE CONTAINER-->    
</body>
<?php if($print=='1'): ?>
    <script type="text/javascript">
        $(window).load(function(){
            window.print(); 
            setTimeout('window.close()', 1000);  
        });
    </script>
<?php endif; ?>
</html>