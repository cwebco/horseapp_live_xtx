<html>
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        <style>
            .table thead tr th {
                font-weight: 900;
            }
            .table tr td {
                font-size:13px;
                font-weight: normal;
            }
        </style>
    </head>
    <!-- section coding -->
    <?php
    include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');
    isset($_GET['sex']) ? $sex = $_GET['sex'] : $sex = 'horse';
    isset($_GET['print']) ? $print = $_GET['print'] : $print = '0';

    if ($sex == 'stallion'):
        $QueryObj = new horse();
        $QueryObj->getAllstallions2();
        $total = $QueryObj->GetNumRows();
    elseif ($sex == 'mare'):
        $QueryObj = new horse();
        $QueryObj->getAllmares2();
        $total = $QueryObj->GetNumRows();
    else:
        $QueryObj = new horse();
        $QueryObj->getAllHorses2();
        $total = $QueryObj->GetNumRows();
    endif;
    ?>
    <body>
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid print_layout" <?php echo ($print == 1) ? 'style="margin:0px;width:94%;"' : ''; ?>>
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span6">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h5 class="page-title" style="font-size:23px;">Print Horses Report with Notes</h5>
                    <small>Date: <?php echo date('d M Y'); ?></small>
                </div>
                <div class="span6 pull-right">
                    <img src="assets/img/kaithlogo.png" alt="Logo"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- END PAGE HEADER-->
            <div class="row-fluid invoice">
                <div style="overflow-y: hidden"> 
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <?php if ($sex == 'stallion'): ?>
                                    <th>Sex</th>
                                <?php elseif ($sex == 'mare'): ?>
                                    <th>Sire</th>    
                                <?php else: ?>
                                    <th>Sex</th>
                                <?php endif; ?>
                                <th>Arrival Date</th>
                                <th width="20%">Owner</th>
                                <th>Returned Date</th>
                                <th>Posted Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? if($QueryObj->GetNumRows()!=0):?>
                            <?php
                            $sr = 1;
                            while ($horse = $QueryObj->GetObjectFromRecord()):
                                $query = new horse_notes;
                                $notes_detail = $query->by_horseid($horse->id);

                                $total_notes = horse_notes::total_notes($horse->id);
                                $query = new horse_log();
                                $count_getLogs = $query->count_getLogs($horse->id, 'farrier');
                                $query = new horse_log();
                                $count_getLogs_wormer = $query->count_getLogs($horse->id, 'wormer');
                                ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $sr++; ?></td>
                                    <td><?php echo $horse->name ?></td>
                                    <?php if ($sex == 'stallion'): ?>
                                        <td><?php echo $horse->horse_sex; ?></td>
                                    <?php elseif ($sex == 'mare'): ?>
                                        <td><?php echo $horse->sire; ?></td>
                                    <?php else: ?>
                                        <?php if ($horse->sex == 'stallion'): ?>
                                            <td>Horse</td>
                                        <?php else: ?>
                                            <td><?php echo ucfirst($horse->sex); ?></td>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <td><?php echo $horse->arrived ?></td>
                                    <td><?php echo $horse->owner_name ?></td>
                                    <td><?php echo $horse->date ?></td>
                                    <td><?php echo $horse->posted_date ?></td>
                                </tr>
                            <?php endwhile; ?>
                        </tbody>
                        <?php else: ?>
                        <tr>
                            <td colspan="6">
                                No record Found;
                            </td>
                        </tr>
                        <?php endif; ?>  
                        </tbody>
                    </table>
                </div>
                <hr />
                <div class="row-fluid hidden-print">
                    <div class="clearfix" style="height:20px;"></div>
                    <div class="span11 invoice-block">
                        <a class="btn blue big pull-right" target="_blank" href="<?php echo make_admin_url('client', 'list', 'list', 'sex=' . $sex . '&print=1'); ?>">Print <i class="icon-print icon-big"></i></a>
                        <a class="btn green big pull-right" style="margin-right:20px;" href="javascript:history.back()">Back <i class="icon-print icon-backward"></i></a>
                    </div>
                </div>
                <div class="clearfix" style="heigth:30px;"></div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->    
    </body>
    <?php if ($print == '1'): ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                setTimeout('window.close()', 1000);
            });
        </script>
    <?php endif; ?>
</html>