<?php
isset($_POST['mare_id'])?$mare_id=$_POST['mare_id']:$mare_id='0';
isset($_POST['season'])?$season=$_POST['season']:$season='';

include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');
/*GET INFO FROM SEASON INFO TABLE*/
$q = new query('mare_info');
$q->Where=" where mare_id='$mare_id' AND season='$season'";
$mare_info = $q->DisplayOne();
?>
<script type="text/javascript">
         var mare_status = '<?php echo $mare_info->mare_status; ?>';
         if(mare_status!='In Foal'){
                 $("#due_date").attr('disabled',true);
                 jQuery("#in_foal_to").attr('disabled',true);
                 jQuery("#foaling_date").attr('disabled',true);
                 jQuery("#foaling_location").attr('disabled',true);
                 jQuery("#foal_details").attr('disabled',true);
                 jQuery("#due_date").val('');
                 jQuery("#in_foal_to").val('');
                 jQuery("#foaling_date").val('');
                 jQuery("#foaling_location").val('');
                 jQuery("#foal_details").val('');
         }
         jQuery("#mare_status").live("click",function(){
             var status=$(this).val();
             if(status!='In Foal'){
                 jQuery("#due_date").attr('disabled',true);
                 jQuery("#in_foal_to").attr('disabled',true);
                 jQuery("#foaling_date").attr('disabled',true);
                 jQuery("#foaling_location").attr('disabled',true);
                 jQuery("#foal_details").attr('disabled',true);
                 jQuery("#due_date").val('');
                 jQuery("#in_foal_to").val('');
                 jQuery("#foaling_date").val('');
                 jQuery("#foaling_location").val('');
                 jQuery("#foal_details").val('');
             }
             else{
                 jQuery("#due_date").attr('disabled',false);
                 jQuery("#in_foal_to").attr('disabled',false);
                 jQuery("#foaling_date").attr('disabled',false);
                 jQuery("#foaling_location").attr('disabled',false);
                 jQuery("#foal_details").attr('disabled',false);
                 jQuery("#due_date").val('<?php echo $mare_info->due_date; ?>');
                 jQuery("#in_foal_to").val('<?php echo $mare_info->in_foal_to; ?>');
                 jQuery("#foaling_date").val('<?php echo $mare_info->foaling_date; ?>');
                 jQuery("#foaling_location").val('<?php echo $mare_info->foaling_location; ?>');
                 jQuery("#foal_details").val('<?php echo $mare_info->foal_details; ?>');
             }
         });
</script>
<!--update form div---->
                          <!-- / Box -->
                          <div class="portlet box blue" style="margin-bottom: 0px;">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-heart-empty"></i>Edit Season Info: <strong>"<?php echo $mare_info->season;?>"</strong></div>
                                        </div>
                                    <div class="portlet-body form">
                                        <form class="horizontal-form" action="<?php echo make_admin_url('mare', 'update2', 'update2','id='.$mare_id)?>" method="POST" enctype="multipart/form-data" id="validation">
                                        <div class="row-fluid">
                                            <div class="span6">
						<div class="control-group">
  	                                        <label class="control-label" for="mare_type">Type</label>
                                                    <div class="controls">
                                                            <select name="mare_type" id="mare_type" class="span12 m-wrap">
                                                              <?php $query1  = new dropdownValues();
                                                                $type = $query1->getDropdown('3');
                                                                foreach($type as $kkk=>$vvv): ?>
                                                                <option value="<?php echo $vvv->value?>" <?php echo ($vvv->value==$mare_info->mare_type)?'selected':'';?>><?php echo $vvv->title?></option>
                                                              <?php endforeach;?>  
                                                            </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="covering_sire">Covering sire</label>
                                                    <div class="controls">
                                                      <input type="text" name="covering_sire" value="<?php echo $mare_info->covering_sire; ?>" id="covering_sire" class="span12 m-wrap"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_service">Last service</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_service" value="<?php echo ($mare_info->last_service!='' && $mare_info->last_service!='0000-00-00')?date('d/m/Y',  strtotime($mare_info->last_service)):'';?>" id="last_service" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
											
                                                <div class="control-group">
  	                                        <label class="control-label" for="current_status">Current Status</label>
                                                    <div class="controls">
                                                      <!--<input type="text" name="current_status" value="<?php echo $mare_info->current_status; ?>" id="current_status" class="span12 m-wrap"/>-->
                                                        <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="In Foal" <?php echo ("In Foal"==$mare_info->current_status)?'checked':'';?> style="margin-left: 0px;"/>
                                                        In Foal
                                                        </label>
                                                        <label class="radio">
                                                        <input type="radio" id="current_status" name="current_status" value="NIF" <?php echo ("NIF"==$mare_info->current_status)?'checked':'';?> style="margin-left: 0px;"/>
                                                        NIF
                                                        </label> 
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="last_scan">Last Scan Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="last_scan" value="<?php echo ($mare_info->last_scan!='' && $mare_info->last_scan!='0000-00-00')?date('d/m/Y',  strtotime($mare_info->last_scan)):'';?>" id="last_scan" class="span4 m-wrap new_format"/>
                                                      &nbsp;
                                                      <input type="text" name="last_scan_text" value="<?php echo $mare_info->last_scan_text; ?>" id="last_scan_text" class="span6 m-wrap"/>
                                                    </div>
                                                </div>
                                                
                                        </div>	
                                            <div class="span6">
                                                <div class="control-group">
  	                                        <label class="control-label" for="mare_status">Status</label>
                                                    <div class="controls">
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="In Foal" <?php echo ("In Foal"==$mare_info->mare_status)?'checked':'';?> style="margin-left: 0px;"/>
                                                            In Foal
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="Maiden" <?php echo ("Maiden"==$mare_info->mare_status)?'checked':'';?> style="margin-left: 0px;"/>
                                                            Maiden
                                                            </label>
                                                            <label class="radio">
                                                            <input type="radio" id="mare_status" name="mare_status" value="Barren" <?php echo ("Barren"==$mare_info->mare_status)?'checked':'';?> style="margin-left: 0px;"/>
                                                            Barren
                                                            </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="in_foal_to">In Foal to</label>
                                                    <div class="controls">
                                                      <input type="text" name="in_foal_to" value="<?php echo $mare_info->in_foal_to; ?>" id="in_foal_to" class="span12 m-wrap"/>
                                                    </div>
                                                </div>
						
                                                <div class="control-group">
  	                                        <label class="control-label" for="due_date">Due Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="due_date" value="<?php echo ($mare_info->due_date!='' && $mare_info->due_date!='0000-00-00')?date('d/m/Y',  strtotime($mare_info->due_date)):'';?>" id="due_date" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_date">Foaling Date</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_date" value="<?php echo ($mare_info->foaling_date!='' && $mare_info->foaling_date!='0000-00-00')?date('d/m/Y',  strtotime($mare_info->foaling_date)):'';?>" id="foaling_date" class="span12 m-wrap new_format"/>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
  	                                        <label class="control-label" for="foaling_location">Foaling Location</label>
                                                    <div class="controls">
                                                      <input type="text" name="foaling_location" value="<?php echo $mare_info->foaling_location; ?>" id="foaling_location" class="span12 m-wrap"/>
                                                    </div>
                                                </div>

                                                
                                                <div class="control-group">
                                                    <label class="control-label" for="foal_details">Foal details</label>
                                                        <div class="controls">
                                                          <textarea name="foal_details" id="foal_details" class="span12 m-wrap"><?php echo $mare_info->foal_details; ?></textarea>
                                                        </div>
                                                </div>
                                                
                                            </div>
                                    </div>
                                   <input type="hidden" name="mare_id" value="<?php echo $mare_id;?>"/>
                                   <input type="hidden" name="season" value="<?php echo $mare_info->season; ?>" id="season"/>
                                   <div class="form-actions">
                                         <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" />
                                         <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
<!--                                         <a href="<?php echo make_admin_url('mare', 'update', 'update','id='.$id);?>" class="btn" name="cancel" > Cancel</a>-->
                                   </div>
                                </form>
                            </div>
                        </div>
                     <div class="clearfix"></div>
