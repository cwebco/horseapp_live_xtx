<?php
#handle sections here.
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/list.php');
        break;
    /* for individual reports */
    case 'individual':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/individual.php');
        break;
    case 'owner':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/owner.php');
        break;
    case 'notes':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/notes.php');
        break;
    case 'reports':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/reports.php');
        break;
    case 'invoice':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/invoice.php');
        break;
    case 'client':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/reports/client.php');
        break;
    default:break;
endswitch;
?>
<script type="text/javascript">
    jQuery("#print").live("click", function () {

        var id = $(this).attr('horse_id');
        var dataString = 'id=' + id;
        var new_link = "id=" + id;
        var newurl = "<?php echo make_admin_url_window('printhorse', 'list', 'list'); ?>";
        newurl = newurl + new_link;

        $.ajax({
            type: "GET",
            url: newurl,
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
    /*print all*/
    jQuery("#printall").live("click", function () {
        var sex = $(this).attr('sex');
        var dataString = 'sex=' + sex;
        var new_link = "sex=" + sex;
        var newurl = "<?php echo make_admin_url_window('printallhorses', 'list', 'list'); ?>";
        newurl = newurl + new_link;

        $.ajax({
            type: "GET",
            url: newurl,
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>
