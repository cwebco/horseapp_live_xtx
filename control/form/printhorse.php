
<html>
    <head>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        <style>
            .title{
                padding: 8px 10px 2px 10px;
                border-bottom: 1px solid #eee;
                color: #000 !important;
                font-size: 18px;
                font-weight: 400;
            }
            .invoice table{
                margin-top: 10px;
            }
            .table_outer{
                padding:2%;
                margin-bottom:20px;
            }
        </style>
    </head>
<!-- section coding -->
<?php
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['type'])?$type=$_GET['type']:$type='all';
isset($_GET['print'])?$print=$_GET['print']:$print='0';
/*get horse main info*/
$QueryObj = new horse();
$horse = $QueryObj->get_horse_obj($id);

if($type=='departed'):
    /*get horse last details*/
    $query = new horse_log();
    $query->Where = "JOIN (select MAX(id) as maxid from horse_log WHERE horse_id = '$id' GROUP BY type ORDER BY id DESC) as b ON horse_log.id = b.maxid";
    $last_log = $query -> ListOfAllRecords();
    
	$QueryObj1 = new query("mare_info as M JOIN (SELECT id, max(id) as maxid from mare_info WHERE mare_id = '$id' GROUP BY mare_id ORDER BY id DESC) as b on M.id = b.maxid");
    $QueryObj1->Field ="M.*";
    $QueryObj1 -> DisplayAll();
else:
    $QueryObj1 = new horse_log();
    $horse_log = $QueryObj1->getLogs($id); 

    if($horse->sex=='mare'):
         $QueryObj1 = new mare();
         $QueryObj1->getMareSeasons($id);
    endif;
endif;    
?>
<body>
    <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid print_layout"  <?php echo ($print==1)?'style="margin:0px;width:94%;"':'';?>>
            <!--include template file for print out-->
            <?php 
				include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/printhorse/'.$type.'.php');?>
            <?php if($print!='1'): ?>
            <div class="row-fluid hidden-print">
                <div class="clearfix" style="height:20px;"></div>
                <div class="span11 invoice-block">
                    <a target="_blank" class="btn blue big pull-right" href="<?php echo make_admin_url('printhorse', 'view', 'view', 'id='.$id.'&type='.$type.'&print=1')?>">Print <i class="icon-print icon-big"></i></a>&nbsp;&nbsp;
                    <a class="btn green big pull-right" style="margin-right:20px;" href="javascript:history.back()">Back <i class="icon-print icon-backward"></i></a>
                </div>
                <div class="clearfix" style="margin-bottom:30px;"></div>
            </div>
            <?php endif; ?>
        </div>
    <!-- END PAGE CONTAINER-->    
</body>
<?php if($print=='1'): ?>
    <script type="text/javascript">
        $(window).load(function(){
            window.print(); 
            setTimeout('window.close()', 1000);  
        });
    </script>
<?php endif; ?>
</html>
