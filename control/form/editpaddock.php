<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');

isset($_POST['pad_id'])?$pad_id=$_POST['pad_id']:$pad_id='0';
isset($_POST['pge'])?$Page=$_POST['pge']:$Page='0';
isset($_POST['id'])?$id=$_POST['id']:$id='0';

if($pad_id!='new'):
    /*get paddock info from paddock log table*/
    $query_obj = new paddock_log();
    $p_log = $query_obj->getPaddock($pad_id);
    
else:
    /*get current paddock of horse*/
    $query = new paddock_log();
    $current = $query -> currentPaddock($id);
endif;

?>
<?php if($pad_id!='new'): ?>
        <div class="portlet box green" style="margin-bottom: 0px;">
            <div class="portlet-title">
                <div class="caption"><i class="icon-edit"></i>Edit Paddock Info</strong></div>
            </div>
            <div class="portlet-body form">
                <form class="horizontal-form" action="<?php echo make_admin_url('editpaddock','update','update');?>" method="POST" enctype="multipart/form-data" id="validation">
                     <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <label class="control-label" for="date">Edit Paddock </label>
                                    <div class="controls">
                                      <input type="text" name="date" value="<?php echo date('d/m/Y',  strtotime($p_log->date));?>" id="date" class="span6 m-wrap new_format"/>
                                        &nbsp;
                                        <select name="paddock" id="paddock" class="span6 m-wrap">
                                          <?php $query12  = new dropdownValues();
                                            $paddokss = $query12->getDropdown('2');
                                            foreach($paddokss as $kkk=>$vvv): ?>
                                            <option value="<?php echo $vvv->title.'**'.$vvv->id?>" <?php echo ($p_log->paddock_id==$vvv->id)?'selected':''; ?>><?php echo $vvv->title?></option>
                                          <?php endforeach;?>  
                                        </select>
                                    </div>
                            </div> 	 
                            <div class="control-group">
                                <label class="control-label" for="move">Move</label>
                                    <div class="controls">
                                        <label class="radio">
                                            <input type="radio" name="move" value="in" style="margin-left: 0px" <?php echo ($p_log->move=='in')?'checked':'';?>/>In
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="move" value="out" style="margin-left: 0px" <?php echo ($p_log->move=='out')?'checked':'';?>/>Out
                                        </label>
                                    </div>
                            </div>
                            <input type="hidden" name="pad_id" value="<?php echo $pad_id; ?>"/>
                            <input type="hidden" name="page" value="<?php echo $Page; ?>"/>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                            <div class="form-actions" style="padding-left: 20px;">
                               <input class="btn green" type="submit" name="submit" value="Submit" tabindex="3" />
                               <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                            </div>
                        </div>
                     </div>
                </form>
            </div>
        </div>
<?php else: ?>
         <div class="portlet box blue" style="margin-bottom: 0px;">
            <div class="portlet-title">
                <div class="caption"><i class="icon-edit"></i>Move to new paddock</div>
            </div>
            <div class="portlet-body form">
                <form class="horizontal-form" action="<?php echo make_admin_url('editpaddock','new','new');?>" method="POST" enctype="multipart/form-data" id="validation">
                     <div class="row-fluid">
                        <div class="span12">
                            <?php if(!empty($current)): ?>
                            <div class="control-group">
                                <label class="control-label" for="date">Current Paddock: </label>
                                <div class="controls">
                                    <strong><?php echo $current->paddock_title;?></strong>
                                </div>
                            </div>
                            <?php endif;?>
                            <div class="control-group">
                                <label class="control-label" for="date">Move to: </label>
                                    <div class="controls">
                                        <select name="to_paddock" id="to_paddock" class="span6 m-wrap">
                                          <?php $query12  = new dropdownValues();
                                            $paddokss = $query12->getDropdown('2');
                                            foreach($paddokss as $kkk=>$vvv): ?>
                                            <?php if($current->paddock_id!=$vvv->id): ?>
                                            <option value="<?php echo $vvv->title.'**'.$vvv->id?>" ><?php echo $vvv->title?></option>
                                            <?php endif; ?>
                                          <?php endforeach;?>  
                                        </select>&nbsp;
                                        <input type="text" name="to_paddock_date" value="<?php echo date('d/m/Y');?>" id="to_paddock_date" class="span6 m-wrap new_format"/>
                                    </div>
                            </div> 
                            
                            <input type="hidden" name="page" value="<?php echo $Page; ?>"/>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                            <div class="form-actions" style="padding-left: 20px;">
                               <input class="btn green" type="submit" name="submit" value="Submit" tabindex="3" />
                               <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                            </div>
                        </div>
                     </div>
                </form>
            </div>
        </div>
<?php endif; ?>
