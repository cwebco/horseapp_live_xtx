<html>
    <head>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        


    </head>
    
    
<!-- section coding -->
<?php
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
isset($_GET['id'])?$id=$_GET['id']:$id='0';

$QueryObj = new horse();
$horse = $QueryObj->get_horse_obj($id);

/*get owner details if owner id exists*/
if($horse->owner_id!=''):
    $owner = getOwnerDetails($horse->owner_id);
else:
    $owner = array();
endif;
/*get mare season info*/
if($horse->sex=='mare'):
     $QueryObj1 = new mare();
     $QueryObj1->getMareSeasons($id);
endif;
?>
<body>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
                <div class="span6">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title"><?php echo ucfirst($horse->name); ?></h3>
<!--                        <ul class="breadcrumb hidden-print">
                                <li>
                                        <i class="icon-home"></i>
                                        <a href="<?php echo make_admin_url('horse', 'list', 'list');?>">Horse</a> 
                                        <i class="icon-angle-right"></i>
                                </li>
                                <li><?php echo ucfirst($horse->name); ?></li>
                        </ul>-->
                        <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            <div class="span6 pull-right">
                <img src="assets/img/kaithlogo.png" alt="Logo"/>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row-fluid invoice">
                <div class="row-fluid invoice-logo">
<!--
                            <?php if($horse->image!=''): ?>
                                <?php if(file_exists(DIR_FS_SITE_UPLOAD.'/photo/horse/big/'.$horse->image)): ?>
                                  <div class="span6 invoice-logo-space">  
                                    <img src="<?php echo DIR_WS_SITE_UPLOAD.'/photo/horse/big/'.$horse->image ?>" alt="<?php echo $horse->image;?>"/>
                                  </div>
                                <?php else: ?>
                                    <div class="span6 invoice-logo-space hidden-print">  
                                    <img src="assets/img/noimage_horse.png" alt="owner"/>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="span6 invoice-logo-space hidden-print">  
                                    <img src="assets/img/noimage_horse.png" alt="owner"/>
                                </div>
                            <?php endif; ?>
-->
                    <div class="profile-classic row-fluid">
                        <div class="span6">
                                <ul class="unstyled span12">
                                        <li><strong>Name : </strong><?php echo ucfirst($horse->name);?></li>
                                        <?php if($horse->sex=='stallion'):  ?>
                                        <li><strong>Status : </strong><?php echo ucfirst($horse->horse_status);?></li>
                                        <?php endif; ?>
                                        <li><strong>Born : </strong><?php echo ($horse->born!='0000-00-00')?date('d/m/Y',  strtotime($horse->born)):'';?></li>
                                        <li><strong>Color : </strong><?php echo ucfirst($horse->color);?></li>
                                        <li><strong>Sex : </strong><?php echo ($horse->sex=='stallion')?'Youngstock':ucfirst($horse->sex);?></li>
                                        <li><strong>Passport Number : </strong><?php echo $horse->passport_number;?></li>
                                </ul>
                        </div>
                        <div class="span6">
                                <ul class="unstyled span12">
                                    <li><strong>Sire : </strong><?php echo ucfirst($horse->sire);?></li>
                                    <li><strong>Dam : </strong><?php echo ucfirst($horse->dam);?></li>
                                    <li><strong>Grandsire : </strong><?php echo ucfirst($horse->grandsire);?></li>
                                    <li><strong>Arrived Date : </strong><?php echo ($horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';?></li>
                                    <li><strong>Departed Date : </strong><?php echo ($horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';?></li>
                                </ul>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row-fluid">
<!--                        <div class="span6">
                            <div class="profile-classic row-fluid">
                                <h4>Paddock Details:</h4>
                                <ul class="unstyled">
                                        <li><strong>To Paddock: </strong><?php echo $horse->to_paddock;?></li>
                                        <li><strong>To Paddock Date: </strong><?php echo date('d/m/Y',  strtotime($horse->to_paddock_date));?></li>
                                        <li><strong>Last Farrier: </strong><?php echo ucfirst($horse->last_farrier);?></li>
                                        <li><strong>Last wormer: </strong><?php echo ucfirst($horse->last_wormer);?></li>
                                        <li><strong>Last flu vaccination: </strong><?php echo date('d/m/Y',  strtotime($horse->last_flu_vaccination));?></li>
                                </ul>
                            </div>
                        </div>-->
                        <div class="span6">
                            <div class="profile-classic row-fluid">
                                <h4>Owner Details:</h4>
                                <ul class="unstyled">
                                        <li><strong>Owner Name: </strong><?php echo $horse->owner_name;?></li>
                                        <?php if(!empty ($owner)): ?>
                                        <li><strong>Contact Type: </strong><?php echo $owner->contact_type;?></li>
                                        <li><strong>Email Id: </strong><?php echo $owner->primary_email_id;?></li>
                                        <li><strong>Contact Home: </strong><?php echo $owner->home_phone;?></li>
                                        <li><strong>Mobile No.: </strong><?php echo $owner->mobile_phone;?></li>
                                        <?php endif; ?>
                                        <li><strong>Rate agreed to keep: </strong><?php echo $horse->rate_to_keep;?></li>
                                        <?php if($horse->sex=='mare'):  ?>
                                        <li><strong>Rate agreed foaling fee: </strong><?php echo $horse->rate_foaling_fee;?></li>
                                        <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                </div>
                <?php if($horse->sex=='mare'): ?>
                <hr />
                <?php if($QueryObj1->GetNumRows()!=0):?>
                         <?php $sr=1; while($season=$QueryObj1->GetObjectFromRecord()):?>
                            <?php $margin = ($sr%4==0)?'style="margin-left:0px;"':''; ?>
                            <div class="row-fluid">
                                <div class="span4">
                                    <h4>Season <?php echo $season->season; ?> </h4>
                                    <ul class="pricing-content unstyled">
                                            <li><i class="icon-circle-arrow-right"></i><span>Status :</span><?php echo $season->mare_status; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Type: </span><?php echo $season->mare_type; ?></li>
                                            <?php if($season->mare_status=='In Foal'): ?>
                                            <li><i class="icon-circle-arrow-right"></i><span>In foal to:</span><?php echo $season->in_foal_to; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Due date:</span><?php echo date('d/m/Y',  strtotime($season->due_date));?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Foaling date:</span><?php echo date('d/m/Y',  strtotime($season->foaling_date));?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Foaling location:</span><?php echo $season->foaling_location; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Foal details:</span><?php echo $season->foal_details; ?></li>
                                            <?php endif; ?>
                                            <li><i class="icon-circle-arrow-right"></i><span>Covering sire:</span><?php echo $season->covering_sire; ?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Last service:</span><?php echo date('d/m/Y',  strtotime($season->last_service));?></li>
                                            <li><i class="icon-circle-arrow-right"></i><span>Current status:</span><?php echo $season->current_status; ?></li>
                                    </ul>
                                </div>
                                <?php if($sr%3==0): ?>
                                    <div class="clearfix"></div>
                                <?php endif; ?>
                                <?php $sr++; endwhile; ?>
                            </div>
                <?php endif; ?>
                <?php endif; ?>
<!--                <div class="row-fluid">
                        <div class="span12 invoice-block pull-right">
                                <a class="btn blue big hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a>
                        </div>
                </div>-->
                <div class="row-fluid" style="height: 30px;"></div>
        </div>
</div>
<!-- END PAGE CONTAINER-->    
</body>
<script type="text/javascript">
    $(window).load(function(){
        window.print(); 
        setTimeout('window.close()', 1000);  
    });
</script>
</html>