<html>
    <head>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
        <style>
            .table thead tr th {
                font-weight: 900;
            }
            .table tr td {
                font-size:13px;
                font-weight: normal;
            }
        </style>
    </head>
    <!-- section coding -->
    <?php
    include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');
    isset($_GET['sex']) ? $sex = $_GET['sex'] : $sex = 'horse';
    isset($_GET['print']) ? $print = $_GET['print'] : $print = '0';

    if ($sex == 'stallion'):
        $QueryObj = new horse();
        $QueryObj->getAllstallions();
        $total = $QueryObj->GetNumRows();
    elseif ($sex == 'mare'):
        $QueryObj = new horse();
        $QueryObj->getAllmares();
        $total = $QueryObj->GetNumRows();
    else:
        $QueryObj = new horse();
        $QueryObj->getAllHorses();
        $total = $QueryObj->GetNumRows();
    endif;
    ?>
    <body>
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid print_layout" <?php echo ($print == 1) ? 'style="margin:0px;width:94%;"' : ''; ?>>
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span6">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h5 class="page-title" style="font-size:23px;">Report : All Active <?php echo ucfirst(($sex == 'stallion') ? 'Horse' : $sex) . 's (' . $total . ')'; ?></h5>
                    <small>Date: <?php echo date('d M Y'); ?></small>
                </div>
                <div class="span6 pull-right">
                    <img src="assets/img/kaithlogo.png" alt="Logo"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- END PAGE HEADER-->
            <div class="row-fluid invoice">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr style="background-color:#EEEEEE;">
                            <th>#</th>
                            <th>Name</th>
                            <?php //if($sex=='stallion'):  ?>
                                <!--<th class="hidden-480">Sex</th>--->
                            <?php if ($sex == 'mare' || $sex == 'stallion'): ?>
                                <th class="hidden-480">Sire</th>    
                            <?php else: ?>
                                <th class="hidden-480">Sex</th>
                            <?php endif; ?>
                            <th>D.O.B.</th>
                            <th>Owner</th>
                        <!--<th class="hidden-480">Arrived</th>
                            <th class="hidden-480">Departed</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($QueryObj->GetNumRows() != 0): ?>
                            <?php $sr = 1;
                            while ($horse = $QueryObj->GetObjectFromRecord()):
                                ?>  
                                <tr>
                                    <td><?php echo $sr; ?></td>
                                    <td><?php echo $horse->name; ?></td>
                                    <?php //if($sex=='stallion'):  ?>
                                       <!-- <td class="hidden-480"><?php //echo $horse->horse_sex;  ?></td>-->
                                    <?php if ($sex == 'mare' || $sex == 'stallion'): ?>
                                        <td class="hidden-480"><?php echo $horse->sire; ?></td>
                                    <?php else: ?>
                                        <?php if ($horse->sex == 'stallion'): ?>
                                            <td class="hidden-480">Horse</td>
                                        <?php else: ?>
                                            <td class="hidden-480"><?php echo ucfirst($horse->sex); ?></td>
                                        <?php endif; ?>
        <?php endif; ?>
                                    <td><?php echo ($horse->born != '' && $horse->born != '0000-00-00') ? date('d/m/Y', strtotime($horse->born)) : ''; ?></td>
                                    <td><?php echo trim(trim(str_replace(array('(', ')'), " ", $horse->owner_name)), ','); ?></td>
                              <!--  <td class="hidden-480"><?php //echo ($horse->arrived!='' && $horse->arrived!='0000-00-00')?date('d/m/Y',  strtotime($horse->arrived)):'';  ?></td>
                                    <td class="hidden-480"><?php //echo ($horse->departed!='' && $horse->departed!='0000-00-00')?date('d/m/Y',  strtotime($horse->departed)):'';  ?></td>-->
                                </tr>
                                <?php $sr++;
                            endwhile;
                            ?>
                        </tbody>
<?php else: ?>
                        <tr>
                            <td colspan="6">
                                No record Found;
                            </td>
                        </tr>
<?php endif; ?>  
                    </tbody>
                </table>
                <hr />

                <div class="row-fluid hidden-print">
                    <div class="clearfix" style="height:20px;"></div>
                    <div class="span11 invoice-block">
                        <a class="btn blue big pull-right" target="_blank" href="<?php echo make_admin_url('printallhorses', 'list', 'list', 'sex=' . $sex . '&print=1'); ?>">Print <i class="icon-print icon-big"></i></a>
                        <a class="btn green big pull-right" style="margin-right:20px;" href="javascript:history.back()">Back <i class="icon-print icon-backward"></i></a>
                    </div>
                </div>
                <div class="clearfix" style="heigth:30px;"></div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->    
    </body>
<?php if ($print == '1'): ?>
        <script type="text/javascript">
            $(window).load(function () {
                window.print();
                setTimeout('window.close()', 1000);
            });
        </script>
<?php endif; ?>
</html>