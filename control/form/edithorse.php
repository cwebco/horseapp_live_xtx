<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/horseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/dropdownClass.php');

isset($_POST['log_id'])?$log_id=$_POST['log_id']:$log_id='0';
isset($_POST['pge'])?$Page=$_POST['pge']:$Page='0';
isset($_POST['id'])?$id=$_POST['id']:$id='0';
isset($_POST['type'])?$type=$_POST['type']:$type='';

if($log_id!='new'):
    /*get log info from horse log table*/
    $query_obj = new horse_log();
    $h_log = $query_obj->getLog($log_id);
    $type = (isset($h_log->type) && $h_log->type!='')?$h_log->type:'';  
endif;
?>
<?php if($log_id!='new'): ?>
        <div class="portlet box blue" style="margin-bottom: 0px;">
            <div class="portlet-title">
                <div class="caption"><i class="icon-edit"></i>Edit Info</strong></div>
            </div>
            <div class="portlet-body form">
                <form class="horizontal-form" action="<?php echo make_admin_url('edithorse','update','update');?>" method="POST" enctype="multipart/form-data" id="validation">
                     <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <label class="control-label" for="date">Edit <?php echo ucfirst($type);?> </label>
                                    <div class="controls">
                                      <input type="text" name="date" value="<?php echo date('d/m/Y',  strtotime($h_log->date));?>" id="date" class="span10 m-wrap new_format"/>
                                       <?php if($type=='ehv' || $type=='flu'):?>  
                                        &nbsp;
                                        <select name="value" id="value" class="span6 m-wrap">
                                                <option value="1st" <?php echo ($h_log->value=='1st')?'selected':''?>>1st</option>
                                                <option value="2nd" <?php echo ($h_log->value=='2nd')?'selected':''?>>2nd</option>
                                                <?php if($type=='flu'): ?>
                                                    <option value="Booster" <?php echo ($h_log->value=='Booster')?'selected':''?>>Booster</option> 
                                                <?php else: ?>
                                                    <option value="3rd" <?php echo ($h_log->value=='3rd')?'selected':''?>>3rd</option> 
                                                <?php endif; ?>
                                        </select>
                                      <?php elseif($type=="wormer"): ?>
                                            <input type="text" placeholder="Name" name="value" value="<?php echo $h_log->value;?>" id="value" class="span10 m-wrap"/>     
                                      <?php endif; ?>
                                    </div>
                            </div> 	 

                            <input type="hidden" name="log_id" value="<?php echo $log_id; ?>"/>
                            <input type="hidden" name="page" value="<?php echo $Page; ?>"/>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <input type="hidden" name="type" value="<?php echo $type; ?>"/>
                            <div class="form-actions" style="padding-left: 20px;">
                               <input class="btn green" type="submit" name="submit" value="Submit" tabindex="3" />
                               <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                            </div>
                        </div>
                     </div>
                </form>
            </div>
        </div>
<?php else: ?>
         <div class="portlet box blue" style="margin-bottom: 0px;">
            <div class="portlet-title">
                <div class="caption"><i class="icon-edit"></i>Add New <?php echo ucfirst($type);?> Info</strong></div>
            </div>
            <div class="portlet-body form">
                <form class="horizontal-form" action="<?php echo make_admin_url('edithorse','new','new');?>" method="POST" enctype="multipart/form-data" id="validation">
                     <div class="row-fluid">
                        <div class="span12">
                            <div class="control-group">
                                <label class="control-label" for="date">New <?php echo ucfirst($type);?> </label>
                                    <div class="controls">
                                      <input type="text" name="date" value="<?php echo date('d/m/Y');?>" id="date" class="span10 m-wrap new_format"/>
                                       <?php if($type=='ehv' || $type=='flu'):?>  
                                            &nbsp;
                                            <select name="value" id="value" class="span6 m-wrap">
                                                <option value="1st" >1st</option>
                                                <option value="2nd" >2nd</option>
                                                <?php if($type=='flu'): ?>
                                                    <option value="Booster">Booster</option> 
                                                <?php else: ?>
                                                    <option value="3rd">3rd</option> 
                                                <?php endif; ?>
                                            </select>
                                      <?php elseif($type=="wormer"): ?>
                                            <input type="text" placeholder="Name" name="value" id="value" class="span10 m-wrap"/>     
                                      <?php else: ?>
                                            <input type="hidden" name="value" value=""/>
                                      <?php endif; ?>
                                    </div>
                            </div> 	 

                            <input type="hidden" name="page" value="<?php echo $Page; ?>"/>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <input type="hidden" name="type" value="<?php echo $type; ?>"/>
                            <div class="form-actions" style="padding-left: 20px;">
                               <input class="btn green" type="submit" name="submit" value="Submit" tabindex="3" />
                               <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                            </div>
                        </div>
                     </div>
                </form>
            </div>
        </div>
<?php endif; ?>
