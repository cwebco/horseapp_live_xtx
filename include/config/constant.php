<?php
# WEBSITE CONSTANTS
$constants = new query('setting');
$constants->DisplayAll();
while($constant=$constants->GetObjectFromRecord()):
	define("$constant->key", $constant->value, true);
endwhile;

# PHP Validation types
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL',"email", true);
define("VALIDATE_MAX_LENGTH","maxlength");
define("VALIDATE_MIN_LENGTH","minlength");
define("VALIDATE_NUMERIC","num");
define("VALIDATE_ALPHA","alpha");
define("VALIDATE_ALPHANUM","alphanum");

define("TEMPLATE","default");
define("CURRENCY_SYMBOL", "&#8377;");

define("ADMIN_FOLDER",'control');
define("ADMIN_PUBLIC_FOLDER",'admin');
define("PUBLIC_FOLDER",'public');

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

$conf_shipping_type=array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'Quantity');

define('CSS_VERSION', "1.4", true);
define('JS_VERSION',"1.5", true);

$AllowedImageTypes=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes=array('application/vnd.ms-excel');

# new allowed photo mime type array.
$conf_allowed_file_mime_type=array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','applications/vnd.pdf','application/pdf');
$conf_allowed_import_file_mime_type=array('application/vnd.ms-excel', 'application/msexcel');
$conf_allowed_photo_mime_type=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_allowed_audio_mime_type= array('audio/mpeg','audio/mpeg','audio/mpg','audio/x-mpeg','audio/mp3','application/force-download','application/octet-stream');

$conf_order_status=array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

/* set soft delete status */
define("SOFT_DELETE", 1, true);
        
 
 $imageThumbConfig=array(
    'horse'=>array(
        'thumb'=>array('width'=>'200', 'height'=>'150','crop'=>'1'),
        'medium'=>array('width'=>'450', 'height'=>'337','crop'=>'1'),
        'big'=>array('width'=>'800', 'height'=>'600'),
    ),


);
       
?>
