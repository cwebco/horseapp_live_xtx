<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.get_pagecontent.php
 * Type:        function
 * Name:        get_pagecontent
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - id         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * <div>{get_pagecontent id="3434323"} ago </div>
 */

function smarty_function_get_pagecontent($params, &$smarty)
{
    if(!isset($params['id'])) {
        $smarty->trigger_error("gravatar: neither 'email' nor 'default' attribute passed");
        return;
    }
    $id=explode('id=',$params['id']);
    include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
    $query=new content();    
    $getcont=$query->getPage($id[1]);
    
    return $getcont->page;
}
?>
