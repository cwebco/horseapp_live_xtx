<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.add_css.php
 * Type:        function
 * Name:        add_css
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * make_gmap w=200 h=100 z=12
 * w = map width
 * h = map height
 * z = map zoom level
 */

function smarty_function_make_gmap($params, &$smarty)
{
       $width=isset($params['w'])?$params['w']:50;
       $height=isset($params['h'])?$params['h']:150;
       $zoom=isset($params['z'])?$params['z']:14;
       $loc=isset($params['l'])?$params['l']:'A';
       $Gmap= '<iframe frameborder="0" height="'.$height.'" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.co.in/maps?q='.ADDRESS1.','.ADDRESS2.',+'.CITY.'&hl=en&t=m&z='.$zoom.'&iwloc='.$loc.'&output=embed" width="'.$width.'%"></iframe>';
       echo $Gmap;
}
?>
