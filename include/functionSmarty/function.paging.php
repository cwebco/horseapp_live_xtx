<?php 
/** 
 * Smarty plugin for paging
 * 
 * @package Smarty 
 * @subpackage Plugin Paging
 * @author varun@cwebconsultants.com 
 */ 



function smarty_function_paging($params, &$smarty)
{
                 $url='';$querystring='';$LClass='cat';
                 $page=$params['page'];
                 $totalPages=$params['total_pages'];
                 $totalRecords=$params['total_records'];
               
                
                 if(isset($params['query'])):
                   $querystring=$params['query'];
                 endif;
                 if(isset($params['url'])):
                   $url=$params['url'];
                 endif;
              
                 if(isset($params['lclass'])):
                   $LClass=$params['lclass'];
                 endif;
    
       
    
       
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
	
             
             <div class="row-fluid">
				 <div class="pagination pagination-centered">
				   <ul>  
					   <li class="disabled">  <a href="<?=make_url_user($url, 'p='.$Pp.'&'.$querystring)?>" title="Previous Page">«</a></li>
							<?
							for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
								if($i==$page):?>
									<li class="active"> <?php echo  display_url_user($i, $url, 'p='.$i.'&'.$querystring,'blockselected'); ?></li>
								<?php else: ?>
									<li><?php echo display_url_user($i, $url, 'p='.$i.'&'.$querystring,$LClass);?></li>
								<?php endif;					
							endfor;
							?>
					   <li><a href="<?=make_url_user($url, 'p='.$Np.'&'.$querystring)?>"  title="Next Page"  >»</a></li>
				   </ul>
				 </div>						   
			</div>
               
		<?
       
}	
?>