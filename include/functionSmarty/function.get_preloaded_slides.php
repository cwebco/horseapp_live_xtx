<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.get_preloaded_slides.php
 * Type:        function
 * Name:        get_preloaded_slides
 * Description: Check User logged in or not.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 *
 * -------------------------------------------------------------
 * Example usage:
 *
 * get_preloaded_slides
 */

function smarty_function_get_preloaded_slides($params, &$smarty)
{
    $template_name=array();
	$files = glob(DIR_FS_SITE_PRELOADED_SLIDES);
	foreach($files as $file)
	{
		$template_name[]=basename($file);
	}
	
	$smarty->assign('arrayslides', $template_name); 	
}
?>
