<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.is_logged_in.php
 * Type:        function
 * Name:        is_logged_in
 * Description: Check User logged in or not.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 *
 * -------------------------------------------------------------
 * Example usage:
 *
 * if(is_logged_in)
 */

function smarty_function_is_logged_in($params, &$smarty)
{
       $result=isset($_SESSION['admin_session_secure']['login_type']) && isset($_SESSION['admin_session_secure']['login_type'])=='user' && LOGIN_USER_ID == $_SESSION['admin_session_secure']['user_id'] ? true : false;
	   return $result;
}
?>
