<?php
/*
 *     Smarty plugin
 * -------------------------------------------------------------
 * File:        function.add_css.php
 * Type:        function
 * Name:        add_css
 * Description: This TAG creates a "x minute ago" like timestamp.
 *
 * -------------------------------------------------------------
 * @license GNU Public License (GPL)
 *
 * -------------------------------------------------------------
 * Parameter:
 * - name         = the email to fetch the gravatar for (required)
 * -------------------------------------------------------------
 * Example usage:
 *
 * name = css name
 */

function smarty_function_add_css($params, &$smarty)
{
       $array=explode(',',$params['name']);
       foreach ($array as $k=>$v):
            echo '<link href="'.DIR_WS_SITE_CSS.trim($v).'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
       endforeach;
}
?>
