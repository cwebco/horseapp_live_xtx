<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.make_admin_url.php
 * Type:     function
 * Name:     make url
 * Purpose:  to make url function
 * Author    parshant@cwebconsultants.com
 * -------------------------------------------------------------
 */

function smarty_function_make_admin_url($params, &$smarty)
{
    $query=null; 
    $page=$params['page'];
	$action=isset($params['action']) && $params['action']!='' ? $params['action'] : 'list';
	$section=isset($params['section']) && $params['section']!='' ? $params['section'] : 'list';
    if(isset($params['query'])):
     $query=$params['query'];    
    endif;
	
    return  make_admin_url($page, $action, $section, $query);
   
}
?>