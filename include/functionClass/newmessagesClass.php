<?php

class newmessages extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
	parent::__construct('newmessages');
	$this->orderby = $orderby;
	$this->order = $order;
	$this->requiredVars = array('id','subject','message');
	
    }

    function saveMessage() {
	$this->Data = $this->_makeData($POST, $this->requiredVars);

	if (isset($this->Data['id']) && $this->Data['id'] != '') {
	    if ($this->Update()) {
		return $this->Data['id'];
	    }
	} else {
	    $this->Data['add_date'] = time();
	    $this->Insert();
	    return $this->GetMaxId();
	}
    }
    
     function all() {
        return $this->ListOfAllRecords();
    }  
     function purge($id) {
        if (!is_array($id)) {
            $this->id = $id;
            return $this->Delete();
        } else {
            $where = '';
            $array_count = count($id);
            $k = 1;
            foreach ($id as $key => $value) {
                if ($k++ != $array_count) {
                    $where .= $key . '=' . '"' . $value . '"' . ' AND ';
                } else {
                    $where .= $key . '=' . '"' . $value . '"';
                    $this->Where = 'WHERE ' . $where;
                }
            }
            return $this->Delete_where();
        }
    }

function view_one_message($id) {
	$this->Where = "WHERE `id` = '$id'";
	return $this->DisplayOne();
    }

}
