<?php

/*Class to find sub themes of theme*/
class theme_sub_rel extends cwebc {

    function __construct() {
        parent::__construct('theme_sub_rel');
        $this->requiredVars=array('id','theme_id','theme_sub_id','date');
     }


    function deleteRecordsOfTheme($t_id){
           $this->Where="where theme_sub_id='$t_id'";
	   $this->Delete_where();

    }
    
 
    
    function saveThemeRel($sub_theme,$theme_id){
                    /*Firstly delete existing sub thmes then insert new*/
                    $this->deleteRecordsOfTheme($theme_id);
 
                    $q= new query('theme_sub_rel');

                    $q->Data['theme_sub_id']=$sub_theme;

                    $q->Data['theme_id']=$theme_id;

                    $q->Insert();
             
    }
    
    

      function getAllSubthemesOfTheme($t_id)
     {
                $this->Field="id,theme_sub_id";
		$this->Where="where theme_id='".mysql_real_escape_string($t_id)."'";
                $all_subs=array();
                $all_catt= $this->ListOfAllRecords();
                foreach($all_catt as $kkk=>$vvv):
                  $all_subs[]=$vvv['theme_sub_id'];
                 endforeach;
                return $all_subs ;
     }


     /*Functions for front*/
      function setSubThemeId($id){
        $this->theme_sub_id=$id;
      }

     /*Get themes of particular category*/
     function getThemeOfSubTheme(){
        $this->Where="where theme_sub_id='".mysql_real_escape_string($this->theme_sub_id)."'";
        return $this->ListOfAllRecords('object');
    }
    
    function getParentThemeIdOfSubTheme($id){
        $parent_id=0;
        $this->Field="id,theme_id";
        $this->Where="where theme_sub_id='".mysql_real_escape_string($id)."'";
        $object= $this->DisplayOne();
        if(is_object($object)):
            $parent_id=$object->theme_id;
        endif;
        return $parent_id;
        
    }

   
}
?>
