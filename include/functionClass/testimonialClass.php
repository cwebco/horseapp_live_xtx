<?php
/*
 * Testimonial Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class testimonial extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('user_testimonial');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','user_id', 'name','date_added', 'urlname', 'date_show','short_description', 'long_description', 'position','meta_name', 'meta_keyword', 'meta_description',  'is_deleted', 'image', 'is_active','last_update_dt');

	}

    /*
     * Create new testimonial or update existing theme
     */
    function saveTestimonial($POST,$user_id){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        }

        $this->Data['user_id']=$user_id;
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';


        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
       
        if($image_obj->upload_photo('testimonial', $_FILES['image'], $rand)):
                $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get testimonial by id
     */
    function getTestimonial($id){
        return $this->_getObject('user_testimonial', $id);
    }


    /*
     * Get testimonial by id
     */
    function getTestimonialByID($id,$show_active=1){
        if($show_active)
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0' AND is_active='1'";
        else
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0'";
        return $this->DisplayOne();
    }
    
    
    
    
    
    /*
     * Get List of all testimonial in array
     */
    function listTestimonials($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc";
		else
			$this->Where="where user_id='$this->user_id' AND is_deleted='0'  ORDER BY position asc";

		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

    function listLimitedActiveTestimonials($result_type='array',$limit=5){
		if($limit)
		     $this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc LIMIT 0,$limit";
		else
                     $this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc ";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }
    
  
    /*
     * Get count of all testimonials
     */
    function countTestimonials($show_active=0){
                $total_count=0;
		$this->Fields="id";
		if($show_active):
			$this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc";
		else:
			$this->Where="where user_id='$this->user_id' AND is_deleted='0'  ORDER BY position asc";

		endif;
                
                $object= $this->ListOfAllRecords('object');
                
                if(count($object)):
                    $total_count=count($object);
                endif;
                
                return $total_count;
                
    }
    
    

    /*
     * delete a theme by id
     */
    function deleteTestimonial($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * Update theme position
     */

    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

     function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }
    
    /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }

   
}
?>