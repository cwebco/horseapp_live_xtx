<?php
/*
 * Website Plan Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class website_plan extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('website_plan');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','title','recurring_cost','period_in_month','discount_type','discount','description','date_added','ip_address','is_active','is_deleted','last_update_dt','position');

	}

    /*
     * Create new theme or update existing theme
     */
    function saveWebsitePlan($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        $this->Data['ip_address']=$_SERVER['SERVER_NAME'];

        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

   
    
    /*
     * Get List of all website plans in array
     */
    function listWebsitePlans($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' ORDER BY position asc";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  


    /*
     * Update theme position
     */

    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

    
    function getTotalPayableAmount($id){
        $plan=$this->getObject($id);
        
        $total_payable=0;
        $total_payable_after_dis=0;
        
        if(is_object($plan)):
            
           $discount_amount=0;
        
           $total_payable= (float)($plan->recurring_cost);
           
           if($plan->discount && $plan->discount!=0):
               
                if($plan->discount_type=='percent'):
                    
                    $discount_amount=(float)($total_payable*($plan->discount/100));/*discount in percentage*/
                else:
                    $discount_amount=(float)($plan->discount);/* flat discount*/
            
                endif;
               
           endif;
                     
           $total_payable_after_dis=(float)($total_payable-$discount_amount);
            
        endif;
        
        return $total_payable_after_dis;
        
    }
    
    
     function getTotalDiscountAmount($id){
        $plan=$this->getObject($id);
        
        $discount_amount=0;
    
        
        if(is_object($plan)):
            
           $discount_amount=0;
        
           $total_payable= (float)($plan->recurring_cost);
           
           if($plan->discount && $plan->discount!=0):
               
                if($plan->discount_type=='percent'):
                    
                    $discount_amount=(float)($total_payable*($plan->discount/100));/*discount in percentage*/
                else:
                    $discount_amount=(float)($plan->discount);/* flat discount*/
            
                endif;
               
           endif;
                     
                       
        endif;
        
        return $discount_amount;
        
    }
    
    
    

   
}
?>