<?php

/*
 * Category Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class horse extends cwebc {

    function __construct() {
        parent::__construct('horse');
        $this->requiredVars = array('id', 'name', 'horse_status', 'born', 'color', 'sex', 'sire', 'dam', 'grandsire', 'passport_number',
            'arrived', 'departed', 'owner_id', 'owner_name', 'invoice_details', 'rate_to_keep', 'rate_foaling_fee',
            'horse_sex', 'is_active', 'is_deleted', 'added_date', 'microchip', 'departure_notes', 'date', 'posted_date');
    }

    /* Functions for front */

    function getAllfillterhorses($id) {
//        $this->print=1;
        $this->Where = "WHERE `id` = '$id'";
        return $this->ListOfAllRecords();
    }

    function horseses() {
        return $this->ListOfAllRecords();
    }

    function getAllHorses() {
        $this->Where = "where is_deleted='0' order by name asc";
        return $this->DisplayAll('object');
    }

    function getAllHorsess() {
        $this->Where = "where is_deleted='0' order by name asc";
        return $this->ListOfAllRecords('object');
    }

    function getAllHorses_by_id($id) {
        $this->Where = "where is_deleted='0' AND `id` = '$id' order by name asc";
        return $this->DisplayAll();
    }

    function getAllHorses2() {
        $this->Where = "where is_deleted='0' order by owner_name asc";
        return $this->DisplayAll();
    }

    function all_horse_excel() {
        return $this->ListOfAllRecords();
    }

    function all_mare_excel() {
        $this->Where = "where sex='mare'  order by owner_name asc";
        return $this->ListOfAllRecords();
    }

    /* function for get all stallions */

    function getAllstallions() {
        $this->Where = "where is_deleted='0' and sex='stallion' order by name asc";
        return $this->DisplayAll('object');
    }

    function getAllstallionss() {
        $this->Where = "where is_deleted='0' and sex='stallion' order by name asc";
        return $this->ListOfAllRecords('object');
    }

    function getAllstallions2() {
        $this->Where = "where is_deleted='0' and sex='stallion' order by owner_name asc";
        return $this->DisplayAll();
    }

    /* function for get all mares */

    function getAllmares() {
        $this->Where = "where is_deleted='0' and sex='mare' order by name asc";
        return $this->DisplayAll();
    }

    function getAllmaresss() {
        $this->Where = "where is_deleted='0' and sex='mare' order by name asc";
        return $this->ListOfAllRecords('object');
    }
    function getAllmaresssWithInfo() {
        $date = date("Y");

// SELECT hs.*,ma.due_date FROM  horse hs LEFT JOIN mare_info ma ON hs.id=ma.mare_id;
        $query = new query('horse as hs');
        // $query->print=1;
        $query->Field='hs.*,ma.due_date,ma.season,ma.covering_sire,ma.in_foal_to,ma.last_service,ma.foaling_date';
        $query->Where = "LEFT JOIN `mare_info`  ma  ON hs.`id` = ma.`mare_id`";
        $query->Where .= "where hs.`is_deleted`='0' and hs.`sex`='mare' and  ma.`season` LIKE '%$date%' order by ma.due_date asc";
        return $query->ListOfAllRecords('object');
    }

    function getAllmaresexcl() {
        $this->Where = "where sex='mare' order by name asc";
        return $this->DisplayAll();
    }

    function getAllmares2() {
        $this->Where = "where is_deleted='0' and sex='mare' order by owner_name asc";
        return $this->DisplayAll();
    }

    /* function for get all stallions */

    function getTopstallions() {
        $this->Where = "where is_deleted='0' and sex='stallion' order by added_date desc LIMIT 0,5";
        return $this->DisplayAll();
    }

    /* function for get all mares */

    function getTopmares() {
        $this->Where = "where is_deleted='0' and sex='mare' order by added_date desc LIMIT 0,5";
        return $this->DisplayAll();
    }

    /* Get all  categories */

    function getAllHorsesName() {
        $this->Field = "id,name";
        $this->Where = "where is_deleted='0' and is_active='1' order by name asc";
        return $this->ListOfAllRecords('object');
    }

    function ListHorses() {
        $this->Where = "where is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();
    }

    function get_horse_obj($id) {
        //$this->print=1;
        $this->Where = " where id='$id' AND is_deleted='0'";
        $cat = $this->DisplayOne();
        return $cat;
    }

    function get_horse_obj_hr($id) {
        //$this->print=1;
        $this->Where = " where id='$id' ";
        $cat = $this->DisplayOne();
        return $cat;
    }

    function get_horse_ob($id) {
        //$this->print=1;
        $this->Where = " where id='" . mysqli_real_escape_string($this->ConRe, $id) . "' AND is_deleted='0'";
        $cat = $this->DisplayOne();
        return $cat;
    }

    /*
     * Create new page or update existing page
     */

    function saveHorse($POST) {
        /* set owner id and name */
        $owner_id = '';
        $owner_name = '';

        if (isset($POST['owner']) && $POST['owner'] != ''):
            foreach ($POST['owner'] as $kk => $vv):
                $owner = $vv;
                $owner_parts = explode('**', $owner);
                if (!empty($owner_parts)):
                    $owner_id .= "(" . $owner_parts['0'] . "),";
                    $owner_name .= "(" . $owner_parts['1'] . "),";
                endif;
            endforeach;
        endif;

        $POST['owner_id'] = rtrim(trim($owner_id), ',');
        $POST['owner_name'] = rtrim(trim($owner_name), ',');

        $date_obj = new date();

        if ($POST['arrived'] != ''):
            $POST['arrived'] = $date_obj->TocustomDate($POST['arrived']);
        endif;
        if ($POST['departed'] != ''):
            $POST['departed'] = $date_obj->TocustomDate($POST['departed']);
        endif;

        if ($POST['born'] != ''):
            $POST['born'] = $date_obj->TocustomDate($POST['born']);
        endif;
        $this->born = NULL;
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = '1';
        $this->Data['is_deleted'] = '0';

         // $this->print=1;
        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('horse', $_FILES['image'], $rand)):
            $this->Data['image'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;
// $this->print=1;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* get archived */

    function getarchived($type = '') {
        if ($type == ''):
            $this->Where = "where is_deleted='1'";
        else:
            $this->Where = "where sex='$type' and is_deleted='1'";
        endif;
        return $this->DisplayAll();
    }

    /* get owner by horse count */

    public static function getOwnerWithHorses($owner_id) {
        $query = new horse();
        $query->Field = "id,name,horse_sex";
        $query->Where = "where owner_id like'%($owner_id)%' and is_deleted='0'";
        return $query->ListOfAllRecords('object');
    }

    function horse_not_in_filter($horses = array()) {
        $where = '';
        $horses_counter = 1;
        $total_horses = count($horses);
        foreach ($horses as $horse) {
            if ($total_horses != $horses_counter++) {
                $where .= "id != '$horse' AND ";
            } else {
                $where .= "id != '$horse'";
            }
        }
        $this->Field = "id,name,arrived,departed,sex,owner_name";
        $this->Where = "WHERE $where AND is_deleted='0'";
        return $this->ListOfAllRecords();
    }

}

/* horse moore detial table class */

class horse_log extends cwebc {

    function __construct() {
        parent::__construct('horse_log');
        $this->requiredVars = array('id', 'horse_id', 'type', 'value', 'date', 'mail_sent');
    }

    function addHorseLog($id, $type, $value = '', $date) {
        if ($id != ''):
            $date_obj = new date();
            if ($date != ''):
                $date = $date_obj->TocustomDate($date);
            endif;

            $this->Data['horse_id'] = $id;
            $this->Data['type'] = $type;
            $this->Data['value'] = $value;
            $this->Data['date'] = $date;
            $this->Insert();
            return true;
        else:
            return false;
        endif;
    }

    /* get all logs of horse */

    function getLogs($id) {
        $this->Where = " where horse_id='$id' order by date desc";
        return $this->ListOfAllRecords();
    }

    function getLogs_by_type($id, $type) {
        $this->Where = " where horse_id='$id' AND `type` = '$type' order by date desc";
        return $this->ListOfAllRecords();
    }

    function count_getLogs($id, $type) {
        $this->Field = "COUNT(*) as count";
        $this->Where = " where horse_id='$id' AND `type` = '$type' order by date desc";
        $data = $this->DisplayOne();
        return $data->count;
    }

    /* get single log of horse */

    function getLog($id) {
        $this->Where = " where id='$id'";
        return $this->DisplayOne();
    }

    /* edit log */

    function editLog($id, $date = '', $value = '', $type = '') {
        $date_obj = new date();
        if ($date != ''):
            $date = $date_obj->TocustomDate($date);
        endif;
        $this->Data['id'] = $id;
        $this->Data['date'] = $date;
        $this->Data['value'] = $value;
        $this->Update();
    }

    /* delete log */

    function deleteLog($id) {
        $this->id = $id;
        if ($this->Delete()):
            return true;
        else:
            return false;
        endif;
    }

    /* function for printing out reports */

    function printReport($type, $fromdate = '', $todate = '') {
        if ($type != ''):
            if ($fromdate != '' && $todate != ''):
                $date_obj = new date();
                $fromdate = $date_obj->TocustomDate($fromdate);
                $todate = $date_obj->TocustomDate($todate);

                $this->Where = " where type='$type' and date >='$fromdate' and date <='$todate' ORDER BY date desc,horse_id desc";
            else:
                $this->Where = " where type='$type' ORDER BY date desc,horse_id desc";
            endif;
            return $this->DisplayAll();
        else:
            return false;
        endif;
    }

    function email_milestone($INTERVAL, $value) {
        $this->Field = "horse.id as horse_table_id, horse.name, horse.is_active,";
        $this->Field .= "horse_log.id, horse_log.horse_id, horse_log.date, horse_log.value, horse_log.mail_sent, horse_log.type";
        
        $this->Where = " LEFT JOIN horse ON horse.id = horse_log.horse_id";
        $this->Where .= " WHERE horse_log.`type` = 'flu' AND horse_log.`date` = DATE(NOW()) - INTERVAL $INTERVAL DAY AND `value` = '$value' AND mail_sent = 0";
        $this->Where .= " AND horse.`is_active` = '1' AND horse.`is_deleted` = '0'";
        
        return $this->ListOfAllRecords();
    }
    
    function email_milestone_ehv($INTERVAL) {
        $this->Field = "horse.id as horse_table_id, horse.name, horse.is_active,";
        $this->Field .= "horse_log.id, horse_log.horse_id, horse_log.date, horse_log.value, horse_log.mail_sent, horse_log.type";
        
        $this->Where = " LEFT JOIN horse ON horse.id = horse_log.horse_id";
        $this->Where .= " WHERE horse_log.`type` = 'ehv' AND horse_log.`date` = DATE(NOW()) - INTERVAL $INTERVAL DAY AND mail_sent = 0";
        $this->Where .= " AND horse.`is_active` = '1' AND horse.`is_deleted` = '0'";
        
        return $this->ListOfAllRecords();
    }
    
    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

}

/* horse padock detail class */

class paddock_log extends cwebc {

    function __construct() {
        parent::__construct('paddock_log');
        $this->requiredVars = array('id', 'horse_id', 'paddock_id', 'paddock_title', 'move', 'date');
    }

    function addPaddock($id, $post) {

        /* set paddock id and name */
        if ($post['to_paddock'] != ''):
            $paddock = $post['to_paddock'];
            $paddock_parts = explode('**', $paddock);
            if (!empty($paddock_parts)):
                $this->Data['paddock_id'] = $paddock_parts['1'];
                $this->Data['paddock_title'] = $paddock_parts['0'];
            endif;
        endif;

        $this->Data['horse_id'] = $id;
        $this->Data['move'] = 'in';

        if ($post['to_paddock_date'] != ''):
            $date_obj = new date();
            $this->Data['date'] = $date_obj->TocustomDate($post['to_paddock_date']);
        endif;
        $this->Insert();
    }

    /* get horse paddock details */

    function getPaddocks($id) {
        $this->Where = " where horse_id='$id' order by date desc, move asc";
        return $this->ListOfAllRecords();
    }

    /* get paddock entry by id from paddock log table */

    function getPaddock($id) {
        $this->Where = " where id='$id'";
        return $this->DisplayOne();
    }

    /* edit paddock details */

    function editPaddockLog($pid, $data_array) {

        $this->Data['id'] = $pid;
        /* set paddock id and name */
        if ($data_array['paddock'] != ''):
            $paddock = $data_array['paddock'];
            $paddock_parts = explode('**', $paddock);
            if (!empty($paddock_parts)):
                $this->Data['paddock_id'] = $paddock_parts['1'];
                $this->Data['paddock_title'] = $paddock_parts['0'];
            endif;
        endif;

        $this->Data['move'] = $data_array['move'];

        if ($data_array['date'] != ''):
            $date_obj = new date();
            $this->Data['date'] = $date_obj->TocustomDate($data_array['date']);
        endif;
        $this->Update();
    }

    /* delete log */

    function deletePaddockLog($id) {
        $this->id = $id;
        if ($this->Delete()):
            return true;
        else:
            return false;
        endif;
    }

    /* get current paddock of horse */

    function currentPaddock($id) {
        $this->Where = " where horse_id='$id' and move='in' order by date DESC";
        return $this->DisplayOne();
    }

    /* automatically out from current paddock */

    function outFromCurrentPaddock($date, $arrayy) {

        $this->Data['horse_id'] = $arrayy->horse_id;
        $this->Data['paddock_id'] = $arrayy->paddock_id;
        $this->Data['paddock_title'] = $arrayy->paddock_title;
        $this->Data['move'] = 'out';

        if ($date != ''):
            $date_obj = new date();
            $this->Data['date'] = $date_obj->TocustomDate($date);
        endif;
        $this->Insert();
    }

    /* print paddock report */

    function printPaddockReport($from = '', $to = '') {

        if ($from != '' && $to != ''):
            $date_obj = new date();
            $from = $date_obj->TocustomDate($from);
            $to = $date_obj->TocustomDate($to);
            $this->Where = " where date >='$from' and date <='$to' ORDER BY date desc,horse_id desc";
        else:
            $this->Where = " where 1=1 ORDER BY date desc,horse_id desc";
        endif;
        return $this->DisplayAll();
    }

}

/* detail about the mare season */

class mare extends cwebc {

    function __construct() {
        parent::__construct('mare_info');
        $this->requiredVars = array('id', 'mare_id', 'season', 'mare_status', 'mare_type', 'in_foal_to', 'due_date', 'foaling_date', 'foaling_location',
            'foal_details', 'covering_sire', 'last_service', 'current_status', 'last_scan', 'last_scan_text');
    }

    /* add mare season info */

    function saveMareSeasonInfo($post) {
        $date_obj = new date();
        if ($post['due_date'] != ''):
            $post['due_date'] = $date_obj->TocustomDate($post['due_date']);
        endif;
        if ($post['foaling_date'] != ''):
            $post['foaling_date'] = $date_obj->TocustomDate($post['foaling_date']);
        endif;
        if ($post['last_service'] != ''):
            $post['last_service'] = $date_obj->TocustomDate($post['last_service']);
        endif;
        if ($post['last_scan'] != ''):
            $post['last_scan'] = $date_obj->TocustomDate($post['last_scan']);
        endif;

        $this->Data = $this->_makeData($post, $this->requiredVars);
        $mare_idd = $this->Data['mare_id'];
        $season = $this->Data['season'];

        if ($this->checkMareSeason($mare_idd, $season)):
            $query = new query('mare_info');
            $query->Data = $this->Data;
            $query->Where = " where mare_id='$mare_idd' AND season='$season'";
            if ($query->UpdateCustom()):
                return $mare_idd;
            endif;
        else:
            $query = new query('mare_info');
            $query->Data = $this->Data;
            $query->Insert();
            return $query->Data['mare_id'];
        endif;
    }


    function getMareSeasonscurrentLatest($id) {
            $this->Where = "where mare_id='$id' ORDER BY mare_info.season DESC LIMIT 1";
            // $this->print = 1;
            return $this->ListOfAllRecords();
        }

    /* check mare season info */

    function checkMareSeason($id, $season) {
        $qury = new query('mare_info');
        $qury->Where = "where mare_id='$id' and season='$season'";
        $object = $qury->DisplayOne();
        if (is_object($object)):
            return true;
        else:
            return false;
        endif;
    }

    /* get mare season info */

    function getmareinfo($id) {
        $this->Where = "where mare_id='$id'";
        $cat = $this->DisplayOne();
        return $cat;
    }

    function getmareinfos($id) {
        $date = date("Y");
        $this->Where = "where mare_id='$id' and season LIKE '%$date%'";
        $cat = $this->DisplayOne();
        return $cat;
    }

    function getmareinfoseason($id) {
        $this->Where = "where mare_id='$id' order by season desc";
        $cat = $this->DisplayOne();
        return $cat;
    }

    /* get all mare seasons */

    function getMareSeasons($id) {
        $this->Where = "where mare_id='$id' order by season desc";
        
        return $this->DisplayAll();
    }
    
    function getMareSeasonSingle($id) {
        $this->Where = "where mare_id='$id' order by season desc";
        
        return $this->DisplayOne();
    }

    function getMareSeasonscurrent($id) {
        $this->Where = "where mare_id='$id'";
        $this->print = 1;
        return $this->DisplayAll();
    }
    

    function getMareSeasonsexcel() {

        return $this->ListOfAllRecords();
    }

    /* print paddock report */

    function printServiceReport($from = '', $to = '') {

        if ($from != '' && $to != ''):
            $date_obj = new date();
            $from = $date_obj->TocustomDate($from);
            $to = $date_obj->TocustomDate($to);
            $this->Where = " where last_service >='$from' and last_service <='$to' ORDER BY last_service desc,mare_id desc";
        else:
            $this->Where = " where 1=1 ORDER BY last_service desc,mare_id desc";
        endif;
        //           $this->print=1;
        return $this->DisplayAll();
    }

}

class horse_documents extends cwebc {

    function __construct() {
        parent::__construct('horse_documents');
        $this->requiredVars = array('id', 'horse_id', 'file', 'time');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function all($horse_id) {
        $this->Where = "WHERE `horse_id` = '$horse_id'";
        return $this->ListOfAllRecords();
    }

    function single($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

    function delete_horse($id) {
        $this->id = $id;
        $this->Delete();
    }

}

class horse_notes extends cwebc {

    function __construct() {
        parent::__construct('horse_notes');
        $this->requiredVars = array('id', 'horse_id', 'name', 'note', 'date', 'time', 'invoice_note');
    }

    function save($POST) {
        $POST['invoice_note'] = htmlentities($POST['invoice_note']);
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function all($horse_id) {
        $this->Where = "WHERE `horse_id` = '$horse_id'";
        return $this->ListOfAllRecords();
    }

    function notes_all($horse_id) {
        $this->Where = "WHERE `horse_id` = '$horse_id' AND `note` != ''";
        return $this->ListOfAllRecords();
    }

    function invoice_all($horse_id) {
        $this->Where = "WHERE `horse_id` = '$horse_id' AND `invoice_note` != ''";
        return $this->ListOfAllRecords();
    }

    function single($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

    function delete_horse($id) {
        $this->id = $id;
        $this->Delete();
    }

    public static function total_notes($horse_id) {
        $query = new horse_notes;
        $query->Field = "COUNT(*) as count";
        $query->Where = "WHERE `horse_id` = '$horse_id'";
        $data = $query->DisplayOne();
        return $data->count;
    }

    function by_horseid($horse_id) {
        $this->Where = "WHERE `horse_id` = '$horse_id'";
        return $this->ListOfAllRecords();
    }

//    function filter_horses($from_date, $to_date) {
//        //$this->print = 1;
//        $this->Where = "LEFT JOIN `horse` ON `horse`.`id` = `horse_notes`.`horse_id` WHERE (`horse_notes`.`date` BETWEEN '$from_date' AND '$to_date') ";
//        //$this->print = 1;
//        return $this->ListOfAllRecords();
//    }


    function filter_horses($from_date, $to_date) {
        $query = new query('horse_notes as n');
        $query->Field = ("n.*");

        $query->Where = "LEFT JOIN `horse` as  h  ON h.`id` = n.`horse_id` WHERE (n.`date` BETWEEN '$from_date' AND '$to_date')  ";
        //$query->print=1;
        return $query->ListOfAllRecords();
    }

    //    function filter_horses($from_date, $to_date) {
//        $query = new query('horse_notes as n');
//        $query->Field = ("n.*");
//
//        $query->Where = "LEFT JOIN `horse` as  h  ON h.`id` = n.`horse_id` WHERE (n.`date` BETWEEN '$from_date' AND '$to_date') || (h.`departed` BETWEEN '$from_date' AND '$to_date') || (`h`.`departed` = '0000-00-00') ";
//        //$query->print=1;
//        return $query->ListOfAllRecords();
//    }




    function all_notes() {
//        $this->print=1;
        $this->Where = "ORDER BY `horse_id` ASC ";
        return $this->ListOfAllRecords();
    }

}
