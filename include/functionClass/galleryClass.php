<?php
/*
 * Gallery Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class gallery extends cwebc{
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order='asc', $orderby='position'){
        $this->InitilizeSQL();
        $this->orderby=mysql_real_escape_string($orderby);
        $this->order=mysql_real_escape_string($order);
        parent::__construct('user_gallery');
        //$this->TableName='content';
        $this->requiredVars=array('id','user_id', 'image', 'position', 'caption', 'is_deleted', 'is_active', 'parent_id', 'link_url', 'target','date_added');
    }



    function saveImage($post, $user_id){
        $this->Data=$this->_makeData($post, $this->requiredVars);
        
        if(!isset($this->Data['caption']) or $this->Data['caption']==''){
            $this->Data['caption']=SITE_NAME;
        }
        
        $this->Data['user_id']=$user_id;
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';

        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
       
        if($image_obj->upload_photo('gallery', $_FILES['image'], $rand)):
                $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;

        if(isset($this->Data['id']) && $this->Data['id']!=''){

            if($this->Update())
              return $Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }


    function getImage($id){

         return $this->_getObject('gallery', mysql_real_escape_string($id));
    }

    function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }

    function listImages($show_active=0, $result_type='object'){
        if($show_active)
          $this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
        else
          $this->Where="where user_id='$this->user_id' AND is_deleted='0' order by $this->orderby $this->order";
        if($result_type=='object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function deleteImage($id){
        $this->id=mysql_real_escape_string($id);
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    
     /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }
    
    
    /*
     * Get image by id
     */
    function getImageByID($id,$show_active=1){
        if($show_active)
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0' AND is_active='1'";
        else
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0'";
        return $this->DisplayOne();
    }
    
    /*change status of image*/
    function changeStatus($id){
        $image_obj=$this->getImageByID($id,0);
      
        if(is_object($image_obj)):
            $query_obj=new gallery();
            $query_obj->Data['is_active']=$image_obj->is_active==1?0:1;
            $query_obj->Data['id']=$id;
         
            $query_obj->Update();
        else:
            return false;
        endif;
        
        
    }
    
    
   
}
?>