<?php
/*
 * Theme Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class theme extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('theme');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'name','date_added', 'urlname', 'date_show', 'description', 'position','meta_name', 'meta_keyword', 'meta_description',  'is_deleted', 'image', 'is_active','last_update_dt','is_main');

	}

    /*
     * Create new theme or update existing theme
     */
    function saveTheme($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        }

        
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        $this->Data['is_main']=isset($this->Data['is_main'])?'1':'0';


        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get theme by id
     */
    function getTheme($id){
        return $this->_getObject('theme', $id);
    }


    
    /*
     * Get List of all themes in array
     */
    function listThemes($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' ORDER BY position asc";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }
    
     /*
     * Get List of all themes in array
     */
    function GetParentThemes($show_active=0, $result_type='object'){
		$this->Field="id,name";
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1' AND is_main='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' AND is_main='1' ORDER BY position asc";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a theme by id
     */
    function deleteTheme($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * Update theme position
     */

    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

    
      /*Get themes of particular category*/
     function getThemesOfCategory($p, $max_records,$category_id){
         $all_themes=array();
         $query= new query("theme,theme_cat_rel");
         $query->Field="distinct theme.*,theme_cat_rel.c_id as category_id";
         $query->Where="where theme.is_active='1' AND theme.is_deleted='0' and theme.id=theme_cat_rel.t_id and theme_cat_rel.c_id='$category_id' ORDER BY theme.position asc";
         
         $query->PageNo=$p;
         $query->PageSize=$max_records;
         $query->AllowPaging=true;
         $all_themes =$query->ListOfAllRecords('object');
         
         $result['themes']=$all_themes;
         $result['TotalPages']=$query->TotalPages;
         $result['TotalRecords']=$query->TotalRecords;
               
         return $result;
     
    }
    
    /*Get parent themes of particular category: ignore sub themes*/
     function getParentThemesOfCategory($p, $max_records,$category_id){
         $all_themes=array();
         $query= new query("theme,theme_cat_rel");
         $query->Field="distinct theme.*,theme_cat_rel.c_id as category_id";
         $query->Where="where theme.is_active='1' AND is_main='1' AND theme.is_deleted='0' and theme.id=theme_cat_rel.t_id and theme_cat_rel.c_id='$category_id' ORDER BY theme.position asc";
         
         $query->PageNo=$p;
         $query->PageSize=$max_records;
         $query->AllowPaging=true;
         $all_themes =$query->ListOfAllRecords('object');
         
         $result['themes']=$all_themes;
         $result['TotalPages']=$query->TotalPages;
         $result['TotalRecords']=$query->TotalRecords;
               
         return $result;
     
    }
    
    
    /*Get parent themes of particular category: ignore sub themes*/
     function getParentThemesOfCat($category_id){
         $all_themes=array();
         $query= new query("theme,theme_cat_rel");
         $query->Field="distinct theme.*,theme_cat_rel.c_id as category_id";
         $query->Where="where theme.is_active='1' AND is_main='1' AND theme.is_deleted='0' and theme.id=theme_cat_rel.t_id and theme_cat_rel.c_id='$category_id' ORDER BY theme.position asc";
    
         $all_themes =$query->ListOfAllRecords('object');
            
         return $all_themes;
     
    }
    
    
      /*Get themes of particular category*/
     function ListThemesOfCategory($category_id){
         $all_themes=array();
         $query= new query("theme,theme_cat_rel");
         $query->Field="distinct theme.*,theme_cat_rel.c_id as category_id";
         $query->Where="where theme.is_active='1' AND theme.is_deleted='0' and theme.id=theme_cat_rel.t_id and theme_cat_rel.c_id='$category_id' ORDER BY theme.position asc";
    
         $all_themes =$query->ListOfAllRecords('object');

               
         return $all_themes;
     
    }

    /*
     * Get count of all categories
     */
    function countThemes($show_active=0){
                $total_count=0;
		$this->Fields="id";
		if($show_active):
			$this->Where="where  is_deleted='0' AND is_active='1'  ORDER BY position asc";
		else:
			$this->Where="where  is_deleted='0'  ORDER BY position asc";

		endif;
                
                $object= $this->ListOfAllRecords('object');
                
                if(count($object)):
                    $total_count=count($object);
                endif;
             
                return $total_count;
                
    }
    
    function getThemeName($id)
     {          
                $this->Field='id,name';
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
              
                $theme= $this->DisplayOne();
                return $theme->name;
     }  
     
     function getThemeFolderName($id)
     {          
                $this->Field='id,urlname';
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
              
                $theme= $this->DisplayOne();
                return $theme->urlname;
     }  
     
     
     function getFullThemeWithSubThemes($themes_id)
     {
                $all_theme=array();
                $this->Field="id,name,image";
		$this->Where="where is_deleted='0' AND is_active='1' AND id IN($themes_id) order by $this->orderby $this->order";
                $all_theme=$this->ListOfAllRecords('object');
              
                return $all_theme;

     }
    
}
?>