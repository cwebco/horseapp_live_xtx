<?php
/*
 * Message Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */



/*Class to save Messages */
class message extends cwebc {
    
    function __construct() {
        parent::__construct('message');
        $this->requiredVars=array('id','subject','message','date','ip_address','id_sender','id_receiver');
        $this->placeholders=array('inbox'=>'1','sent'=>'2','draft'=>'3','trash'=>'4');
     }
     
     
     /*save message for reviever and sender*/
     function saveMessage($sender_id,$receiver_id,$subject,$message){
       
            $this->Data['subject']=$subject;
            $this->Data['message']=$message;
            $this->Data['id_sender']=$sender_id;
            $this->Data['id_receiver']=$receiver_id;
            $this->Data['ip_address']=$_SERVER['SERVER_NAME'];
            $this->Insert();
            $message_id= $this->GetMaxId();
            
            /*save enteries in bit_message_user table for this message*/
            /*For sender*/ 
            $q= new query('message_user');
            $q->Data['user_id']=$sender_id;
            $q->Data['message_id']=$message_id;
            $q->Data['placeholder_id']=$this->placeholders['sent'];
            $q->Insert(); 
            /*For receiver*/  
            $q1= new query('message_user');
            $q1->Data['user_id']=$receiver_id;
            $q1->Data['message_id']=$message_id;
            $q1->Data['placeholder_id']=$this->placeholders['inbox'];
            $q1->Insert();
            return $message_id;
            
        
    }
    
    
    /*get sender name of message */
    function getSenderName($id){
        
        $sender_name="";
        
        $message=$this->getObject($id);
        
        if(is_object($message)):
        
             $user_obj=new user();
             $user=$user_obj->getUserLimitedInfo($message->id_sender);
             
             if($user):
                 if($user->user_type=='admin'):
                    $sender_name=$user->department;
                 else:
                    $sender_name=$user->first_name." ".$user->last_name;
                 endif;
             endif;
           
        endif;
        
        return $sender_name;
        
    }
    
    /*get receiver name of message */
    function getReceiverName($id){
        
        $receiver_name="";
        
        $message=$this->getObject($id);
        
        if(is_object($message)):
        
             $user_obj=new user();
             $user=$user_obj->getUserLimitedInfo($message->id_receiver);
             
             if($user):
                 if($user->user_type=='admin'):
                    $receiver_name=$user->department;
                 else:
                    $receiver_name=$user->first_name." ".$user->last_name;
                 endif;
             endif;
           
        endif;
        
        return $receiver_name;
        
    }
    
   
     
}

/*class to save relation of message and user and placeholders*/
class message_user extends cwebc {
    
    function __construct() {
        parent::__construct('message_user');
        $this->requiredVars=array('id','user_id','message_id','placeholder_id','is_read','is_starred');
        $this->placeholders=array('inbox'=>'1','sent'=>'2','draft'=>'3','trash'=>'4');
     }
     
     
     
     /*
     * Get List of all messages of user in folder(inbox,draft,sent)
     */

    function listMessagesOfUserInPlacehoder($user_id,$placehoder_id,$p,$max_records) {

        $messages=array();
        $d_query= new query('message,message_user');
        $user_id= mysql_real_escape_string($user_id);
        $placehoder_id= mysql_real_escape_string($placehoder_id);
        
        $d_query->Field="message.*,message_user.id as message_id,message_user.user_id,message_user.placeholder_id,message_user.is_starred,message_user.is_read";
        $d_query->Where = "where message_user.placeholder_id='$placehoder_id' AND message_user.user_id='$user_id' AND message_user.message_id=message.id  ORDER BY message.date desc,message.id desc";
        $d_query->PageNo=$p;
        $d_query->PageSize=$max_records;
        $d_query->AllowPaging=true;
        //$d_query->print=1;
        $messages =$d_query->ListOfAllRecords('object');
        $result['messages']=$messages;
        $result['TotalPages']=$d_query->TotalPages;
        $result['TotalRecords']=$d_query->TotalRecords;
        return $result;
  
   }
   
     function limitedMessagesOfUserInPlacehoder($user_id,$placehoder_id,$limit='3') {

        $messages=array();
        $d_query= new query('message,message_user');
        $user_id= mysql_real_escape_string($user_id);
        $placehoder_id= mysql_real_escape_string($placehoder_id);
        
        $d_query->Field="message.*,message_user.id as message_id,message_user.user_id,message_user.placeholder_id,message_user.is_starred,message_user.is_read";
        $d_query->Where = "where message_user.placeholder_id='$placehoder_id' AND message_user.user_id='$user_id' AND message_user.message_id=message.id  ORDER BY message.date desc,message.id desc LIMIT 0,$limit";
     
        //$d_query->print=1;
        $messages =$d_query->ListOfAllRecords('object');
      
        return $messages;
  
   }
   
   
   
   
   function CountUnreadMessageOfUser($user_id) {

        $messages=array();
        $d_query= new query('message,message_user');
        $user_id= mysql_real_escape_string($user_id);
  
        $d_query->Field="message.*,message_user.id as message_id,message_user.user_id,message_user.placeholder_id,message_user.is_starred,message_user.is_read";
        $d_query->Where = "where message_user.placeholder_id='1' AND message_user.is_read='0' AND message_user.user_id='$user_id' AND message_user.message_id=message.id  ORDER BY message.date desc,message.id desc";
   
        $messages =$d_query->ListOfAllRecords('object');
    
        return count($messages);
  
   }
   
   
   function getMessage($id,$user_id) {

        $messages=array();
        $d_query= new query('message,message_user');
        $id= mysql_real_escape_string($id);
        $user_id= mysql_real_escape_string($user_id);
   
        $d_query->Field="message.*,message_user.id as message_id,message_user.user_id,message_user.placeholder_id,message_user.is_starred,message_user.is_read";
        $d_query->Where = "where message_user.user_id='$user_id' AND message_user.message_id=message.id AND message_user.message_id='$id'";
   
        return $d_query->DisplayOne();
    
  
   }
   
   
   
    function setMessageRead($id){
        $id= mysql_real_escape_string($id);
        $this->Data['is_read']='1';
        $this->Data['id']=$id;

        $this->Update();
        
        
    }
    
    function change_placeholder($id,$placeholder_id){
        $id= mysql_real_escape_string($id);
        $this->Data['placeholder_id']=$placeholder_id;
        $this->Data['id']=$id;

        $this->Update();
        
        
    }
    
    function change_starred($id){
        $message=$this->getObject($id);
        
        if(is_object($message)):
            $query=new message_user();
            $id= mysql_real_escape_string($id);
            $query->Data['is_starred']=$message->is_starred==1?0:1;
            $query->Data['id']=$id;
            $query->Update();
            return true;
        else: 
            return false;
        endif;
        
        
        
    }


}
