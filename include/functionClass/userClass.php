<?php
/*
 * User Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

include_once(DIR_FS_SITE.'include/functionClass/serviceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/productClass.php');
include_once(DIR_FS_SITE.'include/functionClass/moduleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/userMetaClass.php');
class user extends cwebc {
    
    function __construct() {
        parent::__construct('user');
        $this->requiredVars=array('id','username','password','phone','email','last_sign_in','is_active','is_deleted','last_update_date','ip_address','last_access','allow_pages','is_loggedin');
	
        }   
        
      /*
     * Get List of all users in array
     */
    function listUsers(){
        $this->Field="id,business_email,first_name,last_name,user_type,department";
        $this->Where="where is_deleted='0' AND user_type='user'";    
        $this->DisplayAll();        
    }   
    
      /*
     * Get List of all users in array
     */
    function listAllUsers(){

        $this->Where="where is_deleted='0' AND user_type='user'";    
        $this->DisplayAll();        
    }   
    
    
    function getUsers(){
        $this->Where="where is_deleted='0' AND user_type='user'";    
        return $this->ListOfAllRecords('object');        
    }   
    
     /*
     * Get List of all admin users in array
     */
    function listAdminUsers(){
        $this->Where="where is_deleted='0'";    
        $this->DisplayAll();        
    }
    
    function getAdminUsers(){
        $this->Field="id,business_email,first_name,last_name,user_type,department";
        $this->Where="where is_deleted='0' AND user_type='admin'";    
     
        return $this->ListOfAllRecords('object');        
    } 
    
    
    
     /*
     * Create new admin
     */
    function saveAdminUser($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['user_type']='admin';
        $this->Data['password']=md5($this->Data['password']);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
           
            if($this->Update())
              return $Data['id'];
        }
        else{
            
            $this->Insert();
            return $this->GetMaxId();
        }
    }
    
    /*save user data*/
        
    function saveUserDetail($POST){

        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        if(isset($POST['password'])):
          $this->Data['password']=md5($this->Data['password']);
        endif; 
        
        $this->Data['ip_address']=$_SERVER['REMOTE_ADDR']; 
        //$this->print=1;
        if(isset($this->Data['id']) && $this->Data['id']!=''){ 
            if($this->Update())
              return $POST['id'];
        }
        else{  
            $this->Data['reg_date']=date('Y-m-d');
            $this->Data['is_active']='1';
            $this->Insert();
            return $this->GetMaxId();
        }
    }
    
    
    function updateUserDetail($POST){

        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        if(isset($POST['password'])):
           $this->Data['password']=md5($this->Data['password']);
        endif; 
       
        if(isset($this->Data['id']) && $this->Data['id']!=''){ 
            if($this->Update())
              return $POST['id'];
        }
       
    }
         
    function getUserObject($id){
        $this->Where="where id='".$id."' AND user_type='user'";  
        $user=$this->DisplayOne();
        return $user;
   
    }
    
    
    
    function getUser($id){
        return $this->_getObject('user', $id);
    }
    
    function getUserLimitedInfo($id){
        $this->Field="id,business_email,first_name,last_name,user_type,department";
        $this->Where="where id='".$id."'";  
        $user=$this->DisplayOne();
        return $user;

    }
    
     /*
     * Get admin by id
     */
    function getAdminUser($id){
        return $this->_getObject('user', $id);
    }
 
  
   function check_user_name_exists($username,$id=0){
        $username=mysql_real_escape_string($username);
        $id=mysql_real_escape_string($id);
        
        if($username==""):
            return false;
        endif;
        
        $this->Field="id";
        if($id && $id!=0 && $id!=""):
            $this->Where="where username='".$username."' AND id!='".$id."'";  
        else:
            $this->Where="where username='".$username."'";  
        endif;
             

        $user=$this->DisplayOne(); 
        if($user && is_object($user)): 
           
             return $user; 
        else: 
             return false;
        endif;
   } 
   

    function check_business_name_exists($bussname,$id=0){
        
        $bussname=mysql_real_escape_string($bussname);
        $id=mysql_real_escape_string($id);
        
        if($bussname==""):
            return false;
        endif;
        

        $this->Field="id";
        if($id && $id!=0 && $id!=""):
            $this->Where="where business_name='".$bussname."' AND id!='".$id."'";  
        else:
            $this->Where="where business_name='".$bussname."'";   
        endif;
        $buss=$this->DisplayOne(); 
        if($buss && is_object($buss)): 
             return $buss; 
        else: 
             return false;
        endif;
    } 
   
    
 function check_business_email_exists($business_email,$id=0){
        
        $business_email=mysql_real_escape_string($business_email);
        $id=mysql_real_escape_string($id);
        
        if($business_email==""):
            return false;
        endif;
        

        $this->Field="id";
        if($id && $id!=0 && $id!=""):
            $this->Where="where business_email='".$business_email."' AND id!='".$id."'";  
        else:
            $this->Where="where business_email='".$business_email."'";   
        endif;
   
        $buss=$this->DisplayOne(); 
        if($buss && is_object($buss)): 
             return $buss; 
        else: 
             return false;
        endif;
    } 
    
    function verify_url_string($string) {

        if ($string != '')
            if (substr($string, -1) == '/')
                return substr($string, 0, strlen($string) - 1);

        return $string;
    }
        
        
    function getUsernameFromURL(){
        

        $string_parts=array();
        $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE);
        $URL = $_SERVER['REQUEST_URI'];
        $string = substr($URL, -(strlen($URL) - strlen($prefix)));
        $string = $this->verify_url_string($string);
        $string_parts = explode('/', $string);

        $user_name= isset($string_parts['1'])?$string_parts['1']:"";
        
        return $user_name;
        
    }
    
    
    
    function getUserFromUsername($username){
        $username=mysql_real_escape_string($username);
        if($username==""):
            return false;
        endif;
       
        $this->Where="where username='".$username."'";       
        $buss=$this->DisplayOne(); 
        if($buss && is_object($buss)): 
             return $buss; 
        else: 
             return false;
        endif;
        
        
    }
    
    
    
    /*set user email verified*/
    
    function setEmailVerified($id){
        
        $this->Data['id']=$id;
        $this->Data['is_email_verified']='1';
        $this->Update();
                
    }
    
    
    /*set user email verified*/
    
    function setEmailVerifiedAndSignUpComplete($id){
        
        $this->Data['id']=$id;
        $this->Data['is_email_verified']='1';
        $this->Data['is_signup_complete']='1';
        $this->Data['is_on_demo']='0';
        $this->Update();
                
    }
        
        
    /*
     * check admin user with name
     */
    function checkUsers($username,$email){
        $this->Where="where is_deleted='0' AND (username='".mysql_real_escape_string($username)."' OR email='".mysql_real_escape_string($email)."')";    
        $this->DisplayAll();        
    }
    
    /*
     * check admin user with name in update case
     */
    function checkUsersWithID($username,$email,$id){
        $this->Where="where is_deleted='0' AND id!='$id' AND (username='".mysql_real_escape_string($username)."' OR business_email='".mysql_real_escape_string($email)."')";  
        $this->DisplayAll();        
    }
    
     /*
     * delete a page by id
     */
    function deleteAdminUser($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
     /*update business logo*/
     function updateLogo($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
       
        if($image_obj->upload_photo('logo', $_FILES['business_logo'], $rand)):
                $this->Data['business_logo']=$image_obj->makeFileName($_FILES['business_logo']['name'], $rand);
        endif;
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{
         
            return false;
        }
    }
    
    
     /*update business favicon*/
     function updateFavicon($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
      
        if($image_obj->upload_photo('favicon', $_FILES['business_favicon'], $rand)):
                $this->Data['business_favicon']=$image_obj->makeFileName($_FILES['business_favicon']['name'], $rand);
        endif;
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){

     
            if($this->Update())
              return $this->Data['id'];
        }
        else{
         
            return false;
        }
    }
    
    /*update theme id of user*/
    
    function updateThemeId($id,$theme_id){
        
        $this->Data['id']=$id;
        $this->Data['theme_id']=$theme_id;
        $this->Update();
                
    }
    
    
    /*check if demo period of user is not expired*/
    
    function ValidateDemoPeriod($reg_date,$is_on_demo=0){
       
        if($is_on_demo && $is_on_demo!=0):
            
            /*user is on demo period, check date*/
            $new_obj=new date();
            $new_date=$new_obj->add_days_to_date(COUNT_DEMO_DAYS,$reg_date);
            $current_date=date('Y-m-d');
           
            if(strtotime($current_date)>strtotime($new_date)):
                return false;
            else:
                return true;
            endif;
           
            
        else:    
            return true;
        endif;
                
    }
    
     /*check if demo period of user is not expired*/
    
    function GetTotalCompleteSteps($com_steps){
           $total_complete=0;
           
           if(count($com_steps) && is_array($com_steps)):
               foreach($com_steps as $k=>$v):
                   if($v==1):
                       $total_complete++;
                   endif;

               endforeach;
           endif;    
           
           return $total_complete;

        }
        
        
      /*get array of user navigation according to selected modules
       * $id is user id
       * $sub_navigation, send it true for getting sub navigation
       * $sub_navigation_limit, set it 0 if no limit
       */
    
     function GetUserNavigation($id,$sub_navigation=0,$sub_navigation_limit=0){
           global $user_meta;
           $navigation=array();
           $selected_modules=array();
           $this->Field="id,module";
           $this->Where="where user_type='user' AND id='".$id."'";  
           
           $user=$this->DisplayOne(); 
            if($user && is_object($user)): 
                $modules=array();
            
               /*get all selected theme modules*/
                $modules=array();
                $modules=unserialize(html_entity_decode($user_meta->user_module_config));
                
                $modules_arr=html_entity_decode($user->module);
                $selected_modules= unserialize($modules_arr);
             
                $sr=0;
                foreach($modules as $k => $v):
                    
                    if(in_array($v['page_name'],$selected_modules)):
                        $navigation[$v['page_name']]['page_name']=$v['title'];
                        $navigation[$v['page_name']]['page']=$v['page_name'];
                        
                        if($sub_navigation && $sub_navigation==1):/*get subnavigation according to page*/
                            
                            if($k=='service'): /*get user services*/
                                /*Get limited services*/
                                $services=array();
                                $service_obj=new service();
                                $service_obj->setUserId($id);
                                $services= $service_obj->listLimitedActiveServices('array',$sub_navigation_limit,1);
                                $navigation[$v['page_name']]['sub_navigation']=$services;  
                                
                            elseif($k=='product'):  
                                /*Get limited products*/
                                $products=array();
                                $product_obj=new product();
                                $product_obj->setUserId($id);
                                $products= $product_obj->listLimitedActiveProducts('array',$sub_navigation_limit,1);
                                $navigation[$v['page_name']]['sub_navigation']=$products;  
                            endif;
                            
                         
                            
                        endif;
                    endif;
                    
                    
                endforeach;
           
          
            endif;
            
           return $navigation; 
           
           

     }    
    
    /*get user not to open pages*/
     function GetUserNotOpenPages($id){
            global $user_meta; 
           $not_open_pages=array();
           $selected_modules=array();
           $this->Field="id,module";
           $this->Where="where user_type='user' AND id='".$id."'";  
           
           $user=$this->DisplayOne(); 
           if($user && is_object($user)): 
              
               /*get all selected theme modules*/
                $modules=array();
                $modules=unserialize(html_entity_decode($user_meta->user_module_config));
            
                $modules_arr=html_entity_decode($user->module);
                $selected_modules= unserialize($modules_arr);
                $sr=0;
                foreach($modules as $k => $v):
                    
                    if(!in_array($v['page_name'],$selected_modules)):
                         $not_open_pages[]=$v['page_name'];
                    endif;
                    
               endforeach;     
                
           endif;
           
           return $not_open_pages;
    
     }
     
     
      /*
     * Get count of all users
     */
    function countUsers($user_type='user',$show_active=0){
                $total_count=0;
		$this->Field="id";
		if($show_active):
			$this->Where="where user_type='$user_type' AND is_deleted='0' AND is_active='1' ";
		else:
			$this->Where="where user_type='$user_type' AND is_deleted='0' ";

		endif;
               
                $object= $this->ListOfAllRecords('object');
                
                if(count($object)):
                    $total_count=count($object);
                endif;
          
                return $total_count;
                
    }
    
    
    /*check user password is correct*/
    function checkUserPassword($id,$password){
              
                $password=md5($password);
		$this->Field="id";
		$this->Where="where id='$id' AND password='$password'";
               
                $object= $this->DisplayOne();
                
                if(is_object($object) && count($object)):
                    return true;
                endif;
          
                return false;
                
    }
    
    
    /*update forgot password verify code of user*/
    
    function updateForgotPasswordVerifyCode($id,$code){
        
        $this->Data['id']=$id;
        $this->Data['forgot_verify_code']=$code;
        $this->Update();
                
    }
    
     /*update  verify code of user*/
    
    function updatedVerifyCode($id,$code){
        
        $this->Data['id']=$id;
        $this->Data['verify_code']=$code;
        $this->Update();
                
    }
    
    /*set wizard complete*/
    
    function setWizardComplete($id){
        
        $this->Data['id']=$id;
        $this->Data['is_wizard_complete']='1';
        $this->Update();
                
    }
    
    
     /*copy logo to favicon*/
     function copyFavicon($image,$id){
   
        $this->Data['id']=$id;
        $this->Data['business_favicon']=$image;
        $this->Update();
        
        /*copy logo image to favicon folder*/
        $source_path=DIR_FS_SITE_UPLOAD.'photo/logo/large/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/favicon/large/'.$image;
       
        copy($source_path, $destination_path);
        
        $image_fav=new imageManipulation();
        $image_fav->create_resized_for_module('favicon', $image);
          
         
     }
     
     
    /*function to hide a module for user*/
     
     function HideModuleForUser($id,$module){
        $module_find=0; 
        $selected_modules=array();
        $this->Field="id,module";
        $this->Where="where user_type='user' AND id='".$id."'";   
        $user=$this->DisplayOne(); 
        if($user && is_object($user)): 

            $modules_arr=html_entity_decode($user->module);
            $selected_modules= unserialize($modules_arr);
            
            foreach($selected_modules as $k=>$v):
                
                if($v==$module):
                    unset($selected_modules[$k]);
                    $module_find=1;
                endif;
                
            endforeach;
                  
            if($module_find==1): /*unset module and save array again*/
                $query=new user();
                $query->Data['id']=$id;
                $query->Data['module']=serialize($selected_modules);
                $query->Update();
                return true;
            else:
                return false;/*failed to hide*/
            endif;     
           
        else:
            return false;/*failed to hide*/
            
        endif; 
         
         
     }
     
    
}