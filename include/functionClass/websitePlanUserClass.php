<?php
/*
 * Website Plan User Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class website_plan_user extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('website_plan_user');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','user_id','website_plan_id','website_plan_title','subscription_id','recurring_cost','period_in_month','discount_type','discount','payment_type','payment_made','on_date','from_date','to_date','next_payment_date','ip_address','last_update_dt','is_deleted');


	}

    /*
     * Create new website plan or update existing theme
     */
    function saveWebsitePlanUser($user_id,$website_plan_id,$id=0){
        
         /*get website plan detail by plan id*/
         $plan_obj=new website_plan();
         $plan=$plan_obj->getObject($website_plan_id);
         
         /*from and to date*/
         $date_obj=new date();
         $from_date=date('Y-m-d');
         $to_date=$date_obj->add_months_to_date($plan->period_in_month,$from_date);
         
         /*total payment made*/
         $total_payable_amount=0;
         $totak_obj=new website_plan();
         $total_payable_amount=$totak_obj->getTotalPayableAmount($website_plan_id);
         
         /*save website plan of user*/
         $query=new query('website_plan_user');
         $query->Data=$this->_makeData($_POST, $this->requiredVars);
         $query->Data['is_active']='0';
         
         $query->Data['user_id']=$user_id;
         $query->Data['website_plan_id']=$website_plan_id;
         $query->Data['website_plan_title']=$plan->title;
         $query->Data['recurring_cost']=$plan->recurring_cost;
         $query->Data['period_in_month']=$plan->period_in_month;
         $query->Data['discount_type']=$plan->discount_type;
         $query->Data['payment_made']=$total_payable_amount;
         $query->Data['discount']=$plan->discount;
         $query->Data['from_date']=$from_date;
         $query->Data['to_date']=$to_date;
         $query->Data['next_payment_date']=$to_date;
         $query->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
       
        
         if(isset($id) && $id && $id!=0){
              $query->Data['id']=$id;
           
              if($query->Update())
              return $query->Data['id'];
         }
         else{
            $query->Data['on_date']=date('Y-m-d');
            $query->Insert();
            return $query->GetMaxId();
        }
    }

    
    
    /*
     * Create website plan of user 
     */
    function saveManualWebsitePlanUser($plan){
     
        
         /*from and to date*/
         $date_obj=new date();
         $from_date=$date_obj->ToUSDate($plan['from_date']);
         $to_date=$date_obj->add_months_to_date($plan['period_in_month'],$from_date);
         
         
         /*save website plan of user*/
         $query=new query('website_plan_user');
         $query->Data=$this->_makeData($plan, $this->requiredVars);
         
           
         $query->Data['is_active']='1'; /*activate plan for manual payment*/
         $query->Data['user_id']=$plan['user_id'];
         $query->Data['from_date']=$from_date;
         $query->Data['to_date']=$to_date;
         $query->Data['next_payment_date']=$to_date;
         $query->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
         $query->Data['on_date']=date('Y-m-d');
         $query->Data['payment_type']="manual";
              
         $query->Insert();
         return $query->GetMaxId();
      
    }

   
    /*Extend website plan of user*/
    function ExtendPlanOfUser($plan_id){
        
        $plan_obj=new website_plan_user();
        $plan=$plan_obj->getObject($plan_id);
        
        /*from and to date*/
        $date_obj=new date();
        $from_date=$date_obj->ToUSDate($plan->to_date);
        $to_date=$date_obj->add_months_to_date($plan->period_in_month,$from_date);
         
        if(is_object($plan)):
            $query=new website_plan_user();
            $query->Data['next_payment_date']=$to_date;
            $query->Data['to_date']=$to_date;
            $query->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
                   
            $query->Data['id']=$plan_id;
          
            $query->Update();
            return $plan_id;
          

        else:
            return false;
        endif;
    
    }
    
    
     /*Extend website plan of user*/
    function getExtendPlanDateOfUser($plan_id){
        
        $plan_obj=new website_plan_user();
        $plan=$plan_obj->getObject($plan_id);
        
        /*from and to date*/
        $date_obj=new date();
        $from_date=$date_obj->ToUSDate($plan->to_date);
        $to_date=$date_obj->add_months_to_date($plan->period_in_month,$from_date);
        $to_date=$date_obj->ToUKDate($to_date);
      
        return $to_date;
    
    }
    
    /*check service plan of user already exists if yes then return id*/
   
    function checkUserWebsitePlanExists($user_id){
        $user_id=mysql_real_escape_string($user_id);
        
        
        if($user_id==""):
            return 0;
        endif;
        
        $this->Field="id";

        $this->Where="where user_id='".$user_id."' AND is_deleted='0' ";  
      
        $plan=$this->DisplayOne(); 
        if($plan && is_object($plan)): 
          
             return $plan->id; 
        else: 
             return 0;
        endif;
   } 
    
   
   /*validate user website plan: whether it is activated or not expired*/
    
    function checkUserWebsitePlanActivated($user_id){
        
        $user_id=mysql_real_escape_string($user_id);
        
        if($user_id==""):
            return 0;
        endif;
        
        $this->Field="id,to_date,is_active";

        $this->Where="where user_id='".$user_id."' AND is_active='1' AND is_deleted='0'";  
      
        $plan=$this->DisplayOne(); 
        if($plan && is_object($plan)): /*if plan exists check for dates*/
          
             return $plan->id;/*user website plan id*/
          
        else: 
             return 0;
        endif;
       
        
    }
     
    
   
    /*set user email verified*/
    
    function ActivateWebsitePlan($id){
        
        $this->Data['id']=$id;
        $this->Data['is_active']='1';
       
        $this->Update();
                
    }
    
     /*update to date of user plan */
    
    function UpdateToDate($id,$to_date){
        
        $this->Data['id']=$id;
        $date_obj=new date();
        $this->Data['next_payment_date']=$date_obj->ToUSDate($to_date);
        $this->Data['to_date']=$date_obj->ToUSDate($to_date);
      
        $this->Update();
                
    }
        
    /*validate user website plan: whether it is activated or not expired*/
    
    function validateUserWebsitePlan($user_id){
        
        $user_id=mysql_real_escape_string($user_id);
        
        if($user_id==""):
            return 0;
        endif;
        
        $this->Field="id,to_date,is_active";

        $this->Where="where user_id='".$user_id."' AND is_active='1' AND is_deleted='0'";  
      
        $plan=$this->DisplayOne(); 
        if($plan && is_object($plan)): /*if plan exists check for dates*/
          
            $current_date=date('Y-m-d');
            $to_date=$plan->to_date;
            
            if(strtotime($current_date)>strtotime($to_date)):/*plan expired*/
                
               return 0;
            
            else:
                
               return $plan->id;/*user website plan id*/
               
            endif;
          
        else: 
             return 0;
        endif;
       
        
    }
    
    
    function getTotalDiscountAmount($id){
        $plan=$this->getObject($id);
        
        $discount_amount=0;
    
        
        if(is_object($plan)):
            
           $discount_amount=0;
        
           $total_payable= (float)($plan->recurring_cost);
           
           if($plan->discount && $plan->discount!=0):
               
                if($plan->discount_type=='percent'):
                    
                    $discount_amount=(float)($total_payable*($plan->discount/100));/*discount in percentage*/
                else:
                    $discount_amount=(float)($plan->discount);/* flat discount*/
            
                endif;
               
           endif;
                     
                       
        endif;
        
        return $discount_amount;
        
    }
    
    
    

    
    #total payments
   
    function getTotalAmountToBePaidOfUser($plan_id){
        
        $total_amount=0;
        
        $plan=$this->getObject($plan_id);
        
        #get total periods
        $noOfPayments=1;
        $date_obj=new date();
        $total_month=$date_obj->MonthsBetweenTwoDates($plan->from_date,$plan->to_date);
        
        /*total number of payments*/
        $noOfPayments=ceil($total_month/$plan->period_in_month);
        
        /*as there can be discount in first payment calculate first payment to be pay by user*/
        $first_payment=0;
        $discount=0;
        $first_obj=new website_plan_user();
        $discount=$first_obj->getTotalDiscountAmount($plan_id);
        
        $first_payment=$plan->recurring_cost-$discount; /*first payment after discount*/
        
        $total_amount=$first_payment+$plan->recurring_cost*($noOfPayments-1); /*total amount to be paid by user*/
        
        return $total_amount;
        
       
        
    }
    
    /*get website plan title*/
    function getWebsitePlanTitle($id){
        $this->Field="id,website_plan_title";
        $this->Where="where id='$id'";    
     
        $plan=$this->DisplayOne();  
        return $plan->website_plan_title;
    } 

   
}
?>