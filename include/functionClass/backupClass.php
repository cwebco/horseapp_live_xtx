<?php
/*
 * MySQL Database Backup Class
 * 
 */
define('MSB_VERSION', '1.0.0');
define('MSB_NL', "\r\n");
define('MSB_STRING', 2);
define('MSB_DOWNLOAD', 1);
define('MSB_SAVE', 2);
define('__SEP__', "/*sep*/" );

set_time_limit(600);

class mysqlBackup extends cwebc{    
    
       var $server = 'localhost';
       var $port = 3306;
       var $username;
       var $password;
       var $database;
       var $link_id = -1;
       var $connected = false;
       var $tables = array();
       var $drop_tables = true;
       var $struct_only = false;
       var $comments = true;
       var $backup_dir = 'upload/backup/';
       var $fname_format = 'd_m_y__H_i_s';
       var $error = '';

       var $complete_inserts  = true;
       var $inserts_block     = 200;
       
       function __construct(){
             parent::__construct('backup');
             $this->requiredVars=array('id','document', 'position', 'is_deleted', 'is_active', 'ip_address');
             
             global $DBHostName;
             global $DBDataBase;
             global $DBUserName;
             global $DBPassword;
             
             $this->server=$DBHostName;
             $this->username=$DBUserName;
             $this->password=$DBPassword;
             $this->database=$DBDataBase;
             
             //$this->backup_dir = DIR_FS_SITE.'upload/backup/';
             
       }
    
       
       
   function _execute($task = MSB_STRING, $fname = '', $compress = false) {
       
       
       if ( !( $sql = $this->_retrieve() ) ) {
           return false;
       }
       if ( $task == MSB_SAVE ) {
           if (empty($fname)) {
               $fname = $this->backup_dir;
               $fname .= date($this->fname_format);
               $fname .= ($compress ? '.sql.gz' : '.sql');
           }
           return $this->_saveToFile($fname, $sql, $compress);
       } elseif ($task == MSB_DOWNLOAD) {
           if ( empty( $fname ) ) {
               $fname = date($this->fname_format);
               $fname .= ($compress ? '.sql.gz' : '.sql');
           }
           return $this->_downloadFile($fname, $sql, $compress);
       } else {
           return $sql;
       }
   }

   function _connect() {
       $value = false;
       if (!$this->connected) {
           $host = $this->server . ':' . $this->port;
           $this->link_id = mysqli_connect($host, $this->username, $this->password);
       }
       if ($this->link_id) {
           if (empty($this->database)) {
               $value = true;
           } elseif ($this->link_id !== -1) {
               $value = mysqli_select_db($this->link_id,$this->database);
           } else {
               $value = mysqli_select_db($this->database);
           }
       }
       if (!$value) {
           $this->error = mysqli_error($this->link_id);
       }
       return $value;
   }

   function _query($sql) {
       if ($this->link_id !== -1) {
           $result = mysqli_query($this->link_id, $sql);
       } else {
           $result = mysqli_query($this->link_id,$sql);
       }
       if (!$result) {
           $this->error = mysqli_error($this->link_id);
       }
       return $result;
   }

   function _getTables() {
       $value = array();
       if ( !( $result = $this->_query('SHOW TABLES') ) ) {
           return false;
       }
       while ( $row = mysqli_fetch_row( $result ) ) {
           if ( empty( $this->tables) || in_array( $row[0], $this->tables ) ) {
               $value[] = $row[0];
           }
       }
       if (!sizeof($value)) {
           $this->error = 'No tables found in database.';
           return false;
       }
       return $value;
   }

   function _dumpTable( $table ) {
       $value = '';
       $this->_query( 'LOCK TABLES ' . $table . ' WRITE' );
       if ( $this->comments ) {
           $value .= '#' . MSB_NL;
                   $value .= '# Table structure for table `' . $table . '`' . MSB_NL;
                   $value .= '#' . MSB_NL . MSB_NL;
       }
       if ( $this->drop_tables ) {
           $value .= 'DROP TABLE IF EXISTS `' . $table . '`;' . __SEP__ . MSB_NL;
       }
       if ( !( $result = $this->_query('SHOW CREATE TABLE ' . $table) ) ) {
           return false;
       }
       $row = mysqli_fetch_assoc($result);
       $value .= str_replace("\n", MSB_NL, $row['Create Table']) . ';' . __SEP__;
       $value .= MSB_NL . MSB_NL;
       if (!$this->struct_only) {
               if ($this->comments) {
                   $value .= '#' . MSB_NL;
                   $value .= '# Dumping data for table `' . $table . '`' . MSB_NL;
                   $value .= '#' . MSB_NL . MSB_NL;
               }
               $value .= $this->_getInserts($table);
       }
       $value .= MSB_NL . MSB_NL;
       $this->_query('UNLOCK TABLES');
       return $value;
   }

   function _getInserts($table) {
       $value = '';
       if (!($result = $this->_query('SELECT * FROM ' . $table))) {
           return false;
       }
       if ( $this->complete_inserts ) {
               while ($row = mysqli_fetch_row($result)) {
                   $values = '';
                   foreach ($row as $data) {
                       $values .= '\'' . addslashes($data) . '\', ';
                   }
                   $values = substr($values, 0, -2);
                   $value .= 'INSERT INTO ' . $table . ' VALUES (' . $values . ');' . __SEP__ . MSB_NL;
               }
       } else {
               $blocks_counter = 0;
           $blocks = array();
           while ($row = mysqli_fetch_row($result)) {
               $values = array();
               foreach ($row as $data) {
                   $values[] = '\'' . addslashes($data) . '\'';
               }
               $blocks[] = '(' . implode( ',', $values ) . ')';

               if ( $blocks_counter < $this->inserts_block ) {
                   $blocks_counter++;
               } else {
                       $value .= 'INSERT INTO ' . $table . ' VALUES ' . implode( ',', $blocks ) . ";" . __SEP__ . MSB_NL;
                       $blocks = array();
                       $blocks_counter = 0;
               }
           }
               if ( count( $blocks ) ) {
               $value .= 'INSERT INTO ' . $table . ' VALUES ' . implode( ',', $blocks ) . ";" . __SEP__ . MSB_NL;
               }
       }
       return $value;
   }

   function _retrieve() {
       $value = '';
       if (!$this->_connect()) {
           return false;
       }
       if ($this->comments) {
           $value .= '#' . MSB_NL;
           $value .= '# MySQL database dump' . MSB_NL;
           $value .= '# Created by MySQL_Backup class, ver. ' . MSB_VERSION . MSB_NL;
           $value .= '#' . MSB_NL;
           $value .= '# Host: ' . $this->server . MSB_NL;
           $value .= '# Generated: ' . date('M j, Y') . ' at ' . date('H:i') . MSB_NL;
           $value .= '# MySQL version: ' . mysqli_get_server_info($this->link_id) . MSB_NL;
           $value .= '# PHP version: ' . phpversion() . MSB_NL;
           if (!empty($this->database)) {
               $value .= '#' . MSB_NL;
               $value .= '# Database: `' . $this->database . '`' . MSB_NL;
           }
           $value .= '#' . MSB_NL . MSB_NL . MSB_NL;
       }
       if (!($tables = $this->_getTables())) {
           return false;
       }
       foreach ($tables as $table) {
           if (!($table_dump = $this->_dumpTable($table))) {
               $this->error = mysqli_error($this->link_id);
               return false;
           }
           $value .= $table_dump;
       }
       return $value;
   }

   function _saveToFile($fname, $sql, $compress) {
       if ($compress) {
           if (!($zf = gzopen($fname, 'w9'))) {
               $this->error = 'Can\'t create the output file.';
               return false;
           }
           gzwrite($zf, $sql);
           gzclose($zf);
       } else {
               if (!($f = fopen($fname, 'w'))) {
                   $this->error = 'Can\'t create the output file.';
                   return false;
               }
               fwrite($f, $sql);
               fclose($f);
       }
       return true;
   }

   function _downloadFile($fname, $sql, $compress) 
   {
       header('Content-disposition: filename=' . $fname);
       header('Content-type: application/octetstream');
       header('Pragma: no-cache');
       header('Expires: 0');
       echo ($compress ? gzencode($sql) : $sql);
       return true;
   } 
   
 
}
?>