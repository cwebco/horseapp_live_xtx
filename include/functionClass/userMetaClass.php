<?php
/*
 * User Meta Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

include_once(DIR_FS_SITE.'include/functionClass/userClass.php');

class user_meta extends cwebc {
    
    function __construct() {
        parent::__construct('user_meta');
        $this->requiredVars=array('id','user_id','user_module_config','user_image_config','date','user_wizard_steps');
	
        }   
    
        
    /*get user meta  */
    function getUserMeta($user_id){
             
		$this->Where="where user_id='$user_id'";
               
                $object= $this->DisplayOne();
                
                if(is_object($object) && count($object)):
                    return $object;
                endif;
          
                return false;
                
    }     
        
     /*check user meta exists or not */
    function checkUserMetaExists($user_id){
              
                
		$this->Field="id";
		$this->Where="where user_id='$user_id'";
               
                $object= $this->DisplayOne();
                
                if(is_object($object) && count($object)):
                    return $object->id;
                endif;
          
                return '0';
                
    }   
        
        
    /*save user image congig*/
        
    function saveUserImageConfig($user_id,$theme_id){
        
       $id=0;
       
       /*check user meta exists or not */
       $id=$this->checkUserMetaExists($user_id);
     
       /*get theme folder name*/
       $theme_obj=new theme();
       $theme_fname=$theme_obj->getThemeFolderName($theme_id);

      /*get image sizes from theme config file*/
      $imageThumbConfig=array();
      if(file_exists(DIR_FS_SITE_DESIGN.$theme_fname.'/config.xml')):/*if config file exists*/
          $xml=simplexml_load_file(DIR_FS_SITE_DESIGN.$theme_fname.'/config.xml');
          foreach($xml->images as $images)
          {
             foreach($images as $module) 
             {
                 foreach($module as $size) 
                 {
                    $imageThumbConfig[$module->getName()][$size->getName()]=(array) $size;

                 }

             }
          }
        else:
           return false;
        endif;
        
        $imageThumbConfig= serialize($imageThumbConfig);
         
       /*save image configuration*/
        $query=new user_meta();
        $query->Data['user_image_config']=$imageThumbConfig;
        $query->Data['user_id']=$user_id;
        
        if(isset($id) && $id!='' && $id!='0'){ 
            $query->Data['id']=$id;
            if($query->Update())
              return $id;
        }
        else{  
       
            $query->Insert();
            return $query->GetMaxId();
        }
    }    
    
    
         
    /*save user module congig*/
        
    function saveUserModuleConfig($user_id,$theme_id){
        
       $id=0;
       
       /*check user meta exists or not */
       $id=$this->checkUserMetaExists($user_id);
     
       /*get theme folder name*/
       $theme_obj=new theme();
       $theme_fname=$theme_obj->getThemeFolderName($theme_id);

      /*get module array*/
      $moduleConfig=array();
      if(file_exists(DIR_FS_SITE_DESIGN.$theme_fname.'/config.xml')):/*if config file exists*/
          $xml=simplexml_load_file(DIR_FS_SITE_DESIGN.$theme_fname.'/config.xml');
          foreach($xml->modules as $modules)
          {
             foreach($modules as $module) 
             {
                 foreach($module as $page) 
                 {
                    $moduleConfig[$module->getName()]=(array) $module;

                 }

             }
          }
        else:
           return false;
        endif;
        
        $moduleConfig= serialize($moduleConfig);
         
       /*save module configuration*/
        $query=new user_meta();
        $query->Data['user_module_config']=$moduleConfig;
        $query->Data['user_id']=$user_id;
        
        if(isset($id) && $id!='' && $id!='0'){ 
            $query->Data['id']=$id;
            if($query->Update())
              return $id;
        }
        else{  
       
            $query->Insert();
            return $query->GetMaxId();
        }
    }    
    
    
    
    /*save user wizard steps*/
        
    function saveUserWizardSteps($user_id,$steps){
       $id=0;
       
       /*check user meta exists or not */
       $id=$this->checkUserMetaExists($user_id);
        
       
       /*save steps*/
        $query=new user_meta();
        $query->Data['user_wizard_steps']=$steps;
        $query->Data['user_id']=$user_id;
        
        if(isset($id) && $id!='' && $id!='0'){ 
            $query->Data['id']=$id;
            if($query->Update())
              return $id;
        }
        else{  
       
            $query->Insert();
            return $query->GetMaxId();
        }
       
           
        
    }
    
    
    /*to check whether a module is cumpulsory for particular user*/
    function checkModuleIsCompulsory($user_id,$module){
        
        $website_modules=array();
        $user_meta=$this->getUserMeta($user_id);
        
        if($user_meta && is_object($user_meta)):
            
             /*get website modules according to theme from user meta*/
             $website_modules=unserialize(html_entity_decode($user_meta->user_module_config));
            
             if($website_modules[$module]['is_compulsory']=='1'):
                 return true;
             endif;
         
        endif;
            
        return false;    
        
            
    }
    
    
    
    
}