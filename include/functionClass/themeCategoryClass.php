<?php

/*Class to find Theme of a particular Category*/
class theme_cat_rel extends cwebc {

    function __construct() {
        parent::__construct('theme_cat_rel');
        $this->requiredVars=array('id','t_id','c_id','position');
     }


    function delete_records_of_theme($t_id){
           $this->Where="where t_id='$t_id'";
	   $this->Delete_where();

    }
    function saveThemeCatRel($categories,$theme_id){
                    /*Firstly delete existing categories then insert new*/
                    $this->delete_records_of_theme($theme_id);
                    foreach ($categories as $k=>$v):
                           $q= new query('theme_cat_rel');

                            $q->Data['c_id']=$v;

                            $q->Data['t_id']=$theme_id;

                            $q->Insert();
                     endforeach;;
    }

      function get_all_cat_of_theme($t_id)
     {
                $this->Field="id,c_id";
		$this->Where="where t_id='".mysql_real_escape_string($t_id)."'";
                $all_cat=array();
                $all_catt= $this->ListOfAllRecords();
                foreach($all_catt as $kkk=>$vvv):
                  $all_cat[$vvv['id']]=$vvv['c_id'];
                 endforeach;
                return $all_cat ;
     }


     /*Functions for front*/
      function setCategoryId($id){
        $this->c_id=$id;
      }

     /*Get themes of particular category*/
     function getThemesOfCategory(){
        $this->Where="where c_id='".mysql_real_escape_string($this->c_id)."' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');
    }

     /*Get themes of particular category*/
     function getCategoriesHavingTheme(){
        $this->Field="distinct c_id";
        $this->Where="order by position";
        $all_cat_id=array();
        $all_cat_id= $this->ListOfAllRecords();
        $all_cat_string="0,";
        foreach($all_cat_id as $kkk=>$vvv):
          $all_cat_string.=$vvv['c_id'].",";
        endforeach;
        $string=substr_replace($all_cat_string ,"",-1);
        return $string;
    }

}
?>
