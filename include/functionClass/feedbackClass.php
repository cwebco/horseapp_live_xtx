<?php

/*
 * Feedback Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class feedback extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order='desc', $orderby='id') {
        parent::__construct('user_feedback');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id','user_id', 'title', 'email', 'address', 'phone', 'message', 'is_active', 'is_deleted','is_read');
    }

    /*
     * Create new page or update existing page
     */

    function saveFeedback($_POST,$user_id) {
        $this->Data = $this->_makeData($_POST, $this->requiredVars);
        $this->Data['is_active']='1';
        $this->Data['user_id']=$user_id;
        $this->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
        $this->Insert();
        return $this->GetMaxId();
    }

    /*
     * Get page by id
     */

    function getsFeedback($id) {
        return $this->_getObject('feedback', $id);
    }

    function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }
    
    
    /*
     * Get List of all pages in array
     */

    function listFeedback($show_active=0, $result_type='object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where is_deleted='0' AND  user_id='$this->user_id' And is_active='1' ORDER BY date desc,id desc";
        else
            $this->Where = "where is_deleted='0' AND  user_id='$this->user_id' ORDER BY date desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    
    function getUnreadFeedbacks() {
     
            $this->Where = "where is_deleted='0' AND  user_id='$this->user_id' And is_active='1' AND is_read='0' ORDER BY date desc,id desc";
    
            return $this->ListOfAllRecords('object');
    }
    
    function countUnreadFeedbacks() {
        
            $contacts=array();
     
            $this->Where = "where is_deleted='0' AND  user_id='$this->user_id' And is_active='1' AND is_read='0' ORDER BY date desc,id desc";
    
            $contacts= $this->ListOfAllRecords('object');
            
            return(count($contacts));
    }
    
    
    function getLimitedFeedbacks($limit) {
     
            $this->Where = "where is_deleted='0' AND  user_id='$this->user_id' And is_active='1' ORDER BY date desc,id desc LIMIT 0,$limit";
    
            return $this->ListOfAllRecords('object');
    }
    
    
    
    function setFeedbackRead($id){
        
        $this->Data['is_read']='1';
        $this->Where = "where  user_id='$this->user_id' AND id='$id'";
        $this->UpdateCustom();
        
        
    }
    
    
    function setAllFeedbacksRead(){
        
        $this->Data['is_read']='1';
        $this->Where = "where  user_id='$this->user_id' ";
        $this->UpdateCustom();
        
        
    }
    
    
    
    /*
     * delete a page by id
     */

    function deleteFeedback($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
     /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }

    /*
     * Get feedback by id
     */
    function getFeedbackByID($id,$show_active=1){
        if($show_active)
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0' AND is_active='1'";
        else
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0'";
      
        return $this->DisplayOne();
    }
    
}

?>