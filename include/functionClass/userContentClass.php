<?php
/*
 * Content Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class user_content extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('user_content');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','user_id','name','description','urlname','page','meta_name','meta_keyword','meta_description','page_type','position');
    }

    /*
     * Create new content or update existing content
     */
    function savePage($POST,$user_id){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['urlname']);
        }
        
        $this->Data['user_id']=$user_id;

        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{

            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Page by id
     */
    function getPage($id){
        return $this->_getObject('user_content', $id);
    }

    
    function getParticularPageOfUser($page_type,$user_id){
        
        $this->Where='Where page_type="'.mysql_real_escape_string($page_type).'" AND user_id="'.mysql_real_escape_string($user_id).'"';
       
        return $this->DisplayOne();
       
    }
    
    /*check if page exists for user*/
    function checkUserPage($page_type,$user_id){
        
            $this->Where='Where page_type='.mysql_real_escape_string($page_type).' AND user_id="'.mysql_real_escape_string($user_id).'"';
            $this->DisplayOne();
            if($this->GetNumRows()>0):
                return true;/*page exists*/
            else:
                return false;/*page does not exits*/
            endif;
        
        
    }
    
    
     /*check if page exists for user*/
    function getUserPage($page_type){
        
            $this->Where="Where page_type='".mysql_real_escape_string($page_type)."' AND user_id='$this->user_id'";
          
            $page=$this->DisplayOne();
            if($this->GetNumRows()>0):
                return $page;/*page exists*/
            else:
                return false;/*page does not exits*/
            endif;
      
    }
    
    
    function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }  
    

      
}
?>