<?php
/*
 * Date Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class date{
    
   function ToUKDate($date)
    {
            return date("d-m-Y", strtotime($date));
    }
   
    function ToUSDate($date)
    {

    $date= date("m/d/Y",strtotime($date));
    $Parts=array();
    $Parts=explode('/',$date);
    $Result=$Parts['2'].'-'.$Parts['0'].'-'.$Parts['1'];
    return $Result;
    }
    
    function TocustomDate($date)
    {
    $date= str_replace('/', '-', $date);
    $date= date("d/m/Y",strtotime($date));
    $Parts=array();
    $Parts=explode('/',$date);
    $Result=$Parts['2'].'-'.$Parts['1'].'-'.$Parts['0'];
    return $Result;
    }
    
    function add_days_to_date($days,$date)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$days." days");
            endif;
            $date_result=date('Y-m-d', $date );

            return $date_result;	
    }
    
     function add_months_to_date($months,$date)
    {

            if($months==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +1 month");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " +".$months." months");
            endif;
            $date_result=date('Y-m-d', $date );

            return $date_result;	
    }

    function subtract_days_from_date($days,$date)
    {

            if($days==1):
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -1 day");
            else:
                $date = strtotime(date("Y-m-d", strtotime($date)) . " -".$days." days");
            endif;
            $date_result=date('Y-m-d', $date );

            return $date_result;	
    }


    function dateDiff($start, $end) {

            $start_ts = strtotime($start);

            $end_ts = strtotime($end);

            if($end_ts>$start_ts):

                 $diff = $end_ts - $start_ts;

                 return round($diff / 86400);

            else:

                return 0;

            endif;


    }
    
    function MonthsBetweenTwoDates($date1, $date2)
    {
        
       
        $ts1 = strtotime($date1);
        $ts2 = strtotime($date2);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

        return $diff;
        
        
       
    }
    
    
}

$date_obj= new date();

?>