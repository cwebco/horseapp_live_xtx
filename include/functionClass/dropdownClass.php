<?php
/*
 * Drop Down Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class dropdown extends cwebc {
    
    function __construct() {
        parent::__construct('dropdown');
        $this->requiredVars=array('id', 'name','image', 'is_active','position','is_deleted');
       
        	
    }
     /*
     * Get List of all Dropdowns in array
     */
    function ListDropdowns(){
        $this->Where="where is_deleted='0'";
        $this->DisplayAll();        
    }
  
}


class dropdownValues extends cwebc{
   
    function __construct(){
        parent::__construct('dropdown_values');
        $this->requiredVars=array('id','dd_id','title','value','image','parent_id','description','is_active','position','is_deleted');
      
    }
    function setDropdownId($id){
        $this->dd_id=$id;
    }
   
    function getAllItems(){
        $this->Where="where dd_id='$this->dd_id'  AND is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();     
    }
    
    function ListAllItems(){
        $this->Where="where dd_id='$this->dd_id'  and is_active='1' AND is_deleted='0' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');     
    }
    function ListAllActiveItems(){
        $this->Where="where dd_id='$this->dd_id'  AND is_active='1' and is_deleted='0' order by $this->orderby $this->order";
        return $this->ListOfAllRecords('object');     
    }    
    
    function getTitleFromValue($dd_id,$value){
        $this->Fields="title";
        $this->Where="where dd_id='$dd_id'  AND value='$value'";
        $array=$this->DisplayOne();   
        if($array):
            return $array->title;
        else:
            return "";
        endif;
    }
    
    function getSelectedTitleFromValues($dd_id,$values){
        $titles="";
        $selected_values=array();
        $selected_values=explode(',', $values);
        if($selected_values && is_array($selected_values)):
            foreach($selected_values as $k=>$v):
                $title_obj=new dropdownValues();
                $title=$title_obj->getTitleFromValue($dd_id,$v);
                if($title!="" && is_string($title)):
                   $titles.= $title.",";
                endif;

            endforeach;
            $titles=rtrim(trim(rtrim($titles, ",")));
        endif;    
        return $titles;
        
        
    }
    
    
    /*
     * Create new Dropdown value or update existing Dropdown value 
     */
    function saveDropdownValue($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        
        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();

        if($image_obj->upload_photo('values', $_FILES['image'], $rand)):
                $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;
                  
        if(isset($this->Data['id']) && $this->Data['id']!=''){
                     
            if($this->Update())
              return $Data['id'];
        }
        else{
            
                       
            $this->Insert();
            return $this->GetMaxId();
        }
    }
   function get_all_cat_of_dropdown($dd_id){
            $this->Field="id";
            $this->Where="where dd_id='$dd_id' and is_active='1' and is_deleted='0'";  
            $all_cat=array();
            $all_catt= $this->ListOfAllRecords();
            foreach($all_catt as $kkk=>$vvv):
              $all_cat[]=$vvv['id'];
             endforeach;
            return $all_cat ;
 } 
   
   function getHorseStatus()
   {
        /* return array of horse status*/
       $loantenure= new query('dropdown_values');
       $loantenure->Where="where dd_id='1' and is_active='1' order by position";
       $loantenure->DisplayAll();
       $loanten=array();
            if($loantenure->GetNumRows()):
                while($object2 = $loantenure->GetObjectFromRecord()):
                         $loanten[]=$object2;
                endwhile;
            endif;
        return($loanten);
   } 
   
   function getDropdown($id){
       /* return array of dropdown*/
       $loantenure= new query('dropdown_values');
       $loantenure->Where="where dd_id='$id' and is_active='1' order by position";
       $loantenure->DisplayAll();
       $loanten=array();
            if($loantenure->GetNumRows()):
                while($object2 = $loantenure->GetObjectFromRecord()):
                         $loanten[]=$object2;
                endwhile;
            endif;
        return($loanten);
   }
}
?>