<?php
/*
 * News Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class searchIndex extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='desc', $orderby='id'){
        parent::__construct('search_index');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'table_name', 'page_name', 'keyword', 'description', 'item_id', 'meta_keyword', 'is_deleted', 'is_active');
    }

    /*
     * Create new page or update existing page
     */
    function saveIndex($POST,$item_id,$tableName,$pageName){
        /** Get Description *******************************/
        if(isset($POST['short_description']) && $POST['short_description']!='')
        {
            $des=$POST['short_description'];
        }
        else if(isset($POST['page']) && $POST['page']!=''){
            $des=$POST['page'];
        }
        else if(isset($POST['answer']) && $POST['answer']!='')
        {
            $des=$POST['answer'];
        }
        else if(isset($POST['description']) && $POST['description']!='')
        {
            $des=$POST['description'];
        }
        else
        {
            $des=$POST['long_description'];
        }
        /******** End description code *******************/

        /***** Get Keyword ******************************/
        if(isset($POST['name']) && $POST['name']!='')
        {
            $keyword=$POST['name'];
        }
        else if(isset($POST['question']) && $POST['question']!='')
        {
            $keyword=$POST['question'];
        }
        else
        {
            $keyword=$POST['page_name'];
        }
        /****** End Keyword Code ******************************/

        $arr=array('table_name'=>$tableName,'page_name'=>$pageName,'keyword'=>$keyword,'description'=>$des,'item_id'=>$item_id,'is_active'=>$POST['is_active']);


        $this->Data=$this->_makeData($arr, $this->requiredVars);

        if($tableName!='content')
        {
            $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        }
        else
        {
            $this->Data['is_active']='1';
        }
            $this->Insert();
            return $this->GetMaxId();
    }

    function updateIndex($POST,$item_id,$tableName,$pageName)
    {
        if(isset($POST['short_description']) && $POST['short_description']!='')
        {
            $des=$POST['short_description'];
        }
        else if(isset($POST['page']) && $POST['page']!=''){
            $des=$POST['page'];
        }
        else if(isset($POST['answer']) && $POST['answer']!='')
        {
            $des=$POST['answer'];
        }
        else if(isset($POST['description']) && $POST['description']!='')
        {
            $des=$POST['description'];
        }
        else
        {
            $des=$POST['long_description'];
        }
        /******** End description code *******************/

        /***** Get Keyword ******************************/
        if(isset($POST['name']) && $POST['name']!='')
        {
            $keyword=$POST['name'];
        }
        else if(isset($POST['question']) && $POST['question']!='')
        {
            $keyword=$POST['question'];
        }
        else
        {
            $keyword=$POST['page_name'];
        }
        /****** End Keyword Code ******************************/

        $arr=array('page_name'=>$pageName,'keyword'=>$keyword,'description'=>$des,'is_active'=>$POST['is_active']);


        $this->Data=$this->_makeData($arr, $this->requiredVars);
        if($tableName!='content')
        {
            $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        }
        else
        {
            $this->Data['is_active']='1';
        }

        $this->Where="where table_name='$tableName' AND item_id=".mysql_real_escape_string($item_id);
        $this->UpdateCustom();

    }


     /* delete a page by id */
    function deleteIndex($id,$tableName){
        $this->Where="where table_name='$tableName' AND item_id=".mysql_real_escape_string($id);
        return $this->SoftDeleteCustom();
    }

    /*Restore*/
    function RestoreIndex($id,$tableName){
        $this->Where="where table_name='$tableName' AND item_id=".mysql_real_escape_string($id);
        return $this->RestoreCustom();
    }

    /*Permanent Delete*/
    function permanentIndexDelete($id,$tableName){
        $this->Where="where table_name='$tableName' AND item_id=".mysql_real_escape_string($id);
        return $this->Delete_where();
    }

    /*Update status*/
    function updateIndexIs_Active($tableName,$item_id,$is_active)
    {
        $this->Data['is_active']=$is_active;
        $this->Where="where table_name='$tableName' AND item_id='".mysql_real_escape_string($item_id)."'";
        $this->UpdateCustom();
    }
    function updateIndexPageName($tableName,$item_id,$pageName)
    {
        $this->Data['page_name']=$pageName;
        $this->Where="where table_name='$tableName' AND item_id='".mysql_real_escape_string($item_id)."'";
        $this->UpdateCustom();
    }

    function searchResults($keyword)
    {
        $this->Where="where is_active='1' AND is_deleted='0' AND keyword LIKE '%".$keyword."%' OR description LIKE '%".$keyword."%'";
        return $this->ListOfAllRecords('object');
    }

}
?>