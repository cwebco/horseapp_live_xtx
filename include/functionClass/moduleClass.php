<?php
/*
 * Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class module extends cwebc {
    
    function __construct() {
        parent::__construct('module');
        $this->requiredVars=array('id','page_name','display_name','table_name','is_active','is_compulsory','date');
        
     }
          

      function getAllModules(){
		
		$this->Where="where is_active='1' order by id asc";    
		return $this->ListOfAllRecords('object');        
     }
     
  
    
     function getModuleName($id)
     {          
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
              
                $module= $this->DisplayOne();
                return $module->display_name;
     }   
     
     function getModuleObj($id)
     {          
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
                $cat= $this->DisplayOne();
                return $cat;
     }   
     
      /*used in both front and admin for showing modules*/
    function getAllModuleNames()
     {
               $all_mod=array();
                $this->Field="id,display_name";
		$this->Where="where is_active='1'";
                $all_catt=$this->ListOfAllRecords();
                 foreach($all_catt as $kkk=>$vvv):
                  $all_mod[$vvv['id']]=$vvv['display_name'];
                 endforeach;
                return $all_mod;

     }
     
      function getAllModulesOfCategory($module_id)
     {
                $all_mod=array();
                $this->Where="where is_active='1' AND id IN($module_id)";
                $all_mod=$this->ListOfAllRecords();
                
                return $all_mod;

     }

       
     
    
     
}
