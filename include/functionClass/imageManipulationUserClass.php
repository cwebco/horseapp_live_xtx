<?php

/*
 * Image Manupulation Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */



class imageManipulation {

    private $mimeTypes;
    private $errorMsg;

    function __construct() {
        global $conf_allowed_photo_mime_type;
        $this->mimeTypes = $conf_allowed_photo_mime_type;
        $this->isCrop = false;
        $this->errorMsg = new errorManipulation();
    }



    function makeSourcePath($type, $image, $folderName='large') {

        if ($type == '') {
            return false;
        }
        if ($image == '') {
            return false;
        }
        if ($folderName == '') {
            $folderName = 'large';
        }

        return DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName . '/' . $image;
    }

    function makeDestinationPath($type, $image, $folderName='thumb') {

        if ($type == '') {
            $type = 'default';
        }
        if ($image == '') {
            $image = date("ymdhis") . '.jpg';
        }
        if ($folderName == '') {
            $folderName = 'thumb';
        }

        if (!is_dir(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName))
            @mkdir(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName,755);
            chmod(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName,755);
        return DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $folderName . '/' . $image;
    }



    function makeImageName($name, $id) {
        $file_name_parts=pathinfo($name);
        $file_name_parts['filename'].=$id;
        $file=$file_name_parts['filename'].'.'.$file_name_parts['extension'];
        return $file;
    }

    function makeFileName($name, $id) {
        $file_name_parts=pathinfo($name);
        $file_name_parts['filename'].=$id;
        $file=$file_name_parts['filename'].'.'.$file_name_parts['extension'];
        return $file;
    }

    function create_resized_for_module($module, $large_image){
	global $imageThumbConfig;

	if(isset($imageThumbConfig[$module]) && is_array($imageThumbConfig[$module])){

		foreach($imageThumbConfig[$module] as $k=>$v){
		        $crop=0;
                        (isset($v['crop']) &&  ($v['crop']==='1' || $v['crop']===true) )?$crop='1':"";
                        $this->create_resized_crop($module, $large_image, $k, $v['width'], $v['height'],$crop);
		}
	}
}

 /*
     * type = module
     * image = image name
     * folderName = thumbnail folder name (for identification and access)
     * width = final image width (max)
     * height = final image height  (max)
     */

function create_resized_crop($type, $image, $folderName, $Rwidth, $Rheight,$crop=0){
        /* Get the original geometry and calculate scales */
	$source_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/large/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName.'/'.$image;

        if(!is_dir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName)){
		@mkdir(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName, 755);   //maximum 777
                chmod(DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folderName, 755);
	}

	list($width, $height, $img_type, $attr) = getimagesize($source_path);

	/* should we resize the image? only if height and width both are higher than the base values */
	if($width>$Rwidth && $height>$Rheight){
                $xscale=$width/$Rwidth;
                $yscale=$height/$Rheight;

                /* Yes - we should resize it */
		/* Recalculate new size with default ratio */
		if ($yscale>$xscale){
                    /* image is portrait */
                    $new_width = round($width * (1/$xscale));
                    $new_height = round($height * (1/$xscale));
		}
		else {
                    /* image is landscape */
                    $new_width  = round($width  * (1/$yscale));
                    $new_height = round($height * (1/$yscale));
		}
		/* Resize the original image & output */
		$imageResized = imagecreatetruecolor($new_width, $new_height);

                /*Turn on image alpha blending for png images*/
                if(strtolower(substr($source_path, -3))=='png'):
                    imagealphablending($imageResized, false);
                    imagesavealpha($imageResized,true);
                    $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                    imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

                endif;


		/* check image format and create image */
		if(strtolower(substr($source_path, -3))=='jpg')
			 $imageTmp     = imagecreatefromjpeg ($source_path);
		elseif(strtolower(substr($source_path, -3))=='gif')
			 $imageTmp     = imagecreatefromgif($source_path);
		elseif(strtolower(substr($source_path, -3))=='png')
			  $imageTmp     = imagecreatefrompng($source_path);



		imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		$this->output_img($imageResized, image_type_to_mime_type($img_type), $destination_path);
                if($crop==1):
                   $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                endif;
        }
	else{
                /* We should crop the image */
                if($crop==1):
                    /* firstly, we should copy the image to right folder */
                    if(copy($source_path, $destination_path)){
                       $this->crop_image($type, $image, $folderName, $Rwidth, $Rheight);
                    }
                    else{
                       echo 'sorry! could not copy the file to destination folder. Plese check folder permissions';
                    }
                else:
                    if(!copy($source_path, $destination_path)){
                      echo 'sorry! could not copy the file to destination folder. Plese check folder permissions';
                    }

                endif;



        }
}



function crop_image($type, $image, $folder, $twidth, $theight){
        $source_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folder.'/'.$image;
	$destination_path=DIR_FS_SITE_UPLOAD.'photo/'.$type.'/'.$folder.'/'.$image;
	list($width, $height) = getimagesize($source_path);

        $new_width=$twidth;
        $new_height=$theight;

        if($width<=$twidth){ $new_width=$width; }
	if($height<=$theight){ $new_height=$height; }

	$cropping=$this->cropFromCenter($twidth, $theight, $width, $height);
	if(count($cropping))
	{
            $cropX=$cropping['cropX'];
            $cropY=$cropping['cropY'];

            $imageResized = imagecreatetruecolor($new_width, $new_height);

          /*Turn on image alpha blending for png images*/
            if(strtolower(substr($source_path, -3))=='png'):
                imagealphablending($imageResized, false);
                imagesavealpha($imageResized,true);
                $transparent = imagecolorallocatealpha($imageResized, 255, 255, 255, 127);
                imagefilledrectangle($imageResized, 0, 0, $new_width, $new_height, $transparent);

            endif;
            
            #check image format and create image
            if(strtolower(substr($source_path, -3))=='jpg'):
                     $imageTmp     = imagecreatefromjpeg ($source_path);
            elseif(strtolower(substr($source_path, -3))=='gif'):
                     $imageTmp     = imagecreatefromgif($source_path);
            elseif(strtolower(substr($source_path, -3))=='png'):
                      $imageTmp     = imagecreatefrompng($source_path);
            endif;

            //imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagecopyresampled($imageResized, $imageTmp, 0, 0, $cropX, $cropY, $new_width, $new_height, $new_width, $new_height);
            $this->output_img($imageResized, image_type_to_mime_type(getimagesize($source_path)), $destination_path);
	}
}

function cropFromCenter($new_w, $new_h, $curr_w, $curr_h) {

        if($new_w > $curr_w)$new_w=$curr_w;
        if($new_h > $curr_h)$new_h=$curr_h;

        $cropping=array();
    $cropX = intval(($curr_w - $new_w) / 2);
    $cropY = intval(($curr_h - $new_h) / 2);

        $cropping['cropX']=$cropX;
        $cropping['cropY']=$cropY;

        return $cropping;
}

    function upload_photo($type, $file_name, $rand='') {
        $admin_user = new admin_session();
        global $conf_allowed_photo_mime_type;
        if ($file_name['error']):
            // $this->errorMsg->errorAdd("unable to upload!!");
            return false;
        endif;
        $image_name = $this->makeImageName($file_name['name'], $rand);

        if (in_array($file_name['type'], $conf_allowed_photo_mime_type)):
            if (move_uploaded_file($file_name['tmp_name'], DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image_name)):
                if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/thumb/' . $image_name)):
                    $admin_user->set_pass_msg('Image already exists with same name.Please select another image .');
                    return false;
                else:
                    $this->create_resized_for_module($type, $image_name);

                    //$this->createThumbs($type, $image_name);
                    return true;
                endif;
            else:
                return false;
            endif;
        endif;
        return false;
    }

    /*
     * type = module
     * image = image name
     * size= thumb,medium,large etc
     * rtype = return image path or not
     */

    function get_image($type, $size, $image, $rtype=false) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image) && $image):
            if ($rtype):
                return DIR_WS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;
            else:
                echo DIR_WS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;
            endif;
        else:
            if ($rtype):
                return DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
            else:
                echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
            endif;
        endif;
    }

    function get_image_tag($type, $size, $image, $rtype=false) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image) && $image):
            if ($rtype)
                return '<img src="' . $this->get_image($type, $size, $image, true) . '" border="0" alt="' . $image . '">';
            else
                echo '<img src="' . $this->get_image($type, $size, $image) . '" border="0" alt="' . $image . '">';
        else:
            if ($rtype):
                return '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg' . '" border="0" alt="' . $image . '">';
            else:
                echo '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg' . '" border="0" alt="' . $image . '">';
            endif;
        endif;
    }

    function delete_if_image_exists($type, $size, $image) {
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image)):
            unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image);
        endif;
    }

    function DeleteImagesFromAllFolders($type, $image) {
        global $imageThumbConfig;
        /* delete large image if exists */
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image)):
            @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image);
        endif;
        /* delete resized image if exists */
        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image)):
            @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image);
        endif;
        /* delete from all other folders */
        foreach ($imageThumbConfig[$type] as $key => $value):
            if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $key . '/' . $image)):
                @unlink(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $key . '/' . $image);
            endif;
        endforeach;
    }

    /* Crop Image */

    function crop_photo($type, $size, $image, $crop_width, $crop_height, $crop_x, $crop_y, $new_width, $new_height) {

        if (file_exists(DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image)):

            $this->create_resized($type, $size, $image, $new_width, $new_height);

            /* Get the original geometry and calculate scales */

            $source_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image;
            $destination_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/' . $size . '/' . $image;

            list($width, $height, $itype) = getimagesize($source_path);
            /* Resize the original image & output */

            /* check image format and create image */
            if (strtolower(substr($source_path, -3)) == 'jpg'):
                $imageTmp = imagecreatefromjpeg($source_path);
            elseif (strtolower(substr($source_path, -3)) == 'gif'):
                $imageTmp = imagecreatefromgif($source_path);
            elseif (strtolower(substr($source_path, -3)) == 'png'):
                $imageTmp = imagecreatefrompng($source_path);
            endif;
            $imageResized = imagecreatetruecolor($crop_width, $crop_height);
            imagecopyresized($imageResized, $imageTmp, 0, 0, $crop_x, $crop_y, $crop_width, $crop_height, $crop_width, $crop_height);
            $this->output_img($imageResized, image_type_to_mime_type($itype), $destination_path);

            return true;
        else:

            return false;
        endif;
    }

    function create_resized($type, $size, $image, $w, $h) {

        // Get the original geometry and calculate scales

        $source_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/large/' . $image;
        $destination_path = DIR_FS_SITE_UPLOAD . 'photo/' . $type . '/resized/' . $image;

        list($width, $height, $itype, $attr) = getimagesize($source_path);
        $new_width = $w;
        $new_height = $h;

        #check image format and create image
        if (strtolower(substr($source_path, -3)) == 'jpg'):
            $imageTmp = imagecreatefromjpeg($source_path);
        elseif (strtolower(substr($source_path, -3)) == 'gif'):
            $imageTmp = imagecreatefromgif($source_path);
        elseif (strtolower(substr($source_path, -3)) == 'png'):
            $imageTmp = imagecreatefrompng($source_path);
        endif;

        // Resize the original image & output
        $imageResized = imagecreatetruecolor($new_width, $new_height);
        # $imageTmp     = imagecreatefromjpeg ($source_path);
        imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $this->output_img($imageResized, image_type_to_mime_type($itype), $destination_path);
    }

    function output_img($rs, $mime, $path) {
        switch ($mime) {
            case 'image/jpeg':
            case 'image/jpg':
            case 'image/pjpeg':
            case 'image/pjpg':
                imagejpeg($rs, $path, 100);
                break;
            case 'image/gif':
                imagegif($rs, $path, 100);
                break;
            case 'image/png':
                imagepng($rs, $path, 9);
                break;
            default:
                imagejpeg($rs, $path, 100);
        }
    }

}

?>