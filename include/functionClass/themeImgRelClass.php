<?php

/*Class to find images of theme according to category*/
class theme_img_rel extends cwebc {

    function __construct() {
        parent::__construct('theme_img_rel');
        $this->requiredVars=array('id','theme_id','category_id','image','url','date');
     }


    function deleteRecordsOfTheme($t_id){
           $this->Where="where theme_id='$t_id'";
	   $this->Delete_where();

    }
    
    function deleteRecordsOfThemeOfCategory($t_id,$c_id){
           $this->Where="where theme_id='$t_id' AND category_id='$c_id'";
	   $this->Delete_where();

    }
    
 
    function saveThemeCatImageRel($categories,$images,$theme_id){
                  
                    foreach ($categories as $k=>$v):
                        
                            $_FILES['image']=array();
                            $_FILES['image']=array('name'=>$images['name'][$v],'type'=>$images['type'][$v],'tmp_name'=>$images['tmp_name'][$v],'error'=>$images['error'][$v],'size'=>$images['size'][$v]);
                           
                            if($_FILES['image']['name']!="" && !empty($_FILES['image']['name'])): /*if image uploaded for category*/
                                
                                    $rand=rand(0, 99999999);
                                    $image_obj=new imageManipulation();

                                    if($image_obj->upload_photo('theme', $_FILES['image'], $rand)):

                                             /*Get image of theme of particular category*/
                                             $theme_image_obj=new theme_img_rel();
                                             $theme_image=$theme_image_obj->getThemeCategoryImage($theme_id,$v);

                                             #delete images from all folders
                                             $image_obj=new imageManipulation();
                                             $image_obj->DeleteImagesFromAllFolders('theme',$theme_image->image);
                                        
                                             /*delete old theme category image entry*/
                                        
                                             $query_image=new theme_img_rel();
                                             $query_image->deleteRecordsOfThemeOfCategory($theme_id,$v);
                                          
                                             
                                             /*add new image of category*/ 
                                        
                                             $image_uploaded=$image_obj->makeFileName($_FILES['image']['name'], $rand);

                                             $q= new query('theme_img_rel');

                                             $q->Data['category_id']=$v;

                                             $q->Data['theme_id']=$theme_id;

                                             $q->Data['image']=$image_uploaded; 

                                             $q->Insert();

                                    endif;
                            endif;        
                            
                     endforeach;;
    }
    
    

     /*set theme id */
      function setThemeId($id){
        $this->theme_id=$id;
      }

      
      /*Get image of theme of particular category*/
     function getThemeCategoryImage($theme_id,$category_id){
        $this->Where="where theme_id='".mysql_real_escape_string($theme_id)."' AND category_id='".mysql_real_escape_string($category_id)."'";
        return $this->DisplayOne();
    } 
    

   
}
?>
