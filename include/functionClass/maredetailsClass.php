<?php 



class maredetails extends cwebc {

	function __construct() {
		parent::__construct('mare_details');
		$this->requiredVars = array('id', 'mare_id', 
			'mare_name', 'boarding_at',  'visting',   'date_of_arrival_borading_stud',   'date_of_birth',   'color','sex', 'sire',  'signed', 'name',   'position','on_behalf', 
			'date', 'additional_info',    'is_passport_or_legible_photocopy', 'is_attached_one_clitoral_swab',   'attached_one_clitoral_swab','is_endometrial', 'endometrial','is_negative_EVA_blood','negative_EVA_blood','is_EIA_blood_test', 'EIA_blood_test',  'date_of_outside_arrival_in_Country',  'country_of_origin', 
			'is_EVA_blood_test_no_more_than_28_days', 'EVA_blood_test_no_more_than_28_days', 
			'is_EVA_blood_test_minimum_of_14_days', 'EVA_blood_test_minimum_of_14_days', 'is_EIA_blood_test_no_more_14_days', 'EIA_blood_test_no_more_14_days', 
			'is_EIA_blood_test_taken_min_14_days',	'EIA_blood_test_taken_min_14_days', 
			'is_EIA_blood_test_taken_within_21_days','EIA_blood_test_taken_within_21_days',  'is_arrived_from_germany', 'arrived_from_germany', 'international_travel_documents','date_add', 'date_upd');

	}

	function savemaredetails($POST)
	{
		$this->Data = $this->_makeData($POST, $this->requiredVars);
		if (isset($this->Data['id']) && $this->Data['id'] != '') {
			$id = $this->Data['id'];
			if ($this->Update()):
				return $id;
			endif;
		}
		else {
			$this->Data['date_add'] = date("Y-m-d H:i:s");
			if ($this->Insert()):
				return $this->GetMaxId();
			endif;
		}
		return false;
	}
	function getmaredata($id)
	{
		$this->Where = " where mare_id='$id'";
        return $this->DisplayOne();
	}
}

class infectionFree extends cwebc {

	function __construct() {
		parent::__construct('freedom_from_infection');
		$this->requiredVars = array('id', 'mare_name', 'date_of_birth', 'color', 'sire', 'dam', 'dam_sire', 'grand_dam', 'passport_no', 'microchip', 'status', 'last_covering_sire', 'last_service_date', 'boarding_stud_2018', 'foal_details_2019', 'boarded_at_2015', 'boarded_at_2016', 'boarded_at_2017', 'mated_with_2015', 'mated_with_2016', 'mated_with_2017', 'result_2015', 'result_2016', 'result_2017', 'owner_name_address', 'owner_phone_no', 'owner_mobile', 'owner_fax', 'owner_mail', 'stud_name_address', 'stud_phone_no', 'stud_mobile', 'stud_fax', 'stud_mail', 'is_tested_positive_for_CEMO', 'tested_positive_for_CEMO', 'is_stud_farm', 'stud_farm', 'is_any_other_infectious', 'any_other_infectious', 'is_non_throughbreads', 'non_throughbreads', 'is_stiched', 'is_readily_to_a_teaser', 'is_temperamental_abnormalities', 'temperamental_abnormalities', 'is_ouside_the_UK', 'ouside_the_UK', 'is_walked', 'walked', 'signed', 'name', 'date', 'agent', 'date_add', 'date_upd', 'mare_id');

	}

	function saveinfectionFreedetails($POST)
	{
		// $this->print=1;
		$this->Data = $this->_makeData($POST, $this->requiredVars);
		if (isset($this->Data['id']) && $this->Data['id'] != '') {
			$id = $this->Data['id'];
			if ($this->Update()):
				return $id;
			endif;
		}
		else {
			$this->Data['date_add'] = date("Y-m-d H:i:s");
			if ($this->Insert()):
				return $this->GetMaxId();
			endif;
		}
		return false;
	}
	function getinfectionFreeddata($id)
	{
		$this->Where = " where mare_id='$id'";
        return $this->DisplayOne();
	}
}

?>