<?php
/*
 * Website User Payment  Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class user_payment_history extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('user_payment_history');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','user_id','website_plan_user_id','amount','date_of_payment','payment_method','transaction_id','date','recurring_cost','is_deleted','comment','is_payment','to_date');

	}

     /*
     * Create new payment
     */
        
    function saveWebsiteUserPayment($payment){
        
        $this->Data=$this->_makeData($payment, $this->requiredVars);
       
      
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){ 
            if($this->Update())
              return $_POST['id'];
        }
        else{  
            $this->Data['date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
    
    
    
    function CreateUserFirstManualPayment($website_plan_user_id,$payment){
        $this->Data=$this->_makeData($payment, $this->requiredVars);
        
        $this->Data['amount']=$payment['payment_made'];
        $this->Data['payment_method']="manual";
        $this->Data['is_payment']="1";
        $this->Data['website_plan_user_id']=$website_plan_user_id;
        $this->Data['date']=$date_obj->ToUSDate($payment['date']);
      
        
        $this->Insert();
        return $this->GetMaxId();
      
    }
    
    

     /*
     * Get List of all payments of user
     */
    function listWebsiteUserPayments($user_id,$result_type='object'){
		$user_id=mysql_real_escape_string($user_id);
		$this->Where="where user_id='$user_id' AND is_deleted='0' AND is_payment='1' ORDER BY date_of_payment desc, id asc";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }
    
        
    
    
    
    function getTotalAmountPaidByUser($user_id){
        $this->Field=('SUM(amount) as amount');
        $this->Where="where user_id='".$user_id."' AND is_deleted='0' AND is_payment='1' ";
        $record=$this->DisplayOne();
        if($record && $record->amount!=""):
           return $record->amount;
        else:
           return '0';
        endif;
     }
     
     function getTotalAmountPaidByUserUnderWebsitePlan($user_id,$user_website_plan_id){
        $this->Field=('SUM(amount) as amount');
        $this->Where="where user_id='".$user_id."' AND website_plan_user_id='".$user_website_plan_id."' AND is_deleted='0' AND is_payment='1'";
        $record=$this->DisplayOne();
        if($record && $record->amount!=""):
            return $record->amount;
        else:
           return '0';
        endif;
     }


     
      
     /*update  payment status */
    
    function updatePaymentStatus($id){
        
        $this->Data['id']=$id;
        $this->Data['is_payment']='1';
        $this->Update();
                
    }
    
    function getPayment($id,$user_id){
        
        $this->Where="where user_id='".$user_id."' AND id='".$id."' AND is_deleted='0'";
        return $this->DisplayOne();
        
     }

   
}
?>