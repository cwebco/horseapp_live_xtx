<?php
/*
 * Email Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class email {

    function SendEmail($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = true) {
        return $this->send_email($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = false);
    }

    function send_email($Subject, $ToEmail, $FromEmail, $FromName, $Message, $Bcc = "", $Format = "html", $FileAttachment = false, $AttachmentFileName = "", $IS_SMTP = false) {

        $mail = new PHPMailer();
        $mail->CharSet = "UTF-8";
        if ($IS_SMTP) {
            $mail->IsSMTP();            // send via SMTP
            $mail->SMTPDebug = 0;       // enables SMTP debug information (for testing)
            $mail->SMTPSecure = strtolower(SMTP_SECURITY); // sets the prefix to the servier
            $mail->Host = SMTP_HOST;      // sets GMAIL as the SMTP server
            $mail->Port = SMTP_PORT;                   // set the SMTP port for the GMAIL server
            $mail->Username = SMTP_USERNAME;  // GMAIL username
            $mail->Password = SMTP_PASSWORD;
            #$mail->Host     = "exchange.webcreationuk.com"; // SMTP servers
            $mail->SMTPAuth = (SMTP_AUTH) ? true : false;     // turn on SMTP authentication	
            #$mail->Username = "testing@wcukdev.co.uk";  // SMTP username
            #$mail->Password = "development"; // SMTP password
        }

        $mail->From = $FromEmail;
        $mail->FromName = $FromName;
        $mail->AddAddress(trim($ToEmail), trim($ToEmail));
//        $MyBccArray = explode(",", $Bcc);
//
//        $MyBccArray[] = "";
//
//        foreach ($MyBccArray as $Key => $Value) {
//            if (trim($Value) != "") {
//                $mail->AddBCC("$Value");
//            }
//        }
        if ($Format == "html") {
            $mail->IsHTML(true);
        }                               // send as HTML
        else {
            $mail->IsHTML(false);
        }                               // send as Plain

        $mail->Subject = $Subject;
        $mail->Body = $Message;
        //$mail->AltBody  =  $Message;

        if ($FileAttachment) {
            $mail->AddAttachment($AttachmentFileName, basename($AttachmentFileName));
        }
        if (!$mail->Send()) {
            return false;
        }
        return true;
    }

    function send_db_email($email_id, $to_email = ADMIN_EMAIL, $array = array()) {
        $object = get_object('email', $email_id);
        $content = $object->email_text;
        $subject = $object->email_subject;

        $commonArray = array(
            "SITE_NAME" => SITE_NAME,
            "ADMIN_EMAIL" => ADMIN_EMAIL,
            "SITE_URL" => DIR_WS_SITE,
            "LOGO_URL" => "<img src='" . DIR_WS_SITE_GRAPHIC . "social/logo.png'/>",
            "LOGO" => DIR_WS_SITE_GRAPHIC . 'logo.png',
        );

        $finalArray = array_merge($commonArray, $array);

        /* Check Header */

        if (isset($object->header) && $object->header == '1'):
            $header = get_object('email', 26);
            $content = $header->email_text . "&nbsp;" . $content;
        endif;

        /* Check Footer */
        if (isset($object->header) && $object->header == '1'):
            $footer = get_object('email', 27);
            $content = $content . "&nbsp;" . $footer->email_text;
        endif;


        if (is_object($object)):
            if (count($finalArray)):
                foreach ($finalArray as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = htmlspecialchars_decode(str_replace($literal, $v, $content));
                    $subject = html_entity_decode(str_replace($literal, $v, $subject));
                endforeach;
            endif;
            if ($this->SendEmail($subject, $to_email, SMTP_USERNAME, SITE_NAME, $content, ADMIN_EMAIL, 'html', false, '', true)):
                return true;
            else:
                return false;
            endif;
        else:
            return false;
        endif;
    }

}

?>
