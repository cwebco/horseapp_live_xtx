<?php
function get_control_icon($name)
{
	return '<img src="'.DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/images/table/'.$name.'.png" border="none" >';

}

function get_facebook_icon($module,$id)
{
    if(defined('FDP') && FDP==1){

          return '<a href="'.DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/facebook/post.php?mod='.$module.'&id='.$id.'" class="tipTop fancybox" style="width:499;height:600px;" title="Click Here to Post a Message in Facebook Account"><img src="'.DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/images/table/facebook.png" border="none" ></a>';
        }
}
function get_twitter_icon($module,$msg)
{
    if(defined('TDP') && TDP==1){

         return '<a href="'.DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/twitter/post.php?mod='.$module.'&msg='.$msg.'" class="tipTop fancybox" title="Click Here to Post a Message in Twitter Account"><img src="'.DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/images/table/twitter.png" border="none" >';

         }
}

function get_thumb($type, $image)
{
	echo DIR_WS_SITE.'upload/photo/'.$type.'/thumb/'.$image;
}

 /*
 * type = module
 * image = image name
 * size= thumb,medium,large etc
 * rtype = return image path or not
 */

function get_image($type, $size, $image, $rtype=false) {
    if (file_exists(DIR_FS_SITE_PUBLIC . 'upload/photo/' . $type . '/' . $size . '/' . $image) && $image):
        if ($rtype):
            return DIR_WS_SITE . 'upload/photo/' . $type . '/' . $size . '/' . $image;
        else:
            echo DIR_WS_SITE . 'upload/photo/' . $type . '/' . $size . '/' . $image;
        endif;
    else:
        if ($rtype):
            return DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
        else:
            echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg';
        endif;
    endif;
}
    
    
?>