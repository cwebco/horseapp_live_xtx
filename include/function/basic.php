<?php
include_once(DIR_FS_SITE.'include/function/users.php');
function include_functions($functions)
{
	foreach ($functions as $value):
		if(file_exists(DIR_FS_SITE.'include/function/'.$value.'.php')):
			include_once(DIR_FS_SITE.'include/function/'.$value.'.php');
		endif;
	endforeach;
}


function __autoload($class_name)
{
    include DIR_FS_SITE.'include/functionClass/'.$class_name . '.php';
}


function display_message($unset=0)
{
	$admin_user= new admin_session();

	if($admin_user->isset_pass_msg()):
                if(isset($_SESSION['admin_session_secure']['msg_type']) && $_SESSION['admin_session_secure']['msg_type']==0):
                   $error_type='error';
                else:
                   $error_type='success'; 
                endif;
		foreach ($admin_user->get_pass_msg() as $value):
                    echo '<div class="alert alert-'.$error_type.'">';
                    echo '<button class="close" data-dismiss="alert"></button><strong>';
                    echo $value;
                    echo '</strong></div>';
                endforeach;
	endif;
      ($unset)?$admin_user->unset_pass_msg():'';
}

function get_var_if_set($array, $var, $preset='')
{
	return isset($array[$var])?$array[$var]:$preset;
}

function get_var_set($array, $var, $var1)
{
	if(isset($array[$var])):
		return $array[$var];
	else:
		return $var1;
	endif;
}

function get_template($template_name, $array, $selected='')
{
	include_once(DIR_FS_SITE.'template/'.TEMPLATE.'/'.$template_name.'.php');
}

function get_meta($page){
	$page=trim($page);
	if($page!=''):
		$query = new query('keywords');
		$query->Where="where page_name='$page'";
		if($content=$query->DisplayOne()):
			return $content;
		else:
			return null;
		endif;
	endif;
	return null;

}

function head($content='')
{
	/*include all the header related things here... like... common meta tags/javascript files etc.*/
		global $page;
                global $content;
		if(is_object($content)){
		?>
			<title><?php echo isset($content->name) && $content->name?$content->name:'';?></title>
                        <meta name='copyright' content='Cweb Consultants India'>
                        <meta name='author' content='Cweb Consultants India'>
                        <meta name='og:site_name' content='<?php echo defined('SITE_NAME')?SITE_NAME:''?>'>
			<meta name='og:email' content='<?php echo defined('SUPPORT_EMAIL')?SUPPORT_EMAIL:''?>'>
                        <meta name='og:phone_number' content='<?php echo defined('PHONE_NUMBER')?PHONE_NUMBER:''.', '.defined('MOBILE_NO')?MOBILE_NO:'';?>'>
                        <meta name='og:fax_number' content='<?php echo defined('FAX_NO')?FAX_NO:''?>'>
                        <meta name='og:street-address' content='<?php echo defined('ADDRESS1')?ADDRESS1:''?>'>
                        <meta name='og:postal-code' content='<?php echo defined('ZIP_CODE')?ZIP_CODE:''?>'>
                        <meta name='og:country-name' content='<?php echo defined('COUNTRY_NAME')?COUNTRY_NAME:''?>'>
                        <meta name="keywords" content="<?php echo isset($content->meta_keyword)?$content->meta_keyword:'';?>" />
			<meta name="description" content="<?php echo isset($content->meta_description)?$content->meta_description:'';?>" />
		<?php } ?>

		<link rel="shortcut icon" href="<?php echo DIR_WS_SITE_GRAPHIC?>favicon.ico" />
		<?php include_once(DIR_FS_SITE.'include/template/stats/google_analytics.php');?>
	<?
}


function css($array=array('reset','master')){
		foreach ($array as $k=>$v):
			if($v=='style' && isset($_SESSION['use_stylesheet'])):
				echo '<link href="'.FRONT_WEBSITE_PATH.'/css/'.$_SESSION['use_stylesheet'].'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
			else:
				echo '<link href="'.FRONT_WEBSITE_PATH.'/css/'.$v.'.css" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";
			endif;
		endforeach;

}
  function js($array=array('jquery-1.2.6.min','search-reset')){
		foreach ($array as $k=>$v):
			echo '<script src="'.FRONT_WEBSITE_PATH.'/javascript/'.$v.'.js" type="text/javascript"></script> '."\n";
		endforeach;
}




function body()
{
	/*# include all the body related things like... tracking code here.*/

}

function footer()
{
	/*# if you need to add something to the website footer... please add here.*/
}

function array_map_recursive($callback, $array) {
  $b = Array();
  foreach ($array as $key => $value) {
    $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
  }
  return $b;
}

function if_set_in_post_then_display($var){
	if(isset($_POST[$var])):
		echo $_POST[$var];
	endif;
	echo '';
}

function validate_captcha(){
	global  $privatekey;

	if ($_POST["recaptcha_response_field"])
	{
        $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
        if ($resp->is_valid) {
               return  true;
        } else {
               /* # set the error code so that we can display it*/
               return false;
        }
	}
}

function is_set($array=array(), $item, $default=1)
{
	if(isset($array[$item]) && $array[$item]!=0){
		return $array[$item];
	}
	else{
		return $default;
	}
}

function limit_text($text, $limit=100)
{
	if(strlen($text)>$limit):
		return substr($text, 0, strpos($text, ' ', $limit));
	else:
		return $text;
	endif;
}

function get_object($tablename, $id, $type='object')
{
		$query= new query($tablename);
		$query->Where="where id='$id'";
		return $query->DisplayOne($type);
}

function get_object_by_col($tablename, $col, $col_value, $type='object')
{
		$query= new query($tablename);
		$query->Where="where $col='$id'";
		return $query->DisplayOne($type);
}

function get_object_by_col_urlrewrite($tablename, $col, $col_value, $type='object')
{
		$query= new query($tablename);
		$query->Where="where $col='$id' AND is_active='1' AND is_deleted='0'";
		return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var)
{
	$q= new query($tablename);
	$q->Field="$var";
	$q->Where="where id='$id'";
	if($obj=$q->DisplayOne()):
		return $obj->$var;
	else:
		return false;
	endif;
}


function echo_y_or_n($status)
{
	echo ($status)?'Yes':'No';
}

function target_dropdown($name, $selected='', $tabindex=1)
{
	$values=array('new window'=>'_blank', 'same window'=>'_parent');
	echo '<select name="'.$name.'" size="1" tabindex="'.$tabindex.'">';
	foreach ($values as $k=>$v):
		if($v==$selected):
			echo '<option value="'.$v.'" selected>'.ucfirst($k).'</option>';
		else:
			echo '<option value="'.$v.'">'.ucfirst($k).'</option>';
		endif;
	endforeach;
	echo '</select>';
}

function make_csv_from_array($array)
{
	$sr=1;
	$heading='';
	$file='';
	foreach ($array as $k=>$v):
		foreach ($v as $key=>$value):
			if($sr==1):$heading.=$key.', ';endif;
			$file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))).', ';
		endforeach;
		$file=substr($file, 0, strlen($file)-2);
		$file.="\n";
		$sr++;
	endforeach;
	return $file=$heading."\n".$file;
}


function get_y_n_drop_down($name, $selected)
{
	echo '<select class="span6 m-wrap" name="'.$name.'">';
	if($selected):
		echo '<option value="1" selected>Yes</option>';
		echo '<option value="0">No</option>';
	else:
		echo '<option value="0" selected>No</option>';
		echo '<option value="1">Yes</option>';
	endif;
	echo '</select>';
}

function get_setting_control($key, $type, $value)
{
	switch ($type)
	{
	case 'text':
			echo '<input class="span8 m-wrap" type="text" name="key['.$key.']" value="'.$value.'">';
			break;
        case 'textarea':
			echo '<textarea class="span8 m-wrap" name="key['.$key.']" rows="8" >'.$value.'</textarea>';
			break;
	case 'select':
			echo get_y_n_drop_down('key['.$key.']', $value);
			break;
	default: echo get_y_n_drop_down('key['.$key.']', $value);
	}
}

function css_active($page, $value, $class)
{
	if($page==$value)
		echo 'class='.$class;
}

function parse_into_array($string)
{
	return explode(',', $string);
}


function MakeDataArray($post, $not)
	{
		$data=array();
		foreach ($post as $key=>$value):
			if(!in_array($key, $not)):
				$data[$key]=$value;
			endif;
		endforeach;
		return $data;
	}

function is_var_set_in_post($var, $check_value=false)
{
	if(isset($_POST[$var])):
		if($check_value):
			if($_POST[$var]===$check_value):
				return true;
			else:
				return false;
			endif;
		endif;
		return true;
	else:
		return false;
	endif;
}

function display_form_error()
	{
		$login_session =new user_session();
		if($login_session->isset_pass_msg()):
			$array=$login_session->get_pass_msg();
			?>
                            <div class="errorMsg">
                                   <?php
                                       foreach ($array as $value):
                                            echo $value.'<br/>';
                                       endforeach;
                                   ?>
                            </div>
		<?php
		elseif(isset($_GET['msg']) && $_GET['msg']!=''):?>
			<div class="errorMsg">
				<?php echo urldecode($_GET['msg']).'<br/>';?>
                        </div>
		<?php endif;
		$login_session->isset_pass_msg()?$login_session->unset_pass_msg():'';
	}

	function Redirect($URL)
	{
		header("location:$URL");
		exit;
	}

	function Redirect1($filename)
	{
    	if (!headers_sent())
       		 header('Location: '.$filename);
   	else {
		        echo '<script type="text/javascript">';
		        echo 'window.location.href="'.$filename.'";';
		        echo '</script>';
		        echo '<noscript>';
		        echo '<meta http-equiv="refresh" content="0;url='.$filename.'" />';
		        echo '</noscript>';
    	}
	}


	function re_direct($URL)
	{
		header("location:$URL");
		exit;
	}
	
    function make_url($page, $query=null)
    {
     	return DIR_WS_SITE.'?page='.$page.'&'.$query;
    }

      /*
        function make_url_user($page, $query=null)
	{
		return DIR_WS_SITE_USER_URL.'?page='.$page.'&'.$query;
	}
         */       
        function display_url_user($title, $page, $query='', $class='')
	{
		return '<a href="'.make_url_user($page, $query).'" class="'.$class.'">'.$title.'</a>';
	}
        
	function display_url($title, $page, $query='', $class='')
	{
		return '<a href="'.make_url($page, $query).'" class="'.$class.'">'.$title.'</a>';
	}

	function make_admin_url($page, $action='list', $section='list', $query='')
	{
		return DIR_WS_SITE.'control.php?Page='.$page.'&action='.$action.'&section='.$section.'&'.$query;

	}
        function display_admin_url($title, $page,$action='list', $section='list', $query='', $class='')
	{
		return '<a href="'.make_admin_url($page,$action,$section, $query).'" class="'.$class.'">'.$title.'</a>';
	}
        function make_admin_url_window($page, $action='list', $section='list', $query='')
	{
		return DIR_WS_SITE.'control_window.php?Page='.$page.'&action='.$action.'&section='.$section.'&'.$query;

	}

        function admin_css_js($page, $query='')
	{

                return DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/control.php?Page='.$page.'&'.$query;

	}


	function make_admin_url2($page, $action='list', $section='list', $query='')
	{
		if($page=='home'):
			return DIR_WS_SITE.'index.php';
		else:
			return DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/control_window.php?Page='.$page.'&action2='.$action.'&section2='.$section.'&'.$query;
		endif;
	}


function make_admin_link($url, $text, $title='', $class='', $alt='')
{
	return  '<a href="'.$url.'" class="'.$class.'" title="'.$title.'" alt="'.$alt.'" >'.$text.'</a>';
}

function quit($message='processing stopped here'){
    echo $message;
    exit;
}

function download_orders($payment_status,$order_status)
{
	$orders= new query('orders');
	$orders->Field="id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
	if($order_status=='paid'):
		$orders->Where="where payment_status=".$payment_status." and order_status!='delivered'";
	elseif($order_status=='attempted'):
		$orders->Where="where payment_status=".$payment_status." and order_status='received'";
	else:
		$orders->Where="where payment_status=".$payment_status." and order_status='delivered'";
	endif;
	$orders->DisplayAll();

	$orders_arr= array();
	if($orders->GetNumRows()):
		while($order= $orders->GetArrayFromRecord()):
			$order['Username']=get_username_by_orders($order['user_id']);
			array_push($orders_arr, $order);
		endwhile;
	endif;
	$file=make_csv_from_array($orders_arr);
	$filename="orders".'.csv';
	$fh=@fopen('download/'.$filename,"w");
	fwrite($fh, $file);
	fclose($fh);
	download_file('download/'.$filename);
}


function download_orders_by_criteria($from_date, $to_date, $payment_status, $order_status)
{
	$orders= new query('orders');
	$orders->Field="id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
	$orders->Where="where order_status='$order_status' AND payment_status='$payment_status' AND (order_date BETWEEN CAST('$from_date' as DATETIME) AND CAST('$to_date' as DATETIME)) order by order_date";
	$orders->DisplayAll();

	$orders_arr= array();
	if($orders->GetNumRows()):
		while($order= $orders->GetArrayFromRecord()):
			$order['Username']=get_username_by_orders($order['user_id']);
			array_push($orders_arr, $order);
		endwhile;
	endif;
	$file=make_csv_from_array($orders_arr);
	$filename="orders".'.csv';
	$fh=@fopen('download/'.$filename,"w");
	fwrite($fh, $file);
	fclose($fh);
	download_file('download/'.$filename);
}


function get_username_by_orders($id)
{	if($id==0):
		return 'Guest';
	elseif($id):
		$q= new query('user');
		$q->Field="firstname,lastname";
		$q->Where="where id='".$id."'";
		$o=$q->DisplayOne();
		return $o->firstname;
	endif;
}

function get_zones_box($selected=0)
{
	$q= new query('zone');
	$q->DisplayAll();
	echo '<select name="zone" size="1">';
	while($obj=$q->GetObjectFromRecord()):
		if($selected=$obj->id):
			echo '<option value="'.$obj->id.'" selected>'.$obj->name.'</option>';
		else:
			echo '<option value="'.$obj->id.'">'.$obj->name.'</option>';
		endif;
	endwhile;
	echo '</select>';
}
function zone_drop_down($zone_id,$id,$s,$z)
{
	$query=new query('zone_country');
	$query->Where="where zone_id=$zone_id";
	$query->DisplayAll();
	$country_list=array();
	$country_name='';
	while($object=$query->GetObjectFromRecord()):
		$country_name=get_country_name_by_id($object->country_id);
		$idd=$object->country_id;
		/*$country_list('id'=>$object->id, 'name'=>$country_name));*/
		array_push($country_list, array('name'=>$country_name,'id'=>$object->id));
		/*$country_list[$object->id]=$country_name;*/
	endwhile;
	$total_list=array();
	foreach ($country_list as $k=>$v):
		$total_list[]=$v['name'];
    endforeach;
	array_multisort($total_list, SORT_ASC, $country_list);

	echo '<select name="'.$id.'" size="10" style="width:200px;" multiple>';
	foreach ($country_list as $k=>$v):
		if(($z == $zone_id) && $s):
			echo '<option value="'.$v['id'].'" selected="selected">'.ucfirst($v['name']).'</option>';
		else:
			echo '<option value="'.$v['id'].'">'.ucfirst($v['name']).'</option>';
		endif;
	endforeach;
	echo'</select>';
}



/*Function to add meta tags to content pages*/
 function add_metatags($title="", $keyword="", $description="")
{
/* description rule */
$description_length=150; /* characters */
if(strlen($description)>$description_length)
$description=substr($description, 0, $description_length);

/* title rule */
$title_length=100;
if(strlen($title)>$title_length)
$title= SITE_NAME.' | '.substr($title, 0, $title_length);
else
$title =SITE_NAME.' | '.$title;

$content = new stdClass();
$content->name=ucwords($title);
$content->meta_keyword=$keyword;
$content->meta_description=ucfirst($description);

return $content;
}

/* sanitize funtion*/
function sanitize($value){
	$prohibited_chars=array(" ", "/", "$", "&", "'", '%', '"', "@");
	foreach($prohibited_chars  as $k=>$v):
		$value=str_replace($v, '-', $value);
	endforeach;
	return strtolower($value);
}



function category_chain($id, $tablename='category')
{
	$chain=array();
	while($id!=0):
		$QueryObj =new query($tablename);
		$QueryObj->Where="where id='".$id."'";
		//$QueryObj->print=1;
		$cat=$QueryObj->DisplayOne();
		$chain[]=make_admin_link(make_admin_url($tablename, 'list', 'list', 'id='.$cat->parent_id), $cat->name, 'click here to reach this '.$tablename);
		$id=$cat->parent_id;
	endwhile;
	$cat_chain='';
	for($i=count($chain)-1; $i>=0;$i--):
		$cat_chain.=$chain[$i].' :: ';
	endfor;
	return $cat_chain.ucfirst(get_plural($tablename));
}

function get_plural($noun)
{
	switch ($noun)
	{
		case 'category': return 'Categories';
		case 'gallery': return 'Galleries';
		case 'product': return 'Products';
		default:'Categories';
	}
}

 function make_file_name($name, $id)
{
	$file_name_parts=explode('.', $name);
	$file_name_parts['0'].=$id;
	$file=$file_name_parts['0'].'.'.$file_name_parts['1'];
	return $file;
}


function get_all_modules()
{
    $query= new query('module');
    $query->DisplayAll();
    $modules=array();
    if($query->GetNumRows()):
    while($object = $query->GetObjectFromRecord()):
    $modules[$object->page_name]=$object->display_name;
    endwhile;
    endif;
    return($modules);

}

/*Get all Content Pages */
function get_all_content_pages()
{
    $query= new query('content');
    $query->DisplayAll();
    $pages=array();
    if($query->GetNumRows()):
    while($object = $query->GetObjectFromRecord()):
    $pages[$object->id]=$object->page_name;
    endwhile;
    endif;
    return($pages);

}
function get_blog_category($category_id=0){
    $query= new query('blog_category');
    $query->Where="where is_active='1' AND is_deleted='0'";
    $query->DisplayAll();
    $options='';
    while($faq = $query->GetObjectFromRecord()){
    if($category_id && $category_id==$faq->id)
    $options.='<option value="'.$faq->id.'" selected>'.ucwords($faq->name).'</option>';
    else

    $options.='<option value="'.$faq->id.'">'.ucwords($faq->name).'</option>';
    }
    return $options;
}


function pagingAdmin($page,$totalPages,$totalRecords,$url,$action='list',$section='list',$querystring='',$type=1,$Class='pad',$tdclass='',$Title='Records',$LClass='cat')
{
	
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
		?>
	
                <div class="row-fluid">
                      <div class="span12">
                            <div class="pagination pull-right">
                                    <ul>
                                            <li>  <a href="<?php echo $page==1?"":make_admin_url($url,$action,$section,'p='.$Pp.'&'.$querystring)?>" title="Previous Page"><?php echo "&larr; Prev"?></a></li>
                                            <?
                                            for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
                                                    if($i==$page):?>
                                                            <li> <?php echo  display_admin_url($i, $url,$action,$section,'p='.$i.'&'.$querystring,'blockselected'); ?></li>
                                                    <?php else: ?>
                                                            <li><?php echo display_admin_url($i, $url,$action,$section,'p='.$i.'&'.$querystring,$LClass);?></li>
                                                    <?php endif;					
                                            endfor;
                                            ?>
                                            <li><a href="<?php echo $totalPages==$page?"":make_admin_url($url,$action,$section,'p='.$Np.'&'.$querystring)?>"  title="Next Page"  >&nbsp;<?php echo "Next &rarr;"?></a></li>
                                    </ul>
                            </div>
                    </div>
                 </div>
                 
               
		<?
       
}	


/*make website url with urlname*/

function make_website_url_with_username($username=""){
      
        $url=DIR_WS_SITE;
        
        if($username!=""){
           /*define user path*/
            if( $_SERVER['HTTP_HOST'] == 'localhost'|| $_SERVER['HTTP_HOST'] == 'webgarh_website' ) {

                    $url= DIR_WS_SITE."user/".$username."/";

            }
            else{

                    $input = HTTP_SERVER;

                    // in case scheme relative URI is passed, e.g., //www.google.com/
                    $input = trim($input, '/');

                    // If scheme not included, prepend it
                    if (!preg_match('#^http(s)?://#', $input)) {
                            $input = 'http://' . $input;
                    }

                    $urlParts = parse_url($input);

                    // remove www
                    $domain = preg_replace('/^www\./', '', $urlParts['host']);

                    $url="http://".$username.".".$domain."/";

            }
            
        }
        
        return $url;
    
    
}


/*logout admin and account*/

function logout_admin_and_account(){
    global $admin_user;
    
    session_destroy();
    $admin_user->logout_user();
    setcookie('admin','', time()-3600000000);
    setcookie('user','', time()-3600000000);
}
function getOwnerDetails($id=""){
    $contacts = array();
    global $DBHostNameCMA, $DBUserNameCMA, $DBPasswordCMA, $DBDataBaseCMA;
    $conn = mysqli_connect($DBHostNameCMA,$DBUserNameCMA,$DBPasswordCMA) or die("Connection close");
    if(!mysqli_select_db($conn, $DBDataBaseCMA))
        {
            die("Could not select database");
        }
    if($id==""):
        $query = "select * from contacts where is_deleted='0' order by surname ASC";
        $arrayy = mysqli_query($conn, $query);
        if(mysqli_num_rows($arrayy)>0):
           while ($row = mysqli_fetch_assoc($arrayy)) {
               array_push($contacts, $row);
            }
        endif;
    else:
        $query = "select * from contacts where id='$id'";
        $arrayy = mysqli_query($conn, $query);
        $contacts = mysqli_fetch_object($arrayy);
    endif;    
    
    mysqli_close($conn);
    
    return $contacts;
}
    
/*get all infoaling mares*/
function allInFoalMares(){
        $q = new query('horse,mare_info');
        $q -> Field=" horse.id, horse.name,mare_info.due_date,mare_info.foaling_date";
        $q->Where=" where horse.is_deleted='0' AND horse.sex='mare' AND horse.id=mare_info.mare_id AND mare_info.mare_status='In Foal' GROUP BY mare_info.mare_id order by mare_info.due_date desc ";
        return $q->ListOfAllRecords();  
}
/* get selected details of horse*/
function getHorseSelectedDetails($id){
    $query = new query('horse');
    $query -> Field="name,sex,owner_name,arrived";
    $query -> Where=" where id='$id'";
    return $query ->DisplayOne();
}

function pr($e) {
    echo '<pre>';
    print_r($e);
    echo '</pre>';
}

use Mailgun\Mailgun;
// ['filePath'=>'/tmp/foo.jpg', 'filename'=>'test.jpg'];
function sendMailgunEmail($to = 'bharat@cwebconsultants.com',$text = 'Testing', $subject = 'Testing', $from = SITE_NAME.'<info@cwebconsultants.com>') {
	
	$key = 'dc84b5a831290993f580b00e9d851bc5-0e6e8cad-a7635910'; // Working
    require DIR_FS_SITE.'/include/harte-mailgun/vendor/autoload.php';
    $mg = new Mailgun($key);
    $domain = "mg.keithharte.com";
    
    $result = $mg->sendMessage($domain, array(
        'from'=>$from,
        'to'=> $to,
        'subject' => $subject,
		'html' => $text,
		// 'attachment' => [
		// 	$attachment
		//   ]
        )
    );
    return $result; 
}
?>