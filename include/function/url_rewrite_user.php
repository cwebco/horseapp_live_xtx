<?php
include_once(DIR_FS_SITE.'include/functionClass/serviceClass.php');

 function make_url_user($page, $query=null)
	{
		global $conf_rewrite_url;
		parse_str($query, $string);
		
		if (isset($conf_rewrite_url[strtolower($page)]))
			
			return _makeurluser($page, $string);

		else
			return DIR_WS_SITE_USER_URL.'?page='.$page.'&'.$query;
	}	
	
	



function verify_string($string) {



    if ($string != '')

        if (substr($string, -1) == '/')

            return substr($string, 0, strlen($string) - 1);



    return $string;

}



function load_url_user() {
 
    global $conf_rewrite_url;

    $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE_USER_URL);
 
    $URL = $_SERVER['REQUEST_URI'];

    if (strpos($URL, '?') === false):

        $string = substr($URL, -(strlen($URL) - strlen($prefix)));

        $string = verify_string($string);

        $string_parts = explode('/', $string);
       
        $url_array = array_flip($conf_rewrite_url);

        if (isset($url_array[$string_parts['0']])):

            _load($url_array[$string_parts['0']], $string_parts);

        endif;

    endif;

}



function _makeurluser($page, $string) {



    switch ($page) {

        case 'home':

            if (isset($string['msg'])):

                return DIR_WS_SITE_USER_URL.'home/'.$string['msg'];

            else:
                return DIR_WS_SITE_USER_URL;

            endif;

            break;
			
		case 'about_us':
			return DIR_WS_SITE_USER_URL.'about_us';
		break;
		
		case 'gallery':
			if (isset($string['p'])):
				return DIR_WS_SITE_USER_URL.'gallery/'.$string['p'];
			else:
				return DIR_WS_SITE_USER_URL.'gallery';
			endif;
		break;
		
		case 'contact_us':
			if (isset($string['msg'])):
				return DIR_WS_SITE_USER_URL.'contact_us/'.$string['msg'];
			else:
				return DIR_WS_SITE_USER_URL.'contact_us';
			endif;

			break;
			
		case 'service':
			if (isset($string['p'])):
				return DIR_WS_SITE_USER_URL.'service/'.$string['p'];
			else:
				return DIR_WS_SITE_USER_URL.'service';
			endif;
		break;
		
		case 'service_detail':
			if (isset($string['id'])):
				$serviceObj1=new service();
				$result1=$serviceObj1->getServiceByUserID($string['id'],LOGIN_USER_ID);
					if($result1):
						return DIR_WS_SITE_USER_URL.'service_detail/'.$result1->urlname;
					else:
						return DIR_WS_SITE_USER_URL.'service_detail';
					endif;
			else:
				return DIR_WS_SITE_USER_URL.'service';
			endif;
		break;

        default:break;

    }

}



function _load($page, $string_parts) {

    global $conf_rewrite_url;

    switch ($page) {

        case 'home':

            if (count($string_parts) == 2):

                $_REQUEST['page'] = 'home';

                $_GET['msg'] = $string_parts['1'];

            else:

                $_REQUEST['page'] = 'home';

            endif;

            break;
			
		case 'about_us':
			$_REQUEST['page'] = 'about_us';
		break;

		case 'gallery':
			if (count($string_parts) == 2):
				$_REQUEST['page'] = 'gallery';
				$_GET['p'] = $string_parts['1'];
			else:
				$_REQUEST['page'] = 'gallery';
			endif;
		break;	
		
		case 'service':
			if (count($string_parts) == 2):
				$_REQUEST['page'] = 'service';
				$_GET['p'] = $string_parts['1'];
			else:
				$_REQUEST['page'] = 'service';
			endif;
		break;
		
		case 'service_detail':
			if (count($string_parts) == 2):
                
				$result=get_object_by_col_urlrewrite('user_service', 'urlname', urldecode($string_parts['1']));
				if($result):
					$_GET['id'] =$result->id;					
				else:
					$_GET['id'] = 0;
				endif;
				$_REQUEST['page'] = 'service_detail';
            else:
                $_REQUEST['page'] = 'service_detail';
            endif;
		break;
		
		case 'contact_us':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'contact_us';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'contact_us';
            endif;

            break;

        default:break;

    }

}


?>