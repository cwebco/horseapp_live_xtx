<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require '../../include/config/config.php';
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/horseClass.php');

// $ToEmail = 'info@keithharte.com';
$ToEmail = FLU_NOTIFICATION_EMAIL;
$from = SITE_NAME.'<info@keithharte.com>';
$with_mail = true;
$test_milestone = false;

if(!$test_milestone) {
    $first_milestone_days = 21;
    $seconds_milestone_days = 150;
    $Boosters_milestone_days = 360;
    $ehv_any = 180;
}   else {
    $first_milestone_days = 1;
    $seconds_milestone_days = 1;
    $Boosters_milestone_days = 1;
    $ehv_any = 1; 
}


$first_count = 0;
$seconds_count = 0;
$Boosters_count = 0;
$ehv_any_count = 0;

$header = '<div style="color:rgba(0,0,0,0.54);padding-top:30px;margin-bottom:25px">
<img src="'.DIR_WS_SITE.'assets/img/logo.jpg" alt="Logo" width="300px">
    <div class="" style="height:2px;border-bottom:1px solid #e5e5e5;padding-top: 10px;"></div>
</div>';

require 'first.php';
require 'seconds.php';
require 'Boosters.php';
require 'ehv_any.php';

echo '1st - ' . $first_count . ' <br /><br />';
echo '2nd - ' . $seconds_count . ' <br /><br />';
echo 'Booster - ' . $Boosters_count . ' <br /><br />';
echo 'Ehv - ' . $ehv_any_count . ' <br /><br />';
?>