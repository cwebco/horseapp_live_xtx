<?php

$query = new horse_log;
$ehv_any = $query->email_milestone_ehv($ehv_any);

if ($ehv_any) {
    foreach ($ehv_any as $ehv_a) {
        if ($with_mail) {

            $subject = 'Flu Date( '.$ehv_a['name'].',  '.$ehv_a['type'].'  ) - '.SITE_NAME;

            $text = '<h3>State -  '.$ehv_a['value'];
            $text .= '<br /><br />';
            $text .= 'Horse Name -  '.$ehv_a['name'];
            $text .= '<br /><br />';
            $text .= ucfirst($ehv_a['type']).' -  '.$ehv_a['date'].'</h3>';
            $text = $header.$text;
            $is_mail_send = sendMailgunEmail($ToEmail, $text, $subject, $from);
        } else {
            $is_mail_send = true;
        }

        if ($is_mail_send) {
            $query = new horse_log;
            $query->save(array(
                'id' => $ehv_a['id'],
                'mail_sent' => 1
            ));
            $ehv_any_count++;
        }
    }
}
?>