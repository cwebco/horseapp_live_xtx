<?php
$query = new horse_log;
$seconds = $query->email_milestone($seconds_milestone_days, '2nd');

if ($seconds) {
    foreach ($seconds as $second) {
        if ($with_mail) {

            $subject = 'Flu Date( '.$second['name'].',  '.$second['type'].' ) - '.SITE_NAME;

            $text = '<h3>State -  '.$second['value'];
            $text .= '<br /><br />';
            $text .= 'Horse Name -  '.$second['name'];
            $text .= '<br /><br />';
            $text .= ucfirst($second['type']).' -  '.$second['date'].'</h3>';
            $text = $header.$text;
            $is_mail_send = sendMailgunEmail($ToEmail, $text, $subject, $from);
        } else {
            $is_mail_send = true;
        }

        if ($is_mail_send) {
            $query = new horse_log;
            $query->save(array(
                'id' => $second['id'],
                'mail_sent' => 1
            ));
            $seconds_count++;
        }
    }
}
?>