<?php
$query = new horse_log;
$firsts = $query->email_milestone($first_milestone_days, '1st');

if ($firsts) {
    foreach ($firsts as $first) {

        if ($with_mail) {

            $subject = 'Flu Date( '.$first['name'].',  '.$first['type'].' ) - '.SITE_NAME;

            $text = '<h3>State -  '.$first['value'];
            $text .= '<br /><br />';
            $text .= 'Horse Name -  '.$first['name'];
            $text .= '<br /><br />';
            $text .= ucfirst($first['type']).' -  '.$first['date'].'</h3>';
            $text = $header.$text;

            $is_mail_send = sendMailgunEmail($ToEmail, $text, $subject, $from);

        } else {
            $is_mail_send = true;
        }

        if ($is_mail_send) {
            $query = new horse_log;
            $query->save(array(
                'id' => $first['id'],
                'mail_sent' => 1
            ));
            $first_count++;
        }
    }
}
?>
