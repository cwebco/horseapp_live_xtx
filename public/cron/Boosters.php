<?php
$query = new horse_log;
$Boosters = $query->email_milestone($Boosters_milestone_days, 'Booster');

if ($Boosters) {
    foreach ($Boosters as $Booster) {

        if ($with_mail) {

            $subject = 'Flu Date( '.$Booster['name'].',  '.$Booster['type'].' ) - '.SITE_NAME;

            $text = '<h3>State -  '.$Booster['value'];
            $text .= '<br /><br />';
            $text .= 'Horse Name -  '.$Booster['name'];
            $text .= '<br /><br />';
            $text .= ucfirst($Booster['type']).' -  '.$Booster['date'].'</h3>';
            $text = $header.$text;
            $is_mail_send = sendMailgunEmail($ToEmail, $text, $subject, $from);
        } else {
            $is_mail_send = true;
        }

        if ($is_mail_send) {
            $query = new horse_log;
            $query->save(array(
                'id' => $Booster['id'],
                'mail_sent' => 1
            ));
            $Boosters_count++;
        }
    }
}
?>